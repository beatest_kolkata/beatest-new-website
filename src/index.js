import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import Routes from './routes';
import { browserHistory } from 'react-router';
import configureStore from './store/configure-store';
import './theme.css';
import './custom.css';

const store = configureStore();
const axios = 'Test';

ReactDOM.render(
    <Provider store={store}>
        <Routes history={browserHistory} />
    </Provider>,
    document.getElementById('root')
);
