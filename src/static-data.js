export const staticData = {
    features: [
        {
            key: 1,
            heading: "Track your progress in real time",
            icon_class: "icon-Bar-Chart3"
        },
        {
            key: 2,
            heading: "Prepare yourself along with your peer group",
            icon_class: "icon-Network"
        },
        {
            key: 3,
            heading: "Manage your time during the tests",
            icon_class: "icon-Clock-Forward"
        },
        {
            key: 4,
            heading: "Read the success stories of the achievers",
            icon_class: "icon-Speach-BubbleDialog"
        }
    ],
    courses: [
        {
            key: 1,
            icon_class:"icon-Student-Male",
            heading: "MBA",
            text: "Aiming for to the 99th percentile and more? Start your journey to your dream B-school with Beatest",
            exams: ["CAT","XAT"]
        },
        {
            key: 2,
            icon_class:"icon-Bank",
            heading: "Banking",
            text: "Gear up for your banking exams with carefully designed mock tests and topic tests in the latest pattern",
            exams: ["SBI PO","IBPS RRB","IBPS PO"]
        },
        {
            key: 3,
            icon_class:"icon-University",
            heading: "Campus Placements",
            text: "Prepare for your upcoming placement season with the aptitude exams and technical trainings in line with the various companies",
            exams: ["Aptitude Exam","Technical Training","Aptitude Training"]
        }
    ],
    campusPartners: ["iit_kanpur.png","iitbhu.png","iit_kgp.png","bits.png","srcc.png","ssc_delhi.png","mhud.png","iiest.png","techno.png"],
    testimonials: [
        {
            key: 1,
            profile_img:"tarun_saraswat.png",
            quote: "The content kept me thoroughly engaged as it was so well drafted. It was really beneficial to be a part of this. Questions are especially helpful and appropriate.",
            client_name: "Tarun Saraswat",
            designation: "IITBHU, Dept of Chemistry  - Placed at EXL Services"
        },
        {
            key: 2,
            profile_img:"arijit_bhatta.png",
            quote: "The quality of the content was perfect for placement preparation and it had undoubtedly been a great help to crack the exams.",
            client_name: "Arijit Bhattacharya",
            designation: "Jadavpur University, Dept of Chemical Engineering – Placed at Reliance Industries"
        },
        {
            key: 3,
            profile_img:"salil_abbas.png",
            quote: "Despite the Verbal questions being a tad bit difficult, I certainly feel that the content was really helpful and would definitely be rated as 8/10 ",
            client_name: "Salil Abbas",
            designation: "IIT Kanpur, Dept of Civil Engineering – Placed at American Express"
        },
        {
            key: 4,
            profile_img:"avatar_girl.png",
            quote: "Content was excellent and really helped me beyond placement exams. Although, would have loved to view a few more questions that were meant for mediocre students. Overall it was extremely useful.",
            client_name: "Piyali Biswas",
            designation: "Jadavpur University – Dept of Food technology"
        },
        {
            key: 5,
            profile_img:"anulekha_chatterjee.png",
            quote: "Beatest proved to be an useful tool for my campus placement preparation. Along with quality questions and solutions, the UI was also friendly.",
            client_name: "Anulekha Chatterjee",
            designation: "Meghnad Saha Institute of Technology, Dept of IT – Placed at Bentley Systems"
        },
        {
            key: 6,
            profile_img:"avatar_girl.png",
            quote: "The questions were above average in comparison to the actual company aptitude paper which helped me prepare better for not only the companies but also for other aptitude based exams.",
            client_name: "Swagata Banerjee",
            designation: "Meghnad Saha Institute of Technology"
        },
        {
            key: 7,
            profile_img:"shubham_yadav.png",
            quote: "The overall experience was quite good. The content was satisfactory and the UI was friendly.",
            client_name: "Shubham Yadav",
            designation: "IIT Kanpur , Dept of Electrical Engineering,  – Placed at Intel"
        },
        {
            key: 8,
            profile_img:"avatar_girl.png",
            quote: "The exams on TCS paper boosted my preparations for the company and helped me in getting placed. The overall experience was really good.",
            client_name: "Sharmistha Bakshi",
            designation: "Meghnad Saha Institute of Technology, Dept of CSE, - Placed in TCS"
        }
    ],
    exams: [
        {
            name: "CAT",
            starterPrice: 799,
            championPrice: 999
        },
        {
            name: "SBI PO - Mains",
            starterPrice: 149,
            championPrice: 249
        },
        {
            name: "IBPS PO - Mains",
            starterPrice: 149,
            championPrice: 249
        }
    ]
};