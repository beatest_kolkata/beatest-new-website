import axios from 'axios';
const api = axios.create({
    //baseURL: 'https://beatest.in',
    //baseURL: 'http://ec2-35-154-69-56.ap-south-1.compute.amazonaws.com',
    //baseURL: 'http://beta.beatest.in',
    //baseURL: 'http://localhost:3000',
    timeout: 10000,
    responseType: 'json',
    headers: {
        contentType: 'application/json'
    },
    withCredentials: true
});

  const settings = {
    //apiBaseUrl : "https://beatest.in"
    apiBaseUrl : "http://localhost:5000"
  };

  export {api,settings};