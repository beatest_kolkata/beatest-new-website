import $ from 'jquery';

const initialState = {
    currentView: 'TESTS_LIST',
    testsList: [],
    collegeList: [],
    questionsList: [],
    user: null,
    loginError: false,
    loginErrorMessage: '',
    profile: null,
    sectionIndex: 0,
    loginPanelVisibility: 'none',
    registrationPanelVisibility: 'none',
    userMenuVisibility: 'none',
    features: [],
    courses:[],
    campuspartners:[],
    testimonials:[],
    exams:[],
    toppers:[],
    mocktest:[],
    topictest:[],
    scoreData: null,
}

export default (state = initialState, action) => {
    switch (action.type) {
        case 'LOADING_TESTS_BEGIN':
            return Object.assign({}, state, {
                currentView: 'TESTS_LIST',
                isTestsLoading: true
            });
        case 'LOADING_TESTS_SUCCESS':
            return Object.assign({}, state, {
                isTestsLoading: false,
                testsList: action.tests
            });
        case 'CREATE_EXAM_BEGIN':
            return Object.assign({}, state, {
                isExamCreating: true
            });
        case 'CREATE_EXAM_SUCCESS':
            return Object.assign({}, state, {
                testAttempt: action.testAttempt,
                sectionAttempts: action.sectionAttempts,
                testName: action.testName,
                isExamCreating: false,
                timeleft: action.sectionAttempts[state.sectionIndex].timeleft
            });
        case 'ASSIGN_COLLEGES':
            return Object.assign({}, state, {
                collegeList: action.collegeList
            });
        case 'LOADING_QUESTIONS_BEGIN':
            return Object.assign({}, state, {
                isQuestionsLoading: true
            });
        case 'LOADING_QUESTIONS_SUCCESS':
            if(action.questionsList.length) {
                return Object.assign({}, state, {
                    currentView: 'TEST_AREA',
                    isQuestionsLoading: false,
                    currentQuestionIndex: 0,
                    questionsList: action.questionsList,
                    currentQuestionId: action.questionsList[0].id
                });
            } else window.location.pathname = '/userprofile';
        case 'SUBMIT_ANSWER':
            let question = state.questionsList[state.currentQuestionIndex];
            let newQuestion = Object.assign({}, question, {
                status: 'submitted'
            });
            let newQuestionsList = state.questionsList.map((question, index) => {
                if (index === state.currentQuestionIndex) {
                    return newQuestion;
                } else {
                    return question;
                }
            });
            return Object.assign({}, state, {
                questionsList: newQuestionsList
            });
        case 'SUBMIT_TITA_ANSWER':
            let titaQuestion = state.questionsList[state.currentQuestionIndex];
            let newTitaQuestion = Object.assign({}, titaQuestion, {
                status: 'submitted',
                titaChoice: action.titaChoice
            });
            let newTitaQuestionsList = state.questionsList.map((titaQuestion, index) => {
                if (index === state.currentQuestionIndex) {
                    return newTitaQuestion;
                } else {
                    return titaQuestion;
                }
            });
            return Object.assign({}, state, {
                questionsList: newTitaQuestionsList
            });
        case 'DELETE_ANSWER':
            let question2 = state.questionsList[state.currentQuestionIndex];
            let choices = question2.choices;
            $.each(choices, function (index, choice) {
                choice.attempted = false;
            });
            let newQuestion2 = Object.assign({}, question2, {
                status: 'unattempted',
                choices: choices
            });
            let newQuestionsList2 = state.questionsList.map((question2, index) => {
                if (index === state.currentQuestionIndex) {
                    return newQuestion2;
                } else {
                    return question2;
                }
            });
            return Object.assign({}, state, {
                questionsList: newQuestionsList2
            });
        case 'DELETE_TITA_ANSWER':
            let titaQuestion2 = state.questionsList[state.currentQuestionIndex];
            let newTitaQuestion2 = Object.assign({}, titaQuestion2, {
                status: 'unattempted',
                titaChoice: undefined
            });
            let newTitaQuestionsList2 = state.questionsList.map((titaQuestion2, index) => {
                if (index === state.currentQuestionIndex) {
                    return newTitaQuestion2;
                } else {
                    return titaQuestion2;
                }
            });
            return Object.assign({}, state, {
                questionsList: newTitaQuestionsList2
            });
        case 'MARK_ANSWER':
            let question3 = state.questionsList[state.currentQuestionIndex];
            let question3Choices = false;
            question3Choices = question3.choices.reduce((prev, choice) => {
                return prev || choice.attempted
            }, false);
            let newQuestion3;
            if (question3Choices) {
                newQuestion3 = Object.assign({}, question3, {
                    status: 'marked-answered'
                });
            } else {
                newQuestion3 = Object.assign({}, question3, {
                    status: 'marked-unanswered'
                });
            }
            let newQuestionsList3 = state.questionsList.map((question3, index) => {
                if (index === state.currentQuestionIndex) {
                    return newQuestion3;
                } else {
                    return question3;
                }
            });
            return Object.assign({}, state, {
                questionsList: newQuestionsList3
            });
        case 'MARK_TITA_ANSWER':
            let titaQuestion3 = state.questionsList[state.currentQuestionIndex];
            let newTitaQuestion3;
            if (action.tita_choice !== '' && action.tita_choice !== undefined) {
                newTitaQuestion3 = Object.assign({}, titaQuestion3, {
                    status: 'marked-answered',
                    titaChoice: action.tita_choice
                });
            } else {
                newTitaQuestion3 = Object.assign({}, titaQuestion3, {
                    status: 'marked-unanswered'
                });
            }
            let newTitaQuestionsList3 = state.questionsList.map((titaQuestion3, index) => {
                if (index === state.currentQuestionIndex) {
                    return newTitaQuestion3;
                } else {
                    return titaQuestion3;
                }
            });
            return Object.assign({}, state, {
                questionsList: newTitaQuestionsList3
            });
        case 'CHOICE_SELECTED':
            if (action.bool) {
                let checkAndChangeStatus = () => {
                    let question = state.questionsList[state.currentQuestionIndex];
                    return (question.status !== 'submitted');
                }
                if (checkAndChangeStatus()) {
                    let question = state.questionsList[state.currentQuestionIndex];
                    let newChoices = question.choices.map((choice) => {
                        return Object.assign({}, choice, {
                            attempted: choice.id === action.choiceId
                        });
                    });
                    let newQuestion = Object.assign({}, question, {
                        status: 'attempted',
                        choices: newChoices
                    });
                    let newQuestionsList = state.questionsList.map((question, index) => {
                        if (index === state.currentQuestionIndex) {
                            return newQuestion;
                        } else {
                            return question;
                        }
                    });
                    return Object.assign({}, state, {
                        questionsList: newQuestionsList
                    });
                }
            }
        case 'SHOW_NEXT_QUESTION':
            let nextIndex = state.currentQuestionIndex + 1;
            let nextId = state.questionsList[nextIndex].id;
            return Object.assign({}, state, {
                currentQuestionIndex: nextIndex,
                currentQuestionId: nextId
            });
        case 'SHOW_PREV_QUESTION':
            let prevIndex = state.currentQuestionIndex - 1;
            let prevId = state.questionsList[prevIndex].id;
            return Object.assign({}, state, {
                currentQuestionIndex: prevIndex,
                currentQuestionId: prevId
            });
        case 'SHOW_SELECTED_QUESTION':
            return Object.assign({}, state, {
                currentQuestionIndex: action.selectedQuestionIndex - 1,
                currentQuestionId: action.selectedQuestionId
            });
        case 'GOTO_NEXT_SECTION':
            let nextSectionIndex = state.sectionIndex + 1;
            return Object.assign({}, state, {
                sectionIndex: nextSectionIndex,
                currentQuestionIndex: 0
            });
        case 'GOTO_SELECTED_SECTION':
            return Object.assign({}, state, {
                sectionIndex: action.selectedSectionIndex
            });
        case 'TOGGLE_LOGIN_PANEL_VISIBILITY':
            let loginPanelVisibility = state.loginPanelVisibility;
            if (loginPanelVisibility === 'none') {
                return Object.assign({}, state, {
                    loginPanelVisibility: 'block'
                });
            } else {
                return Object.assign({}, state, {
                    loginPanelVisibility: 'none'
                });
            }
        case 'TOGGLE_REGISTRATION_PANEL_VISIBILITY':
            let registrationPanelVisibility = state.registrationPanelVisibility;
            if (registrationPanelVisibility === 'none') {
                return Object.assign({}, state, {
                    registrationPanelVisibility: 'block'
                });
            } else {
                return Object.assign({}, state, {
                    registrationPanelVisibility: 'none'
                });
            }
        case 'TOGGLE_USER_MENU_VISIBILITY':
            let userMenuVisibility = state.userMenuVisibility;
            if (userMenuVisibility === 'none') {
                return Object.assign({}, state, {
                    userMenuVisibility: 'block'
                });
            } else {
                return Object.assign({}, state, {
                    userMenuVisibility: 'none'
                });
            }
        case 'LOGIN':
            return Object.assign({}, state, {
                user: action.user,
                loginError: false,
                loginErrorMessage: ''
            });
        case 'LOGIN_ERROR':
            return Object.assign({}, state, {
                loginError: true,
                loginErrorMessage: action.data
            });
        case 'SET_USER_DATA':
            return Object.assign({}, state, {
                user: action.data,
                loginError: false,
                loginErrorMessage: ''
            });
        case 'SET_PROFILE_DATA':
            return Object.assign({}, state, {
                profile: action.data
            });
        case 'LOGOUT':
            return Object.assign({}, state, {
                user: null,
                loginError: false,
                loginErrorMessage: ''
            });
        case 'CHANGE_TIME_LEFT':
            let sectionAttempt = state.sectionAttempts[state.sectionIndex];
            let newSectionAttempt = Object.assign({}, sectionAttempt, {
                timeleft: action.newTimeleft
            });
            let newSectionAttempts = state.sectionAttempts.map((sectionAttempt, index) => {
                if (index === state.sectionIndex) {
                    return newSectionAttempt;
                } else {
                    return sectionAttempt;
                }
            });
            return Object.assign({}, state, {
                timeleft: action.newTimeleft,
                sectionAttempts: newSectionAttempts
            });
        case 'SHOW_CAT_TESTS':
            return Object.assign({}, state, {
                selectedTestType: 'CAT'
            });
        case 'SHOW_IBPS_TESTS':
            return Object.assign({}, state, {
                selectedTestType: 'IBPS'
            });
        case 'SHOW_SBI_TESTS':
            return Object.assign({}, state, {
                selectedTestType: 'SBI'
            });
        case 'CHANGE_COMPLETION_STATUS':
            return Object.assign({}, state, {
                completionStatus: action.status
            });
        case 'SET_TEST_DETAILS':
            return Object.assign({}, state, {
                testId: action.test.id,
                testName: action.test.name,
                testType: action.test.type,
                testInstructions: action.test.instruction_html
            });
        case 'SET_SOLUTION_DATA':
            return Object.assign({}, state, {
                sectionAttempts: action.data.section_attempts,
                sectionIndex: 0,
                currentQuestionIndex: 0
            });
        case 'GOTO_NEXT_QUESTION':
            let newQuestionIndex = state.currentQuestionIndex + 1;
            return Object.assign({}, state, {
                currentQuestionIndex: newQuestionIndex
            });
        case 'GOTO_PREV_QUESTION':
            return Object.assign({}, state, {
                currentQuestionIndex: state.currentQuestionIndex - 1
            });
        case 'GOTO_PREV_SECTION':
            let prevSectionIndex = state.sectionIndex - 1;
            return Object.assign({}, state, {
                sectionIndex: prevSectionIndex,
                currentQuestionIndex: 0
            });
        case 'LOADING_USER_SUCCESS':
            return Object.assign({}, state, {
                isUserLoading: false
            });
        case 'SET_TEST_SCORE':
            return Object.assign({}, state, {
                scoreData: action.scoreData
            });
        case 'TOGGLE_TEST_TYPE':
            return Object.assign({}, state, {
                currentTestType: action.testType
            });
        case 'HOME_LOAD_FEATURES':
            return Object.assign({}, state, {
                features: [{
                    key: 1,
                    heading: "Track your progress in real time",
                    icon_class: "icon-Bar-Chart3"
                },
                {
                    key: 2,
                    heading: "Prepare yourself along with your peer group",
                    icon_class: "icon-Network"
                },
                {
                    key: 3,
                    heading: "Manage your time during the tests",
                    icon_class: "icon-Clock-Forward"
                },
                {
                    key: 4,
                    heading: "Read the success stories of the achievers",
                    icon_class: "icon-Speach-BubbleDialog"
                }]
            });
        case 'HOME_LOAD_COURSES':
            return Object.assign({}, state, {
                courses: [{
                    key: 1,
                    icon_class:"icon-Student-Male",
                    heading: "MBA",
                    text: "Aiming for to the 99th percentile and more? Start your journey to your dream B-school with Beatest",
                    exams: ["CAT","XAT"]
                },
                {
                    key: 2,
                    icon_class:"icon-Bank",
                    heading: "Banking",
                    text: "Gear up for your banking exams with carefully designed mock tests and topic tests in the latest pattern",
                    exams: ["SBI PO","IBPS RRB","IBPS PO"]
                },
                {
                    key: 3,
                    icon_class:"icon-University",
                    heading: "Campus Placements",
                    text: "Prepare for your upcoming placement season with the aptitude exams and technical trainings in line with the various companies",
                    exams: ["Aptitude Exam","Technical Training","Aptitude Training"]
                }]
            });

/*
            IIT Kanpur
            IEM/UEM
            Jadavpur University
            IIT-BHU
            BITS Goa
            CMRIT
            Christ University
            IIEST, Shibpur
*/

        case 'HOME_LOAD_CAMPUS_PARTNERS':
            return Object.assign({}, state,{
                campuspartners: ["iit_kanpur.png","iitbhu.png","iit_kgp.png","bits.png","srcc.png","ssc_delhi.png","mhud.png","iiest.png","techno.png"]

            });
        case 'HOME_LOAD_TESTIMONIALS':
            return Object.assign({}, state, {
                testimonials: [{
                    key: 1,
                    profile_img:"tarun_saraswat.png",
                    quote: "The content kept me thoroughly engaged as it was so well drafted. It was really beneficial to be a part of this. Questions are especially helpful and appropriate.",
                    client_name: "Tarun Saraswat",
                    designation: "IITBHU, Dept of Chemistry  - Placed at EXL Services"
                },
                {
                    key: 2,
                    profile_img:"arijit_bhatta.png",
                    quote: "The quality of the content was perfect for placement preparation and it had undoubtedly been a great help to crack the exams.",
                    client_name: "Arijit Bhattacharya",
                    designation: "Jadavpur University, Dept of Chemical Engineering – Placed at Reliance Industries"
                },
                {
                    key: 3,
                    profile_img:"salil_abbas.png",
                    quote: "Despite the Verbal questions being a tad bit difficult, I certainly feel that the content was really helpful and would definitely be rated as 8/10 ",
                    client_name: "Salil Abbas",
                    designation: "IIT Kanpur, Dept of Civil Engineering – Placed at American Express"
                },
                {
                    key: 4,
                    profile_img:"avatar_girl.png",
                    quote: "Content was excellent and really helped me beyond placement exams. Although, would have loved to view a few more questions that were meant for mediocre students. Overall it was extremely useful.",
                    client_name: "Piyali Biswas",
                    designation: "Jadavpur University – Dept of Food technology"
                },
                {
                    key: 5,
                    profile_img:"anulekha_chatterjee.png",
                    quote: "Beatest proved to be an useful tool for my campus placement preparation. Along with quality questions and solutions, the UI was also friendly.",
                    client_name: "Anulekha Chatterjee",
                    designation: "Meghnad Saha Institute of Technology, Dept of IT – Placed at Bentley Systems"
                },
                {
                    key: 6,
                    profile_img:"avatar_girl.png",
                    quote: "The questions were above average in comparison to the actual company aptitude paper which helped me prepare better for not only the companies but also for other aptitude based exams.",
                    client_name: "Swagata Banerjee",
                    designation: "Meghnad Saha Institute of Technology"
                },
                {
                    key: 7,
                    profile_img:"shubham_yadav.png",
                    quote: "The overall experience was quite good. The content was satisfactory and the UI was friendly.",
                    client_name: "Shubham Yadav",
                    designation: "IIT Kanpur , Dept of Electrical Engineering,  – Placed at Intel"
                },
                {
                    key: 8,
                    profile_img:"avatar_girl.png",
                    quote: "The exams on TCS paper boosted my preparations for the company and helped me in getting placed. The overall experience was really good.",
                    client_name: "Sharmistha Bakshi",
                    designation: "Meghnad Saha Institute of Technology, Dept of CSE, - Placed in TCS"
                }
            ]
            });
        case 'PRICING_LOAD_EXAMS':
            return Object.assign({}, state,{
                exams: [
                    {
                        name: "CAT",
                        starterPrice: 799,
                        championPrice: 999
                    },
                    {
                        name: "SBI PO - Mains",
                        starterPrice: 149,
                        championPrice: 249
                    },
                    {
                        name: "IBPS PO - Mains",
                        starterPrice: 149,
                        championPrice: 249
                    }
                ]
            });
        case 'PRICING_OUR_TOPPERS':
        return Object.assign({}, state,{
            toppers: [{
                key: 1,
                text : "Stack was easy to set-up and more importantly, was dead simple to customize. Buy this on sight",
                profile_img : "/img/avatar-4.png",
                student_name: "Robert S.",
                percentage : "99.89 - CAT 2016"
            },
            {
                key: 2,
                text : "Stack was easy to set-up and more importantly, was dead simple to customize. Buy this on sight",
                profile_img : "/img/avatar-4.png",
                student_name: "Robert S.",
                percentage : "99.89 - CAT 2016"
            },
            {
                key: 3,
                text : "Stack was easy to set-up and more importantly, was dead simple to customize. Buy this on sight",
                profile_img : "/img/avatar-4.png",
                student_name: "Robert S.",
                percentage : "99.89 - CAT 2016"
            },
            {
                key: 4,
                text : "Stack was easy to set-up and more importantly, was dead simple to customize. Buy this on sight",
                profile_img : "/img/avatar-4.png",
                student_name: "Robert S.",
                percentage : "99.89 - CAT 2016"
        }]
        });
        case 'TESTS_MOCK_TEST':
        return Object.assign({}, state,{
            mocktest: [{
                key: 1,
                icon : "<i class='icon icon--lg icon-Notepad'></i>",
                tittle : "BEAT CAT 01",
                number_of_ques: "100",
                time : "60",
                test_btn : "<a class='btn btn--sm type--uppercase flip-bg-white' href='/testslist'><span class='btn__text'>Start test</span></a>"
            },
            {
                key: 2,
                icon : "<i class='icon icon--lg icon-Notepad'></i>",
                tittle : "BEAT CAT 02",
                number_of_ques: "100",
                time : "60",
                test_btn : "<a class='btn btn--sm type--uppercase flip-bg-white' href='/testslist'><span class='btn__text'>Start test</span></a>"
            },
            {
                key: 3,
                icon : "<i class='icon icon--lg icon-Notepad'></i>",
                tittle : "BEAT CAT 03",
                number_of_ques: "100",
                time : "60",
                test_btn : "<a class='btn btn--sm type--uppercase flip-bg-white' href='/testslist'><span class='btn__text'>Start test</span></a>"
            }]           

        });
        case 'TESTS_TOPIC_TEST':
        return Object.assign({}, state,{
            topictest: [{
                key: 4,
                icon : "<i class='icon icon--lg icon-Notepad'></i>",
                tittle : "Number System1",
                number_of_ques: "100",
                time : "60",
                test_btn : "<a class='btn btn--sm type--uppercase flip-bg-white' href='/testslist'><span class='btn__text'>Start test</span></a>"
            },
            {
                key: 5,
                icon : "<i class='icon icon--lg icon-Notepad'></i>",
                tittle : "Number System2",
                number_of_ques: "100",
                time : "60",
                test_btn : "<a class='btn btn--sm type--uppercase flip-bg-white' href='/testslist'><span class='btn__text'>Start test</span></a>"
            }]
            

        });
            break;
        default:
            return state;
    }
};
