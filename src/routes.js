import React from 'react';
import { Router, Route } from 'react-router';
import NotFound from './components/NotFound';
import Pricing from './components/Pricing/index';
import Terms from './components/Terms';
import OurTeam from './components/OurTeam';
import ProfileContainer from './components/ProfileContainer';
import Login from './components/Login'
import CreateAccount from './components/Accounts/CreateAccount'
import Home from "./components/Home/index";
import TrainingCourses from "./components/TrainingCourses";
import FaqContainer from './components/FaqContainer';

const Routes = (props) => (
    <Router {...props}>
        <Route path="/" component={Home} />
        <Route path="/index" component={Home} />
        <Route path="/trainings" component={Pricing} />
        <Route path="/faq" component={FaqContainer} />
        <Route path="/terms" component={Terms} />
        <Route path="/aboutus" component={OurTeam} />
        <Route path="/profile" component={ProfileContainer} />
        <Route path="/login" component={Login} />
        <Route path="/signup" component={CreateAccount} />
        {/*<Route path="/trainings" component={TrainingCourses} />*/}
        <Route path="*" component={NotFound} />
    </Router>
);

export default Routes;