import React, { Component } from 'react';
import classnames from 'classnames';
import NavBarTransparent from '../NavBarLogin'
import LoginContainer from '../LoginContainer'

class Login extends Component {
    render() {
        const { className, ...props } = this.props;
        return (
            <div>
                <NavBarTransparent/>
                <LoginContainer/>
            </div>
        );
    }
}

export default Login;
