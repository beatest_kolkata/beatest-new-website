import React, { Component } from 'react';
import classnames from 'classnames';
import ScoresContainer from '../ScoresContainer';
/*import './style.css';*/

class Scores extends Component {
    render() {
        const { className, ...props } = this.props;
        return (
            <ScoresContainer className={classnames('Scores', className)} {...props} />
        );
    }
}

export default Scores;
