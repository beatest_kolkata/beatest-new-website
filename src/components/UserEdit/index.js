import React, { Component } from 'react';
import classnames from 'classnames';
/*import './style.css';*/
import UserEditContainer from '../UserEditContainer';

class UserEdit extends Component {
    render() {
        const { className, ...props } = this.props;
        return (
            <UserEditContainer className={classnames('User', className)} {...props}/>
        );
    }
}

export default UserEdit;
