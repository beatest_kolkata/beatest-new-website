import React, {PropTypes, Component} from 'react';

class QuestionButton extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        var startIndex = 0;
        if(this.props.testType === 'CAT') {
            if(this.props.sectionAttempts[this.props.sectionIndex].start) {
                startIndex = this.props.sectionAttempts[this.props.sectionIndex].start;
            }
        }
        this.props.showSelectedQuestion(this.props.id, this.props.questionIndex - startIndex);
    }

    render() {
        var className = 'question-button ' + this.props.status;
        return (
            <div className={className} onClick={this.handleClick}>
                {this.props.questionIndex}
                {(() => {
                    if(this.props.status === 'marked-answered') {
                        return <div className="question-button-tick">
                            <i className="fa fa-check "/>
                        </div>
                    }
                })()}
            </div>
        );
    }
}

QuestionButton.propTypes = {
    id: PropTypes.number,
    currentQuestionIndex: PropTypes.number,
    status: PropTypes.string
};

export default QuestionButton;
