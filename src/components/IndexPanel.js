/* eslint-disable */
import React, {PropTypes, Component} from 'react';
import QuestionButton from './QuestionButton';
import SubmitExam from './SubmitExam';
import CountDownTimer from './CountDownTimer';
import $ from 'jquery';

let startIndex = 0;
class IndexPanel extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.gotoNextSection = this.gotoNextSection.bind(this);
        this.openQuestionPaper = this.openQuestionPaper.bind(this);
    }

    gotoNextSection() {
        var sectionIndex = this.props.sectionIndex;
        $.ajax({
            url: '/api/section_attempt/' + this.props.sectionAttempts[sectionIndex].id,
            dataType: 'json',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({})
        });
        if(sectionIndex < this.props.sectionAttempts.length - 1) {
            sectionIndex += 1;
            this.props.gotoNextSection();
            this.props.loadingQuestionsBegin();
            $.get('/api/section_attempt/' + this.props.sectionAttempts[sectionIndex].id + '/questions', (questionsList) => {
                this.props.loadingQuestionsSuccess(questionsList);
            }).then(() => {
                $.get('/api/section_attempt/' + this.props.sectionAttempts[sectionIndex].id + '/timeleft', (data) => {
                    this.props.changeTimeleft(data.timeleft);
                }).fail(() => {
                    this.props.changeTimeleft(this.props.sectionAttempts[sectionIndex].totalTime);
                });
                location.reload();
            });
        } else {
            // $.get('/api/test_attempt/' + this.props.testAttempt.id + '/get_score', (score) => {
            //     //TODO: Revert if there is no UI to display Test score for User
            //     //alert('Your score : '+ score.score);
            //     alert('Visit Test section to view your Scores!');
            //     window.close();
            // });
            $.ajax({
                url: '/api/test_attempt/' + this.props.testAttempt.id + '/submit_test',
                dataType: 'json',
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify({})
            }).then(function (data) {
                alert('Visit Tests to view your Scores!');
                window.close();
            }.bind(this));
        }
    }

    openQuestionPaper() {
        var w = window.open("/index", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=800,height=800");
        var j = 1;
        this.props.questionsList.map((question) => {
            w.document.write(j + '.  ');
            j += 1;
            w.document.write(question.html);
            w.document.write('</br>');
            var i = 1;
            if (question.type !== 'TITA') {
                question.choices.map((choice) => {
                    if (i === 1)
                        w.document.write('A.  ');
                    else if (i === 2)
                        w.document.write('B.  ');
                    else if (i === 3)
                        w.document.write('C.  ');
                    else if (i === 4)
                        w.document.write('D.  ');
                    i += 1;
                    w.document.write(choice.html);
                    w.document.write('</br>');
                });
            }
            else {
                w.document.write('(TITA) </br>');
            }
            w.document.write('</br></br>');
        });
    }

    openInstructions() {
        console.log(this.props);
        var url = '/test_instructionsonly/';
        var height = screen.height - 150;
        var width = screen.width - 50;
        window.open(url,"_blank", "channelmode=1,height=" + height + ",width=" + width + ",fullscreen=1,resizable=0,location=0,menubar=0,toolbar=0,status=0,top=0,left=0");
    }

    render() {
        var index = 0;
        return (
            <div className="index-panel">
                <div className="user-index-panel">
                    <span>{this.props.userName}</span>
                    <div>Time Remaining - <CountDownTimer timeleft={this.props.timeleft}
                                        changeTimeleft={this.props.changeTimeleft}
                                        sectionIndex={this.props.sectionIndex}
                                        loadingQuestionsBegin={this.props.loadingQuestionsBegin}
                                        loadingQuestionsSuccess={this.props.loadingQuestionsSuccess}
                                        gotoNextSection={this.gotoNextSection}
                                        sectionAttempts={this.props.sectionAttempts}
                                        testAttempt={this.props.testAttempt} />
                    </div>
                </div>
                <div className="index-panel-buttons">
                    {this.props.questionsList.map((question) => {
			            if(this.props.testType === 'CAT') {
				            if(this.props.sectionAttempts[this.props.sectionIndex].start) {
		   		               	startIndex = this.props.sectionAttempts[this.props.sectionIndex].start;
				            }
		            	}
                        index = index + 1;
                        return <QuestionButton key={question.id} id={question.id}
                                questionIndex={startIndex + index} status={question.status}
                                showSelectedQuestion={this.props.showSelectedQuestion}
                                currentQuestionIndex={this.props.currentQuestionIndex}
                                questionsList={this.props.questionsList}
				                sectionIndex={this.props.sectionIndex}
				                sectionAttempts={this.props.sectionAttempts} 
				                testType={this.props.testType} />
                    })}
                </div>
                <div>
                {(() => {
                    if(this.props.testType === 'CAT') {
                        return <div>
                            <button className="next-prev-button" onClick={this.gotoNextSection}>Submit Test</button>
                            <button className="next-prev-button" onClick={this.openQuestionPaper}>Question Paper</button>
                            <button className="next-prev-button" onClick={this.openInstructions.bind(this)}>Instructions</button>
                        </div>
                    } else {
                        return <SubmitExam testAttempt={this.props.testAttempt}/>
                    }
                })()}
                </div>
            </div>
        );
    }
}

IndexPanel.propTypes = {
    questionsList: PropTypes.array,
    showSelectedQuestion: PropTypes.func
};

export default IndexPanel;
