import React, { PropTypes, Component } from 'react';
import $ from 'jquery';
import Header from './Common/InternalHeader';
import Footer from './Common/InternalFooter';
import {Plans} from './Pricing/Plans';
import Faq from './Common/Faq';
import {staticData} from '../static-data';

const initialState = {
    user: null,
    isUserLogged: false,
    exams: []
};

class PricingContainer extends Component {
    constructor(props) {
        super(props);
        this.state = initialState;
    }

    getUser() {
        $.get('/api/user', (data) => {
            if (typeof data === 'object') {
                this.setState({user: data.user});
                this.setState({isUserLogged: true});
            }
        });
    }

    componentDidMount(){
        this.getUser();

        this.setState({
            exams: staticData.exams
        })
    }

    render() {
        return (
            <div>
                <a id="start"></a>
                <Header transparent={true} user={this.state.user} />
                <div className="main-container">
                    <section className="text-center imagebg" data-overlay="4">
                        <div className="background-image-holder background--bottom">
                            <img alt="background" src="/img/landing-3.jpg" />
                        </div>
                        <div className="container">
                            <div className="row">
                                <div className="col-sm-12">
                                    <h1><strong>Pricing</strong></h1>
                                </div>
                            </div>
                        </div>
                    </section>
                    <Plans data = {this.state.exams} user={this.state.user} loggedIn={this.state.isUserLogged} />
                    {/* <Toppers data = {this.props.toppers} /> */}
                    <Faq />
                </div>
                <Footer />
                <a className="back-to-top inner-link" href="#start" data-scroll-className="100vh:active">
                    <i className="stack-interface stack-up-open-big"></i>
                </a>
            </div>
        );
    }
}

export default PricingContainer;