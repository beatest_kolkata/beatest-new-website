/* eslint-disable */
import React, {PropTypes, Component} from 'react';
import $ from 'jquery';
import Choice from './Choice';

let startIndex = 0;
class Question extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.clearValue();
        this.getSelectedChoiceId = this.getSelectedChoiceId.bind(this);
        this.submitAnswer = this.submitAnswer.bind(this);
        this.getNextQuestion = this.getNextQuestion.bind(this);
        this.getPrevQuestion = this.getPrevQuestion.bind(this);
        this.deleteAnswer = this.deleteAnswer.bind(this);
        this.submitTest = this.submitTest.bind(this);
        // calling to initialize time spent
        this.timeSpent(this.props.currentQuestionId);
        // calling to update time spent
        window.setInterval(function() {
            this.timeSpent(this.props.currentQuestionId);
        }.bind(this), 5000);
    }

    timeSpent(questionId) {
        $.ajax({
            url: '/api/time/question/track',
            dataType: 'json',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                "question_id": questionId
            }),
            success: (() => {
            }),
            error: (() => {
                //alert('Lost network connection. Your test will begin from here next time.');
                // window.close();
            })
        });
    }

    getSelectedChoiceId() {
        let choiceId = this.props.question.choices.reduce((prevId, choice) => {
            if (prevId) {
                return prevId;
            }
            if (choice.attempted === true) {
                return choice.id;
            }
        }, null);
        return choiceId;
    }

    submitAnswer() {
        var tita_choice = $('#tita-input').val();
        if(this.props.question.type === 'TITA' && tita_choice !== '') {
            $.ajax({
                url: '/api/section_attempt/' + this.props.sectionAttempts[this.props.sectionIndex].id + '/questions/' + this.props.currentQuestionId,
                dataType: 'json',
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify({
                    tita_choice: tita_choice
                })
            });
            this.props.submitTitaAnswer(tita_choice);
        } else if (this.props.question.type !== 'TITA') {

            if (this.getSelectedChoiceId()) {
                $.ajax({
                    url: '/api/section_attempt/' + this.props.sectionAttempts[this.props.sectionIndex].id + '/questions/' + this.props.currentQuestionId,
                    dataType: 'json',
                    type: 'POST',
                    contentType: 'application/json',
                    data: JSON.stringify({
                        choice_id: this.getSelectedChoiceId()
                    })
                });
            }
            if(!this.props.hasNext) {
                this.props.submitAnswer();
            }
        }
    }

    submitTest() {
        var sectionIndex = this.props.sectionIndex;
        $.ajax({
            url: '/api/section_attempt/' + this.props.sectionAttempts[sectionIndex].id,
            dataType: 'json',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({})
        });
        if(sectionIndex < this.props.sectionAttempts.length - 1) {
            sectionIndex += 1;
            this.props.gotoNextSection();
            this.props.loadingQuestionsBegin();
            $.get('/api/section_attempt/' + this.props.sectionAttempts[sectionIndex].id + '/questions', (questionsList) => {
                this.props.loadingQuestionsSuccess(questionsList);
            }).then(() => {
                $.get('/api/section_attempt/' + this.props.sectionAttempts[sectionIndex].id + '/timeleft', (data) => {
                    this.props.changeTimeleft(data.timeleft);
                }).fail(() => {
                    this.props.changeTimeleft(this.props.sectionAttempts[sectionIndex].totalTime);
                });
                location.reload();
            });
        } else {
            $.ajax({
                url: '/api/test_attempt/' + this.props.testAttempt.id + '/submit_test',
                dataType: 'json',
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify({})
            }).then(function (data) {
                alert('Visit Tests to view your Scores!');
                window.close();
            }.bind(this));
        }
    }

    getNextQuestion() {
        if(this.props.question.status !== 'unattempted') {
            this.submitAnswer();
            this.props.submitAnswer();
        }
        if(this.props.question.type === 'TITA') {
            this.submitAnswer();
        }
        this.props.showNextQuestion();
        if(this.props.question.type === 'TITA') {
            this.clearValue();
        }
    }

    getPrevQuestion() {
        if(this.props.question.status !== 'unattempted') {
            this.submitAnswer();
            this.props.submitAnswer();
        }
        this.props.showPrevQuestion();
        if(this.props.question.type === 'TITA') {
            this.clearValue();
        }
    }

    markForReviewAndNext() {
        if(this.props.question.type !== 'TITA') {
            this.props.markAnswer();
        } else {
            var tita_choice = $('#tita-input').val();
            this.props.markTitaAnswer(tita_choice);
        }
        if(this.props.hasNext) {
            this.props.showNextQuestion();
            if(this.props.question.type === 'TITA') {
                this.clearValue();
            }
        }
    }

    deleteAnswer() {
        $.ajax({
            url: '/api/section_attempt/' + this.props.sectionAttempts[this.props.sectionIndex].id + '/questions/' + this.props.currentQuestionId,
            dataType: 'json',
            type: 'DELETE',
            contentType: 'application/json'
        }).then(() => {
            if(this.props.question.type === 'TITA') {
                this.props.deleteTitaAnswer();
            } else {
                this.props.deleteAnswer();
            }
        });
        if(this.props.question.type === 'TITA') {
            this.clearValue();
        }
    }

    addValue(item) {
        $('#tita-input').val($('#tita-input').val() + item);
    }

    clearValue() {
        $('#tita-input').val('');
    }

    render() {
        if(this.props.testType === 'CAT'){
		    if(this.props.sectionAttempts[this.props.sectionIndex].start) {
			       startIndex = this.props.sectionAttempts[this.props.sectionIndex].start;
		    }
	    }
        if(this.props.question.titaChoice !== undefined) {
            $('#tita-input').val(this.props.question.titaChoice);
        }
        return (
            <div className="question">
                <p>Question no : {this.props.currentQuestionIndex + startIndex + 1}</p>
                {(() => {
                    if(this.props.question.type === 'RC') {
                        return <div className="rc-passage">
                                    <span>Direction for questions:</span><br />
                                    <div className="modify" dangerouslySetInnerHTML={{__html: this.props.question.rc_passage}}></div>
                                </div>
                    }
                })()}
                <h3><p className= "modify" dangerouslySetInnerHTML={{__html: this.props.question.html.replace("`", "")}}></p></h3>
                {(() => {
                    if(this.props.question.type === 'TITA') {
                        return <div className="choices-form">
                                    <input key={this.props.question.id} id="tita-input" type="text"
                                        defaultValue={(this.props.question.titaChoice === undefined) ? "" : this.props.question.titaChoice}
                                        readOnly />
                                    <div className="tita-input-buttons">
                                        <button className="tita-input-button" onClick={this.addValue.bind(this, "1")}>1</button>
                                        <button className="tita-input-button" onClick={this.addValue.bind(this, "2")}>2</button>
                                        <button className="tita-input-button" onClick={this.addValue.bind(this, "3")}>3</button>
                                        <button className="tita-input-button" onClick={this.addValue.bind(this, "4")}>4</button>
                                        <button className="tita-input-button" onClick={this.addValue.bind(this, "5")}>5</button>
                                        <button className="tita-input-button" onClick={this.addValue.bind(this, "6")}>6</button>
                                        <button className="tita-input-button" onClick={this.addValue.bind(this, "7")}>7</button>
                                        <button className="tita-input-button" onClick={this.addValue.bind(this, "8")}>8</button>
                                        <button className="tita-input-button" onClick={this.addValue.bind(this, "9")}>9</button>
                                        <button className="tita-input-button" onClick={this.addValue.bind(this, ".")}>.</button>
                                        <button className="tita-input-button" onClick={this.addValue.bind(this, "0")}>0</button>
                                        <button className="tita-input-button" onClick={this.clearValue.bind(this)}>Clear</button>
                                    </div>
                                </div>
                    } else {
                        return <form className="choices-form">
                                    <ul className="choices">
                                    {this.props.question.choices.map((choice) => {
                                        return <Choice key={choice.id}
                                                question_id={this.props.question.id}
                                                choice={choice}
                                                isSelected={choice.id === this.getSelectedChoiceId()}
                                                choiceSelected={this.props.choiceSelected}
                                                sectionIndex={this.props.sectionIndex}
                                                sectionAttempts={this.props.sectionAttempts} />
                                    })}
                                    </ul>
                                </form>
                    }
                })()}
                {(() => {
                    if(this.props.hasPrev) {
                        return <button className="next-prev-button save-prev demo-tst" onClick={this.getPrevQuestion}>Previous</button>
                    }
                })()}
                {(() => {
                    if(this.props.hasNext) {
                        return <button className="next-prev-button save-next demo-tst" onClick={this.getNextQuestion}>Save & Next</button>
                    }
                })()}
                <button className="next-prev-button submit-next demo-tst" onClick={this.markForReviewAndNext.bind(this)}>Mark For Review</button>
                
                <button className="next-prev-button demo-tst" onClick={this.deleteAnswer}>Clear Response</button>
            </div>
        );
    }
}

Question.propTypes = {
    question: PropTypes.object,
    choiceId: PropTypes.number,
    currentQuestionIndex: PropTypes.number,
    currentQuestionId: PropTypes.number,
    hasNext: PropTypes.bool,
    hasPrev: PropTypes.bool,
    sectionAttempts: PropTypes.array,
    sectionIndex: PropTypes.number,
    choiceSelected: PropTypes.func,
    submitAnswer: PropTypes.func,
    showNextQuestion: PropTypes.func,
    showPrevQuestion: PropTypes.func,
    testAttempt: PropTypes.object,
    deleteAnswer: PropTypes.func
};

export default Question;
