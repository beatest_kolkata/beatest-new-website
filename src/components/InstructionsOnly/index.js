import React, { Component } from 'react';
import classnames from 'classnames';
import InstructionsOnlyContainer from '../InstructionsOnlyContainer';
/*import './style.css';*/

class InstructionsOnly extends Component {
    render() {
        const { className, ...props } = this.props;
        return (
            <InstructionsOnlyContainer className={classnames('InstructionsOnly', className)} {...props}/>
        );
    }
}

export default InstructionsOnly;
