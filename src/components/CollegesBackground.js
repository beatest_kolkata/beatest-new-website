import React, {PropTypes, Component } from 'react';
import $ from 'jquery';
import * as action from '../actions/action';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Navlink from './Navlink';
import UserPanel from './UserPanel';
import UserMenu from './UserMenu';
import NotFound from './NotFound';
import Footer from './Footer';
import Dropdown, { DropdownTrigger, DropdownContent } from '../../node_modules/react-simple-dropdown/lib/components/Dropdown.js';

const initialState = {
    user: null,
    collegeList: [],
    loginPanelVisibility: 'none',
    registrationPanelVisibility: 'none',
    userMenuVisibility: 'none',
    tmp: 0
}

class Banner extends Component {
    constructor(props) {
        super(props);
        this.state = initialState;
        this.handleLinkClick = this.handleLinkClick.bind(this);
        this.handleOtherClick = this.handleOtherClick.bind(this);
        this.getColleges = this.getColleges.bind(this);
        this.getCollegeList = this.getCollegeList.bind(this);
        this.submit = this.submit.bind(this);
        this.getColleges();
        this.tmp=0;
    }

    submit() {
        var x,y,z;

        y = document.getElementById("dropcon2").innerHTML;
        z = document.getElementById("dropcon2x").value;

        if(z.length <= 2 && y.length <= 2){
        } else {
            if(y.length <= 2) x=z;
            else x=y;

            var college_name = x;
                $.ajax({
                    url: '/api/new_add_college',
                    dataType: 'json',
                    type: 'POST',
                    contentType: 'application/json',
                    data: JSON.stringify({
                        college_name
                    })
                }).then(function(data) {
                    window.location.href = "/collegesignup/"+data.message;
                }.bind(this));

        }
    }

    handleLinkClick = (e) => {
//        console.log(this.tmp);
        this.tmp = 0;
//        console.log(this.tmp);
        document.getElementById("dropcon2x").innerHTML = "";
        document.getElementById("dropcon2x").style.visibility = 'hidden';
        document.getElementById("dropcon2").innerHTML = e;
        document.getElementById("dropcon2").style.visibility = 'visible';
        this.refs.dropdown.hide();
    }

    handleOtherClick() {
//        console.log(this.tmp);
        this.tmp = 1;
//        console.log(this.tmp);
        document.getElementById("dropcon2").innerHTML = "";
        document.getElementById("dropcon2").style.visibility = 'hidden';
        document.getElementById("dropcon2x").style.visibility = 'visible';
        this.refs.dropdown.hide();
    }

    getColleges() {
        $.get('/api/get_colleges', (data) => {
        this.props.actions.assignColleges(data);
        }).then(() => {
        });
    }

    getCollegeList() {
        return  this.props.collegeList;
    }

    render() {

        var lis=[];
        if(typeof this.props.collegeList == 'object'){
            {this.getCollegeList().map((college, index) => {
                 lis.push(  <li type="none" key={college.id} className="account-dropdown__link">
                              <a className="account-dropdown__link__anchor" href="#" onClick={() => this.handleLinkClick(college.college_name)}>
                                {college.college_name}
                              </a>
                            </li>
                 );
            })}
        }

        return (
        <div>
        <Navlink toggleLoginPanelVisibility={this.props.actions.toggleLoginPanelVisibility}
                    toggleUserMenuVisibility={this.props.actions.toggleUserMenuVisibility}
                    onPage="index" user={this.props.user} login={this.props.actions.login} />

        <div className="banner">
			<div className="dropdown-embedded">
                <Dropdown className="account-dropdown" ref="dropdown">
                        <DropdownTrigger>
                          <img className="account-dropdown__avatar" src="" /><span id="dropcon" className="account-dropdown__name"> -- Select College -- </span>
                        </DropdownTrigger>
                        <DropdownContent>
                          <div className="account-dropdown__identity account-dropdown__segment">
                          </div>
                          <ul className="account-dropdown__quick-links account-dropdown__segment">
                            {lis}
                          </ul>
                          <ul className="account-dropdown__management-links account-dropdown__segment">
                            <li type="none" className="account-dropdown__link">
                              <a className="account-dropdown__link__anchor" href="#" onClick={this.handleOtherClick}>
                                Others
                              </a>
                            </li>
                          </ul>
                        </DropdownContent>
                </Dropdown>
                <br/>
                <div id="dropcon2" className="tt"> </div>
                <br/>
                <textarea placeholder="-- Enter College Name --" id="dropcon2x"></textarea>
                <br/>
                <br/>
                <button className="banner-button" onClick={this.submit}>Submit</button>
            </div>
			<div className="banner-button-div">
			</div>
        </div>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <Footer />
		</div>
        );
    }
}

Banner.propTypes = {
    loginPanelVisibility: PropTypes.string,
    registrationPanelVisibility: PropTypes.string,
    userMenuVisibility: PropTypes.string,
    user: PropTypes.object,
    collegeList: PropTypes.array
}

function mapStateToProps(state, props) {
    return {
        loginPanelVisibility: state.loginPanelVisibility,
        registrationPanelVisibility: state.registrationPanelVisibility,
        userMenuVisibility: state.userMenuVisibility,
        user: state.user,
        collegeList: state.collegeList
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(action, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Banner);