/* eslint-disable */
import React, { Component } from 'react';
import $ from 'jquery';
import FacebookLogin from 'react-facebook-login';
import GoogleLogin from 'react-google-login';

var mixpanel = require('mixpanel-browser');
mixpanel.init("86e70fa37444d5ab81c1ea058ae2fcd4");

class UserPanel extends Component {
    constructor(props) {
        super(props);
        this.login = this.login.bind(this);
        this.signup = this.signup.bind(this);
        this.showRegistrationPanel = this.showRegistrationPanel.bind(this);
        this.responseFacebook = this.responseFacebook.bind(this);
        this.responseGoogle = this.responseGoogle.bind(this);
    }

    login(e) {
        e.preventDefault();
        var email = $('#email').val();
        mixpanel.identify(email);
        mixpanel.track("User Logged in");
        var password = $('#password').val();
        var email = $('#email').val();
        var password = $('#password').val();
        $('#login-form')[0].reset();
        $.ajax({
            url: '/api/login',
            dataType: 'json',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                email,
                password
            })
        }).then(function(data) {
            if(data.hasOwnProperty('invalid')) {
                if(data.invalid === 'Email') {
                    $('#user-panel-validator').html("Sorry, Beatest doesn't recognize that email.");
                } else if (data.invalid === 'Password') {
                    $('#user-panel-validator').html("Wrong password. Try again.");
                } else if (data.invalid === 'User') {
                    this.props.toggleLoginPanelVisibility();
                    this.props.toggleRegistrationPanelVisibility();
                    $('#registration-panel-validator').html("Please add a password for your account.");
                    $('#new-full-name').val(data.name);
                    $('#new-email').val(data.email);
                }
            } else if (data.hasOwnProperty('user')) {
                this.props.login(data.user);
                this.props.toggleLoginPanelVisibility();
                //location.reload();
            }
        }.bind(this));
    }

    signup(e) {
        e.preventDefault();
        var full_name = $('#new-full-name').val();
        var email = $('#new-email').val();
        var password = $('#new-password').val();
        var confirm_password = $('#new-confirm-password').val();
        var ref_code_used = $('#referral-code').val();
        if (ref_code_used === undefined)
            ref_code_used = "";
        $('#register-form')[0].reset();
        if(password === confirm_password) {
            $.ajax({
                url: '/api/users',
                dataType: 'json',
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify({
                    full_name,
                    email,
                    password,
                    ref_code_used
                })
            }).then(function(data) {
                this.props.login(data.user);
            }.bind(this));
            alert('Please visit your mail to activate your Beatest Account!');
            this.props.toggleRegistrationPanelVisibility();
        } else {
            $('#registration-panel-validator').html("These passwords don't match. Try again.");
        }
     var today = new Date();
    mixpanel.people.set({
            "$name": full_name,
            "$email": email,
        "$created": String(today)
    });
    mixpanel.identify(email);
    mixpanel.track("User Signed in");
    }

    showRegistrationPanel() {
        this.props.toggleLoginPanelVisibility();
        this.props.toggleRegistrationPanelVisibility();
    }

    responseFacebook (response) {
        $.ajax({
            url: '/api/facebook_login',
            dataType: 'json',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                name: response.name,
                id: response.id,
                accessToken: response.accessToken,
                email: response.email,
                url: response.picture.data.url
            })
        }).then((data) => {
            this.props.login(data.user);
            this.props.toggleLoginPanelVisibility();
            // location.reload();
        });
    }

    responseGoogle = (response) => {
        $.ajax({
            url: '/api/google_login',
            dataType: 'json',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                accessToken: response.accessToken,
                googleId: response.googleId,
                tokenId: response.tokenId,
                email: response.profileObj.email,
                name: response.profileObj.name,
                imageUrl: response.profileObj.imageUrl
            })
        }).then((data) => {
            this.props.login(data.user);
            this.props.toggleLoginPanelVisibility();
            //location.reload();
            location.reload();
        });
    }

    render() {
        var userPanelClassName = 'user-panel ' + this.props.loginPanelVisibility;
        var registrationPanelClassName = 'user-panel ' + this.props.registrationPanelVisibility;
        return (
            <div>
            <div className={userPanelClassName}>
                <div className="black-background" onClick={this.props.toggleLoginPanelVisibility}></div>
                <div className="panel">
                    <div className="close-panel" onClick={this.props.toggleLoginPanelVisibility}>X</div>
                    <form id="login-form" onSubmit={this.login}>
                        <input id="email" className="user-panel-input" type="email" placeholder="Email" required />
                        <input id="password" className="user-panel-input" type="password" placeholder="Password" required />
                        <div><label id="user-panel-validator"></label></div>
                        <button className="user-panel-button">Login</button>
                    </form>
                    <p className="login-panel-text">forgot password?
                        <a> click here</a>
                    </p>
                    <p className="login-panel-text">not yet registered?
                        <a onClick={this.showRegistrationPanel}> click here</a>
                    </p>
                    <div className="social-media-login">
                        <FacebookLogin appId="1237812479598094" cssClass="facebook-login-button"
                            icon="fa-facebook" fields="name,email,picture" textButton="Facebook" callback={this.responseFacebook} />
                        <div className="space"></div>
                        <GoogleLogin clientId="709876791157-nh5o4s2rgn89b83hhpnrj56k9a9gupon.apps.googleusercontent.com"
                            icon="fa-google-plus" buttonText="Google" onSuccess={this.responseGoogle} onFailure={this.responseGoogle} className="google-login-button"/>
                    </div>
                </div>
            </div>
            <div className={registrationPanelClassName}>
                <div className="black-background" onClick={this.props.toggleRegistrationPanelVisibility}></div>
                <div className="register panel">
                    <div className="close-panel" onClick={this.props.toggleRegistrationPanelVisibility}>X</div>
                    <form id="register-form" onSubmit={this.signup}>
                        <input id="new-full-name" className="user-panel-input" type="text" placeholder="Full Name" required />
                        <input id="new-email" className="user-panel-input" type="email" placeholder="Email" required />
                        <input id="new-password" className="user-panel-input" type="password" placeholder="Password" required />
                        <input id="new-confirm-password" className="user-panel-input" type="password" placeholder="Confirm Password" required />
                        <input id="referral-code" className="user-panel-input" placeholder="Referral Code (Optional)"/>
                        <div><label id="registration-panel-validator"></label></div>
                        <button className="user-panel-button">Register</button>
                    </form>
                </div>
            </div>
            </div>
        );
    }
}

export default UserPanel;
