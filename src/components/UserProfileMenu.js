import React, { Component } from 'react';
import $ from 'jquery';
import CollegeTestOption from './CollegeTestOption';

class UserProfileMenu extends Component {

    constructor(props){
        super(props);
        this.state={
            tests:[],
            val:'true'
        }
    }

    componentWillMount(){
        $.get('/api/get_college_tests/' + this.props.user.college_id + '/' + this.props.user.id, (data) => {
            this.setState({tests:data.tests});
        });
    }


    render() {
        return(
            <div className="row" id="testsDisplay">
                {this.state.tests.map(test => (
                    <div key={test.id} className="col-lg-4" id="eachTest">
                        <div id="name">{test.name}</div>
                        <CollegeTestOption user={this.props.user}
                                           test={test}
                                           key={test.id} id={test.id} name={test.name}
                                           toggleLoginPanelVisibility={this.props.toggleLoginPanelVisibility} />

                    </div>
                ))}
            </div>

        );
    }
}

export default UserProfileMenu;