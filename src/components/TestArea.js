import React, {PropTypes, Component} from 'react';
import $ from 'jquery';

import Loader from './Loader';
import Sections from './Sections';
import Question from './Question';
import IndexPanel from './IndexPanel';

class TestArea extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sectionLoaded: true
        };
        this.getQuestion = this.getQuestion.bind(this);
        this.buildQuestionIndex = this.buildQuestionIndex.bind(this);
        this.hasPrev = this.hasPrev.bind(this);
        this.hasNext = this.hasNext.bind(this);
        this.saveTimer = this.saveTimer.bind(this);
        window.setInterval(function() {
            this.saveTimer(this.props.timeleft);
        }.bind(this), 3000);
    }

    saveTimer(timeleft) {
        $.ajax({
            url: '/api/section_attempt/' + this.props.sectionAttempts[this.props.sectionIndex].id + '/timeleft',
            dataType: 'json',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                timeleft
            }),
            success: (() => {
            }),
            error: (() => {
                //alert('Lost network connection. Your test will begin from here next time.');
               // window.close();
            })
        });
    }

    getQuestion() {
        return this.props.questionsList[this.props.currentQuestionIndex];
    }

    buildQuestionIndex() {
        this.props.questionsList.map(function(question) {
            return question.id;
        });
    }

    hasNext() {
        if(this.props.currentQuestionIndex < (this.props.questionsList.length - 1)) {
            return true;
        }
        return false;
    }

    hasPrev() {
        if (this.props.currentQuestionIndex > 0) {
            return true;
        }
        return false;
    }

    changeSectionLoaded(sectionLoaded) {
        this.setState({ sectionLoaded });
    }

    render() {
        if (this.props.isQuestionsLoading) {
            return <div className="test-area"></div>;
        }
        return (
            <div className="test-area">
                {this.state.sectionLoaded ? (
                    <div>
                        <div>
                            <Sections sectionAttempts={this.props.sectionAttempts}
                                changeTimeleft={this.props.changeTimeleft}
                                sectionIndex={this.props.sectionIndex}
                                testType={this.props.testAttempt.type}
                                testAttempt={this.props.testAttempt}
                                loadingQuestionsBegin={this.props.loadingQuestionsBegin}
                                loadingQuestionsSuccess={this.props.loadingQuestionsSuccess}
                                gotoSelectedSection={this.props.gotoSelectedSection}
                                changeSectionLoaded={this.changeSectionLoaded.bind(this)} />
                            <Question question={this.getQuestion()}
                                choiceId={this.props.choiceId}
                                currentQuestionIndex={this.props.currentQuestionIndex}
                                currentQuestionId={this.props.currentQuestionId}
                                hasNext={this.hasNext()} hasPrev={this.hasPrev()}
                                sectionAttempts={this.props.sectionAttempts}
                                sectionIndex={this.props.sectionIndex}
                                choiceSelected={this.props.choiceSelected}
                                submitAnswer={this.props.submitAnswer}
                                submitTitaAnswer={this.props.submitTitaAnswer}
                                markAnswer={this.props.markAnswer}
                                markTitaAnswer={this.props.markTitaAnswer}
                                showNextQuestion={this.props.showNextQuestion}
                                showPrevQuestion={this.props.showPrevQuestion}
                                deleteAnswer={this.props.deleteAnswer}
                                deleteTitaAnswer={this.props.deleteTitaAnswer} />
                        </div>
                        <IndexPanel questionIndex={this.buildQuestionIndex()}
                            currentQuestionIndex={this.props.currentQuestionIndex}
                            currentQuestionId={this.props.currentQuestionId}
                            questionsList={this.props.questionsList}
                            testType={this.props.testAttempt.type}
                            testAttempt={this.props.testAttempt}
                            loadingQuestionsBegin={this.props.loadingQuestionsBegin}
                            loadingQuestionsSuccess={this.props.loadingQuestionsSuccess}
                            gotoNextSection={this.props.gotoNextSection}
                            sectionAttempts={this.props.sectionAttempts}
                            timeleft={this.props.timeleft} changeTimeleft={this.props.changeTimeleft}
                            userName={this.props.userName} sectionIndex={this.props.sectionIndex}
                            showSelectedQuestion={this.props.showSelectedQuestion} />
                    </div>
                ) : (
                    <Loader />
                )}
            </div>
        );
    }
}

TestArea.propTypes = {
    questionsList: PropTypes.array,
    currentQuestionId: PropTypes.number,
    currentQuestionIndex: PropTypes.number,
    testAttempt: PropTypes.object,
    isQuestionsLoading: PropTypes.bool,
    sectionIndex: PropTypes.number,
    sectionAttempts: PropTypes.array,
    loadingQuestionsBegin: PropTypes.func,
    loadingQuestionsSuccess: PropTypes.func,
    gotoNextSection: PropTypes.func,
    choiceSelected: PropTypes.func
};

export default TestArea;