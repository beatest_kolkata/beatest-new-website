import React, {PropTypes, Component} from 'react';
import * as action from '../actions/action';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import $ from 'jquery';

import Loader from './Loader';
import TestArea from './TestArea';
import NotFound from './NotFound';

const createCookie = require('./cookie.js').createCookie;
const readCookie = require('./cookie.js').readCookie;

let options = {
    lines: 13,
    length: 20,
    width: 10,
    radius: 30,
    scale: 1.00,
    corners: 1,
    color: '#000',
    opacity: 0.25,
    rotate: 0,
    direction: 1,
    speed: 1,
    trail: 60,
    fps: 20,
    zIndex: 2e9,
    top: '50%',
    left: '50%',
    shadow: false,
    hwaccel: false,
    position: 'absolute'
};

class TestContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            currentView: 'TEST_AREA',
            questionsList: [],
            sectionIndex: 0,
            currentQuestionIndex: 0,
        };
        this.prevent();
        this.getUser = this.getUser.bind(this);
        this.getTest = this.getTest.bind(this);
        this.startTest = this.startTest.bind(this);
        this.getUser();
        this.getTest();
        this.startTest();
    }

    prevent() {
        $(document).bind("contextmenu",function(e) {
            e.preventDefault();
        });
        $(document).keydown(function(e) {
            if(e.which >= 112 && e.which <= 123) return false;
        });
        $(document).keyup(function(e) {
            if(e.which >= 112 && e.which <= 123) return false;
        });
        $(document).keydown(function(e) {
            if(e.which === 17) return false;
        });
        $(document).keyup(function(e) {
            if(e.which === 17) return false;
        });
    }

    getUser() {
        if(readCookie('userData') !== null) {
            this.props.actions.login(JSON.parse(readCookie('userData')));
            this.getTest();
        } else {
            $.get('/api/user', (data) => {
                if (typeof data === 'object') {
                    createCookie('userData', JSON.stringify(data.user), 7);
                    this.props.actions.login(data.user);
                    this.getTest();
                } else {
                    window.location.pathname = '/';
                }
            });
        }
    }

    getTest() {
        $.get('/api/tests/' + this.props.routeParams.testId, (data) => {
            this.props.actions.setTestDetails(data);
        });
    }

    startTest() {
        this.props.actions.createExamBegin();
        var sectionIndex = this.props.sectionIndex;
        $.get('/api/tests/' + this.props.routeParams.testId + '/test_attempt', (data) => {
            if(data.testAttempt !== 'Null') {
                this.props.actions.createExamSuccess(data.testAttempt, data.sectionAttempts, this.props.testName);
                this.props.actions.loadingQuestionsBegin();
                $.get('/api/section_attempt/' + data.sectionAttempts[sectionIndex].id + '/questions', (questionsList) => {
                    this.props.actions.loadingQuestionsSuccess(questionsList);
                    this.setState({ loading: false });
                }).then(() => {
                    $.get('/api/section_attempt/' + this.props.sectionAttempts[sectionIndex].id + '/timeleft', (data) => {
                        this.props.actions.changeTimeleft(data.timeleft);
                    });
                });
            } else {
                $.ajax({
                    url: '/api/test_attempt',
                    dataType: 'json',
                    type: 'POST',
                    contentType: 'application/json',
                    data: JSON.stringify({
                        testId: this.props.routeParams.testId
                    })
                }).then(function(data) {
                    if (data.testAttempt !== 'Null' && data.sectionAttempts !== 'Null') {
                        this.props.actions.createExamSuccess(data.testAttempt, data.sectionAttempts, this.props.testName);
                        this.props.actions.loadingQuestionsBegin();
                        $.get('/api/section_attempt/' + data.sectionAttempts[sectionIndex].id + '/questions', (questionsList) => {
                            this.props.actions.loadingQuestionsSuccess(questionsList);
                            this.setState({ loading: false });
                        }).then(() => {
                            this.props.actions.changeTimeleft(this.props.sectionAttempts[sectionIndex].totalTime);
                        });
                    }
                }.bind(this));
            }
        });
    }

    render() {
        if(this.state.loading) {
            return (
                <div>
                    <Loader />
                </div>
            );
        } else {
            if(this.props.user === null) {
                return (
                    <NotFound />
                );
            } else {
                return (
                    <div className="container">
                        {(() => {
                            if (this.props.currentView === 'TEST_AREA') {
                                return (
                                    <TestArea questionsList={this.props.questionsList}
                                        isQuestionsLoading={this.props.isQuestionsLoading}
                                        currentQuestionId={this.props.currentQuestionId}
                                        currentQuestionIndex={this.props.currentQuestionIndex}
                                        sectionAttempts={this.props.sectionAttempts}
                                        sectionIndex={this.props.sectionIndex}
                                        testAttempt={this.props.testAttempt}
                                        timeleft={this.props.timeleft}
                                        userName={this.props.user.name}
                                        changeTimeleft={this.props.actions.changeTimeleft}
                                        showSelectedQuestion={this.props.actions.showSelectedQuestion}
                                        loadingQuestionsBegin={this.props.actions.loadingQuestionsBegin}
                                        loadingQuestionsSuccess={this.props.actions.loadingQuestionsSuccess}
                                        gotoNextSection={this.props.actions.gotoNextSection}
                                        choiceSelected={this.props.actions.choiceSelected}
                                        submitAnswer={this.props.actions.submitAnswer}
                                        submitTitaAnswer={this.props.actions.submitTitaAnswer}
                                        markAnswer={this.props.actions.markAnswer}
                                        markTitaAnswer={this.props.actions.markTitaAnswer}
                                        showNextQuestion={this.props.actions.showNextQuestion}
                                        showPrevQuestion={this.props.actions.showPrevQuestion}
                                        deleteAnswer={this.props.actions.deleteAnswer}
                                        deleteTitaAnswer={this.props.actions.deleteTitaAnswer}
                                        gotoSelectedSection={this.props.actions.gotoSelectedSection} />
                                )
                            }
                        })()}
                    </div>
                );
            }
        }
    }
}

TestContainer.propTypes = {
    currentView: PropTypes.string,
    testName: PropTypes.string,
    testId: PropTypes.number,
    questionsList: PropTypes.array,
    isQuestionsLoading: PropTypes.bool,
    user: PropTypes.object,
    sectionIndex: PropTypes.number,
    isTestsLoading: PropTypes.bool,
    sectionAttempts: PropTypes.array,
    currentQuestionId: PropTypes.number,
    currentQuestionIndex: PropTypes.number,
    testAttempt: PropTypes.object,
    isExamCreating: PropTypes.bool,
    timeleft: PropTypes.number,
    isUserLoading: PropTypes.bool
}

function mapStateToProps(state, props) {
    return {
        currentView: state.currentView,
        testName: state.testName,
        testId: state.testId,
        questionsList: state.questionsList,
        isQuestionsLoading: state.isQuestionsLoading,
        user: state.user,
        sectionIndex: state.sectionIndex,
        isTestsLoading: state.isTestsLoading,
        sectionAttempts: state.sectionAttempts,
        currentQuestionId: state.currentQuestionId,
        currentQuestionIndex: state.currentQuestionIndex,
        testAttempt: state.testAttempt,
        isExamCreating: state.isExamCreating,
        timeleft: state.timeleft,
        isUserLoading: state.isUserLoading
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(action, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TestContainer);