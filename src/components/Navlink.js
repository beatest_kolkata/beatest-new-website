import React, {Component} from 'react';
import $ from 'jquery';
import MetaTags from 'react-meta-tags';

class Navlink extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showMobileNav: false,
            showNav: false,
            wallet: 0
        };
        this.toggleUserMenu = this.toggleUserMenu.bind(this);
        this.toggleUserPanel = this.toggleUserPanel.bind(this);
        this.scrollToContact = this.scrollToContact.bind(this);
        this.scrollToFeatures = this.scrollToFeatures.bind(this);
        this.scrollToCourses = this.scrollToCourses.bind(this);
        this.openPricingTab = this.openPricingTab.bind(this);
        this.getUser = this.getUser.bind(this);
        this.getUser();
        $(window).resize(function() {
            if($(window).width() < 725) {
                this.setState({
                    showMobileNav: true
                });
            } else {
                this.setState({
                    showMobileNav: false
                });
            }
        }.bind(this));
    }

    componentDidMount() {
        if($(window).width() < 725) {
            this.setState({
                showMobileNav: true
            });
        }
    }

    getUser() {
        $.get('/api/user', (data) => {
            if(typeof data === 'object') {
                this.props.login(data.user);
                if (data.user.wallet !== undefined) {
                    this.setState ({
                        wallet: data.user.wallet
                    });
                }
            }
        });
    }

    toggleUserMenu() {
        this.props.toggleUserMenuVisibility();
    }

    toggleUserPanel() {
        this.props.toggleLoginPanelVisibility();
        if(this.state.showNav) {
            this.setState({
                showNav: false
            });
        }
    }

    scrollToContact() {
        if(this.props.onPage === 'index') {
            $('html, body').animate({
                scrollTop: $('.contact').offset().top - 60
            }, 1500);
        }
        if(this.state.showNav) {
            this.setState({
                showNav: false
            });
        }
    }

    scrollToCourses() {
        if(this.props.onPage === 'index') {
            $('html, body').animate({
                scrollTop: $('.courses').offset().top - 100
            }, 1500);
        }
        if(this.state.showNav) {
            this.setState({
                showNav: false
            });
        }
    }

    scrollToFeatures() {
        if(this.props.onPage === 'index') {
            $('html, body').animate({
                scrollTop: $('.features').offset().top
            }, 1000);
        }
        if(this.state.showNav) {
            this.setState({
                showNav: false
            });
        }
    }

    toggleShowNav() {
        if(this.state.showNav) {
            this.setState({
                showNav: false
            });
        } else {
            this.setState({
                showNav: true
            });
        }
    }

    openPricingTab() {
        window.location.href = "/pricing";
    }

    render() {
        if(this.state.showMobileNav) {
            return (
                <div className="nav">
                <MetaTags>
                    <title>Beatest | Free Test Prep Solution | CAT, Banking Exams</title>
                    <meta property="og:title" content="Beatest | Free Test Prep Solution | CAT, Banking Exams" />
                    <meta name="keywords" content="MBA, CAT, IIM, 2016, 2017, Free exams, Free Tests, Free mock tests, Daily Tests, CAT Mock Tests, IBPS PO Mock Tests, Test Strategy, Exam Strategy, Toppers, IBPS, Bank PO, SBI PO, SBI Clerk, SBI PO Mock Test, SBI PO Mains, SBI PO Mains Mock Test, SBI Clerk Mock test, <SBI PO 2017, SBI Clerk 2017, IBPS PO 2017, IBPS Clerk 2017, IBPS Clerk Mock Test " />
                    <meta name="description" content="A FREE one-stop solution to ace your MBA and Banking exams test preparations online with an excellent team of CAT and Banking exam toppers" />
                    <meta property="og:url" content="https://beatest.in/" />
                    <meta name="author" content="beatest" />
                </MetaTags>
                    <h1>BEATEST</h1>
                    {(() => {
                        if(this.state.showNav) {
                            return <i className="fa fa-close fa-2x mobile-nav-button" onClick={this.toggleShowNav.bind(this)}></i>
                        } else {
                            return <i className="fa fa-bars fa-2x mobile-nav-button" onClick={this.toggleShowNav.bind(this)}></i>
                        }
                    })()}
                    {(() => {
                        if(this.state.showNav) {
                            return <div className="mobile-nav">
                                {(() => {
                                    if(this.props.onPage !== 'index') {
                                        return <a href="/index">HOME</a>
                                    }
                                })()}
                                {(() => {
                                    if(this.props.user && (this.props.onPage === 'pricing' || this.props.onPage === 'profile')) {
                                        return <a>WALLET: &#x20b9;{this.props.user.wallet}</a>
                                    }
                                })()}
                                {(() => {
                                    if(this.props.onPage === 'index') {
                                        return <a onClick={this.scrollToCourses}>COURSES</a>
                                    }
                                })()}
                                {(() => {
                                    if(this.props.onPage === 'index') {
                                        return <a onClick={this.openPricingTab}>PRICING</a>
                                    }
                                })()}
                                {/*{(() => {
                                    if(this.props.onPage === 'index') {
                                        return <a>BLOGS</a>
                                    }
                                })()}*/}
                                {/*{(() => {
                                    if(this.props.onPage === 'index') {
                                        return <a onClick={this.scrollToFeatures}>FEATURES</a>
                                    }
                                })()}*/}
                                {(() => {
                                    if(this.props.onPage === 'index') {
                                        return <a onClick={this.scrollToContact}>CONTACT</a>
                                    }
                                })()}
                                {(() => {
                                    if(this.props.onPage === 'index') {
                                        return <a href="/aboutus"><li>OUR TEAM</li></a>
                                    }
                                })()}
                                <br />
                                {(() => {
                                    if(this.props.user === null) {
                                        return <div className="nav-button mobile" onClick={this.toggleUserPanel}>Login</div>
                                    }
                                })()}
                            </div>
                        }
                    })()}
                    {(() => {
                        if(this.props.user !== null) {
                            if(this.props.user.profile_picture === null) {
                                return <div className="nav-button" onClick={this.toggleUserMenu}>{this.props.user.name}</div>
                            } else {
                                return <div className="profile-pic-nav-button" onClick={this.toggleUserMenu}><img height="40" src={this.props.user.profile_picture} /></div>
                            }
                        }
                   })()}
                </div>
            );
        } else {
            return (
                <div className="nav">
                    <img src="../../logo1.png" alt="Beatest"/>
                    {(() => {
                        if(this.props.user === null) {
                            return <div className="nav-button" onClick={this.toggleUserPanel}>Login</div>
                        } else if(this.props.user.profile_picture === null) {
                            return <div className="nav-button-index" onClick={this.toggleUserMenu}>{this.props.user.name}</div>
                        } else {
                            return <div className="profile-pic-nav-button" onClick={this.toggleUserMenu}><img height="40" src={this.props.user.profile_picture} /></div>
                        }
                    })()}
                    {(() => {
                        if(this.props.onPage === 'index') {
                            return <a onClick={this.scrollToContact}><li>CONTACT</li></a>
                        }
                    })()}
                    {/*{(() => {
                        if(this.props.onPage === 'index') {
                            return <a href="http://beatest.in:9000/blog/"><li>BLOG</li></a>
                        }
                    })()}*/}
                    {(() => {
                        if(this.props.onPage === 'index') {
                            return <a onClick={this.openPricingTab}><li>PRICING</li></a>
                        }
                    })()}
                    {/*{(() => {
                        if(this.props.onPage === 'index') {
                            return <a href="#"><li>TESTIMONIALS</li></a>
                        }
                    })()}*/}
                    {(() => {
                        if(this.props.onPage === 'index') {
                            return <a onClick={this.scrollToCourses}><li>COURSES</li></a>
                        }
                    })()}
                    {(() => {
                        if(this.props.user && (this.props.onPage === 'pricing' || this.props.onPage === 'profile')) {
                            return <a>WALLET: &#x20b9;{this.props.user.wallet}</a>
                        }
                    })()}
                    {(() => {
                        if(this.props.onPage !== 'index') {
                            return <a href="/index"><li>HOME</li></a>
                        }
                    })()}
                </div>
            );
        }
    }
}

export default Navlink;
