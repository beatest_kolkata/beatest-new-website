import React, {Component} from 'react';
import $ from 'jquery';

var mixpanel = require('mixpanel-browser');
mixpanel.init("86e70fa37444d5ab81c1ea058ae2fcd4");

class SubmitExam extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.submitExam = this.submitExam.bind(this);
    }

    submitExam() {
	var s;
        // $.get('/api/test_attempt/' + this.props.testAttempt.id + '/get_score', (score) => {
        //     //alert('Your score : '+ score.score);
        //     s = "Score: " + String(score.score);
        //     alert('Test has been submitted!');
        //     window.close();
        //     mixpanel.track("Test Completed");
        //     mixpanel.track(s);
        // });
        $.ajax({
            url: '/api/test_attempt/' + this.props.testAttempt.id + '/submit_test',
            dataType: 'json',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({})
        }).then(function (data) {
            alert('Visit Tests to view your Scores!');
            window.close();
            mixpanel.track("Test Completed");
            mixpanel.track(s);
        }.bind(this));
    }

    render() {
        return (
            <div className="submit-test">
                <button className="next-prev-button submit-exam" onClick={this.submitExam}>Submit Test</button>
            </div>
        );
    }
}

export default SubmitExam;
