import React, { Component } from 'react';
import $ from 'jquery';

let timer;

class CountDownTimer extends Component {
    constructor (props) {
        super(props);
        this.changeTimeleft = this.changeTimeleft.bind(this);
    }
    
    componentDidMount () {
        timer = setInterval(this.changeTimeleft, 1000);
    }
    
    componentWillUnmount () {
        if(timer){
            clearInterval(timer);
        }
    }

    changeTimeleft() {
        var time = this.props.timeleft;
        time--;
        if(this.props.testAttempt.type === 'CAT') {
            if(time >= 0) {
                this.props.changeTimeleft(time);
            } else {
                this.props.gotoNextSection();
            }
        } else {
            if(time >= 0) {
                this.props.changeTimeleft(time);
            } else {
                $.ajax({
                    url: '/api/test_attempt/' + this.props.testAttempt.id + '/submit_test',
                    dataType: 'json',
                    type: 'POST',
                    contentType: 'application/json',
                    data: JSON.stringify({})
                }).then(function (data) {
                    alert('Visit Tests to view your Scores!');
                    window.close();
                }.bind(this));
            }
        }
    }

    convertTime (seconds) {
        let minutes = Math.floor(seconds / 60);
        let sec = seconds % 60;
        minutes = minutes < 10 ? `0${minutes}` : `${minutes}`;
        sec = sec < 10 ? `0${sec}` : `${sec}`;
        return `${minutes}:${sec}`;
    }

    render () {
        return <span>{this.convertTime(this.props.timeleft)}</span>
    }
}

export default CountDownTimer
