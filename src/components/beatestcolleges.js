import React, { Component } from 'react';

class CollegeDesc extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div className="beatest-colleges">
            <h2>Beatest now for Colleges!</h2>
            <h2 className='college-subheading'>#Campus Placements</h2>
				<p>Beatest strives to make your placements a piece of cake.
                    Join us to experience the difference. Take our aptitude exams
                to experience:</p>
				   <ul>
				   <li>Personalized Analysis</li>
				   <li>Relative Performance</li>
				   </ul>
            </div>
        );
    }
}

export default CollegeDesc;