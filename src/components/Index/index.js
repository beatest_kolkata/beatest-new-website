import React, { Component } from 'react';
import classnames from 'classnames';
import './style.css';
import IndexContainer from '../IndexContainer';

class Index extends Component {
    render() {
        const { className, ...props } = this.props;
        return (
            <IndexContainer className={classnames('Index', className)} {...props}/>
        );
    }
}

export default Index;
