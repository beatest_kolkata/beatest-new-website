import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import {getData} from '../../actions/UserAction';
import Menu from './Menu';
import { browserHistory } from 'react-router';
import $ from 'jquery';

class InternalHeader extends Component {
    constructor(props) {
        super(props);

        this.logout = this.logout.bind(this);
    }

    logout(event) {
        event.preventDefault();
        $.ajax({
            url: '/api/logout',
            dataType: 'json',
            type: 'POST',
            contentType: 'application/json'
        }).then((data) => {
            window.location.href = "/";
        });
    }

    render() {
        return (
            <div className="nav-container ">
                <div className="bar bar--sm visible-xs">
                    <div className="container">
                        <div className="row">
                            <div className="col-xs-3 col-sm-2">
                                <a href="/">
                                    <img className="logo logo-dark" alt="logo" src="img/logo-dark.png" />
                                    <img className="logo logo-light" alt="logo" src="img/logo-light.png" />
                                </a>
                            </div>
                            <div className="col-xs-9 col-sm-10 text-right">
                                <a href="#" className="hamburger-toggle" data-toggle-className="#menu1;hidden-xs">
                                    <i className="icon icon--sm stack-interface stack-menu"></i>
                                </a>
                            </div>
                        </div>

                    </div>

                </div>

                <nav id="menu1" className={"bar bar--sm bar-1 hidden-xs bar--absolute " + (this.props.transparent ? 'bar--transparent' : '')}>
                    <div className="container">
                        <div className="row">
                            <div className="col-md-1 col-sm-2 hidden-xs">
                                <div className="bar__module">
                                    <a href="/">
                                        <img className="logo logo-dark" alt="logo" src="img/logo-dark.png" />
                                        <img className="logo logo-light" alt="logo" src="img/logo-light.png" />
                                    </a>
                                </div>
                            </div>
                            <div className="col-md-11 col-sm-12 text-right text-left-xs text-left-sm">
                                <div className="bar__module">
                                    <Menu />
                                </div>
                                <div className="bar__module">
                                    {(() => {
                                        if (this.props.user) {
                                            return (
                                                <ul className="menu-horizontal text-left">
                                                    <li>
                                                        <a className="btn btn--sm btn--primary type--uppercase" href="/profile">
                                                            <span className="btn__text">Profile</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a className="btn btn--sm type--uppercase" onClick={this.logout}>
                                                            <span className="btn__text">Logout</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            )
                                        } else {
                                            return (
                                                <ul className="menu-horizontal text-left">
                                                    <li>
                                                        <a className="btn btn--sm type--uppercase" href="/login">
                                                            <span className="btn__text">LOGIN</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a className="btn btn--sm btn--primary type--uppercase" href="/signup">
                                                            <span className="btn__text">SIGNUP</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            )
                                        }
                                    })()}
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        );
    }
}

export default InternalHeader;
