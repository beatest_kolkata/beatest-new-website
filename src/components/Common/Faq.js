import React, { Component } from 'react';
export default class Faq extends Component{
    render() {
        return (
            <section>
            <div className="container">
                <div className="row">
                    <div className="col-md-4">
                        <div className="text-block">
                            <h4>Frequently Asked Questions</h4>
                            <p>Read more <a href="/faq">FAQs</a></p>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="text-block">
                            <h5>How do refunds work?</h5>
                            <p>
                                Drop us a mail at <a href="mailto:hello@beatest.in">hello@beatest.in</a> with your
                Transaction ID, Email and Amount Paid. Our customer care executives will resolve the issue
                ASAP.
            </p>
                        </div>
                        <div className="text-block">
                            <h5>Can I pay using AMEX?</h5>
                            <p>
                                Yes, we accept all major credit cards, including AMEX, so rack up those points!
            </p>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="text-block">
                            <h5>Is there a bulk-buy discount?</h5>
                            <p>
                                We have arrangments for college that our pricing team can assist with on a case-by-case
                basis.
                Drop us a mail at <a href="mailto:hello@beatest.in">hello@beatest.in</a>
                            </p>
                        </div>
                        <div className="text-block">
                            <h5>Do I have to login to Purchase Course?</h5>
                            <p>
                                Yes, you need to be logged in to be able to Purchase any Course/Exam!
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        );
    }
}



