import React, { Component } from 'react';
export default class InternalFooter extends Component{
    render() {
        return (
        <footer className="space--sm footer-1 text-center-xs imagebg" data-overlay="4">
            <div className="background-image-holder" style={{ background: 'url("/img/landing-22.jpg")', opacity: 1 }}>
                <img alt="background" src="/img/blog-6.jpg" />
            </div>
            <div className="container">
                <div className="row">
                    <div className="col-sm-6">
                        <ul className="list-inline list--hover">
                            <li>
                                <a href="/aboutus">
                                    <span>Our Team </span>
                                </a>
                            </li>
                            <li>
                                <a href="/faq">
                                    <span>FAQs</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://medium.com/beatest" target="_blank">
                                    <span>Blogs</span>
                                </a>
                            </li>

                        </ul>
                    </div>
                    <div className="col-sm-6 text-right text-center-xs">
                        <ul className="social-list list-inline list--hover">
                            <li>
                                <a href="https://plus.google.com/112722904416257444024" target="_blank">
                                    <i className="socicon socicon-google icon icon--xs"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://twitter.com/beatest_in" target="_blank">
                                    <i className="socicon socicon-twitter icon icon--xs"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.facebook.com/beatest.in/" target="_blank">
                                    <i className="socicon socicon-facebook icon icon--xs"></i>
                                </a>
                            </li>
                        </ul>
                        <a href="/#contact-us" className="btn type--uppercase inner-link">
                            <span className="btn__text">
                                Contact Us
                                </span>
                        </a>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-12">
                        <img alt="Image" className="logo" src="/img/logo-light.png" />
                        <span className="type--fine-print">©
                                <span className="update-year">2017</span>Beatest India Pvt Ltd.</span>
                        <a className="type--fine-print" href="/terms">Privacy Policy</a>
                        <a className="type--fine-print" href="/terms">Legal</a>
                    </div>
                </div>
            </div>
        </footer>
        );
    }
}