import React,{ Component } from 'react';

class Menu extends Component{
    render(){
        return(
            <ul className="menu-horizontal text-left">
                <li className="h5">
                    <a className="inner-link" href="/trainings" data-scroll>
                        Trainings &amp; Courses&nbsp;<span className="label label--inline" style={{background: '#4a90e2', borderColor: '#4a90e2'}}>Early Bird</span>
                    </a>
                </li>
                <li className="h5">
                    <a className="inner-link" href="/#features" data-scroll>Features</a>
                </li>
                {/*<li className="h5">
                    <a href="/pricing">Pricing</a>
                </li>*/}
                {/*<li className="h5">
                    <a className="inner-link" href="/#testimonials">Testimonial</a>
                </li>*/}
                <li className="h5">
                    <a href="https://medium.com/beatest" target="_blank">Blogs</a>
                </li>
            </ul>
        );
    }
} 


export default Menu;