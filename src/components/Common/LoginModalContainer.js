import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {login} from '../../actions/HomeAction';
import $ from 'jquery';

class LoginModalContaner extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: ""
        };
    }

    login(e) {
        e.preventDefault();
        this.props.login({
            email: this.state.email,
            password:this.state.password
        });
    }

    render() {
        return (
            <div className="modal-instance">
                <a className="btn btn--sm type--uppercase modal-trigger" href="#">
                    <span className="btn__text">
                        LOGIN
                    </span>
                </a>
                <div className="modal-container login-modal">
                    <div className="modal-content">
                        <section className="unpad ">
                            <div className="container">
                                <div className="row">
                                    <div className="col-sm-6 col-md-offset-3 col-sm-offset-3 col-xs-12">
                                        <div className="boxed boxed--lg bg--white text-center feature">
                                            <div className="modal-close modal-close-cross"></div>
                                            <h3>Login to Your Account</h3>
                                            {/*<a className="btn block btn--icon bg--facebook type--uppercase" href="#">
                                                <span className="btn__text">
                                                    <i className="socicon-facebook"></i>
                                                    Login with Facebook
                                                </span>
                                            </a>
                                            <a className="btn block btn--icon bg--googleplus type--uppercase" href="#">
                                                <span className="btn__text">
                                                    <i className="socicon-googleplus"></i>
                                                    Login with Google
                                                </span>
                                            </a>*/}
                                            <hr data-title="OR" />
                                            <div className="feature__body">
                                                <form onSubmit= {(event) => this.login(event)}>
                                                    <div className="row">
                                                        <div className="col-sm-12">
                                                            <input type="text" placeholder="Email" onChange={(event) => this.setState({ email: event.target.value }) } />
                                                        </div>
                                                        <div className="col-sm-12">
                                                            <input type="password" placeholder="Password" onChange={(event) => this.setState({ password:event.target.value }) } />
                                                        </div>
                                                        <div className="col-sm-12">
                                                            <button className="btn btn--primary type--uppercase" type="submit">Login</button>
                                                        </div>
                                                        {this.props.loginError ? (
                                                            <span className="note">{this.props.loginErrorMessage}</span>
                                                        ) : (null)}
                                                    </div>
                                                </form>
                                                <span className="type--fine-print block">
                                                    Dont have an account yet?
                                                    <a href="#">Create account</a>
                                                </span>
                                                <span className="type--fine-print block">
                                                    Forgot your username or password?
                                                    <a href="#">Recover account</a>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        user: state.user,
        loginError: state.loginError,
        loginErrorMessage: state.loginErrorMessage
    };
}

const matchDispatchToProps = dispatch => {
    return bindActionCreators({login: login},dispatch);
}

export default connect(mapStateToProps,matchDispatchToProps)(LoginModalContaner);