import React, { Component } from 'react';

export default class OurToppers extends Component{
    renderToppers(){
        return this.props.data.map((topper) => {
            return(
                <div className="col-sm-6 col-md-3" key={topper.key}>
                    <div className="testimonial testimonial-2">
                        <div className="testimonial__body boxed boxed--border bg--secondary">
                            <p>
                                {topper.text}
                            </p>
                        </div>
                        <div className="testimonial__image">
                            <img src={topper.profile_img} />
                            <h5>{topper.student_name}</h5>
                            <span>{topper.percentage}</span>
                        </div>
                    </div>
                </div>
            );
        });
    }
    render() {
        return (
            <section>
            <div className="container text-center">
                <div className="row">
                    <div className="col-sm-12">
                        <h2>Our Toppers</h2>
                    </div>
                </div>
            </div>
            <div className="container">
                <div className="row">
                    { this.renderToppers()}
                </div>
            </div>
        </section>
        );
    }
}