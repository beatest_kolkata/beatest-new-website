import React, {PropTypes, Component} from 'react';
import * as action from '../actions/action';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import $ from 'jquery';

const initialState = {
    user: null,
    loginPanelVisibility: 'none',
    registrationPanelVisibility: 'none',
    userMenuVisibility: 'none',
    isUserLoading: true
}

class InstructionsOnlyContainer extends Component {
	constructor(props) {
        super(props);
        this.state = initialState;
    }

    render() {
        return (
            <div className="instructions">
                <div className="instructions-content">
                    <span>Please read the following instructions carefully</span>
                    <div>
                        <p className="instructions-topic"><u>General Instructions</u> :</p>
                        <ol>
                            {(() => {
                                return <li>Total duration of the test is 180 minutes. You
                                    will be provided 60 minutes per section and will not be able to move
                                    from one section to another within that 60 minute window.</li>
                            })()}
                            <li>A countdown timer at the top right corner of your screen will display the
                                time remaining for you to complete the test. When the clock runs out, the
                                test will get submitted by default.</li>
                            <li>
                                 This paper contains 100 questions divided into 3 sections.
                                 {(() => {
                                     return <ul>
                                         <li>Section 1 - VRC</li>
                                         <li>Section 2 - LRDI</li>
                                         <li>Section 3 - QA</li>
                                     </ul>
                                 })()}
                            </li>
                            {(() => {
                                return <li>There are two kinds of questions in the Exam, MCQ and non-MCQ. For
                                    non-MCQ type questions you will need to type the answer using the virtual
                                    number-board provided.</li>
                            })()}
                            <li>There is only one correct answer to each question.</li>
                            {(() => {
                                return <li>You will be awarded three marks for each correct answer.</li>
                            })()}
                            {(() => {
                                return <li>For the MCQ type questions each wrong answer will attract a penalty of
                                    one mark. There will be no negative marking for the non-MCQ type question</li>
                            })()}
                            <li>The question palette at the right of the screen shows one of the
                                following statuses of each of the questions numbered:
                                <ul>
                                    <li className="index-panel-instructions">
                                        <span className="grey circle"></span>
                                        You have not visited the question yet.
                                    </li>
                                    <li className="index-panel-instructions">
                                        <span className="red circle"></span>
                                        You have not answered the question.
                                    </li>
                                    <li className="index-panel-instructions">
                                        <span className="green circle"></span>
                                        You have answered the question.
                                    </li>
                                    <li className="index-panel-instructions">
                                        <span className="purple circle"></span>
                                        You have marked the question for review.
                                    </li>
                                </ul>
                            </li>
                            <span className="red note">If an answer is selected for a question that is Marked for Review, the
                                answer will be considered in the final evaluation.</span>
                        </ol>
                        <p className="instructions-topic"><u>Navigating to a Question</u> :</p>
                        <ol>
                            <li>To select a question to answer, you can do one of the following:
                                <ol type="a">
                                    <li>Click on the question number on the question palette at the right
                                        of your screen to go to that numbered question directly. Note that
                                        using this option does NOT save your answer to the current question.</li>
                                    <li>Click on Save and Next to save answer to current question and
                                        to go to the next question in sequence.</li>
                                    <li>Click on Mark for Review and Next to save answer to current question, mark
                                        it for review, and to go to the next question in sequence.</li>
                                </ol>
                            </li>
                            <li>You can view the entire paper by clicking on the Question Paper button.</li>
                        </ol>
                        <p className="instructions-topic"><u>Answering questions</u> :</p>
                        <ol>
                            <li>For multiple choice type questions :
                                <ol type="a">
                                    <li>To select your answer, click on one of the option buttons</li>
                                    <li>To change your answer, click the another desired option button</li>
                                    <li>To save your answer, you MUST click on Save & Next</li>
                                    <li>To deselect a chosen answer, click on the chosen option again or
                                        click on the Clear Response button.</li>
                                    <li>To mark a question for review click on Mark for Review & Next.</li>
                                </ol>
                            </li>
                            <span className="red note">If an answer is selected for a question that is Marked for Review, the
                                answer will be considered in the final evaluation.</span>
                            <br />
                            <br />
                            {(() => {
                                return <li>For the non-MCQ type questions :
                                        <ol type="a">
                                            <li>Use the number-board present on the screen to type your answer</li>
                                            <li>Ensure that the answer typed is correct since even a decimal
                                                point might change your answer</li>
                                            <li>Do not provide any space between numbers or alphabets. For example
                                                if the answer to a question is 153, then do not type 1 5 3.</li>
                                            <li>If you encounter a non-MCQ typeParajumble and its correct order is:
                                                4th statement followed by the 3rd statement followed by 2nd statement
                                                and then the 1st statement your answer should be 4321(using the number board)</li>
                                            <li>To mark a question for review click on Mark for Review & Next.</li>
                                        </ol>
                                        <span className="red note">If an answer is selected for a question that is Marked for Review, the
                                            answer will be considered in the final evaluation.</span>
                                </li>
                            })()}
                            <br />
                            <li>To change an answer to a question, first select the question and then click on
                                the new answer option followed by a click on the Save & Next button.</li>
                            <li>Questions that are saved or marked for review after answering will
                                ONLY be considered for evaluation.</li>
                        </ol>
                        <p className="instructions-topic"><u>Navigating through sections</u> :</p>
                        <ol>
                            <li>At any point in time you will only be able to see the questions of the
                                section that you are currently working on.</li>
                            {(() => {
                                return <li>You will not be able to freely navigate between sections.</li>
                            })()}
                            {(() => {
                                return <li>You can move the mouse cursor over the section names
                                    to view the status of the questions for that section.</li>
                            })()}
                        </ol>
                    </div>
                    <div className="user">
                        <div className="user-image"></div>
                        <div>
                            {(() => {
                                return <span className="red">Total Time: 180 mins</span>
                            })()}
                            <br />
                            {(() => {
                                return <span className="red">Active Sectional Time Left: 180mins</span>
                            })()}
                            <p className="note">Candidate</p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

InstructionsOnlyContainer.propTypes = {
    testId: PropTypes.number,
    testName: PropTypes.string,
    testType: PropTypes.string
}

function mapStateToProps(state, props) {
    return {
        testId: state.testId,
        testName: state.testName,
        testType: state.testType
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(action, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(InstructionsOnlyContainer);
