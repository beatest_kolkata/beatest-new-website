import React, { Component } from 'react';
import $ from 'jquery';

var mixpanel = require('mixpanel-browser');
mixpanel.init("86e70fa37444d5ab81c1ea058ae2fcd4");

const initialState = {
    collegeName: 'none',
    data: 'none',
    rank: '0',
    percentile: '00.00'
}

class CollegeRankPercentile extends Component {
    constructor(props) {
        super(props);
        this.state = initialState;
//        console.log(this.props.user.id);
//        console.log(this.props.user.college_id);
        this.getRP = this.getRP.bind(this);
        this.getName = this.getName.bind(this);
        this.getRP();
        this.getName();
    }

    getName(e){
        var id = this.props.user.college_id;
        $.ajax({
            url: '/api/get_collegeName',
            dataType: 'json',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                id
            })
        }).then(function(data) {
            this.setState({"collegeName": data.name});
        }.bind(this));
    }

    getRP(){
        $.get('/api/get_college_rank_percentile/'+this.props.user.id+'/'+this.props.user.college_id, (data) => {
            if(typeof data === 'object') {
                if(data.hasOwnProperty('invalid')){
                    this.setState({"data":data.invalid});
                }
                else{
                    this.setState({"rank":data.rank});
                    this.setState({"percentile":data.percentile.toFixed(2)});
                }
            }
        });
    }

    componentDidMount(){
    }

    render() {
        return(
            <div>
            {(() => {
                if(this.state.data == 'not attempted any test')
                {
                    return(
                        <div className="college-panel">
                            <div className = "xtra"> You have not attempted any tests yet. </div>
                            <div className = "collegeName"> {this.state.collegeName} </div>
                        </div>
                    );
                }
                else
                {
                    return(
                        <div className="college-panel">
                            <div id="rankPercentile">
                                <div className="rank">
                                    <div className='row2'>{this.state.rank}</div>
                                    <br/>
                                    <div className='reader'> Rank </div>
                                </div>
                                <div className="percentile">
                                    <div className='row2'>{this.state.percentile}</div>
                                    <br/>
                                    <div className='reader'> Percentile </div>
                                </div>
                            </div>
                            <div className = "collegeName"> {this.state.collegeName} </div>
                        </div>
                    );
                }
        })()}
        </div>
        );
    }
}

export default CollegeRankPercentile;