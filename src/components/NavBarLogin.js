import React, {Component} from 'react';
import $ from 'jquery';
import MetaTags from 'react-meta-tags';

class NavBarTransparent extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
    }

    getUser() {
        $.get('/api/user', (data) => {
            if(typeof data === 'object') {
                this.props.login(data.user);
            }
        });
    }

    render() {
        return(
            <div className="nav-container ">
                <div className="bar bar--sm visible-xs">
                    <div className="container">
                        <div className="row">
                            <div className="col-xs-3 col-sm-2">
                                <a href="/">
                                    <img className="logo logo-dark" alt="logo" src="/img/logo-dark.png" />
                                    <img className="logo logo-light" alt="logo" src="/img/logo-light.png" />
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <nav id="menu1" className="bar bar--sm bar-1 hidden-xs bar--transparent bar--absolute">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-1 col-sm-2 hidden-xs">
                                <div className="bar__module">
                                    <a href="/">
                                        <img className="logo logo-dark" alt="logo" src="/img/logo-dark.png" />
                                        <img className="logo logo-light" alt="logo" src="/img/logo-light.png" />
                                    </a>
                                </div>
                            </div>
                            <div className="col-md-11 col-sm-12 text-right text-left-xs text-left-sm">
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        );
    }
}

export default NavBarTransparent;