import React, { Component } from 'react';
import classnames from 'classnames';
/*import './style.css';*/

export default class NotFound extends Component {
    render() {
        const { className, ...props } = this.props;
        return (
            <div className="main-container">
                <section className="height-100 bg--dark text-center">
                    <div className="container pos-vertical-center">
                        <div className="row">
                            <div className="col-sm-12">
                                <h1 className="h1--large">404</h1>
                                <p className="lead">
                                    The page you were looking for was not found.
                                </p>
                                <a href="/">Go back to home page</a>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}
