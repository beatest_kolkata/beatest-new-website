import React, { Component } from 'react';
import $ from 'jquery';
import FacebookLogin from 'react-facebook-login';
import GoogleLogin from 'react-google-login';

var mixpanel = require('mixpanel-browser');
mixpanel.init("86e70fa37444d5ab81c1ea058ae2fcd4");

class UserPanel extends Component {
    constructor(props) {
        super(props);
        this.login = this.login.bind(this);
        this.signup = this.signup.bind(this);
        this.user_edit = this.user_edit.bind(this);
        this.showRegistrationPanel = this.showRegistrationPanel.bind(this);
        this.responseFacebook = this.responseFacebook.bind(this);
        this.responseGoogle = this.responseGoogle.bind(this);
    }
    user_edit(e) {
        e.preventDefault();
        var phone_no = $('#phone_no').val();
        var college_id = $('#college_id').val();
        $('#user-edit-form')[0].reset();
        $.ajax({
            url: '/api/user/edit/$()',
            dataType: 'json',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                phone_no,
                college_id
            })
        }).then(function(data) {
            if(data.hasOwnProperty('invalid')) {
                if(data.invalid === 'Ph no and college Id') {
                    $('#user-panel-validator').html("Invalid Phone No. and College Id");
                } else if (data.invalid === 'User') {
                    this.props.toggleLoginPanelVisibility();
                    this.props.toggleRegistrationPanelVisibility();
                    $('#phone_no').val(data.phone_no);
                    $('#college_id').val(data.college_id);
                }
            } else if (data.hasOwnProperty('user')) {
                this.props.login(data.user);
                this.props.toggleLoginPanelVisibility();
                location.reload();
            }
        }.bind(this));
    }
    login(e) {
        e.preventDefault();
	    var email = $('#email').val();
	    mixpanel.identify(email);
	    mixpanel.track("User Logged in");
        var password = $('#password').val();
        $('#login-form')[0].reset();
        $.ajax({
            url: '/api/login',
            dataType: 'json',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                email,
                password
            })
        }).then(function(data) {
            if(data.hasOwnProperty('invalid')) {
                if(data.invalid === 'Email') {
                    $('#user-panel-validator').html("Sorry, Beatest doesn't recognize that email.");
                } else if(data.invalid === 'No other details') {
                    this.props.user_edit();
                }
                  else if (data.invalid === 'Password') {
                    $('#user-panel-validator').html("Wrong password. Try again.");
                } else if (data.invalid === 'User') {
                    this.props.toggleLoginPanelVisibility();
                    this.props.toggleRegistrationPanelVisibility();
                    $('#registration-panel-validator').html("Please add a password for your account.");
                    $('#new-full-name').val(data.name);
                    $('#new-email').val(data.email);
                }

            } else if (data.hasOwnProperty('user')) {
                this.props.login(data.user);
                this.props.toggleLoginPanelVisibility();
                location.reload();
            }
        }.bind(this));
    }

    signup(e) {
        e.preventDefault();
        var full_name = $('#new-full-name').val();
        var email = $('#new-email').val();
        var phone_no = $('#new-phone-no').val();
        var password = $('#new-password').val();
        $('#register-form')[0].reset();
        if(true) {
            $.ajax({
                url: '/api/collegesignup',
                dataType: 'json',
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify({
                    full_name,
                    email,
                    phone_no,
                    password
                })
            }).then(function(data) {
                this.props.login(data.user);
                this.props.toggleRegistrationPanelVisibility();
            }.bind(this));
        } else {
            $('#registration-panel-validator').html("These passwords don't match. Try again.");
        }
	 var today = new Date();
	mixpanel.people.set({
    	    "$name": full_name,
    	    "$email": email,
	    "$created": String(today)
	});
	mixpanel.identify(email);
	mixpanel.track("User Signed in");
    }

    showRegistrationPanel() {
        this.props.toggleLoginPanelVisibility();
        this.props.toggleRegistrationPanelVisibility();
    }

    responseFacebook (response) {
        $.ajax({
            url: '/api/facebook_login',
            dataType: 'json',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                name: response.name,
                id: response.id,
                accessToken: response.accessToken,
                email: response.email,
                url: response.picture.data.url
            })
        }).then((data) => {
            this.props.login(data.user);
            this.props.toggleLoginPanelVisibility();
            // location.reload();
        });
    }

    responseGoogle = (response) => {
        $.ajax({
            url: '/api/google_login',
            dataType: 'json',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                accessToken: response.accessToken,
                googleId: response.googleId,
                tokenId: response.tokenId,
                email: response.profileObj.email,
                name: response.profileObj.name,
                imageUrl: response.profileObj.imageUrl
            })
        }).then((data) => {
            this.props.login(data.user);
            this.props.toggleLoginPanelVisibility();
            location.reload();
        });
    }

    render() {
        var userPanelClassName = 'user-panel ' + this.props.loginPanelVisibility;
        var registrationPanelClassName = 'user-panel ' + this.props.registrationPanelVisibility;
        var userEditPanelClassName = 'user-panel-edit';
        return (
            <div>
            <div className={userPanelClassName}>
                <div className="black-background" onClick={this.props.toggleLoginPanelVisibility}></div>
                <div className="panel">
                    <div className="close-panel" onClick={this.props.toggleLoginPanelVisibility}>X</div>
                    <form id="login-form" onSubmit={this.login}>
                        <input id="email" className="user-panel-input" type="email" placeholder="Email" required />
                        <input id="password" className="user-panel-input" type="password" placeholder="Password" required />
                        <div><label id="user-panel-validator"></label></div>
                        <button className="user-panel-button">Login</button>
                    </form>
                    <p className="login-panel-text">forgot password?
                        <a> click here</a>
                    </p>
                    <p className="login-panel-text">not yet registered?
                        <a onClick={this.showRegistrationPanel}> Sign Up</a>
                    </p>
                    <div className="social-media-login">
                        <FacebookLogin appId="1237812479598094" cssClass="facebook-login-button"
                            icon="fa-facebook" fields="name,email,picture" textButton="Facebook" callback={this.responseFacebook} />
                        <div className="space"></div>
                        <GoogleLogin clientId="709876791157-nh5o4s2rgn89b83hhpnrj56k9a9gupon.apps.googleusercontent.com"
                            icon="fa-google-plus" buttonText="Google" onSuccess={this.responseGoogle} onFailure={this.responseGoogle} className="google-login-button"/>
                    </div>
                </div>
            </div>
            <div className={registrationPanelClassName}>
                <div className="black-background" onClick={this.props.toggleRegistrationPanelVisibility}></div>
                <div className="register panel">
                    <div className="close-panel" onClick={this.props.toggleRegistrationPanelVisibility}>X</div>
                    <form id="register-form" onSubmit={this.signup}>
                        <input id="new-full-name" className="user-panel-input" type="text" placeholder="Name" required />
                        <input id="new-email" className="user-panel-input" type="email" placeholder="Email" required />
                        <input id="new-phone-no" className="user-panel-input" type="text" placeholder="Phone No." required />
                        <input id="new-password" className="user-panel-input" type="password" placeholder="Password" required />
                        <div><label id="registration-panel-validator"></label></div>
                        <button className="user-panel-button">Sign Up</button>
                    </form>
                </div>
            </div>
            <div className={userEditPanelClassName}>
                <div className="black-background" onClick=""></div>
                <div className="register panel">
                    <div className="close-panel" onClick={this.props.toggleRegistrationPanelVisibility}>X</div>
                    <form id="user-edit-form" onSubmit={this.user_edit}>
                        <input id="phone_no" className="user-panel-input" type="text" placeholder="Phone No." required />
                        <input id="college_id" className="user-panel-input" type="text" placeholder="College" required />
                        <div><label id="registration-panel-validator"></label></div>
                        <button className="user-panel-button">Add</button>
                    </form>
                </div>
            </div>
            </div>
        );
    }
}

export default UserPanel;
