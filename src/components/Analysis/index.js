import React, { Component } from 'react';
//import logo from './logo.svg';
import classnames from 'classnames';
/*import './style.css';*/
import PerfAnalysis from '../PerfAnalysis';

class Analysis extends Component {
    render() {
        const { className, ...props } = this.props;
        return (
            <PerfAnalysis className={classnames('Analysis', className)} {...props}/>
        );
    }
}

export default Analysis;