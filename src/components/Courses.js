import React, { Component } from 'react';

var mixpanel = require('mixpanel-browser');
mixpanel.init("86e70fa37444d5ab81c1ea058ae2fcd4");

class Courses extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.showIBPSTests = this.showIBPSTests.bind(this);
        this.showCATTests = this.showCATTests.bind(this);
	      this.showSBITests = this.showSBITests.bind(this);
	      this.showIBPSPOTests = this.showIBPSPOTests.bind(this);
    }

    showIBPSTests() {
        this.props.showIBPSTests();
        window.location.href = "/tests/IBPS";
    }
	showSBITests() {
        this.props.showSBITests();
        window.location.href = "/tests/SBI";
    }

    showCATTests() {
	mixpanel.track("Viewed CAT Tests");
        this.props.showCATTests();
        window.location.href = "/tests/CAT";
    }
    showSBIPOTests() {
	mixpanel.track("Viewed SBI-PO Tests");
        window.location.href = "/info";
    }
    showIBPSPOTests() {
	mixpanel.track("Viewed IBPS-PO Tests");
        window.location.href = "/infoIbpsPO";
    }

    render() {
        return (
          <div className="courses">
            <div className="courses-heading">
              <h1>Courses</h1>
            </div>
            <div className="courses-type">
              <div className="course-box">
                <h1>BANKING</h1>
                <div className="course-name-box">
                  <ul id="course-list">
                    <li id="course-list-item">
                      <a onClick={this.showSBIPOTests}>
                        <div className="course-name-sbipo">
                        </div>
                      </a>
                    </li>
                    <li id="course-list-item">
                      <a onClick={this.showIBPSPOTests}>
                        <div className="course-name-ibpspo">
                        </div>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="course-box">
                <h1>MBA</h1>
                <div className="course-name-box">
                  <ul id="course-list">
                    <li id="course-list-item">
                      <a onClick={this.showCATTests}>
                        <div className="course-name-cat">
                        </div>
                      </a>
                    </li>
                    <li id="course-list-item">
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        );
    }
}

export default Courses;
