import React, {PropTypes, Component } from 'react';
import $ from 'jquery';
import * as action from '../actions/action';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Navlink from './Navlink';
import UserPanel from './UserPanel';
import UserMenu from './UserMenu';
import QuestionButton from './QuestionButton';
import NotFound from './NotFound';
import ViewSolution from './ViewSolution';

const initialState = {
    loginPanelVisibility: 'none',
    currentView: 'PERFORMANCE',
    questionsList: [],
    user: null,
    sectionIndex: 0,
    questionChoices: [],
    isUserLoading: true
}

class PerfAnalysis extends Component {
    constructor(props) {
        super(props);
        this.state = initialState;
        this.getUser = this.getUser.bind(this);
        this.getTest = this.getTest.bind(this);
        this.getNextQuestion = this.getNextQuestion.bind(this);
        this.getPrevQuestion = this.getPrevQuestion.bind(this);
        this.getQuestionsList = this.getQuestionsList.bind(this);
        this.getCurrentQuestion = this.getCurrentQuestion.bind(this);
        this.getNextSection = this.getNextSection.bind(this);
        this.getPrevSection = this.getPrevSection.bind(this);
        this.showPerformance = this.showPerformance.bind(this);
        this.getUser();
        this.getTest();
    }

    getUser() {
        $.get('/user', (data) => {
            if(typeof data === 'object') {
                this.props.actions.login(data.user);
            }
        }).then(() => {
            this.props.actions.loadingUserSuccess();
        });
    }

    getTest() {
//        console.log(this.props.routeParams.testId);
        $.get('/tests/' + this.props.routeParams.testId + '/solution', (data) => {
        }).then((data) => {
            this.props.actions.setSolutionData(data);
        });
    }

    showPerformance(i) {
        this.props.actions.gotoNthQuestion(i);
    }

    getNextQuestion() {
        this.props.actions.gotoNextQuestion();
    }

    getPrevQuestion() {
        this.props.actions.gotoPrevQuestion();
    }

    getQuestionsList() {
        return this.props.sectionAttempts[this.props.sectionIndex].questions;
    }

    getCurrentQuestion() {
        return this.getQuestionsList()[this.props.currentQuestionIndex];
    }

    getChoicesList() {
        return this.getCurrentQuestion().choices;
    }

    getNextSection() {
        this.props.actions.gotoNextSection();
    }

    getPrevSection() {
        this.props.actions.gotoPrevSection();
    }
    

    render() {
        var sil = [];
        if(this.props.sectionAttempts && this.props.sectionAttempts.length>0){
                                var y = 0;
                                var z = [];
                                z.push(0);
                                if(this.props.sectionAttempts.length>1){
                                    z.push(1);
                                    z.push(2);
                                }
                                {z.map((sec, ind) => {

                                    {this.props.sectionAttempts[sec].questions.map((question, index) => {
                                        y = y+1;
                                        sil.push(<tr key={y}> <td> {y} </td> <td>
                                        { question.type == 'RC' ?
                                                    <div className="rc-display">
                                                        <b>Passage:</b>
                                                        <div className="modify" dangerouslySetInnerHTML={{__html: question.rc_passage}}>
                                                        </div>
                                        </div> : "" }
                                        Q: <span dangerouslySetInnerHTML={{__html: question.html}}></span>
                                        {(() => {
                                                     var currentQuestion = question;
                                                    if(currentQuestion.type === 'TITA') {
                                                        if(currentQuestion.tita_choice !== undefined) {
                                                            return <div>
                                                                <p className="tita-choice">Your Answer : {currentQuestion.tita_choice}</p>
                                                                <input id="tita-input" value={currentQuestion.tita_answer} readOnly/>
                                                            </div>
                                                        } else {
                                                            return <input id="tita-input" value={currentQuestion.tita_answer} readOnly/>
                                                        }
                                                    } else {
                                                        return <ul className="choices">
                                                            {question.choices.map((choice, index) => {
                                                                return <li className="choice" key={index}>
                                                                            <input className="choice-radio" type="radio" name="choice" key={choice.id} id={choice.id} checked={choice.attempted} readOnly/>
                                                                            <span dangerouslySetInnerHTML={{__html: choice.html}}></span>
                                                                            {(() => {
                                                                                if(choice.is_correct) {
                                                                                    return <i className="correct-option fa fa-check fa-2x" />
                                                                                } else if(choice.is_correct ^ choice.attempted) {
                                                                                    return <i className="wrong-option fa fa-close fa-2x" />
                                                                                }
                                                                            })()}
                                                                        </li>
                                                            })}
                                                        </ul>
                                                    }
                                        })()}
                                        </td> <td> Probability </td> <td> Easy </td> <td> <span dangerouslySetInnerHTML={{__html: question.time}}></span> </td> <td> <ViewSolution logic={question.logic} question={question}/> </td> </tr>);
                                    })}
                                })}
            }

        return (
            <div className="features">
                <Navlink loadingTestsBegin={this.props.actions.loadingTestsBegin}
                            loadingTestsSuccess={this.props.actions.loadingTestsSuccess}
                            onPage="tests" user={this.props.user} login={this.props.actions.login}
                            toggleLoginPanelVisibility={this.props.actions.toggleLoginPanelVisibility}
                            toggleUserMenuVisibility={this.props.actions.toggleUserMenuVisibility} />
                <UserMenu userMenuVisibility={this.props.userMenuVisibility}
                            toggleUserMenuVisibility={this.props.actions.toggleUserMenuVisibility}
                            logout={this.props.actions.logout} />
                {(() => {
                            if(this.props.loginPanelVisibility === 'block' || this.props.registrationPanelVisibility === 'block') {
                                return <UserPanel loginPanelVisibility={this.props.loginPanelVisibility}
                                        toggleLoginPanelVisibility={this.props.actions.toggleLoginPanelVisibility}
                                        login={this.props.actions.login}
                                        registrationPanelVisibility={this.props.registrationPanelVisibility}
                                        toggleRegistrationPanelVisibility={this.props.actions.toggleRegistrationPanelVisibility} />
                            }
                })()}

                <div className="features-heading"><h1 id="detail">Analysis   Report</h1></div>
                <br />
                <a href="#detail"><img src="https://cdn4.iconfinder.com/data/icons/ionicons/512/icon-arrow-up-b-128.png" id="fixedbutton" /></a>
                <div className="sbi">
                <h2>Performance Analysis</h2>
                <p>
                <br/><br/><br/>
                <table>
                <col class="dynamic"/>
                <col class="narrow"/>
                <col class="dynamic"/>
                <col class="dynamic"/>
                <col class="dynamic"/>
                <col class="dynamic"/>
                <thead><tr><th> Ques. No. </th><th> Question </th> <th> Topic </th><th> Level of Difficulty </th> <th> Time Taken </th> <th> Solution </th></tr></thead>
                <tbody>
                {sil}
                </tbody>
                </table>
                <br/> </p>
                <br/>
				</div>
            </div>
        );
    }
}

PerfAnalysis.propTypes = {
    currentView: PropTypes.string,
    user: PropTypes.object,
    sectionIndex: PropTypes.number,
    isTestsLoading: PropTypes.bool,
    sectionAttempts: PropTypes.array,
    currentQuestionId: PropTypes.number,
    currentQuestionIndex: PropTypes.number,
    testAttemptId: PropTypes.number,
    loginPanelVisibility: PropTypes.string,
    registrationPanelVisibility: PropTypes.string,
    userMenuVisibility: PropTypes.string,
    testName: PropTypes.string,
    loadingQuestionsBegin: PropTypes.func,
    loadingQuestionsSuccess: PropTypes.func,
    gotoNextSection: PropTypes.func,
    choiceSelected: PropTypes.func,
    isUserLoading: PropTypes.bool
}

function mapStateToProps(state, props) {
    return {
        currentView: state.currentView,
        user: state.user,
        sectionIndex: state.sectionIndex,
        isTestsLoading: state.isTestsLoading,
        sectionAttempts: state.sectionAttempts,
        currentQuestionId: state.currentQuestionId,
        currentQuestionIndex: state.currentQuestionIndex,
        loginPanelVisibility: state.loginPanelVisibility,
        registrationPanelVisibility: state.registrationPanelVisibility,
        userMenuVisibility: state.userMenuVisibility,
        testName: state.testName,
        loadingQuestionsBegin: state.loadingQuestionsBegin,
        loadingQuestionsSuccess: state.loadingQuestionsSuccess,
        gotoNextSection: state.gotoNextSection,
        choiceSelected: state.choiceSelected,
        isUserLoading: state.isUserLoading
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(action, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PerfAnalysis);
