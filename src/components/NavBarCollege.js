import React, {Component} from 'react';
import $ from 'jquery';
import MetaTags from 'react-meta-tags';

class NavBarCollege extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showMobileNav: false,
            showNav: false
        };
        this.toggleUserMenu = this.toggleUserMenu.bind(this);
        this.toggleUserPanel = this.toggleUserPanel.bind(this);
        this.getUser = this.getUser.bind(this);
        this.getUser();
        $(window).resize(function() {
            if($(window).width() < 725) {
                this.setState({
                    showMobileNav: true
                });
            } else {
                this.setState({
                    showMobileNav: false
                });
            }
        }.bind(this));
    }

    componentDidMount() {
        if($(window).width() < 725) {
            this.setState({
                showMobileNav: true
            });
        }
    }

    getUser() {
        $.get('/api/user', (data) => {
            if(typeof data === 'object') {
                this.props.login(data.user);
            }
        });
    }

    toggleUserMenu() {
        this.props.toggleUserMenuVisibility();
    }

    toggleUserPanel() {
        this.props.toggleLoginPanelVisibility();
        if(this.state.showNav) {
            this.setState({
                showNav: false
            });
        }
    }

    render() {
        return(
            <nav className="navbar">
                <div className="container-fluid">
                <img src="../../logo1.png" alt="Beatest" id="beatest-logo"/>
                    <div className="navbar-header">
                      <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span className="sr-only">Toggle navigation</span>
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                      </button>
                    </div>
                    <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul className="nav navbar-nav">
                            {(() => {
                                if(this.props.user === null) {
                                    return <li><a href="#" onClick={this.toggleUserPanel}>Login</a></li>
                                } else if(this.props.user.profile_picture === null) {
                                    return <div className="nav-button" onClick={this.toggleUserMenu}>{this.props.user.name}</div>
                                } else {
                                    return <li className="profile-pic-nav-button"><a href="#" onClick={this.toggleUserMenu}><img height="40" src={this.props.user.profile_picture} /></a></li>
                                }
                            })()}
                            {(() => {
                                if(this.props.onPage !== 'index') {
                                    return <a href="/index"><li>HOME</li></a>
                                }
                            })()}
                            {(() => {
                                                if(this.props.onPage === 'user_profile')
                                                {
                                                return <a href="/user_profile" className='active'>Tests</a>
                                                }
                                                else
                                                {return <a href="/user_profile">Tests</a>}
                            })()}
                            {(() => {
                                if(this.props.onPage !== 'index') {
                                    return <a href="/pricing"><li>Pricing</li></a>
                                }
                            })()}
                        </ul>
                    </div>
                </div>
            </nav>
        );
    }
}

export default NavBarCollege;
