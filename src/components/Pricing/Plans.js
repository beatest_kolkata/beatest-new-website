import React, { Component } from 'react';
import PayButton from "../PayButton";
import $ from 'jquery';

export class Plans extends Component{

    constructor(props) {
        super(props);
        this.state = {
            promoCode: '',
            promoValue: 0,
            promoApplied: false
        };

        this.applyPromoCode = this.applyPromoCode.bind(this);
        this.onPromoCodeChange = this.onPromoCodeChange.bind(this);
        this.resetPromoCode = this.resetPromoCode.bind(this);
    }

    renderExamsList(){
        return this.props.data.map((exam) => {
            return(
                <li key={exam.name.toString()}>
                    <div className="tab__title">
                        <span className="h5">{exam.name}</span>
                    </div>
                </li>
            );
        });
    }

    renderExamDetails() {
        return this.props.data.map((examDetails, index) => {
            return (
                <li key={index}>
                    <div className="tab__content">
                        <div className="row">
                            <div className="col-sm-6">
                                <div className="pricing pricing-1 boxed boxed--lg boxed--border">
                                    <h3>Starter</h3>
                                    <span className="h2">
                                            <span className="pricing__dollar">&#x20b9;</span><strong>{examDetails.starterPrice}</strong>
                                        </span>
                                    <span className="type--fine-print">One Time, INR.</span>
                                    <span className="label">Value</span>
                                    <hr />
                                    <ul>
                                        <li>
                                            <span className="checkmark bg--primary-1"/>
                                            <span>20+ Topic Tests</span>
                                        </li>
                                        <li>
                                            <span className="checkmark bg--primary-1"/>
                                            <span>Competitive Questions</span>
                                        </li>
                                        <li>
                                            <span className="checkmark bg--primary-1"/>
                                            <span>Score Analysis</span>
                                        </li>
                                        <li>
                                            <span className="checkmark bg--primary-1"/>
                                            <span>Feature One</span>
                                        </li>
                                    </ul>
                                    <PayButton user={this.props.user} userLogged={false} tests={[105,106]} purchased={false} amount={examDetails.starterPrice} buttonText="Coming Soon!" resetPromoCode={this.resetPromoCode} promoCode={this.state.promoCode} promoDiscount={this.state.promoValue} />
                                </div>
                            </div>
                            <div className="col-sm-6">
                                <div className="pricing pricing-1 boxed boxed--lg boxed--border">
                                    <h3>Champion</h3>
                                    <span className="h2">
                                            <span className="pricing__dollar">&#x20b9;</span><strong>{examDetails.championPrice}</strong>
                                        </span>
                                    <span className="type--fine-print">One Time, INR.</span>
                                    <hr />
                                    <ul>
                                        <li>
                                            <span className="checkmark bg--primary-1"/>
                                            <span><em>Starter</em> Features</span>
                                        </li>
                                        <li>
                                            <span className="checkmark bg--primary-1"/>
                                            <span>10+ Mock Tests</span>
                                        </li>
                                        <li>
                                            <span className="checkmark bg--primary-1"/>
                                            <span>Performance Analysis</span>
                                        </li>
                                        <li>
                                            <span className="checkmark bg--primary-1"/>
                                            <span>24/7 Phone Support</span>
                                        </li>
                                    </ul>
                                    <PayButton user={this.props.user} userLogged={false} amount={examDetails.championPrice} buttonText="Coming Soon!" resetPromoCode={this.resetPromoCode} promoCode={this.state.promoCode} promoDiscount={this.state.promoValue} />
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            );
        });
    }

    onPromoCodeChange(event) {
        this.setState({
            promoCode: event.target.value
        });
    }

    applyPromoCode(event) {
        event.preventDefault();
        if (typeof this.props.user === 'object') {
            let promo_code = this.state.promoCode;
            let user_id = this.props.user.id;

            $.get('/api/validate_code/' + promo_code + '/' + user_id, (data) => {
                if (typeof data === 'object') {
                    this.setState({
                        promoValue: data.value,
                        promoApplied: data.valid
                    })
                }
            });
        }
    }

    resetPromoCode() {
        this.setState({
            promoCode: '',
            promoValue: 0,
            promoApplied: false
        });
    }

    render(){
        return(
        <section className=" bg--secondary">
        <div className="container">
            <p className="lead text-center">Click the tabs on right to view Courses & their Purchase options!</p>
            <div className="tabs-container tabs--vertical">
                <ul className="tabs">
                    <li key="Aptitude Training">
                        <div className="tab__title">
                            <span className="h5">Aptitude Training</span>
                        </div>
                    </li>
                    <li key="Data Science">
                        <div className="tab__title">
                            <span className="h5">Data Science</span>
                        </div>
                    </li>
                    <li key="Big Data">
                        <div className="tab__title">
                            <span className="h5">Big Data</span>
                        </div>
                    </li>
                    <li key="Web Development I">
                        <div className="tab__title">
                            <span className="h5">Web Development I</span>
                        </div>
                    </li>
                    <li key="Web Development II">
                        <div className="tab__title">
                            <span className="h5">Web Development II</span>
                        </div>
                    </li>
                    <li key="CAT">
                        <div className="tab__title">
                            <span className="h5">CAT</span>
                        </div>
                    </li>
                    <li key="CAT Modules I">
                        <div className="tab__title">
                            <span className="h5">CAT Modules I</span>
                        </div>
                    </li>
                    <li key="CAT Modules II">
                        <div className="tab__title">
                            <span className="h5">CAT Modules II</span>
                        </div>
                    </li>
                </ul>
                <ul className="tabs-content">
                    <li>
                        <div className="tab__content">
                            <div className="row">
                                <div className="col-sm-6">
                                    <div className="pricing pricing-1 boxed boxed--lg boxed--border">
                                        <h3>Placement Aptitude Training</h3>
                                        <span className="h2">
                                            <span className="pricing__dollar type--strikethrough">&#x20b9;</span><strong className="type--strikethrough">4,999</strong>
                                            &nbsp;
                                            <span className="pricing__dollar">&#x20b9;</span><strong>2,499</strong>
                                        </span>
                                        <span className="type--fine-print">One Time, INR.</span>
                                        <hr />
                                        <ul>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>Quantitative Aptitude</span>
                                            </li>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>Logical Reasoning</span>
                                            </li>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>Data Interpretation</span>
                                            </li>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>Verbal Ability</span>
                                            </li>
                                        </ul>
                                        <PayButton user={this.props.user} userLogged={this.props.loggedIn} tests={[145]} purchased={false} amount={2499} buttonText="Purchase Plan" resetPromoCode={this.resetPromoCode} promoCode={this.state.promoCode} promoDiscount={this.state.promoValue} />
                                    </div>
                                </div>
                                <div className="col-sm-6"></div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div className="tab__content">
                            <div className="row">
                                <div className="col-sm-6">
                                    <div className="pricing pricing-1 boxed boxed--lg boxed--border">
                                        <h3>Data Exploration with Excel 2010</h3>
                                        <span className="h2">
                                            <span className="pricing__dollar type--strikethrough">&#x20b9;</span><strong className="type--strikethrough">4,999</strong>
                                            &nbsp;
                                            <span className="pricing__dollar">&#x20b9;</span><strong>2,999</strong>
                                        </span>
                                        <span className="type--fine-print">One Time, INR.</span>
                                        <hr />
                                        <ul>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>Basic Operations &amp; Formulae</span>
                                            </li>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>Different functions of Excel</span>
                                            </li>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>Data Visualization with Charts</span>
                                            </li>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>Advanced Sorting, Filtering &amp; Pivot Tables</span>
                                            </li>
                                            <li>
                                                <a href="https://s3.amazonaws.com/beatest-training-courses/Beatest+Business+Analytics+with+Excel.pdf" target="_blank">Download Brochure</a>
                                            </li>
                                        </ul>
                                        <PayButton user={this.props.user} userLogged={this.props.loggedIn} tests={[146]} purchased={false} amount={3500} buttonText="Purchase Plan" resetPromoCode={this.resetPromoCode} promoCode={this.state.promoCode} promoDiscount={this.state.promoValue} />
                                    </div>
                                </div>
                                <div className="col-sm-6">
                                    <div className="pricing pricing-1 boxed boxed--lg boxed--border">
                                        <h3>Data Analytics with R</h3>
                                        <span className="h2">
                                            <span className="pricing__dollar type--strikethrough">&#x20b9;</span><strong className="type--strikethrough">9,999</strong>
                                            &nbsp;
                                            <span className="pricing__dollar">&#x20b9;</span><strong>3,499</strong>
                                        </span>
                                        <span className="type--fine-print">One Time, INR.</span>
                                        <hr />
                                        <ul>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>Basic Operations &amp; Formulae</span>
                                            </li>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>Different functions of Excel</span>
                                            </li>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>Data Visualization with Charts</span>
                                            </li>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>Advanced Sorting, Filtering &amp; Pivot Tables</span>
                                            </li>
                                            <li>
                                                <a href="https://s3.amazonaws.com/beatest-training-courses/Beatest+Business+Analytics+with+Excel.pdf" target="_blank">Download Brochure</a>
                                            </li>
                                        </ul>
                                        <PayButton user={this.props.user} userLogged={this.props.loggedIn} tests={[146]} purchased={false} amount={3999} buttonText="Purchase Plan" resetPromoCode={this.resetPromoCode} promoCode={this.state.promoCode} promoDiscount={this.state.promoValue} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div className="tab__content">
                            <div className="row">
                                <div className="col-sm-6">
                                    <div className="pricing pricing-1 boxed boxed--lg boxed--border">
                                        <h3>Big Data with Hadoop &amp; Elastic Search</h3>
                                        <span className="h2">
                                            <span className="pricing__dollar type--strikethrough">&#x20b9;</span><strong className="type--strikethrough">9,999</strong>
                                            &nbsp;
                                            <span className="pricing__dollar">&#x20b9;</span><strong>3,999</strong>
                                        </span>
                                        <span className="type--fine-print">One Time, INR.</span>
                                        <hr />
                                        <ul>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>MapReduce</span>
                                            </li>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>Hadoop Distributed File System</span>
                                            </li>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>Document Indexing</span>
                                            </li>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>Data Aggregation &amp; Search</span>
                                            </li>
                                            <li>
                                                <a href="https://s3.amazonaws.com/beatest-training-courses/Beatest+Big+Data+with+Hadoop+Elastic+Search.pdf" target="_blank">Download Brochure</a>
                                            </li>
                                        </ul>
                                        <PayButton user={this.props.user} userLogged={this.props.loggedIn} tests={[147]} purchased={false} amount={3999} buttonText="Purchase Plan" resetPromoCode={this.resetPromoCode} promoCode={this.state.promoCode} promoDiscount={this.state.promoValue} />
                                    </div>
                                </div>
                                <div className="col-sm-6"></div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div className="tab__content">
                            <div className="row">
                                <div className="col-sm-6">
                                    <div className="pricing pricing-1 boxed boxed--lg boxed--border">
                                        <h3>NodeJS, ExpressJS &amp; MongoDB</h3>
                                        <span className="h2">
                                            <span className="pricing__dollar type--strikethrough">&#x20b9;</span><strong className="type--strikethrough">7,999</strong>
                                            &nbsp;
                                            <span className="pricing__dollar">&#x20b9;</span><strong>3,499</strong>
                                        </span>
                                        <span className="type--fine-print">One Time, INR.</span>
                                        <hr />
                                        <ul>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>Restful Web Services</span>
                                            </li>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>Quick Prototyping</span>
                                            </li>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>Integration with MongoDB</span>
                                            </li>
                                        </ul>
                                        <PayButton user={this.props.user} userLogged={this.props.loggedIn} tests={[148]} purchased={false} amount={3499} buttonText="Purchase Plan" resetPromoCode={this.resetPromoCode} promoCode={this.state.promoCode} promoDiscount={this.state.promoValue} />
                                    </div>
                                </div>
                                <div className="col-sm-6">
                                    <div className="pricing pricing-1 boxed boxed--lg boxed--border">
                                        <h3>Single Page Applications with AngularJS</h3>
                                        <span className="h2">
                                            <span className="pricing__dollar type--strikethrough">&#x20b9;</span><strong className="type--strikethrough">7,999</strong>
                                            &nbsp;
                                            <span className="pricing__dollar">&#x20b9;</span><strong>3,499</strong>
                                        </span>
                                        <span className="type--fine-print">One Time, INR.</span>
                                        <hr />
                                        <ul>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>Controllers</span>
                                            </li>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>Routing</span>
                                            </li>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>Directives</span>
                                            </li>
                                            <li>
                                                <a href="https://s3.amazonaws.com/beatest-training-courses/Beatest+AngularJS.pdf" target="_blank">Download Brochure</a>
                                            </li>
                                        </ul>
                                        <PayButton user={this.props.user} userLogged={this.props.loggedIn} tests={[149]} purchased={false} amount={3499} buttonText="Purchase Plan" resetPromoCode={this.resetPromoCode} promoCode={this.state.promoCode} promoDiscount={this.state.promoValue} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div className="tab__content">
                            <div className="row">
                                <div className="col-sm-6">
                                    <div className="pricing pricing-1 boxed boxed--lg boxed--border">
                                        <h3>Introduction to Web</h3>
                                        <span className="h2">
                                            <span className="pricing__dollar type--strikethrough">&#x20b9;</span><strong className="type--strikethrough">4,999</strong>
                                            &nbsp;
                                            <span className="pricing__dollar">&#x20b9;</span><strong>2,999</strong>
                                        </span>
                                        <span className="type--fine-print">One Time, INR.</span>
                                        <hr />
                                        <ul>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>Web Server</span>
                                            </li>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>Basics of HTML5 &amp; CSS3</span>
                                            </li>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>Developing with JavaScript</span>
                                            </li>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>Hosting &amp; Deploying Web Apps</span>
                                            </li>
                                        </ul>
                                        <PayButton user={this.props.user} userLogged={this.props.loggedIn} tests={[150]} purchased={false} amount={2999} buttonText="Purchase Plan" resetPromoCode={this.resetPromoCode} promoCode={this.state.promoCode} promoDiscount={this.state.promoValue} />
                                    </div>
                                </div>
                                <div className="col-sm-6">
                                    <div className="pricing pricing-1 boxed boxed--lg boxed--border">
                                        <h3>Web Development with Python</h3>
                                        <span className="h2">
                                            <span className="pricing__dollar type--strikethrough">&#x20b9;</span><strong className="type--strikethrough">7,999</strong>
                                            &nbsp;
                                            <span className="pricing__dollar">&#x20b9;</span><strong>3,499</strong>
                                        </span>
                                        <span className="type--fine-print">One Time, INR.</span>
                                        <hr />
                                        <ul>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>Concepts of Restful Services</span>
                                            </li>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>Micro Services with Flask</span>
                                            </li>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>Connecting to DataSource</span>
                                            </li>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>Deploying &amp; Scaling Web Services</span>
                                            </li>
                                        </ul>
                                        <PayButton user={this.props.user} userLogged={this.props.loggedIn} tests={[151]} purchased={false} amount={3499} buttonText="Purchase Plan" resetPromoCode={this.resetPromoCode} promoCode={this.state.promoCode} promoDiscount={this.state.promoValue} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div className="tab__content">
                            <div className="row">
                                <div className="col-sm-6">
                                    <div className="pricing pricing-1 boxed boxed--lg boxed--border">
                                        <h3>CAT Full Course with Mocks</h3>
                                        <span className="h2">
                                            <span className="pricing__dollar type--strikethrough">&#x20b9;</span><strong className="type--strikethrough">15,999</strong>
                                            &nbsp;
                                            <span className="pricing__dollar">&#x20b9;</span><strong>12,999</strong>
                                        </span>
                                        <span className="type--fine-print">One Time, INR.</span>
                                        <hr />
                                        <ul>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>Live Expert Sessions</span>
                                            </li>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>Rigorous Practice Modules</span>
                                            </li>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>Mock Test Analysis &amp; Doubt Clearing Sessions</span>
                                            </li>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>15 Mock Tests &amp; 15 Sectional Tests</span>
                                            </li>
                                        </ul>
                                        <PayButton user={this.props.user} userLogged={this.props.loggedIn} tests={[]} purchased={false} amount={12999} buttonText="Purchase Plan" resetPromoCode={this.resetPromoCode} promoCode={this.state.promoCode} promoDiscount={this.state.promoValue} />
                                    </div>
                                </div>
                                <div className="col-sm-6"/>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div className="tab__content">
                            <div className="row">
                                <div className="col-sm-6">
                                    <div className="pricing pricing-1 boxed boxed--lg boxed--border">
                                        <h3>Quant Course</h3>
                                        <span className="h2">
                                            <span className="pricing__dollar type--strikethrough">&#x20b9;</span><strong className="type--strikethrough">5,999</strong>
                                            &nbsp;
                                            <span className="pricing__dollar">&#x20b9;</span><strong>4,999</strong>
                                        </span>
                                        <span className="type--fine-print">One Time, INR.</span>
                                        <hr />
                                        <ul>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>Engage in Quant Live Classes</span>
                                            </li>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>Comprehensive Doubt Clearing Sessions</span>
                                            </li>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>Access to 15 full length Mocks &amp; 5 Sectional Mocks</span>
                                            </li>
                                        </ul>
                                        <PayButton user={this.props.user} userLogged={this.props.loggedIn} tests={[]} purchased={false} amount={4999} buttonText="Purchase Plan" resetPromoCode={this.resetPromoCode} promoCode={this.state.promoCode} promoDiscount={this.state.promoValue} />
                                    </div>
                                </div>
                                <div className="col-sm-6">
                                    <div className="pricing pricing-1 boxed boxed--lg boxed--border">
                                        <h3>DILR Course</h3>
                                        <span className="h2">
                                            <span className="pricing__dollar type--strikethrough">&#x20b9;</span><strong className="type--strikethrough">5,999</strong>
                                            &nbsp;
                                            <span className="pricing__dollar">&#x20b9;</span><strong>4,999</strong>
                                        </span>
                                        <span className="type--fine-print">One Time, INR.</span>
                                        <hr />
                                        <ul>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>Engage in DILR Live Classes</span>
                                            </li>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>Comprehensive Doubt Clearing Sessions</span>
                                            </li>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>Access to 15 full length Mocks &amp; 5 Sectional Mocks</span>
                                            </li>
                                        </ul>
                                        <PayButton user={this.props.user} userLogged={this.props.loggedIn} tests={[]} purchased={false} amount={4999} buttonText="Purchase Plan" resetPromoCode={this.resetPromoCode} promoCode={this.state.promoCode} promoDiscount={this.state.promoValue} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div className="tab__content">
                            <div className="row">
                                <div className="col-sm-6">
                                    <div className="pricing pricing-1 boxed boxed--lg boxed--border">
                                        <h3>VARC Course</h3>
                                        <span className="h2">
                                            <span className="pricing__dollar type--strikethrough">&#x20b9;</span><strong className="type--strikethrough">5,999</strong>
                                            &nbsp;
                                            <span className="pricing__dollar">&#x20b9;</span><strong>4,999</strong>
                                        </span>
                                        <span className="type--fine-print">One Time, INR.</span>
                                        <hr />
                                        <ul>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>Engage in VARC Live Classes</span>
                                            </li>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>Comprehensive Doubt Clearing Sessions</span>
                                            </li>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>Access to 15 full length Mocks &amp; 5 Sectional Mocks</span>
                                            </li>
                                        </ul>
                                        <PayButton user={this.props.user} userLogged={this.props.loggedIn} tests={[]} purchased={false} amount={4999} buttonText="Purchase Plan" resetPromoCode={this.resetPromoCode} promoCode={this.state.promoCode} promoDiscount={this.state.promoValue} />
                                    </div>
                                </div>
                                <div className="col-sm-6">
                                    <div className="pricing pricing-1 boxed boxed--lg boxed--border">
                                        <h3>CAT Mocks</h3>
                                        <span className="h2">
                                            <span className="pricing__dollar type--strikethrough">&#x20b9;</span><strong className="type--strikethrough">1,999</strong>
                                            &nbsp;
                                            <span className="pricing__dollar">&#x20b9;</span><strong>799</strong>
                                        </span>
                                        <span className="type--fine-print">One Time, INR.</span>
                                        <hr />
                                        <ul>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>Attempt 15 Mocks &amp; 15 sectional tests</span>
                                            </li>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>Improve your overall performance &amp; speed</span>
                                            </li>
                                            <li>
                                                <span className="checkmark bg--primary-1"/>
                                                <span>Drill down performance analysis</span>
                                            </li>
                                        </ul>
                                        <PayButton user={this.props.user} userLogged={this.props.loggedIn} tests={[]} purchased={false} amount={799} buttonText="Purchase Plan" resetPromoCode={this.resetPromoCode} promoCode={this.state.promoCode} promoDiscount={this.state.promoValue} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div className="row">
                <br />
                <div className="col-sm-4" />
                <div className="col-sm-8">
                    <div className="row">
                        <div className="col-sm-3" />
                        <div className="col-sm-4">
                            <input type="text" name="promo-code" className="validate-required" placeholder="Promo Code" disabled={this.state.promoApplied} onChange={this.onPromoCodeChange} />
                        </div>
                        <div className="col-sm-2">
                            <button type="submit" className="btn btn--primary type--uppercase" disabled={this.state.promoApplied} onClick={this.applyPromoCode}>Apply!</button>
                        </div>
                        <div className="col-sm-3" />
                    </div>
                </div>
            </div>
        </div>
    </section>
        )
    }
}
