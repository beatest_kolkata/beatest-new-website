import React, { Component } from 'react';
import classnames from 'classnames';
/*import './style.css';*/
import PricingContainer from '../PricingContainer';

class Pricing extends Component {
	render () {
		const { className, ...props } = this.props;
		return (
			<PricingContainer className={classnames('Pricing', className)} {...props}/>
		);
	}
}

export default Pricing;