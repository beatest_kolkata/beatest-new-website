import React, { Component} from 'react';

export default class ViewSolution extends Component {
    constructor(props) {
        super(props);
        this.state = {displayLogic: false};
    }

    showLogic() {
        this.setState({displayLogic: !this.state.displayLogic});
    }

    componentWillReceiveProps(nextProps) {
        this.setState({displayLogic: false});
    }

    render() {
        if (this.props.logic && this.props.logic.length > 0) {
            return (
                <div>
                    <button className="next-prev-button view-solution" onClick={this.showLogic.bind(this)}>View Solution</button>
                    { this.state.displayLogic ?
                        <div className="logic-display">
                            <b>Logic:</b>
                            <div dangerouslySetInnerHTML={{__html: this.props.logic}}></div>
                        </div> : "" }
                </div>
            );
        } else {
            return (<button className="next-prev-button view-solution" disabled>View Solution</button>);
        }
    }
}
