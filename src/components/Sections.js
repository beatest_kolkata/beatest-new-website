import React, {PropTypes, Component} from 'react';
import Calculator from './Calculator';
import $ from 'jquery';

import Loader from './Loader';

const buttonMargin = {
    marginTop: '5px'
};

const sectionButtonMargin = {
    marginTop: '10px'
};

let options = {
    lines: 13,
    length: 20,
    width: 10,
    radius: 30,
    scale: 1.00,
    corners: 1,
    color: '#000',
    opacity: 0.25,
    rotate: 0,
    direction: 1,
    speed: 1,
    trail: 60,
    fps: 20,
    zIndex: 2e9,
    top: '50%',
    left: '50%',
    shadow: false,
    hwaccel: false,
    position: 'absolute'
};

class Sections extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.gotoSelectedSection = this.gotoSelectedSection.bind(this);
    }

    gotoSelectedSection(sectionAttemptId, index) {
        this.props.gotoSelectedSection(index);
        this.props.loadingQuestionsBegin();
        this.props.changeSectionLoaded(false);
        $.get('/api/section_attempt/' + sectionAttemptId + '/questions', (questionsList) => {
            this.props.loadingQuestionsSuccess(questionsList);
            this.props.changeSectionLoaded(true);
        });
        this.props.changeTimeleft(this.props.sectionAttempts[this.props.sectionIndex].timeleft);
    }

    showCalculator() {
        $('.calculator-div').removeClass('hide').addClass('show');
    }

    render() {
        if (this.props.testAttempt.type === 'CAT') {
            return (
                <div className="sections">
                    {(() => {
                        return (
                            <div className="row">
                                <div className="col-lg-3" style={buttonMargin}>
                                    <div className="col-lg-2"></div>
                                    <div className="col-lg-8"><i className="calculator-icon fa fa-calculator fa-2x"
                                                                 onClick={this.showCalculator}/></div>
                                    <div className="col-lg-2"></div>
                                </div>
                                {this.props.sectionAttempts.map(function (sectionAttempt, index) {
                                    return (
                                        <div className="col-lg-3" style={sectionButtonMargin} key={index}>
                                            <div className="col-lg-2"></div>
                                            <button className="section-button col-lg-8"
                                                id={sectionAttempt.id}>{sectionAttempt.name}</button>
                                            <div className="col-lg-2"></div>
                                        </div>
                                    );
                                })}
                            </div>
                        )
                    })()}
                    <Calculator/>
                </div>
            );
        } else {
            return (
                <div className="sections">
                    {(() => {
                        return (
                            <div className="row">
                                {this.props.sectionAttempts.map(function (sectionAttempt, index) {
                                    return (
                                        <div className="col-lg-4" style={sectionButtonMargin} key={index}>
                                            <div className="col-lg-2"></div>
                                            <button className="section-button col-lg-8" id={sectionAttempt.id}
                                                    onClick={() => this.gotoSelectedSection(sectionAttempt.id, index)}>{sectionAttempt.name}</button>
                                            <div className="col-lg-2"></div>
                                        </div>
                                    );
                                }.bind(this))}
                            </div>
                        )
                    })()}
                </div>
            );
        }
    }
}

Sections.propTypes = {
    sectionIndex: PropTypes.number,
    sectionAttempts: PropTypes.array,
    testAttempt: PropTypes.object,
    gotoNextSection: PropTypes.func
};

export default Sections;
