import React, {Component} from 'react';
import $ from 'jquery';
import {Card, CardActions, CardHeader, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import {List, ListItem} from 'material-ui/List';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from 'react-tap-event-plugin'

class Packages extends Component {
    constructor(props) {
        super(props);
        this.state = {};

        try {
            injectTapEventPlugin()
        }
        catch (e) {
            //Don't do anything
        }
    }

    render() {
        return (
            <MuiThemeProvider>
                <Card>
                    <CardHeader
                        title="Package Name"
                        subtitle="Package Description"
                        actAsExpander={true}
                        showExpandableButton={true}
                    />
                    <CardActions>
                        <FlatButton label="Buy" secondary={true}/>
                        <FlatButton label="Action2"/>
                    </CardActions>
                    <CardText expandable={true}>
                        <List>
                            <ListItem
                                primaryText="Test Name"
                                secondaryText={
                                    <p>
                                        <span>Test Description</span>
                                    </p>
                                }
                            />
                            <ListItem
                                primaryText="Test Name"
                                secondaryText={
                                    <p>
                                        <span>Test Description</span>
                                    </p>
                                }
                            />
                            <ListItem
                                primaryText="Test Name"
                                secondaryText={
                                    <p>
                                        <span>Test Description</span>
                                    </p>
                                }
                            />
                            <ListItem
                                primaryText="Test Name"
                                secondaryText={
                                    <p>
                                        <span>Test Description</span>
                                    </p>
                                }
                            />
                            <ListItem
                                primaryText="Test Name"
                                secondaryText={
                                    <p>
                                        <span>Test Description</span>
                                    </p>
                                }
                            />
                        </List>
                    </CardText>
                </Card>
            </MuiThemeProvider>
        );
    }
}

export default Packages;