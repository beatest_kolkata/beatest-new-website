import React, { Component } from 'react';

class Footer extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <footer className="space--sm footer-2 ">
                <div className="container">
                    <div className="row">
                        <div className="col-sm-6 col-md-3 col-xs-6">
                            <h6 className="type--uppercase">Company</h6>
                            <ul className="list--hover">
                                <li>
                                    <a href="aboutus">About Company</a>
                                </li>
                                <li>
                                    <a href="#">Careers</a>
                                </li>
                            </ul>
                        </div>
                        <div className="col-sm-6 col-md-3 col-xs-6">
                            <h6 className="type--uppercase">Developers</h6>
                            <ul className="list--hover">
                                <li>
                                    <a href="#">API Reference</a>
                                </li>
                                <li>
                                    <a href="#">Developer Blog</a>
                                </li>
                            </ul>
                        </div>
                        <div className="col-sm-6 col-md-3 col-xs-6">
                            <h6 className="type--uppercase">Support</h6>
                            <ul className="list--hover">
                                <li>
                                    <a href="#">Help Center</a>
                                </li>
                                <li>
                                    <a href="#">Chat</a>
                                </li>
                            </ul>
                        </div>
                        <div className="col-sm-6 col-md-3 col-xs-6">
                            <h6 className="type--uppercase">Locations</h6>
                            <ul className="list--hover">
                                <li>
                                    <a href="#">Kolkata</a>
                                </li>
                                <li>
                                    <a href="#">Bengaluru</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    {/*end of row*/}
                    <div className="row">
                        <div className="col-sm-6">
              <span className="type--fine-print">©
                <span className="update-year" /> Beatest Inc.</span>
                            <a className="type--fine-print" href="#">Privacy Policy</a>
                            <a className="type--fine-print" href="#">Legal</a>
                        </div>
                        <div className="col-sm-6 text-right text-left-xs">
                            <ul className="social-list list-inline list--hover">
                                <li>
                                    <a href="https://plus.google.com/112722904416257444024" target="_blank">
                                        <i className="socicon socicon-google icon icon--xs" />
                                    </a>
                                </li>
                                <li>
                                    <a href="https://twitter.com/beatest_in" target="_blank">
                                        <i className="socicon socicon-twitter icon icon--xs" />
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.facebook.com/beatest.in/" target="_blank">
                                        <i className="socicon socicon-facebook icon icon--xs" />
                                    </a>
                                </li>
                                <li>
                                    <a href="#" target="_blank">
                                        <i className="socicon socicon-instagram icon icon--xs" />
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </footer>
		);
    }
}

export default Footer;
