import React, {PropTypes, Component} from 'react';
import * as action from '../actions/action';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import $ from 'jquery';

import Loader from './Loader';
import QuestionButton from './QuestionButton';
import NotFound from './NotFound';
import ViewSolution from './ViewSolution';

const initialState = {
    currentView: 'PERFORMANCE',
    questionsList: [],
    user: null,
    sectionIndex: 0,
    questionChoices: [],
    isUserLoading: true
}

class PerformanceContainer extends Component {
    constructor(props) {
        super(props);
        this.state = initialState;
        this.getUser = this.getUser.bind(this);
        this.getTest = this.getTest.bind(this);
        this.getNextQuestion = this.getNextQuestion.bind(this);
        this.getPrevQuestion = this.getPrevQuestion.bind(this);
        this.getQuestionsList = this.getQuestionsList.bind(this);
        this.getCurrentQuestion = this.getCurrentQuestion.bind(this);
        this.getNextSection = this.getNextSection.bind(this);
        this.getPrevSection = this.getPrevSection.bind(this);
        this.getUser();
        this.getTest();
    }

    getUser() {
        $.get('/api/user', (data) => {
            if(typeof data === 'object') {
                this.props.actions.login(data.user);
            }
        }).then(() => {
            this.props.actions.loadingUserSuccess();
        });
    }

    getTest() {
        $.get('/api/tests/' + this.props.routeParams.testId + '/solution', (data) => {
        }).then((data) => {
            this.props.actions.setSolutionData(data);
            this.setState({testId: this.props.routeParams.testId});
        });
    }

    getNextQuestion() {
        this.props.actions.gotoNextQuestion();
    }

    getPrevQuestion() {
        this.props.actions.gotoPrevQuestion();
    }

    getQuestionsList() {
        return this.props.sectionAttempts[this.props.sectionIndex].questions;
    }

    getCurrentQuestion() {
        return this.getQuestionsList()[this.props.currentQuestionIndex];
    }

    getChoicesList() {
        return this.getCurrentQuestion().choices;
    }

    getNextSection() {
        this.props.actions.gotoNextSection();
    }

    getPrevSection() {
        this.props.actions.gotoPrevSection();
    }

    showTestScores() {
        window.location.href = '/scores/' + this.state.testId;
    }

    render() {
        if(this.props.isUserLoading  || this.props.isUserLoading === undefined) {
            return (
                <div>
                    <Loader />
                </div>
            );
        } else {
            if(this.props.user === null) {
                return (
                    <NotFound />
                );
            } else {
                return (
                    <div>
                        {/*<Header transparent={false} />*/}
                        <div className="row" style={{marginTop: '50px'}}>
                            <div className="col-lg-4"></div>
                            <div className="col-lg-4"><h3 className="text-center">Performance</h3></div>
                            <div className="col-lg-2">
                                <button className="btn btn--primary float-right" style={{padding: '5px'}} onClick={this.showTestScores.bind(this)}>Scores</button>
                            </div>
                            <div className="col-lg-2"></div>
                        </div>
                        <div className="container" style={{marginTop: '50px'}}>
                            {(() => {
                                if (this.props.sectionAttempts && this.props.sectionAttempts.length > 0) {
                                    return (
                                        <div className="solution-area">
                                            <div className="row">
                                                <div className="col-lg-4">
                                                    <p className="text-center">Topic: <mark>{ this.getCurrentQuestion().topic }</mark></p>
                                                </div>
                                                <div className="col-lg-4">
                                                    <p className="text-center">Difficulty: <mark>{ this.getCurrentQuestion().lod }</mark></p>
                                                </div>
                                                <div className="col-lg-4">
                                                    { typeof this.getCurrentQuestion().time_spent !== 'undefined' && this.getCurrentQuestion().time_spent !== 0 ?
                                                        <p className="text-center">Time Spent: <mark>{ this.getCurrentQuestion().time_spent + " sec" }</mark></p> : "" }
                                                </div>
                                            </div>
                                            <div className="question">
                                                { this.getCurrentQuestion().type === 'RC' ?
                                                    <div className="rc-display">
                                                        <b>Passage:</b>
                                                        <div className="modify" dangerouslySetInnerHTML={{__html: this.getCurrentQuestion().rc_passage}}>
                                                        </div>
                                                    </div> : "" }
                                                <h3>Q : <span dangerouslySetInnerHTML={{__html: this.getCurrentQuestion().html}}></span></h3>
                                                {(() => {
                                                    var currentQuestion = this.props.sectionAttempts[this.props.sectionIndex].questions[this.props.currentQuestionIndex];
                                                    if(currentQuestion.type === 'TITA') {
                                                        if(currentQuestion.tita_choice !== undefined) {
                                                            return <div>
                                                                <p className="tita-choice">Your Answer : {currentQuestion.tita_choice}</p>
                                                                <input id="tita-input" value={currentQuestion.tita_answer} readOnly/>
                                                            </div>
                                                        } else {
                                                            return <input id="tita-input" value={currentQuestion.tita_answer} readOnly/>
                                                        }
                                                    } else {
                                                        return <ul className="choices">
                                                            {this.getChoicesList().map((choice, index) => {
                                                                return <li className="choice" key={index}>
                                                                    <input className="choice-radio" type="radio" name="choice" key={choice.id} id={choice.id} checked={choice.attempted} readOnly/>
                                                                    <span dangerouslySetInnerHTML={{__html: choice.html}}></span>
                                                                    {(() => {
                                                                        if(choice.is_correct) {
                                                                            return <i className="correct-option fa fa-check fa-2x" />
                                                                        } else if(choice.is_correct ^ choice.attempted) {
                                                                            return <i className="wrong-option fa fa-close fa-2x" />
                                                                        }
                                                                    })()}
                                                                </li>
                                                            })}
                                                        </ul>
                                                    }
                                                })()}
                                                <div className="question-nav-buttons">
                                                    {(() => {
                                                        if(this.props.currentQuestionIndex > 0) {
                                                            return <button className="next-prev-button save-prev" onClick={this.getPrevQuestion}>Previous</button>
                                                        }
                                                    })()}
                                                    {(() => {
                                                        if(this.props.currentQuestionIndex < (this.getQuestionsList().length - 1)) {
                                                            return <button className="next-prev-button save-next" onClick={this.getNextQuestion}>Next</button>
                                                        }
                                                    })()}
                                                    <ViewSolution logic={this.getCurrentQuestion().logic} question={this.getCurrentQuestion()}/>
                                                </div>
                                            </div>
                                            <div className="index-panel">
                                                <div className="user-index-panel">
                                                    <span>{this.props.user.name}</span>
                                                </div>
                                                <div className="index-panel-buttons">
                                                    {this.getQuestionsList().map((question, index) => {
                                                        if(this.props.currentQuestionIndex === index) {
                                                            var status = 'attempted';
                                                        } else {
                                                            status = '';
                                                        }
                                                        return <QuestionButton key={question.id} id={question.id}
                                                                               questionIndex={index+1} status={status}
                                                                               questionsList={this.props.sectionAttempts[this.props.sectionIndex].questions}
                                                                               showSelectedQuestion={this.props.actions.showSelectedQuestion} />
                                                    })}
                                                </div>
                                                <div>
                                                    {(() => {
                                                        if(this.props.sectionIndex > 0) {
                                                            return <button className="next-prev-button" onClick={this.getPrevSection}>Previous Section</button>
                                                        }
                                                    })()}
                                                </div>
                                                <div>
                                                    {(() => {
                                                        if(this.props.sectionIndex < (this.props.sectionAttempts.length - 1)) {
                                                            return <button className="next-prev-button" onClick={this.getNextSection}>Next Section</button>
                                                        }
                                                    })()}
                                                </div>
                                            </div>
                                        </div>);
                                }
                            })()}
                        </div>
                    </div>
                );
            }
        }
    }
}

PerformanceContainer.propTypes = {
    currentView: PropTypes.string,
    user: PropTypes.object,
    sectionIndex: PropTypes.number,
    isTestsLoading: PropTypes.bool,
    sectionAttempts: PropTypes.array,
    currentQuestionId: PropTypes.number,
    currentQuestionIndex: PropTypes.number,
    testAttemptId: PropTypes.number,
    loginPanelVisibility: PropTypes.string,
    registrationPanelVisibility: PropTypes.string,
    userMenuVisibility: PropTypes.string,
    testName: PropTypes.string,
    loadingQuestionsBegin: PropTypes.func,
    loadingQuestionsSuccess: PropTypes.func,
    gotoNextSection: PropTypes.func,
    choiceSelected: PropTypes.func,
    isUserLoading: PropTypes.bool
}

function mapStateToProps(state, props) {
    return {
        currentView: state.currentView,
        user: state.user,
        sectionIndex: state.sectionIndex,
        isTestsLoading: state.isTestsLoading,
        sectionAttempts: state.sectionAttempts,
        currentQuestionId: state.currentQuestionId,
        currentQuestionIndex: state.currentQuestionIndex,
        loginPanelVisibility: state.loginPanelVisibility,
        registrationPanelVisibility: state.registrationPanelVisibility,
        userMenuVisibility: state.userMenuVisibility,
        testName: state.testName,
        loadingQuestionsBegin: state.loadingQuestionsBegin,
        loadingQuestionsSuccess: state.loadingQuestionsSuccess,
        gotoNextSection: state.gotoNextSection,
        choiceSelected: state.choiceSelected,
        isUserLoading: state.isUserLoading
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(action, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PerformanceContainer);