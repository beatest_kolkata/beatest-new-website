import React, { Component } from 'react';

class IndexAchievement extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
			<div className="banner-achievement">
				<div className="achievement-one">
					<div className="four_comp" id="comp_questions">
						<img src="" id="comp_questions" alt=""/>
						<h1>Track your progress in real time.</h1>
					</div>
					<div className="four_comp" id="comp_students">
						<img src="" id="comp_students" alt=""/>
						<h1>Prepare yourself along with all your competitors.</h1>
					</div>
					<div className="four_comp" id="comp_tests">
						<img src="" id="comp_tests" alt=""/>
						<h1>Manage your time during the tests.</h1>
						
					</div>
					<div className="four_comp" id="comp_achiever">
						<img src="" id="comp_tests" alt=""/>
						<h1>Blogs from the finest in the field - their journey, their struggles and their success stories.</h1>
					
					</div>
				</div>
				{/*
				<div className="achievement-two">
					<img src="" alt=""/>
					<p>" BeaTest is the awesome place for the preparation of CAT, SBIPO and IBPSPO "</p>
					<span className="signature-acheiver">- Rahul Das | CAT '17 | 99.87</span>
				</div>
				*/}
			</div>
        );
    }
}



export default IndexAchievement;
