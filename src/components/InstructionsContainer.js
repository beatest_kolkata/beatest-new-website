import React, {PropTypes, Component} from 'react';
import * as action from '../actions/action';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import $ from 'jquery';
import Loader from './Loader';
import NotFound from './NotFound';

const createCookie = require('./cookie.js').createCookie;
const readCookie = require('./cookie.js').readCookie;

let options = {
    lines: 13,
    length: 20,
    width: 10,
    radius: 30,
    scale: 1.00,
    corners: 1,
    color: '#000',
    opacity: 0.25,
    rotate: 0,
    direction: 1,
    speed: 1,
    trail: 60,
    fps: 20,
    zIndex: 2e9,
    top: '50%',
    left: '50%',
    shadow: false,
    hwaccel: false,
    position: 'absolute'
};

class InstructionsContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            testId: null,
            userAgrees: false
        };
        this.getTest = this.getTest.bind(this);
        this.startTest = this.startTest.bind(this);
        this.handleUserAgreement = this.handleUserAgreement.bind(this);
    }

    getTest() {
        this.setState({ testId: this.props.routeParams.testId });
        $.get('/api/tests/' + this.props.routeParams.testId, (data) => {
            if(Object.keys(data).length !== 0 && data.constructor === Object) {
                this.props.actions.setTestScore(data);
            }
            this.setState({ loading: false });
        }).fail(() => {
            this.setState({ loading: false });
        });
    }

    startTest() {
        window.location.pathname = '/test/' + this.state.testId;
    }

    handleUserAgreement(e) {
        this.setState({ userAgrees: !this.state.userAgrees });
    }

    componentWillMount() {
        if(readCookie('userData') !== null) {
            this.props.actions.login(JSON.parse(readCookie('userData')));
            this.getTest(this);
        } else {
            $.get('/api/user', (data) => {
                if (typeof data === 'object') {
                    createCookie('userData', JSON.stringify(data.user), 7);
                    this.props.actions.login(data.user);
                }
                this.getTest(this);
            });
        }
    }

    render() {
        if (this.state.loading) {
            return (
                <div>
                    <Loader />
                </div>
            );
        } else {
            if (this.props.user === null) {
                return (
                    <NotFound/>
                );
            } else {
                return (
                    <div>
                        <a id="start"/>
                        <div className="main-container">
                            <section>
                                <div className="container">
                                    <div className="row">
                                        <div className="col-sm-10 col-md-8 col-sm-offset-1 col-md-offset-2">
                                            <article>
                                                <div className="article__title text-center">
                                                    <h1 className="h1">{this.props.testName}</h1>
                                                </div>
                                                <div className="article__body">
                                                    <div className="row">
                                                        <div className="col-sm-12"
                                                             dangerouslySetInnerHTML={{__html: this.props.testInstructions}}/>
                                                        <div className="col-sm-12">
                                                            <div className="input-checkbox">
                                                                <input type="checkbox" name="agree"
                                                                       id="input-assigned-8"
                                                                       onChange={this.handleUserAgreement}/>
                                                                <label htmlFor="input-assigned-8"/>
                                                            </div>
                                                            <span>The computer provided to me is in proper working condition. I have read and understood the instructions given above.</span>
                                                        </div>
                                                        <br/>
                                                        <div className="text-center">
                                                            <a className="btn btn--primary"
                                                               disabled={!this.state.userAgrees}
                                                               onClick={this.startTest}>
                                                                <span className="btn__text">I am ready to begin</span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <a className="back-to-top inner-link" href="#start" data-scroll-class="100vh:active">
                            <i className="stack-interface stack-up-open-big"/>
                        </a>
                    </div>
                );
            }
        }
    }
}

InstructionsContainer.propTypes = {
    user: PropTypes.object,
    testId: PropTypes.number,
    testName: PropTypes.string,
    testType: PropTypes.string,
    testInstructions: PropTypes.string
}

function mapStateToProps(state, props) {
    return {
        user: state.user,
        testId: state.testId,
        testName: state.testName,
        testType: state.testType,
        testInstructions: state.testInstructions
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(action, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(InstructionsContainer);
