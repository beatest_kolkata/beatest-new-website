import React, {PropTypes, Component} from 'react';
import * as action from '../actions/action';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import UserPanel from './UserPanel';
import Navlink from './Navlink';
import UserMenu from './UserMenu';
import CollegeDesc from './beatestcolleges';
import CollegeRegisterUser from './collegeuserpanel';
import EditUserDetails from './EditUserDetails';

const initialState = {
    user: null,
    loginPanelVisibility: 'none',
    registrationPanelVisibility: 'none',
    userMenuVisibility: 'none'
}

class UserEditContainer extends Component {
    constructor(props) {
        super(props);
        this.state = initialState;
    }

    render() {
        return (
            <div>
                {(() => {
                    if(this.props.loginPanelVisibility === 'block' || this.props.registrationPanelVisibility === 'block') {
                        return <UserPanel loginPanelVisibility={this.props.loginPanelVisibility}
                                toggleLoginPanelVisibility={this.props.actions.toggleLoginPanelVisibility}
                                login={this.props.actions.login}
                                registrationPanelVisibility={this.props.registrationPanelVisibility}
                                toggleRegistrationPanelVisibility={this.props.actions.toggleRegistrationPanelVisibility} />
                    }
                })()}
                <div className="two">
                <Navlink toggleLoginPanelVisibility={this.props.actions.toggleLoginPanelVisibility}
                    toggleUserMenuVisibility={this.props.actions.toggleUserMenuVisibility}
                    onPage="index" user={this.props.user} login={this.props.actions.login} />
                <UserMenu userMenuVisibility={this.props.userMenuVisibility}
                    toggleUserMenuVisibility={this.props.actions.toggleUserMenuVisibility}
                    logout={this.props.actions.logout} />
                <EditUserDetails />
                </div>
            </div>
        );
    }
}

UserEditContainer.propTypes = {
    loginPanelVisibility: PropTypes.string,
    registrationPanelVisibility: PropTypes.string,
    userMenuVisibility: PropTypes.string,
    user: PropTypes.object
}

function mapStateToProps(state, props) {
    return {
        loginPanelVisibility: state.loginPanelVisibility,
        registrationPanelVisibility: state.registrationPanelVisibility,
        userMenuVisibility: state.userMenuVisibility,
        user: state.user
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(action, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserEditContainer);