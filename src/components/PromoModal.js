import React from 'react';
import Modal from 'react-modal';
import PayButton from './PayButton';
import $ from 'jquery';

const customStyles = {
	overlay : {
    position          : 'fixed',
    top               : 0,
    left              : 0,
    right             : 0,
    bottom            : 0,
    backgroundColor   : 'rgba(0, 10, 20, 0.75)',
  },
  content : {
    position                   : 'absolute',
    top                        : '120px',
    left                       : '500px',
    right                      : '500px',
    bottom                     : '100px',
    border                     : '1px solid #ccc',
    background                 : 'white',
    overflow                   : 'auto',
    WebkitOverflowScrolling    : 'touch',
    borderRadius               : '10px',
    outline                    : 'none',
    padding                    : '20px',
  }
};

export default class PromoModal extends React.Component {
  	constructor() {
    	super();

    	this.state = {
      		modalIsOpen: false,
      		canClose: false,
          promo: '',
          discount: 0
    	};

    	this.openModal = this.openModal.bind(this);
    	this.afterOpenModal = this.afterOpenModal.bind(this);
    	this.closeModal = this.closeModal.bind(this);
      this.handleUserInput = this.handleUserInput.bind(this);
      this.referralEntry = this.referralEntry.bind(this);
  	}

  	openModal() {
   		this.setState({modalIsOpen: true});
  	}

  	afterOpenModal() {
    	//this.refs.subtitle.style.color = '#FFA500';
  	}

  	closeModal() {
    	this.setState({modalIsOpen: false});
  	}

    referralEntry() {
      $.get('/api/validate_code/' + this.state.promo + '/' + this.props.user.id, (promo) => {
        if (promo.valid) {
          this.setState({
            discount: promo.value
          });
          $("#strike").css({"text-decoration" : "line-through", "color" : "red"});
          $("#bonusPrice").html("INR " + (100 + this.props.test.price - this.state.discount).toString());
          $("#bonusPrice").css({"color" : "green", "font-family": "Dekko", "text-align" : "center", "font-size" : "20px"});   
          $("#query").hide(); 
        }

        else {
          $("#bonusPrice").css({"font-family": "Dekko", "font-size" : "20px", "color" : "red"});
          $("#bonusPrice").html("Sorry, but the promo code " + this.state.promo + " is not valid!");
          this.setState({
            promo: ""
          });
        }
      });
    }

    handleUserInput(e) {
      this.setState ({
        promo: e.target.value
      });  
  }

  	render() {
    	return (
        <div>
      		<button className="section-button" onClick={this.openModal}>Pay</button>
          <div>
            <Modal
              isOpen={this.state.modalIsOpen}
              onAfterOpen={this.afterOpenModal}
              onRequestClose={this.closeModal}
              contentLabel="Modal"
              style={customStyles} >

              <h2 className="promoTitle">Hello {this.props.user.name.split(' ')[0]}!</h2>
              <div className="tests-heading"> 
                <p className="type-toggle">{this.props.test.name}</p>
                <br/><br/><br/><br/>
                <p id="strike"> INR {100 + this.props.test.price} </p>
                <p id="bonusPrice"></p>
              </div>
              <div className="tests-heading" id="query">
                <form>
                  <p> Have a Promo Code? </p>
                  <input onChange={this.handleUserInput} value={this.state.text}/>
                </form>
                <div>
                  <button className="button1" onClick={this.referralEntry}>Apply</button>
                </div>
              </div>
              <br></br><br></br>
              <PayButton 
                price={100 + this.props.test.price - this.state.discount} 
                user={this.props.user} 
                test={this.props.test}
                promo_code={this.state.promo}
                closeModal={this.closeModal}/>
            </Modal>
          </div>
        </div>
    	);
  	}
}