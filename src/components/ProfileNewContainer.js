import React, {PropTypes, Component} from 'react';
import { Redirect } from 'react-router';
import * as action from '../actions/action';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import $ from 'jquery';
import NotFound from './NotFound';
import NavBar from "./NavBar";
import Login from "./Login/index";

const initialState = {
    user: null,
    loginPanelVisibility: 'none',
    registrationPanelVisibility: 'none',
    userMenuVisibility: 'none',
    isUserLoading: true
};

class ProfileNewContainer extends Component {
    constructor(props) {
        super(props);
        this.state = initialState;
        this.getUser = this.getUser.bind(this);
        this.getTests = this.getTests.bind(this);
        this.logout = this.logout.bind(this);
        //this.showSnackbar();
        this.getUser();
    }

    getUser() {
        $.get('/api/user', (data) => {
            if(typeof data === 'object') {
                this.props.actions.login(data.user);
                this.setState({
                    isUserLoading: false
                });
                this.getTests();
            }
        }).then(() => {
            this.props.actions.loadingUserSuccess();
            this.setState({
                isUserLoading: false
            });
        });
    }

    getTests() {
        $.get('/api/tests', (data) => {
            if(typeof data === 'object') {
                this.setState({
                    isUserLoading: false
                });
            }
        }).then(() => {
            this.props.actions.loadingUserSuccess();
            this.setState({
                isUserLoading: false
            });
        });
    }

    logout() {
        $.ajax({
            url: '/api/logout',
            dataType: 'json',
            type: 'POST',
            contentType: 'application/json'
        }).then((data) => {
            this.props.actions.logout();
            return (<Redirect to="/login" />);
        }).then(() => {

        });
    }

    render() {
        if (this.props.user !== null) {
            return (
                <div>
                    <NavBar user={this.props.user}/>
                    <div className="main-container">
                        <section className="bg--secondary space--sm">
                            <div className="container">
                                <div className="row">
                                    <div className="col-md-3">
                                        <div className="boxed boxed--lg boxed--border">
                                            <div className="text-block text-center">
                                                {(() => {
                                                    if(this.props.user.profile_picture !== null) {
                                                        return (
                                                            <img alt="avatar" src="{this.props.user.profile_picture}" className="image--sm" />
                                                        );
                                                    } else {
                                                        return (
                                                            <img alt="avatar" src="images/user.png" className="image--sm" />
                                                        );
                                                    }
                                                })()}
                                                <span className="h4">{this.props.user.name}</span>
                                                <span>Student, BITS Pilani</span>
                                            </div>
                                            {(() => {
                                                // if block anything means that is shown
                                                if(this.props.user !== null) {
                                                    return (
                                                        <a className="btn btn--primary bg--error btn--icon" onClick={this.logout} style={{marginTop: 5}}>
                                                            <span className="btn__text">
                                                                <i className="icon-Power" /> Logout
                                                            </span>
                                                        </a>
                                                    );
                                                }
                                            })()}
                                            <hr />
                                            <div className="text-block">
                                                <ul className="menu-vertical">
                                                    <li>
                                                        <a href="#" data-toggle-class=".account-tab:not(.hidden);hidden|#account-tests;hidden">Tests</a>
                                                    </li>
                                                    <li>
                                                        <a href="#" data-toggle-class=".account-tab:not(.hidden);hidden|#account-college-tests;hidden">College Tests</a>
                                                    </li>
                                                    <li>
                                                        <a href="#" data-toggle-class=".account-tab:not(.hidden);hidden|#account-college-packages;hidden">College Packages</a>
                                                    </li>
                                                    <li>
                                                        <a href="#" data-toggle-class=".account-tab:not(.hidden);hidden|#account-notifications;hidden">Email Notifications</a>
                                                    </li>
                                                    <li>
                                                        <a href="#" data-toggle-class=".account-tab:not(.hidden);hidden|#account-password;hidden">Password</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <hr />
                                            <a className="btn btn--primary btn--icon" href="#" style={{marginTop: 5}}>
                                            <span className="btn__text">
                                                <i className="icon-Sharethis" /> Referral
                                            </span>
                                            </a>
                                        </div>
                                    </div>
                                    <div className="col-md-9">
                                        <div className="boxed boxed--lg boxed--border">
                                            <div id="account-tests" className="account-tab">
                                                <section id="tests" className=" bg--secondary text-center">
                                                    <div className="container">
                                                        <div className="row">
                                                            <div className="col-sm-12">
                                                                <div className="tabs-container">
                                                                    <ul className="tabs">
                                                                        <li className="active">
                                                                            <div className="tab__title">
                                                                                <span className="h5">Mock Tests</span>
                                                                            </div>
                                                                            <div className="tab__content">
                                                                                <div className="row text-center tests-flip-box">
                                                                                    <div className="col-sm-4 col-md-4 flip-container">
                                                                                        <div className="flipper">
                                                                                            <div className="front feature feature-3 boxed boxed--sm boxed--border">
                                                                                                <div className="icon-container text-center">
                                                                                                    <i className="icon icon--lg icon-Notepad" />
                                                                                                </div>
                                                                                                <h5>BEAT CAT 01</h5>
                                                                                            </div>
                                                                                            <div className="back flip-back-bg feature feature-3 boxed boxed--sm boxed--border">
                                                                                                <ul className="flip-list">
                                                                                                    <li className="text-left">
                                                                                                        Number of questions <span className="pull-right">100</span>
                                                                                                    </li>
                                                                                                    <li className="text-left">
                                                                                                        Time <span className="pull-right">60 mins</span>
                                                                                                    </li>
                                                                                                    <li className="text-center">
                                                                                                        <a className="btn btn--sm type--uppercase flip-bg-white" href="tests.html">
                                                                                                        <span className="btn__text">
                                                                                                          Start test
                                                                                                        </span>
                                                                                                        </a>
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </div>
                                                                                            {/*end feature*/}
                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="col-sm-4 col-md-4 flip-container">
                                                                                        <div className="flipper">
                                                                                            <div className="front feature feature-3 boxed boxed--sm boxed--border">
                                                                                                <div className="icon-container text-center">
                                                                                                    <i className="icon icon--lg icon-Notepad" />
                                                                                                </div>
                                                                                                <h5>BEAT CAT 02</h5>
                                                                                            </div>
                                                                                            <div className="back flip-back-bg feature feature-3 boxed boxed--sm boxed--border">
                                                                                                <ul className="flip-list">
                                                                                                    <li className="text-left">
                                                                                                        Number of questions <span className="pull-right">100</span>
                                                                                                    </li>
                                                                                                    <li className="text-left">
                                                                                                        Time <span className="pull-right">60 mins</span>
                                                                                                    </li>
                                                                                                    <li className="text-center">
                                                                                                        <a className="btn btn--sm type--uppercase flip-bg-white" href="tests.html">
                                                                                                        <span className="btn__text">
                                                                                                          Buy now
                                                                                                        </span>
                                                                                                        </a>
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </div>
                                                                                            {/*end feature*/}
                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="col-sm-4 col-md-4 flip-container">
                                                                                        <div className="flipper">
                                                                                            <div className="front feature feature-3 boxed boxed--sm boxed--border">
                                                                                                <div className="icon-container text-center text-center">
                                                                                                    <i className="icon icon--lg icon-Notepad" />
                                                                                                </div>
                                                                                                <h5>BEAT CAT 03</h5>
                                                                                            </div>
                                                                                            <div className="back flip-back-bg feature feature-3 boxed boxed--sm boxed--border">
                                                                                                <ul className="flip-list">
                                                                                                    <li className="text-left">
                                                                                                        Number of questions <span className="pull-right">100</span>
                                                                                                    </li>
                                                                                                    <li className="text-left">
                                                                                                        Time <span className="pull-right">60 mins</span>
                                                                                                    </li>
                                                                                                    <li className="text-center">
                                                                                                        <a className="btn btn--sm type--uppercase flip-bg-white" href="tests.html">
                                                                                                        <span className="btn__text">
                                                                                                          Start test
                                                                                                        </span>
                                                                                                        </a>
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </div>
                                                                                            {/*end feature*/}
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                        <li>
                                                                            <div className="tab__title">
                                                                                <span className="h5">Topic Tests</span>
                                                                            </div>
                                                                            <div className="tab__content">
                                                                                <div className="row text-center tests-flip-box">
                                                                                    <div className="col-sm-4 col-md-3 flip-container">
                                                                                        <div className="flipper">
                                                                                            <div className="front feature feature-3 boxed boxed--sm boxed--border">
                                                                                                <div className="icon-container text-center">
                                                                                                    <i className="icon icon--lg icon-Notepad" />
                                                                                                </div>
                                                                                                <h5>Ratio &amp; Proportion 01</h5>
                                                                                            </div>
                                                                                            <div className="back flip-back-bg feature feature-3 boxed boxed--sm boxed--border">
                                                                                                <ul className="flip-list">
                                                                                                    <li className="text-left">
                                                                                                        Number of questions <span className="pull-right">100</span>
                                                                                                    </li>
                                                                                                    <li className="text-left">
                                                                                                        Time <span className="pull-right">60 mins</span>
                                                                                                    </li>
                                                                                                    <li className="text-center">
                                                                                                        <a className="btn btn--sm type--uppercase flip-bg-white" href="tests.html">
                                    <span className="btn__text">
                                      Start test
                                    </span>
                                                                                                        </a>
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </div>
                                                                                            {/*end feature*/}
                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="col-sm-4 col-md-3 flip-container">
                                                                                        <div className="flipper">
                                                                                            <div className="front feature feature-3 boxed boxed--sm boxed--border">
                                                                                                <div className="icon-container text-center">
                                                                                                    <i className="icon icon--lg icon-Notepad" />
                                                                                                </div>
                                                                                                <h5>Number System 01</h5>
                                                                                            </div>
                                                                                            <div className="back flip-back-bg feature feature-3 boxed boxed--sm boxed--border">
                                                                                                <ul className="flip-list">
                                                                                                    <li className="text-left">
                                                                                                        Number of questions <span className="pull-right">100</span>
                                                                                                    </li>
                                                                                                    <li className="text-left">
                                                                                                        Time <span className="pull-right">60 mins</span>
                                                                                                    </li>
                                                                                                    <li className="text-center">
                                                                                                        <a className="btn btn--sm type--uppercase flip-bg-white" href="tests.html">
                                    <span className="btn__text">
                                      Buy now
                                    </span>
                                                                                                        </a>
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </div>
                                                                                            {/*end feature*/}
                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="col-sm-4 col-md-3 flip-container">
                                                                                        <div className="flipper">
                                                                                            <div className="front feature feature-3 boxed boxed--sm boxed--border">
                                                                                                <div className="icon-container text-center">
                                                                                                    <i className="icon icon--lg icon-Notepad" />
                                                                                                </div>
                                                                                                <h5>Percentages 01</h5>
                                                                                            </div>
                                                                                            <div className="back flip-back-bg feature feature-3 boxed boxed--sm boxed--border">
                                                                                                <ul className="flip-list">
                                                                                                    <li className="text-left">
                                                                                                        Number of questions <span className="pull-right">100</span>
                                                                                                    </li>
                                                                                                    <li className="text-left">
                                                                                                        Time <span className="pull-right">60 mins</span>
                                                                                                    </li>
                                                                                                    <li className="text-center">
                                                                                                        <a className="btn btn--sm type--uppercase flip-bg-white" href="tests.html">
                                    <span className="btn__text">
                                      Start test
                                    </span>
                                                                                                        </a>
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </div>
                                                                                            {/*end feature*/}
                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="col-sm-4 col-md-3 flip-container">
                                                                                        <div className="flipper">
                                                                                            <div className="front feature feature-3 boxed boxed--sm boxed--border">
                                                                                                <div className="icon-container text-center">
                                                                                                    <i className="icon icon--lg icon-Notepad" />
                                                                                                </div>
                                                                                                <h5>TSD and Work 01</h5>
                                                                                            </div>
                                                                                            <div className="back flip-back-bg feature feature-3 boxed boxed--sm boxed--border">
                                                                                                <ul className="flip-list">
                                                                                                    <li className="text-left">
                                                                                                        Number of questions <span className="pull-right">100</span>
                                                                                                    </li>
                                                                                                    <li className="text-left">
                                                                                                        Time <span className="pull-right">60 mins</span>
                                                                                                    </li>
                                                                                                    <li className="text-center">
                                                                                                        <a className="btn btn--sm type--uppercase flip-bg-white" href="tests.html">
                                    <span className="btn__text">
                                      Buy now
                                    </span>
                                                                                                        </a>
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </div>
                                                                                            {/*end feature*/}
                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="col-sm-4 col-md-3 flip-container">
                                                                                        <div className="flipper">
                                                                                            <div className="front feature feature-3 boxed boxed--sm boxed--border">
                                                                                                <div className="icon-container text-center">
                                                                                                    <i className="icon icon--lg icon-Notepad" />
                                                                                                </div>
                                                                                                <h5>Ratio &amp; Proportion 01</h5>
                                                                                            </div>
                                                                                            <div className="back flip-back-bg feature feature-3 boxed boxed--sm boxed--border">
                                                                                                <ul className="flip-list">
                                                                                                    <li className="text-left">
                                                                                                        Number of questions <span className="pull-right">100</span>
                                                                                                    </li>
                                                                                                    <li className="text-left">
                                                                                                        Time <span className="pull-right">60 mins</span>
                                                                                                    </li>
                                                                                                    <li className="text-center">
                                                                                                        <a className="btn btn--sm type--uppercase flip-bg-white" href="tests.html">
                                    <span className="btn__text">
                                      Buy now
                                    </span>
                                                                                                        </a>
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </div>
                                                                                            {/*end feature*/}
                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="col-sm-4 col-md-3 flip-container">
                                                                                        <div className="flipper">
                                                                                            <div className="front feature feature-3 boxed boxed--sm boxed--border">
                                                                                                <div className="icon-container text-center">
                                                                                                    <i className="icon icon--lg icon-Notepad" />
                                                                                                </div>
                                                                                                <h5>Algebra 01</h5>
                                                                                            </div>
                                                                                            <div className="back flip-back-bg feature feature-3 boxed boxed--sm boxed--border">
                                                                                                <ul className="flip-list">
                                                                                                    <li className="text-left">
                                                                                                        Number of questions <span className="pull-right">100</span>
                                                                                                    </li>
                                                                                                    <li className="text-left">
                                                                                                        Time <span className="pull-right">60 mins</span>
                                                                                                    </li>
                                                                                                    <li className="text-center">
                                                                                                        <a className="btn btn--sm type--uppercase flip-bg-white" href="tests.html">
                                    <span className="btn__text">
                                      Buy now
                                    </span>
                                                                                                        </a>
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </div>
                                                                                            {/*end feature*/}
                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="col-sm-4 col-md-3 flip-container">
                                                                                        <div className="flipper">
                                                                                            <div className="front feature feature-3 boxed boxed--sm boxed--border">
                                                                                                <div className="icon-container text-center">
                                                                                                    <i className="icon icon--lg icon-Notepad" />
                                                                                                </div>
                                                                                                <h5>Geometry 01</h5>
                                                                                            </div>
                                                                                            <div className="back flip-back-bg feature feature-3 boxed boxed--sm boxed--border">
                                                                                                <ul className="flip-list">
                                                                                                    <li className="text-left">
                                                                                                        Number of questions <span className="pull-right">100</span>
                                                                                                    </li>
                                                                                                    <li className="text-left">
                                                                                                        Time <span className="pull-right">60 mins</span>
                                                                                                    </li>
                                                                                                    <li className="text-center">
                                                                                                        <a className="btn btn--sm type--uppercase flip-bg-white" href="tests.html">
                                    <span className="btn__text">
                                      Buy now
                                    </span>
                                                                                                        </a>
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </div>
                                                                                            {/*end feature*/}
                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="col-sm-4 col-md-3 flip-container">
                                                                                        <div className="flipper">
                                                                                            <div className="front feature feature-3 boxed boxed--sm boxed--border">
                                                                                                <div className="icon-container text-center">
                                                                                                    <i className="icon icon--lg icon-Notepad" />
                                                                                                </div>
                                                                                                <h5>Modern Maths 01</h5>
                                                                                            </div>
                                                                                            <div className="back flip-back-bg feature feature-3 boxed boxed--sm boxed--border">
                                                                                                <ul className="flip-list">
                                                                                                    <li className="text-left">
                                                                                                        Number of questions <span className="pull-right">100</span>
                                                                                                    </li>
                                                                                                    <li className="text-left">
                                                                                                        Time <span className="pull-right">60 mins</span>
                                                                                                    </li>
                                                                                                    <li className="text-center">
                                                                                                        <a className="btn btn--sm type--uppercase flip-bg-white" href="tests.html">
                                    <span className="btn__text">
                                      Buy now
                                    </span>
                                                                                                        </a>
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </div>
                                                                                            {/*end feature*/}
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                {/*end of tabs container*/}
                                                            </div>
                                                        </div>
                                                        {/*end of row*/}
                                                    </div>
                                                    {/*end of container*/}
                                                </section>
                                            </div>
                                            <div id="account-college-tests" className="hidden account-tab">
                                                <h3><strong>College Tests</strong></h3>
                                                <div className="row">
                                                    <div className="col-sm-4">
                                                        <div className="feature feature-1 boxed boxed--border">
                                                            <h3>Beat Placement 01</h3>
                                                            <a className="btn btn--primary" href="#">
                                                                <span className="btn__text">Go to Test</span>
                                                            </a>
                                                        </div>
                                                        {/*end feature*/}
                                                    </div>
                                                    <div className="col-sm-4">
                                                        <div className="feature feature-1 boxed boxed--border">
                                                            <h3>Beat Placement 02</h3>
                                                            <a className="btn btn--primary" href="#">
                                                                <span className="btn__text">Go to Test</span>
                                                            </a>
                                                        </div>
                                                        {/*end feature*/}
                                                    </div>
                                                    <div className="col-sm-4">
                                                        <div className="feature feature-1 boxed boxed--border">
                                                            <h3>Beat Placement 03</h3>
                                                            <a className="btn btn--primary" href="#">
                                                                <span className="btn__text">Go to Test</span>
                                                            </a>
                                                        </div>
                                                        {/*end feature*/}
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="account-college-packages" className="hidden account-tab">
                                                <h3><strong>College Packages</strong></h3>
                                                <div className="row">
                                                    <div className="col-sm-6">
                                                        <div className="pricing pricing-1 boxed boxed--lg boxed--border">
                                                            <h3>Starter</h3>
                                                            <span className="h2">
                                                                  <span className="pricing__dollar">₹</span><strong>149</strong>
                                                                </span>
                                                            <span className="type--fine-print">One Time, INR.</span>
                                                            <span className="label">Value</span>
                                                            <hr />
                                                            <ul>
                                                                <li>
                                                                    <span className="checkmark bg--primary-1" />
                                                                    <span>20+ Topic Tests</span>
                                                                </li>
                                                                <li>
                                                                    <span className="checkmark bg--primary-1" />
                                                                    <span>Competitive Questions</span>
                                                                </li>
                                                                <li>
                                                                    <span className="checkmark bg--primary-1" />
                                                                    <span>Score Analysis</span>
                                                                </li>
                                                                <li>
                                                                    <span className="checkmark bg--primary-1" />
                                                                    <span>Feature One</span>
                                                                </li>
                                                            </ul>
                                                            <a className="btn btn--primary" href="#">
                                                                  <span className="btn__text">
                                                                    Purchase Plan
                                                                  </span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div className="col-sm-6">
                                                        <div className="pricing pricing-1 boxed boxed--lg boxed--border">
                                                            <h3>Champion</h3>
                                                            <span className="h2">
                                                                  <span className="pricing__dollar">₹</span><strong>249</strong>
                                                                </span>
                                                            <span className="type--fine-print">One Time, INR.</span>
                                                            <hr />
                                                            <ul>
                                                                <li>
                                                                    <span className="checkmark bg--primary-1" />
                                                                    <span><em>Starter</em> Features</span>
                                                                </li>
                                                                <li>
                                                                    <span className="checkmark bg--primary-1" />
                                                                    <span>10+ Mock Tests</span>
                                                                </li>
                                                                <li>
                                                                    <span className="checkmark bg--primary-1" />
                                                                    <span>Performance Analysis</span>
                                                                </li>
                                                                <li>
                                                                    <span className="checkmark bg--primary-1" />
                                                                    <span>24/7 Phone Support</span>
                                                                </li>
                                                            </ul>
                                                            <a className="btn btn--primary" href="#">
                                                                  <span className="btn__text">
                                                                    Purchase Plan
                                                                  </span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="account-notifications" className="hidden account-tab">
                                                <h4>Email Notifications</h4>
                                                <p>Select the frequency with which you'd like to recieve product summary emails:</p>
                                                <form>
                                                    <div className="row">
                                                        <div className="boxed bg--secondary boxed--border">
                                                            <div className="col-xs-4 text-center">
                                                                <div className="input-radio">
                                                                    <span>Never</span>
                                                                    <input type="radio" name="frequency" defaultValue="never" className="validate-required" />
                                                                    <label />
                                                                </div>
                                                            </div>
                                                            <div className="col-xs-4 text-center">
                                                                <div className="input-radio checked">
                                                                    <span>Weekly</span>
                                                                    <input type="radio" name="frequency" defaultValue="weekly" className="validate-required" />
                                                                    <label />
                                                                </div>
                                                            </div>
                                                            <div className="col-xs-4 text-center">
                                                                <div className="input-radio">
                                                                    <span>Monthly</span>
                                                                    <input type="radio" name="frequency" defaultValue="monthly" className="validate-required" />
                                                                    <label />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-4 col-sm-5">
                                                            <button type="submit" className="btn btn--primary type--uppercase">Save Preferences</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div id="account-password" className="hidden account-tab">
                                                <h4>Password</h4>
                                                <p>Passwords must be at least 6 characters in length.</p>
                                                <form>
                                                    <div className="row">
                                                        <div className="col-xs-12">
                                                            <label>Old Password:</label>
                                                            <input type="password" name="old-password" />
                                                        </div>
                                                        <div className="col-sm-6">
                                                            <label>New Password:</label>
                                                            <input type="password" name="new-password" />
                                                        </div>
                                                        <div className="col-sm-6">
                                                            <label>Retype New Password:</label>
                                                            <input type="password" name="new-password-confirm" />
                                                        </div>
                                                        <div className="col-md-3 col-sm-4">
                                                            <button type="submit" className="btn btn--primary type--uppercase">Save Password</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <footer className="footer-3 text-center-xs space--xs ">
                            <div className="container">
                                <div className="row">
                                    <div className="col-sm-6">
                                        <img alt="Image" className="logo" src="/img/logo-dark.png" />
                                        <ul className="list-inline list--hover">
                                            <li>
                                                <a href="#">
                                                    <span className="type--fine-print">Get Started</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <span className="type--fine-print">help@stack.io</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="col-sm-6 text-right text-center-xs">
                                        <ul className="social-list list-inline list--hover">
                                            <li>
                                                <a href="#">
                                                    <i className="socicon socicon-google icon icon--xs" />
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i className="socicon socicon-twitter icon icon--xs" />
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i className="socicon socicon-facebook icon icon--xs" />
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i className="socicon socicon-instagram icon icon--xs" />
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-sm-6">
                                        <p className="type--fine-print">
                                            Supercharge your web workflow
                                        </p>
                                    </div>
                                    <div className="col-sm-6 text-right text-center-xs">
                                          <span className="type--fine-print">©
                                            <span className="update-year" /> Stack Inc.</span>
                                        <a className="type--fine-print" href="#">Privacy Policy</a>
                                        <a className="type--fine-print" href="#">Legal</a>
                                    </div>
                                </div>
                            </div>
                        </footer>
                    </div>
                </div>
            );
        } else {
            if (this.state.isUserLoading) {
                return(<div>Loading User</div>);
            } else {
                return (
                    <NotFound/>
                );
            }
        }
    }
}

ProfileNewContainer.propTypes = {
    loginPanelVisibility: PropTypes.string,
    registrationPanelVisibility: PropTypes.string,
    userMenuVisibility: PropTypes.string,
    user: PropTypes.object,
    isUserLoading: PropTypes.bool
}

function mapStateToProps(state, props) {
    return {
        loginPanelVisibility: state.loginPanelVisibility,
        registrationPanelVisibility: state.registrationPanelVisibility,
        userMenuVisibility: state.userMenuVisibility,
        user: state.user,
        isUserLoading: state.isUserLoading
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(action, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfileNewContainer);
