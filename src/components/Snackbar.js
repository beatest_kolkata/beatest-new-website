import React, { Component } from 'react';

class Snackbar extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
		return (
			<div id="snackbar" className={this.props.className}>
                {this.props.contentHtml}
            </div>
        );
    }
}

export default Snackbar;
