import React, { PropTypes, Component } from 'react';
import TestOption from './TestOption';
import $ from 'jquery';
import * as action from '../actions/action';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {setUserData, loadMockTests, loadTopicTests} from '../actions/TestsAction';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

import Loader from './Loader';
import Header from './Common/InternalHeader';
import Footer from './Common/InternalFooter';
import MockTest from './TestsList/MockTest';
import TopicTest from './TestsList/TopicTest';

const createCookie = require('./cookie.js').createCookie;
const readCookie = require('./cookie.js').readCookie;

class TestsList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            catMockTests: [],
            catTopicTests: [],
            ibpsMockTests: [],
            ibpsTopicTests: [],
            sbiMockTests: [],
            sbiTopicTests: []
        }
    }

    getUser() {
        if(readCookie('userData') !== null) {
            this.props.setUserData(JSON.parse(readCookie('userData')));
            this.getTest(this);
        } else {
            $.get('/api/user', (data) => {
                if (typeof data === 'object') {
                    createCookie('userData', JSON.stringify(data.user), 7);
                    this.props.setUserData(data.user);
                    this.getTest(this);
                } else {
                    window.location.pathname = '/';
                }
            });
        }
    }

    getTest() {
        $.get('/api/tests', (data) => {
            var tests = this.groupBy(data, 'type');
            if(this.props.routeParams.testType !== undefined) {
                if(this.props.routeParams.testType === 'cat' && tests.hasOwnProperty('CAT')) {
                    var catTests = this.groupBy(tests.CAT, 'character');
                    if(catTests.hasOwnProperty('Mock')) this.setState({ catMockTests: catTests.Mock });
                    if(catTests.hasOwnProperty('Topic')) this.setState({ catTopicTests: catTests.Topic });
                } else if(this.props.routeParams.testType === 'ibps' && tests.hasOwnProperty('IBPS')) {
                    var ibpsTests = this.groupBy(tests.IBPS, 'character');
                    if(ibpsTests.hasOwnProperty('Mock')) this.setState({ ibpsMockTests: ibpsTests.Mock });
                    if(ibpsTests.hasOwnProperty('Topic')) this.setState({ ibpsTopicTests: ibpsTests.Topic });
                } else if(this.props.routeParams.testType === 'sbi' && tests.hasOwnProperty('SBI')) {
                    var sbiTests = this.groupBy(tests.SBI, 'character');
                    if(sbiTests.hasOwnProperty('Mock')) this.setState({ sbiMockTests: sbiTests.Mock });
                    if(sbiTests.hasOwnProperty('Topic')) this.setState({ sbiTopicTests: sbiTests.Topic });
                }
            } else {
                if(tests.hasOwnProperty('CAT')) {
                    var catTests = this.groupBy(tests.CAT, 'character');
                    if(catTests.hasOwnProperty('Mock')) this.setState({ catMockTests: catTests.Mock });
                    if(catTests.hasOwnProperty('Topic')) this.setState({ catTopicTests: catTests.Topic });
                }
                if(tests.hasOwnProperty('IBPS')) {
                    var ibpsTests = this.groupBy(tests.IBPS, 'character');
                    if(ibpsTests.hasOwnProperty('Mock')) this.setState({ ibpsMockTests: ibpsTests.Mock });
                    if(ibpsTests.hasOwnProperty('Topic')) this.setState({ ibpsTopicTests: ibpsTests.Topic });
                }
                if(tests.hasOwnProperty('SBI')) {
                    var sbiTests = this.groupBy(tests.SBI, 'character');
                    if(sbiTests.hasOwnProperty('Mock')) this.setState({ sbiMockTests: sbiTests.Mock });
                    if(sbiTests.hasOwnProperty('Topic')) this.setState({ sbiTopicTests: sbiTests.Topic });
                }
            }
            this.setState({ loading: false });
        });
    }

    groupBy(xs, key) {
        return xs.reduce(function(rv, x) {
            (rv[x[key]] = rv[x[key]] || []).push(x);
            return rv;
        }, {});
    }

    componentDidMount() {
        this.getUser(this);
    }

    render() {
        if(this.state.loading) {
            return <Loader />
        } else {
            return (
                <div className="testslist-container">
                    <a id="start"></a>
                    <Header transparent={true} />
                    <div className="main-container">
                        <section className="text-center imagebg" data-overlay="4">
                            <div className="background-image-holder background--bottom">
                                <img alt="background" src="/img/landing-3.jpg" />
                            </div>
                            <div className="container">
                                <div className="row">
                                    <div className="col-sm-12">
                                        <h1><strong>Tests</strong></h1>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section id="tests" className=" bg--secondary text-center">
                            <div className="container">
                                <div className="row">
                                    <div className="col-sm-12">
                                        <div className="tabs-container">
                                            <Tabs>
                                                <TabList className="testslist-tablist">
                                                    <Tab className="testslist-tablist-tab">
                                                        <div className="tab__title">
                                                            <span className="h5">Mock Tests</span>
                                                        </div>
                                                    </Tab>
                                                    <Tab className="testslist-tablist-tab">
                                                        <div className="tab__title">
                                                            <span className="h5">Topic Tests</span>
                                                        </div>
                                                    </Tab>
                                                </TabList>
                                                <TabPanel>
                                                    <div className="tab__content">
                                                        <div className="row">
                                                            <div className="col-sm-6 col-md-4 col-md-offset-2">
                                                                <div className="pricing pricing-1 boxed boxed--lg boxed--border">
                                                                    <i className="icon icon--lg icon-Notepad"></i>
                                                                    <h4>Mock Exam</h4>
                                                                    <ul className="text-left">
                                                                        <li>
                                                                            <span className="checkmark bg--primary-1"></span>
                                                                            <span>20 comprehensive mocks designed by expert faculties which are set according to the latest pattern and test syllabus.</span>
                                                                        </li>
                                                                        <li>
                                                                            <span className="checkmark bg--primary-1"></span>
                                                                            <span>Mocks designed covering all the topics and sections of the IBPS exam.</span>
                                                                        </li>
                                                                        <li>
                                                                            <span className="checkmark bg--primary-1"></span>
                                                                            <span>Up-to-date question content reflects the reality of what you’ll see on test day </span>
                                                                        </li>
                                                                        <li>
                                                                            <span className="checkmark bg--primary-1"></span>
                                                                            <span>TAKE THE 1st MOCK FOR FREE </span>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <div className="col-sm-6 col-md-4">
                                                                <div className="pricing pricing-1 boxed boxed--lg boxed--border">
                                                                    <i className="icon icon--lg icon-Notepad"></i>
                                                                    <h4>Analysis</h4>
                                                                    <ul className="text-left">
                                                                        <li>
                                                                            <span className="checkmark bg--primary-1"></span>
                                                                            <span>Analyze your strengths and weakness with your personalized analysis</span>
                                                                        </li>
                                                                        <li>
                                                                            <span className="checkmark bg--primary-1"></span>
                                                                            <span>Get a detailed solution to each and every question </span>
                                                                        </li>
                                                                        <li>
                                                                            <span className="checkmark bg--primary-1"></span>
                                                                            <span>Join thousands of others in taking the mocks today and be a part of the race. </span>
                                                                        </li>
                                                                        <li>
                                                                            <span className="checkmark bg--primary-1"></span>
                                                                            <span>Join thousands of others in taking the mocks today and be a part of the race. </span>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr />
                                                        {this.state.catMockTests.length ? (
                                                            <div>
                                                                <h3>CAT</h3>
                                                                <MockTest data={this.state.catMockTests} />
                                                            </div>
                                                        ) : (null)}
                                                        {this.state.ibpsMockTests.length ? (
                                                            <div>
                                                                <h3>IBPS</h3>
                                                                <MockTest data={this.state.ibpsMockTests} />
                                                            </div>
                                                        ) : (null)}
                                                        {this.state.sbiMockTests.length ? (
                                                            <div>
                                                                <h3>SBI</h3>
                                                                <MockTest data={this.state.sbiMockTests} />
                                                            </div>
                                                        ) : (null)}
                                                    </div>
                                                </TabPanel>
                                                <TabPanel>
                                                    <div className="tab__content">
                                                        <div className="row">
                                                            <div className="col-sm-6 col-md-4 col-md-offset-2">
                                                                <div className="pricing pricing-1 boxed boxed--lg boxed--border">
                                                                    <i className="icon icon--lg icon-Notepad"></i>
                                                                    <h4>Topic Exam</h4>
                                                                    <ul className="text-left">
                                                                        <li>
                                                                            <span className="checkmark bg--primary-1"></span>
                                                                            <span> Questions from each topic based on the latest syllabus </span>
                                                                        </li>
                                                                        <li>
                                                                            <span className="checkmark bg--primary-1"></span>
                                                                            <span> Short tests to test your skill set in each topic </span>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <div className="col-sm-6 col-md-4">
                                                                <div className="pricing pricing-1 boxed boxed--lg boxed--border">
                                                                    <i className="icon icon--lg icon-Notepad"></i>
                                                                    <h4>Analysis</h4>
                                                                    <ul className="text-left">
                                                                        <li>
                                                                            <span className="checkmark bg--primary-1"></span>
                                                                            <span>Time yourself on each topic to excel your skills </span>
                                                                        </li>
                                                                        <li>
                                                                            <span className="checkmark bg--primary-1"></span>
                                                                            <span>Review detailed answer explanations so you can study smarter</span>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr />
                                                        {this.state.catTopicTests.length ? (
                                                            <div>
                                                                <h3>CAT</h3>
                                                                <TopicTest data={this.state.catTopicTests} />
                                                            </div>
                                                        ) : (null)}
                                                        {this.state.ibpsTopicTests.length ? (
                                                            <div>
                                                                <h3>IBPS</h3>
                                                                <TopicTest data={this.state.ibpsTopicTests} />
                                                            </div>
                                                        ) : (null)}
                                                        {this.state.sbiTopicTests.length ? (
                                                            <div>
                                                                <h3>SBI</h3>
                                                                <TopicTest data={this.state.sbiTopicTests} />
                                                            </div>
                                                        ) : (null)}
                                                    </div>
                                                </TabPanel>
                                            </Tabs>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <Footer />
                    <a className="back-to-top inner-link" href="#start" data-scroll-className="100vh:active">
                        <i className="stack-interface stack-up-open-big"></i>
                    </a>
                </div>
            );
        }
    }
}

const mapStateToProps = state =>{
    return{
        mocktest: state.mocktest,
        topictest: state.topictest
    };
}
const mapDispatchToProps = dispatch => {
    return bindActionCreators ({
        setUserData,
        loadMockTests,
        loadTopicTests
    }, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(TestsList);