import React, { Component } from 'react';
import $ from 'jquery';

class UserMenu extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.logout = this.logout.bind(this);
    }

    logout() {
        $.ajax({
            url: '/api/logout',
            dataType: 'json',
            type: 'POST',
            contentType: 'application/json'
        }).then((data) => {
            this.props.toggleUserMenuVisibility();
            this.props.logout();
        }).then(() => {
            window.location.href = "/index";
        });
    }

    render() {
        var className = 'user-menu ' + this.props.userMenuVisibility;
		return (
			<div className={className}>
                <div className="arrow"></div>
				<a href="/profile"><li>Profile</li></a>
                <a href="/user_profile"><li>College Profile</li></a>
				<li onClick={this.logout}>Logout</li>
            </div>
        );
    }
}

export default UserMenu;
