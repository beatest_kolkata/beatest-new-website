/* eslint-disable */
import React, { Component } from 'react';
import {browserHistory} from "react-router";
import $ from 'jquery';
var mixpanel = require('mixpanel-browser');
mixpanel.init("86e70fa37444d5ab81c1ea058ae2fcd4");

let collegeUserPanelStyle = {
    marginTop: '85px'
};

class CollegeRegisterUser extends Component {
    constructor(props) {
        super(props);
        this.login = this.login.bind(this);
        this.signup = this.signup.bind(this);
    }
        login(e) {
        e.preventDefault();
        var email = $('#email').val();
        mixpanel.identify(email);
        mixpanel.track("User Logged in");
        var password = $('#password').val();
        $('#login-form')[0].reset();
        $.ajax({
            url: '/api/user_login',
            dataType: 'json',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                email,
                password
            })
        }).then(function(data) {
            if(data.hasOwnProperty('invalid')) {
                if(data.invalid === 'Email') {
                    $('#user-panel-validator').html("Sorry, Beatest doesn't recognize that email.");
                } else if(data.invalid === 'No other details') {
                    this.props.user_edit();
                }
                  else if (data.invalid === 'Password') {
                    $('#user-panel-validator').html("Wrong password. Try again.");
                } else if (data.invalid === 'User') {
                    $('#registration-panel-validator').html("Please add a password for your account.");
                    $('#new-full-name').val(data.name);
                    $('#new-email').val(data.email);
                }

            } else if (data.hasOwnProperty('user')) {
                this.props.login(data.user);
                console.log('hi')
                console.log(data.user);
                location.reload();
                browserHistory.push("/user_profile");
            }
        }.bind(this));
    }

    signup(e) {
        e.preventDefault();
        var full_name = $('#new-student-full-name').val();
        var email = $('#new-student-email').val();
        var phone_no = $('#new-student-phone').val();
        var password = $('#new-student-password').val();
        var college_id=this.props.collegeId
        $('#register-college-form')[0].reset();
        if(true) {
            $.ajax({
                url: '/api/signup/'+this.props.college,
                dataType: 'json',
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify({
                    full_name,
                    email,
                    phone_no,
                    password
                })
            }).then(function(data) {
                if(data.invalid=='Invalid Phone No.')
                {
                    $('#register-college-validator').html("Invalid Phone No");

                }
                else if(data.invalid=='User Exists')
                {
                    $('#register-college-validator').html("User already exists");

                }
                else if(data.hasOwnProperty('user')){
                this.props.login(data.user);
                location.reload();
                browserHistory.push("/user_profile");}

                alert('Please visit your mail to activate your Beatest Account!');

            }.bind(this));
        }
     var today = new Date();
    mixpanel.people.set({
            "$name": full_name,
            "$email": email,
        "$created": String(today)
    });
    mixpanel.identify(email);
    mixpanel.track("User Signed in");
    }


    render() {
        return (
            <div>
                <div className='college-user-panel' style={collegeUserPanelStyle}>
                    <form id="register-college-form" onSubmit={this.signup}>
                        <input id="new-student-full-name" className="register-college-input" type="text" placeholder="Name" required />
                        <input id="new-student-email" className="register-college-input" type="email" placeholder="Email" required />
                        <input id="new-student-phone" className="register-college-input" type="tel" placeholder="Phone No." required />
                        <input id="new-student-password" className="register-college-input" type="password" placeholder="Password" required />
                        <div><label id="register-college-validator"></label></div>
                        <button className="register-college-button">SignUp</button>
                    </form>
                    <p>Have an account? <a onClick={this.props.toggleLoginPanelVisibility}>Login</a></p>
                </div>
            </div>
        );
    }
}

export default CollegeRegisterUser;
