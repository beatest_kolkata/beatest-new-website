import React, { PropTypes, Component } from 'react';
import $ from 'jquery';
import * as action from '../actions/action';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import NavBar from './NavBar';
import Header from './Common/InternalHeader';
import Footer from './Common/InternalFooter';
import {CampusPartners} from "./Home/CampusPartners";
import { loadFeatures, loadCourses, loadCampusPartners, loadTestimonials } from '../actions/HomeAction';

const initialState = {
    user: null,
    isUserLogged: false
};

const collegeBannerStyle = {
    paddingTop: '20px',
    marginBottom: '-80px',
    paddingLeft: '50px',
    opacity: '0.85'
};

const h1Style = {
    borderBottom: 'none',
    marginTop: '10px',
    marginLeft: '-40px'
};

class CollegeSignup extends Component {
    constructor(props) {
        super(props);
        this.state = initialState;
        this.getUser();
    }

    componentDidMount() {
        this.props.loadCampusPartners();
    }

    getUser() {
        //console.log("Hi");
        $.get('/api/user', (data) => {
            if (typeof data === 'object') {
                this.state.user = data.user;
            }
        });
    }

    render() {
        var bgImgHolder = {
            opacity: 1, 
            background: 'url("/img/political-1.jpg")'
        }
        return (
            <div>
                <a id="start"></a>
                <Header transparent={true} />
                <div className="main-container">
                <section className="cover height-50 videobg imagebg" data-overlay="4">
                <div className="background-image-holder" style={bgImgHolder}>
                    <img alt="background" src="/img/blog-6.jpg" />
                </div>
                <video autoplay="" loop="" muted="">
                    <source src="video/video.webm" type="video/webm" />
                    <source src="video/video.mp4" type="video/mp4" />
                    <source src="video/video.ogv" type="video/ogv" />
                </video>
                <div className="container pos-vertical-center">
                    <div className="row">
                        <div className="col-sm-12">
                            <h2 className="h2--small">
                                Beatest now for Colleges!
                            </h2>
                            <h4 className="h4--small">
                                The only aspect of college that worries you is the one that we strive to make a cakewalk.
                                That's right, Placements! Here at Beatest, we provide you with mock aptitude exams
                                to prepare you well for the dreaded P-word.
                            </h4>

                        </div>
                    </div>
                </div>
            </section>

            <section className="text-center space--xs">
                <div className="container">
                    <div className="row">
                        {/*<div className="col-sm-12 text-center space--xs space-top-0">
                            <h2>Student Partner</h2>
                        </div>*/}

                        {(() => {
                            if(this.props.params.collegeId == 10) {
                                return <div className="row" style={collegeBannerStyle}>
                                    <div className="col-lg-1"></div>
                                    <div className="col-lg-2"><img src="../../colleges/iit_kgp.png" width="100" height="100" /></div>
                                    <div className="col-lg-8"><h1 style={h1Style}>Welcome, students of IIT Kharagpur!</h1></div>
                                    <div className="col-lg-1"></div>
                                </div>
                            }
                        })()}

                        {(() => {
                            if(this.props.params.collegeId == 6) {
                                return <div className="row" style={collegeBannerStyle}>
                                    <div className="col-lg-1"></div>
                                    <div className="col-lg-3"><img src="../../colleges/iem.png" width="100" height="100" /><img src="../../colleges/uem-logo.png" width="100" height="100" /></div>
                                    <div className="col-lg-7"><h1 style={h1Style}>Welcome, students of IEM &amp; UEM Kolkata!</h1></div>
                                    <div className="col-lg-1"></div>
                                </div>
                            }
                        })()}

                        {(() => {
                            if(this.props.params.collegeId == 13) {
                                return <div className="row" style={collegeBannerStyle}>
                                    <div className="col-lg-1"></div>
                                    <div className="col-lg-2"><img src="../../colleges/msit-techno.png" width="100" height="100" /></div>
                                    <div className="col-lg-7"><h1 style={h1Style}>Welcome, students of MSIT (Techno Group)</h1></div>
                                    <div className="col-lg-2"></div>
                                </div>
                            }
                        })()}

                        {(() => {
                            if(this.props.params.collegeId == 19) {
                                return <div className="row" style={collegeBannerStyle}>
                                    <div className="col-lg-2"></div>
                                    <div className="col-lg-2"><img src="../../colleges/iiest.png" width="100" height="100" /></div>
                                    <div className="col-lg-6"><h1 style={h1Style}>Welcome, students of IIEST, Shibpur</h1></div>
                                    <div className="col-lg-2"></div>
                                </div>
                            }
                        })()}

                        {(() => {
                            if(this.props.params.collegeId == 22) {
                                return <div className="row" style={collegeBannerStyle}>
                                    <div className="col-lg-2"></div>
                                    <div className="col-lg-2"><img src="../../colleges/iitbhu.png" width="100" height="100" /></div>
                                    <div className="col-lg-6"><h1 style={h1Style}>Welcome, students of IIT BHU</h1></div>
                                    <div className="col-lg-2"></div>
                                </div>
                            }
                        })()}

                        {(() => {
                            if(this.props.params.collegeId == 23) {
                                return <div className="row" style={collegeBannerStyle}>
                                    <div className="col-lg-2"></div>
                                    <div className="col-lg-2"><img src="../../colleges/christ.png" width="100" height="100" /></div>
                                    <div className="col-lg-7"><h1 style={h1Style}>Welcome, students of Christ University</h1></div>
                                    <div className="col-lg-1"></div>
                                </div>
                            }
                        })()}

                        {(() => {
                            if(this.props.params.collegeId == 24) {
                                return <div className="row" style={collegeBannerStyle}>
                                    <div className="col-lg-2"></div>
                                    <div className="col-lg-2"><img src="../../colleges/iit_kanpur.png" width="100" height="100" /></div>
                                    <div className="col-lg-7"><h1 style={h1Style}>Welcome, students of IIT Kanpur</h1></div>
                                    <div className="col-lg-1"></div>
                                </div>
                            }
                        })()}

                        {(() => {
                            if(this.props.params.collegeId == 25) {
                                return <div className="row" style={collegeBannerStyle}>
                                    <div className="col-lg-2"></div>
                                    <div className="col-lg-2"><img src="../../colleges/ssc_delhi.png" width="100" height="100" /></div>
                                    <div className="col-lg-7"><h1 style={h1Style}>Welcome, students of St. Stephen's College</h1></div>
                                    <div className="col-lg-1"></div>
                                </div>
                            }
                        })()}

                        {(() => {
                            if(this.props.params.collegeId == 27) {
                                return <div className="row" style={collegeBannerStyle}>
                                    <div className="col-lg-2"></div>
                                    <div className="col-lg-2"><img src="../../colleges/cmrit.png" width="100" height="100" /></div>
                                    <div className="col-lg-7"><h1 style={h1Style}>Welcome, students of CMR Institute of Technology</h1></div>
                                    <div className="col-lg-1"></div>
                                </div>
                            }
                        })()}

                        {(() => {
                            if(this.props.params.collegeId == 28) {
                                return <div className="row" style={collegeBannerStyle}>
                                    <div className="col-lg-4"><img src="../../colleges/bits-goa.png" width="300" height="100" /></div>
                                    <div className="col-lg-8"><h1 style={h1Style}>Welcome, students of BITS Pilani, Goa Campus</h1></div>
                                </div>
                            }
                        })()}

                        {(() => {
                            if(this.props.params.collegeId == 31) {
                                return <div className="row" style={collegeBannerStyle}>
                                    <div className="col-lg-2"></div>
                                    <div className="col-lg-2"><img src="../../colleges/mhud.png" width="100" height="100" /></div>
                                    <div className="col-lg-7"><h1 style={h1Style}>Welcome, students of Miranda House</h1></div>
                                    <div className="col-lg-1"></div>
                                </div>
                            }
                        })()}

                        {(() => {
                            if(this.props.params.collegeId == 32) {
                                return <div className="row" style={collegeBannerStyle}>
                                    <div className="col-lg-2"></div>
                                    <div className="col-lg-2"><img src="../../colleges/nit-patna.png" width="100" height="100" /></div>
                                    <div className="col-lg-7"><h1 style={h1Style}>Welcome, students of NIT Patna</h1></div>
                                    <div className="col-lg-1"></div>
                                </div>
                            }
                        })()}

                        {(() => {
                            if(this.props.params.collegeId == 34) {
                                return <div className="row" style={collegeBannerStyle}>
                                    <div className="col-lg-2"></div>
                                    <div className="col-lg-2"><img src="../../colleges/mnit-jaipur.png" width="100" height="100" /></div>
                                    <div className="col-lg-7"><h1 style={h1Style}>Welcome, students of MNIT Jaipur</h1></div>
                                    <div className="col-lg-1"></div>
                                </div>
                            }
                        })()}

                        {(() => {
                            if(this.props.params.collegeId == 35) {
                                return <div className="row" style={collegeBannerStyle}>
                                    <div className="col-lg-2"></div>
                                    <div className="col-lg-2"><img src="../../colleges/srcc.png" width="100" height="100" /></div>
                                    <div className="col-lg-7"><h1 style={h1Style}>Welcome, students of SRCC, Delhi</h1></div>
                                    <div className="col-lg-1"></div>
                                </div>
                            }
                        })()}
                    </div>
                </div>
            </section>

            <section className="unpad elaborate-form-1">
                <div className="row row--gapless">
                    <div className="col-sm-6 height-50 bg--primary">
                        <div className="pos-vertical-center clearfix">
                            <div className="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                                <span className="h2">Join beatest</span>
                                <form>
                                    <div className="row">
                                        <div className="col-sm-12">
                                            <input type="text" name="name" placeholder="College Name" />
                                        </div>
                                        <div className="col-sm-6">
                                            <input type="email" name="email" placeholder="Email Address" />
                                        </div>
                                        <div className="col-sm-6">
                                            <input type="tel" name="number" placeholder="Contact Number" />
                                        </div>
                                        <div className="col-sm-12">
                                            <input type="password" name="password" placeholder="Password" />
                                        </div>
                                        <div className="col-sm-12 col-xs-12">
                                            <button type="submit" className="btn btn--primary-1">Register</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-6 height-50 bg--primary-1">
                        <div className="pos-vertical-center clearfix" id="college-feature">
                            <div className="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                                <h2 className="h2--small">Why Beatest?</h2>
                                <div className="pricing">
                                    <ul className="text-left">
                                        <li>
                                            <span className="checkmark bg--primary"></span>
                                            <span>Complete sets of mock questions with clear instructions and varying levels of difficulty. </span>
                                        </li>
                                        <li>
                                            <span className="checkmark bg--primary"></span>
                                            <span>well-explained, distinct solutions exclusively designed by experts with shortcut solutions. </span>
                                        </li>
                                        <li>
                                            <span className="checkmark bg--primary"></span>
                                            <span>personalized analysis of the performance including improvement tips on time management </span>
                                        </li>
                                        <li>
                                            <span className="checkmark bg--primary"></span>
                                            <span>Relative analysis of your performance </span>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </section>

            {/*<section className=" ">
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="slider slider--inline-arrows" data-arrows="true">
                                    <ul className="slides">
                                        <li>
                                            <div className="row">
                                                <div className="testimonial">
                                                    <div className="col-md-2 col-md-offset-1 col-sm-4 col-xs-6 text-center">
                                                        <img className="testimonial__image" alt="Image" src="img/avatar-round-1.png" />
                                                    </div>
                                                    <div className="col-md-7 col-md-offset-1 col-sm-8 col-xs-12">
                                                        <span className="h3">&ldquo;We’ve been using Stack to prototype designs quickly and efficiently. Needless to say we’re hugely impressed by the style and value.&rdquo;
                                                        </span>
                                                        <h5>Maguerite Holland</h5>
                                                        <span>Interface Designer &mdash; Yoke</span>
                                                    </div>
                                                </div>
                                            </div>

                                        </li>
                                        <li>
                                            <div className="row">
                                                <div className="testimonial">
                                                    <div className="col-md-2 col-md-offset-1 col-sm-4 col-xs-6 text-center">
                                                        <img className="testimonial__image" alt="Image" src="img/avatar-round-4.png" />
                                                    </div>
                                                    <div className="col-md-7 col-md-offset-1 col-sm-8 col-xs-12">
                                                        <span className="h3">&ldquo;I've been using Medium Rare's templates for a couple of years now and Stack is without a doubt their best work yet. It's fast, performant and absolutely stunning.&rdquo;
                                                        </span>
                                                        <h5>Lucas Nguyen</h5>
                                                        <span>Freelance Designer</span>
                                                    </div>
                                                </div>
                                            </div>

                                        </li>
                                        <li>
                                            <div className="row">
                                                <div className="testimonial">
                                                    <div className="col-md-2 col-md-offset-1 col-sm-4 col-xs-6 text-center">
                                                        <img className="testimonial__image" alt="Image" src="img/avatar-round-3.png" />
                                                    </div>
                                                    <div className="col-md-7 col-md-offset-1 col-sm-8 col-xs-12">
                                                        <span className="h3">&ldquo;Variant has been a massive plus for my workflow &mdash; I can now get live mockups out in a matter of hours, my clients really love it.&rdquo;
                                                        </span>
                                                        <h5>Rob Vasquez</h5>
                                                        <span>Interface Designer &mdash; Yoke</span>
                                                    </div>
                                                </div>
                                            </div>

                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>*/}

                {/*<CampusPartners data={this.props.campuspartners} />*/}

                </div>
                <Footer />
                <a className="back-to-top inner-link" href="#start" data-scroll-className="100vh:active">
                    <i className="stack-interface stack-up-open-big"></i>
                </a>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        campuspartners: state.campuspartners,
        user:state.user
    };
}


const matchDispatchToProps = dipatch => {
    return bindActionCreators({
        loadCampusPartners: loadCampusPartners
    }, dipatch);
};

export default connect(mapStateToProps, matchDispatchToProps)(CollegeSignup);