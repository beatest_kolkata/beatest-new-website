import React, {Component} from 'react';

export default class MockTest extends Component {
    render() {
        return (
            <div className="row text-center tests-flip-box">
                {this.props.data.map((mocktest, index) => {
                    return (
                        <div key={index} className="col-sm-4 col-md-3 flip-container">
                            <div className="flipper">
                                <div className="front feature feature-3 boxed boxed--sm boxed--border">
                                    <div className="icon-container">
                                        <i className="icon icon--lg icon-Notepad"></i>
                                    </div>
                                    <h5>{mocktest.name}</h5>
                                </div>
                                <div className="back flip-back-bg feature feature-3 boxed boxed--sm boxed--border">
                                    <ul className="flip-list">
                                        {mocktest.hasOwnProperty('number_of_ques') ? (
                                            <li className="text-left">
                                                Number of questions <span className="pull-right">{mocktest.number_of_ques}</span>
                                            </li>
                                        ) : (null)}
                                        {mocktest.hasOwnProperty('time') ? (
                                            <li className="text-left">
                                                Time <span className="pull-right"> {mocktest.time} mins</span>
                                            </li>
                                        ) : (null)}
                                        <li className="text-center">
                                            <a className="btn btn--sm type--uppercase flip-bg-white" href={"/test/" + mocktest.id}>
                                                <span className="btn__text">Start test</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                    
                            </div>
                        </div>
                    );
                })}
            </div>
        );
    }
}