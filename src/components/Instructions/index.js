import React, { Component } from 'react';
import classnames from 'classnames';
import InstructionsContainer from '../InstructionsContainer';
import './style.css';

class Instructions extends Component {
    render() {
        const { className, ...props } = this.props;
        return (
            <InstructionsContainer className={classnames('Instructions', className)} {...props}/>
        );
    }
}

export default Instructions;
