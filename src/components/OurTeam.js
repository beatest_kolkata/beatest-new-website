import React, { Component } from 'react';
import Navlink from './Navlink';
import UserPanel from './UserPanel';
import UserMenu from './UserMenu';
import Metatags from 'react-meta-tags';

import Header from './Common/InternalHeader';
import Footer from './Common/InternalFooter';

export default class OurTeam extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		return (
			<div>
			<a id="start"></a>
				<Header transparent={true} />
			<div class="main-container">
				<section className="text-center imagebg space--lg" data-overlay="3">
					<div className="background-image-holder">
						<img alt="background" src="/img/landing-21.jpg" />
					</div>
					<div className="container">
						<div className="row">
							<div className="col-sm-8 col-md-6">
								<h1>Hi, We're Beatest!</h1>
								<p className="lead">
									A groupof of innovative and like-minded folks making useful and
                                enduring technology for your education
                            </p>
								</div>
							</div>

						</div>

					</section>
					{/*<section className="text-center">
						<div className="container">
							<div className="row">
								<div className="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
									<h2>Built on passion and ingenuity</h2>
									<p className="lead">
										Medium Rare is an elite author known for offering high-quality, high-value products backed by timely and personable support. Recognised and awarded by Envato on multiple occasions for producing consistently outstanding products, it's no wonder over 20,000 customers enjoy using Medium Rare templates.
                            </p>
								</div>
							</div>

						</div>

					</section>*/}
					<section className="text-center bg--secondary">
						<div className="container">
							<div className="row">
								<div className="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
									<h2>What drives us</h2>
									<p className="lead">
										Whether you’re preparing for a competitive exam or for your placements,
                                we have your prep needs covered!
                            </p>
								</div>
							</div>

						</div>

					</section>
					<section className="bg--secondary">
						<div className="container">
							<div className="row">
								<div className="col-sm-4">
									<div className="feature">
										<h4>Fervor</h4>
										<p>
											We have a fervent desire to improve the quality of education and
                                    take mediocrity out of the system.
                                    We recognize the limitations and help you to overcome them.
                                </p>
									</div>
								</div>
								<div className="col-sm-4">
									<div className="feature">
										<h4>Ingenuity</h4>
										<p>
											With the zeal to innovate and explore new concepts that can change the
                                    lives of students for the better,
                                    we have a knack for novelty and creativity.
                                </p>
									</div>
								</div>
								<div className="col-sm-4">
									<div className="feature">
										<h4>Singularity</h4>
										<p>
											We share a vision to create an impact on the students across the community
                                    by a product that fulfills their educational requirements easily and effectively.
                                    Being one of a kind, we strive to offer a smarter way to learn.
                                </p>
									</div>
								</div>
							</div>

						</div>

					</section>
					<section className="text-center">
						<div className="container">
							<div className="row">
								<div className="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
									<h2>Meet the makers</h2>
								</div>
							</div>

						</div>

					</section>
					<section id="ourTeam" className="text-center">
						<div className="container">
							<div className="row team-primary">
								<div className="col-sm-6 col-md-3">
									<div className="feature feature-8 bg--secondary boxed boxed--sm boxed--border ">
										<img alt="Image" src="/img/team/team1.png" />
										<h5>Sayantan Chatterjee</h5>
										<p className="text-justify">
											Sayantan, is an alumnus of Jadavpur University, who dropped out of IIM Kozhikode and
                                    left out a cushy job at Oracle in order to become an entrepreneur.
                                </p>
									</div>
								</div>
								<div className="col-sm-6 col-md-3">
									<div className="feature feature-8 bg--secondary boxed boxed--sm boxed--border">
										<img alt="Image" src="/img/team/team5.jpg" />
										<h5>Harshvardhan Gupta</h5>
										<p className="text-justify">
											Harsh is currently pursuing his bachelor degree in Computer Science
                                    at the Rochester Institute of Technolgy. Having designed an adaptive
                                    test preparation app called Marks++.
                                </p>
									</div>
								</div>
								<div className="col-sm-6 col-md-3">
									<div className="feature feature-8 bg--secondary boxed boxed--sm boxed--border">
										<img alt="Image" src="/img/team/team4.png" />
										<h5>Aritra Hazra</h5>
										<p className="text-justify">
											Aritra is an alumnus of IIM-C and Jadavpur
                                    University and has worked with PwC.
                                    He has won many laurels in National level mathematical events.
                                </p>
									</div>
								</div>

								<div className="col-sm-6 col-md-3">
									<div className="feature feature-8 bg--secondary boxed boxed--sm boxed--border">
										<img alt="Image" src="/img/team/team3.png" />
										<h5>Avijit Banerjee</h5>
										<p className="text-justify">
											Avijit is an alumnus of NIT Trichy and had been
                                    associated with Gannon Dunkerley before he joined in at Beatest.
                                </p>
									</div>
								</div>
							</div>
							<div className="row team-secondry">
								<div className="col-sm-6 col-md-4 col-sm-offset-4 col-sm-offset-3">
									<div className="feature feature-8 bg--secondary boxed boxed--sm boxed--border">
										<img alt="Image" src="/img/team/sarveshkverma.jpg" />
										<h5>Sarvesh K. Verma</h5>
										<p className="text-justify">
											Sarvesh K. Verma, is an IIM-ite who is also the author of Quantum CAT
                                    and CATest Latest and is an ideal
                                    mentor cum advisor who has helped us tremendously in our journey.
                                </p>
									</div>
								</div>
							</div>

						</div>

					</section>
					<section className="text-center bg--secondary">
						<div className="container">
							<div className="row">
								<div className="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
									<div className="cta">
										<h2>We're always looking for talent</h2>
										<p className="lead">
											Got what it takes to work with us? Great! Send us a link to your resumé or portfolio to become part of our talent pool.
                                		</p>
										<a className="btn btn--primary type--uppercase" href="#">
											<span className="btn__text">
												See Job Openings
                                    </span>
										</a>
									</div>
								</div>
							</div>

						</div>
					</section>
				</div>
				<Footer />
				<a className="back-to-top inner-link" href="#start" data-scroll-className="100vh:active">
					<i className="stack-interface stack-up-open-big"></i>
				</a>
			</div>

		);
	}
}
