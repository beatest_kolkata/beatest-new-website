import React, { PropTypes, Component } from 'react';
import $ from 'jquery';
import { Route, Redirect } from 'react-router';
import { browserHistory } from 'react-router';
import _ from 'lodash';
import { api } from '../settings';
import Loader from './Loader';
import Header from './Common/InternalHeader';
import Footer from './Common/InternalFooter';
import NotFound from './NotFound';

class ProfileContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: null,
            loading: true,
            percentile: 0,
            rank: 'N/A',
            tests: [],
            collegeTests: [],
            courses: []
        };
        this.getUserProfile = this.getUserProfile.bind(this);
        this.getUserCollegeRankPercentile = this.getUserCollegeRankPercentile.bind(this);
    }

    getUser() {
        $.get('/api/user', (data) => {
            if (typeof data.user === 'object') {
                this.getUserProfile(data.user.id, data.user.college_id);
                //this.getUserCollegeRankPercentile(data.user.id, data.user.college_id);
                this.setState({
                    user: data.user
                });
                this.setState({ loading: false });
            } else {
                window.location.pathname = '/login';
            }
        });
    }

    getUserProfile(user_id, college_id) {
        $.get('/api/user_profile', (data) => {

            let courses = _.filter(data.tests, { 'type': 'COURSE' });

            this.setState({
                tests: data.tests,
                courses: courses
            });

            /*$.get('/api/get_college_tests/' + college_id + '/' + user_id, (data) => {
                this.setState({
                    collegeTests: data.tests
                });

                this.setState({ loading: false });
            });*/
        });
    }

    getUserCollegeRankPercentile(user_id, college_id) {
        api.get('/api/get_college_rank_percentile/' + user_id + '/' + college_id).then((response) => {
            let data = response.data;
            this.setState({ rank: data.rank, percentile: data.percentile });
        });
    }

    logout(event) {
        event.preventDefault();
        $.ajax({
            url: '/api/logout',
            dataType: 'json',
            type: 'POST',
            contentType: 'application/json'
        }).then((data) => {
            window.location.href = "/index";
        });
    }

    componentWillMount() {
        this.getUser(this);
    }

    render() {
        if(this.state.loading) {
            return <Loader />
        } else {
            if(this.state.user === null) {
                return (
                    <NotFound />
                );
            } else {
                return (
                    <div>
                        <a id="start"></a>
                        <Header transparent={true} user={this.state.user} />
                        <div className="main-container">
                            <section className="text-center imagebg" data-overlay="7">
                                <div className="background-image-holder background--bottom" style={{background: "url(/img/landing-3.jpg)", opacity: 1}}>
                                    <img src="/img/landing-3.jpg" />
                                </div>
                                <div className="container">
                                    <div className="row">
                                        <div className="col-sm-12">
                                            <h1><strong>User Profile</strong></h1>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section className="bg--secondary space--sm">
                                <div className="container">
                                    <div className="row">
                                        <div className="col-md-3">
                                            <div className="boxed boxed--lg boxed--border">
                                                <div className="text-block text-center">
                                                    <img alt="avatar" src="images/user.png" className="image--sm" />
                                                    <span className="h5">{this.state.user.name}</span>
                                                    <span>{this.state.user.college_name}</span>
                                                </div>
                                                <a className="btn bg--error btn--icon" onClick={this.logout} style={{marginTop: 0.2 + 'em'}}>
                                                <span className="btn__text">
                                                    <i className="icon-Power"></i> Logout</span>
                                                </a>
                                                <hr />
                                                <div className="text-block">
                                                    <h4>Percentile</h4>
                                                    {/*<p>{this.state.percentile}</p>*/}
                                                    <p>N/A</p>
                                                    <h4>Rank</h4>
                                                    {/*<p>{this.state.rank}</p>*/}
                                                    <p>N/A</p>
                                                </div>
                                                <hr />
                                                <a className="btn btn--primary btn--icon" href="" style={{marginTop: 0.2 + 'em'}}>
                                                <span className="btn__text">
                                                    <i className="icon-Sharethis"></i> Referral</span>
                                                </a>
                                            </div>
                                        </div>
                                        <div className="col-md-9">
                                            <div className="row">
                                                <div className="boxed boxed--lg boxed--border">
                                                    <div id="account-tests" className="account-tab">
                                                        <h3><strong>Courses</strong></h3>
                                                        <div className="row">
                                                            {this.state.courses.map((course) => {
                                                                return (
                                                                    <div className="col-sm-4 user-profile-test-container">
                                                                        <div className="feature feature-1 boxed boxed--border">
                                                                            <h3 title={course.name}>{course.name}</h3>
                                                                        </div>
                                                                    </div>
                                                                );
                                                            })}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="boxed boxed--lg boxed--border">
                                                    <div id="account-tests" className="account-tab">
                                                        <h3><strong>College Tests</strong></h3>
                                                        <h4>Coming Soon</h4>
                                                        <div className="row">
                                                            {/*{this.state.collegeTests.map((test) => {
                                                                return (
                                                                    <div className="col-sm-4 user-profile-test-container">
                                                                        <div className="feature feature-1 boxed boxed--border">
                                                                            <h3 title={test.name}>{test.name}</h3>
                                                                            <a className="btn btn--primary" href={'/test_instructions/' + test.id} target="_blank">
                                                                                <span className="btn__text">Go to Test</span>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                );
                                                            })}*/}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="boxed boxed--lg boxed--border">
                                                    <div id="account-tests" className="account-tab">
                                                        <h3><strong>Tests</strong></h3>
                                                        <h4>Coming Soon</h4>
                                                        <div className="row">
                                                            {/*{this.state.tests.map((test) => {
                                                                return (
                                                                    <div className="col-sm-4 user-profile-test-container">
                                                                        <div className="feature feature-1 boxed boxed--border">
                                                                            <h3 title={test.name}>{test.name}</h3>
                                                                            <a className="btn btn--primary" href={'/test_instructions/' + test.id} target="_blank">
                                                                                <span className="btn__text">Go to Test</span>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                );
                                                            })}*/}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <Footer />
                        <a className="back-to-top inner-link" href="#start" data-scroll-className="100vh:active">
                            <i className="stack-interface stack-up-open-big"></i>
                        </a>
                    </div>
                );
            }
        }
    }
}

export default ProfileContainer;

