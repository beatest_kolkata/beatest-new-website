import React, {PropTypes, Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import { browserHistory } from 'react-router';
import PieChart from "react-svg-piechart";
import $ from 'jquery';

import Loader from './Loader';
import Header from './Common/InternalHeader';
import Footer from './Common/InternalFooter';
import NotFound from './NotFound';

import * as action from '../actions/action';

const createCookie = require('./cookie.js').createCookie;
const readCookie = require('./cookie.js').readCookie;

const buttonMargin = {
    marginBottom: '20px',
    marginTop: '-30px',
    padding: '5px'
};

const textMargin = {
    marginBottom: '20px',
    marginTop: '-25px'
};

const colors = ["#3b5998", "#007bb6", "#00aced", "#cb2027", "#dd4b39"];

class ScoresContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentView: 'SCORES',
            loading: true,
            testId: null,
            expandedSector: null,
        };
        this.handleMouseEnterOnSector = this.handleMouseEnterOnSector.bind(this);
        this.getTest = this.getTest.bind(this);
    }

    prevent() {
        $(document).bind("contextmenu", function (e) {
            e.preventDefault();
        });
        $(document).keydown(function(e) {
            if(e.which >= 112 && e.which <= 123) return false;
        });
        $(document).keyup(function(e) {
            if(e.which >= 112 && e.which <= 123) return false;
        });
        $(document).keydown(function(e) {
            if(e.which === 17) return false;
        });
        $(document).keyup(function(e) {
            if(e.which === 17) return false;
        });
    }

    handleMouseEnterOnSector(sector) {
        this.setState({expandedSector: sector})
    }

    getTest() {
        this.setState({ testId: this.props.routeParams.testId });
        $.get('/api/tests/' + this.props.routeParams.testId + '/score_analysis', (data) => {
            if(Object.keys(data).length !== 0 && data.constructor === Object) {
                this.props.actions.setTestScore(data);
            }
            this.setState({ loading: false });
        }).fail(() => {
            this.setState({ loading: false });
        });
    }

    showTestPerformance() {
        window.location.pathname = '/performance/' + this.state.testId;
    }

    componentWillMount() {
        if(readCookie('userData') !== null) {
            this.props.actions.login(JSON.parse(readCookie('userData')));
            this.getTest(this);
        } else {
            $.get('/api/user', (data) => {
                if (typeof data === 'object') {
                    createCookie('userData', JSON.stringify(data.user), 7);
                    this.props.actions.login(data.user);
                }
                this.getTest(this);
            });
        }
    }

    render() {
        const correctData = [];
        const wrongData = [];

        const {expandedSector} = this.state;

        if(this.state.loading) {
            return (
                <div>
                    <Loader />
                </div>
            );
        } else {
            if(this.props.user === null || this.props.scoreData === null) {
                return (
                    <NotFound/>
                );
            } else {
                //var backToUrl = "/tests/" + this.props.scoreData.test_type;
                return (
                    <div>
                        <a id="start"></a>
                        <Header transparent={true} />
                        <div className="main-container">
                            <section className="text-center imagebg" data-overlay="7">
                                <div className="background-image-holder background--bottom" style={{background: "url(/img/landing-3.jpg)", opacity: 1}}>
                                    <img src="/img/landing-3.jpg" />
                                </div>
                                <div className="container">
                                    <div className="row">
                                        <div className="col-sm-12">
                                            <h2><strong>{this.props.scoreData.test_name}</strong></h2>
                                            <h4 className="text-center">Score Analysis</h4>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section>
                                <div className="container">
                                    <div className="scores-container">
                                        {/*<p><a href={backToUrl}><i className="fa fa-arrow-left" /></a> Your Scores</p>*/}
                                        <div className="row">
                                            {/*<div className="col-lg-5">
                                                <h3 className="text-left" style={textMargin}>{this.props.scoreData.test_name}</h3>
                                            </div>*/}
                                            <div className="col-sm-4">
                                                <h4 className="text-left" style={textMargin}>Rank:&nbsp;{this.props.scoreData.rank}</h4>
                                            </div>
                                            <div className="col-sm-4">
                                                <h4 className="text-left" style={textMargin}>Percentile:&nbsp;{this.props.scoreData.percentile.toFixed(2)}</h4>
                                            </div>
                                            <div className="col-sm-2" />
                                            <div className="col-sm-2">
                                                <button className="btn btn--primary" style={buttonMargin} onClick={this.showTestPerformance}>Performance</button>
                                                {/*<a className="btn btn--primary btn--icon" onClick={this.showTestPerformance} style={{marginTop: 0.2 + 'em'}}>
                                                    <span className="btn__text">Performance</span>
                                                </a>*/}
                                            </div>
                                        </div>
                                        <hr />
                                        <table className="border--round table--alternate-row">
                                            <thead>
                                            <tr>
                                                <th><span>Section</span></th>
                                                <th><span>Attempts</span></th>
                                                <th><span>Correct Questions</span></th>
                                                <th><span>Incorrect Questions</span></th>
                                                <th><span>Score</span></th>
                                                <th><span>Accuracy</span></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {this.props.scoreData.section_attempts.map((sectionAttempt, index) => {
                                                let correct = {
                                                    label: sectionAttempt.name,
                                                    value: sectionAttempt.correct_questions,
                                                    colours: colors[index]
                                                };

                                                let wrong = {
                                                    label: sectionAttempt.name,
                                                    value: sectionAttempt.incorrect_questions,
                                                    colours: colors[index]
                                                };

                                                if (sectionAttempt.name !== 'Overall') {
                                                    correctData.push(correct);
                                                    wrongData.push(wrong);
                                                }

                                                return <tr key={index}>
                                                    <td>{sectionAttempt.name}</td>
                                                    <td>{sectionAttempt.attempts}</td>
                                                    <td>{sectionAttempt.correct_questions}</td>
                                                    <td>{sectionAttempt.incorrect_questions}</td>
                                                    <td>{sectionAttempt.score}</td>
                                                    <td>{sectionAttempt.accuracy}%</td>
                                                </tr>
                                            })}
                                            </tbody>
                                        </table>
                                    </div>
                                    {/*Pie Chart Component*/}
                                    <div className="row" style={{marginTop: '40px'}}>
                                        <div className="col-lg-1"></div>
                                        <div className="col-lg-4">
                                            <h3>Correct Answers</h3>
                                            <div className="row">
                                                <div className="col-lg-6">
                                                    <PieChart
                                                        data={ correctData }
                                                        expandedSector={expandedSector}
                                                        onSectorHover={this.handleMouseEnterOnSector}
                                                        sectorStrokeWidth={2}
                                                        expandOnHover
                                                        shrinkOnTouchEnd
                                                    />
                                                </div>
                                                <div className="col-lg-6"></div>
                                            </div>
                                        </div>
                                        <div className="col-lg-2">
                                            <div style={{marginTop: '60px'}}>
                                                {correctData.map((element, i) => (
                                                    <div key={i}>
                                                        <svg height="12" width="12">
                                                            <circle r="40" fill="{element.color}" />
                                                        </svg>
                                                        <span style={{fontWeight: this.state.expandedSector === i ? "bold" : null}}>&nbsp;&nbsp;{element.label}</span>
                                                    </div>
                                                ))}
                                            </div>
                                        </div>
                                        <div className="col-lg-4">
                                            <h3 className="text-right">Wrong Answers</h3>
                                            <div className="row">
                                                <div className="col-lg-6"></div>
                                                <div className="col-lg-6">
                                                    <PieChart
                                                        data={ wrongData }
                                                        expandedSector={expandedSector}
                                                        onSectorHover={this.handleMouseEnterOnSector}
                                                        sectorStrokeWidth={2}
                                                        expandOnHover
                                                        shrinkOnTouchEnd
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-lg-1"></div>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <Footer/>
                        <a className="back-to-top inner-link" href="#start" data-scroll-className="100vh:active">
                            <i className="stack-interface stack-up-open-big"></i>
                        </a>
                    </div>
                );
            }
        }
    }
}

ScoresContainer.propTypes = {
    currentView: PropTypes.string,
    user: PropTypes.object,
    testAttemptId: PropTypes.number,
    loginPanelVisibility: PropTypes.string,
    registrationPanelVisibility: PropTypes.string,
    userMenuVisibility: PropTypes.string,
    testName: PropTypes.string,
    scoreData: PropTypes.object
}

function mapStateToProps(state, props) {
    return {
        currentView: state.currentView,
        user: state.user,
        testAttemptId: state.testAttemptId,
        loginPanelVisibility: state.loginPanelVisibility,
        registrationPanelVisibility: state.registrationPanelVisibility,
        userMenuVisibility: state.userMenuVisibility,
        testName: state.testName,
        scoreData: state.scoreData
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(action, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ScoresContainer);
