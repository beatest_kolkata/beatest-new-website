import React, { Component } from 'react';
//import logo from './logo.svg';
import classnames from 'classnames';
/*import './style.css';*/
import Sbi from '../Sbi';

class Info extends Component {
    render() {
        const { className, ...props } = this.props;
        return (
            <Sbi className={classnames('Info', className)} {...props}/>
        );
    }
}

export default Info;
