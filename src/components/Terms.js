import React, { Component } from 'react';
import Header from './Common/InternalHeader';
import Metatags from 'react-meta-tags';

class Terms extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
        <div>
        	<meta property="og:title" content="Beatest | Free Test Prep Solution | CAT, Banking Exams" />
			<meta name="keywords" content="MBA, CAT, IIM, 2016, 2017, Free exams, Free Tests, Free mock tests, Daily Tests, CAT Mock Tests, IBPS PO Mock Tests, Test Strategy, Exam Strategy, Toppers, IBPS, Bank PO, SBI PO, SBI Clerk, SBI PO Mock Test, SBI PO Mains, SBI PO Mains Mock Test, SBI Clerk Mock test, <SBI PO 2017, SBI Clerk 2017, IBPS PO 2017, IBPS Clerk 2017, IBPS Clerk Mock Test " />
			<meta name="description" content="A FREE one-stop solution to ace your MBA and Banking exams test preparations online with an excellent team of CAT and Banking exam toppers" />
			<meta property="og:url" content="https://beatest.in/" />
			<meta name="author" content="beatest" />
			<Header transparent={false} />
			<div className="main-container">
				<section>
					<div className="container">
						<div className="row">
							<div className="col-sm-12">
								<h1>Terms &amp; Conditions</h1>
								<hr />
							</div>
						</div>
					</div>
					<div className="container">
						<div className="row">
							<div className="col-sm-12 col-md-12">
								<article>
									<span className="h3">Privacy Policy &amp; General Conditions</span>
									<p>
										<a href="/">www.beatest.in</a> is owned and operated by BEATEST EDUMOCK PRIVATE LIMITED. All rights of this website are reserved with Beatest. No part or content of this website may be reproduced, replicated, distributed or transmitted in any form whatsoever or through any means, which includes printing, recording or other electronic methods or any other methods, as the case may be, without prior written permission from the administrator of this website. Users may download material from the website only for their own personal, non-commercial use. For commercial permission requests, please contact the Web administrator.
										Beatest reserves the right to make changes to any information contained on the website at any time whatsoever, and without notice or limitation of any kind, including, but not limited to, information relating to courses, fees, and other related service.
										We are very sensitive to the privacy concerns of visitors to our website. Beatest is committed to respect your online privacy and recognize your need for appropriate protection and management of any personally identifiable information or private communication. We take care that the content posted on this website is appropriate for their viewing and/or use. This Privacy Policy outlines the way in which Beatest gathers and uses information relating to you.
										The information displayed on this site is provided on a 'best efforts' and 'as is' basis without any warranties of any kind, including, but not limited to any implied warranties of merchantability or fitness for any particular purpose. The users of <a href="/">www.beatest.in</a> claim the entire responsibility for the selection and use of the service(s) and neither Beatest nor any of its information providers, licensers, employees, partners or agents shall have any liability for any errors, malfunctions, defects, loss of data, resulting from the use or inability to use the service. In no event shall the liability of Beatest, if any, exceed the amount paid, if any, by the user. While utmost care is taken to provide correct and up to date information, Beatest does not warrant the accuracy, completeness and timely availability of the information provided on the site and accepts no responsibility or liability for any error or omission in any information provided on the site nor does the site claim to be comprehensive in its coverage of examinations (either competitive or otherwise in nature) and/or of the various educational programmes conducted in India.
										This website may contain information about various academic programs offered by institutes and/or institutions as well as about various examinations that are conducted by them or by independent parties/third parties in India. This information has been compiled from various sources, including primary research conducted by, or on behalf of Beatest as well as secondary or desk research. Beatest will in no event be liable for any direct, incidental, consequential or indirect damages arising out of the use of or inability to use and/or for any delay in the service provided by the site. THE INFORMATION AVAILABLE ON THIS SITE IS FOR GENERAL INFORMATION PURPOSES.
										Beatest expressly disclaims any warranties whether express or implied about the suitability, reliability, availability, timeliness, correctness, completeness, quality, continuity, performance, fitness of products, services contained/displayed within this website for any purpose.
									</p>
									<span className="h3">Payment &amp; Refund Policy:</span>
									<p>
										Following policy is applicable for all the products /services that are bought and/or paid for on <a href="/">www.beatest.in</a>. This policy is applicable ONLY for products/services purchased online on <a href="/">www.beatest.in</a>.
									</p><ol style={{marginLeft: 20}}>
									<li>
										The fees should be paid in full to avail the product/service
									</li>
									<li>
										Any revision in the course fees /subscription fees / any other fees will be at the discretion of Beatest and shall be updated on the website time to time.
									</li>
									<li>
										Fees paid for a specific programme are not transferable to any other programme.
									</li>
									<li>
										Fees once paid shall not be refunded in any circumstances.
									</li>
									<li>
										Fees once paid shall not be adjusted with any other product /service availed subsequently at any Beatest centre.
									</li>
									<li>
										Courseware, content and Fees may be changed at the discretion of Beatest.
									</li>
								</ol>
									All the communication in this regard should be addressed to hello@beatest.in
									<p />
								</article>
							</div>
						</div>
					</div>
				</section>
			</div>
        </div>
        );}
}

export default Terms;