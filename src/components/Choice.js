import React, {PropTypes, Component} from 'react';
import $ from 'jquery';

class Choice extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.check = this.check.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }

    check(){
        if(this.props.isSelected)
            return 'checked';
        else
            return '';
    }

    handleClick() {
       $.ajax({
            url: '/api/section_attempt/' + this.props.sectionAttempts[this.props.sectionIndex].id + '/questions/' + this.props.question_id,
                dataType: 'json',
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify({
                    choice_id: this.props.choice.id
                })
            });

        this.props.choiceSelected(this.props.choice.id, true);
    }

    render() {
        return (
            <li className="choice">
                <input className="choice-radio" type="radio" name="choice" id={this.props.choice.id}
                    onChange={this.handleClick} checked={this.check()} />
                <span dangerouslySetInnerHTML={{__html: this.props.choice.html}}></span>
            </li>
        );
    }
}

Choice.propTypes = {
    question_id: PropTypes.number,
    choice: PropTypes.object,
    isSelected: PropTypes.bool,
    sectionIndex: PropTypes.number,
    sectionAttempts: PropTypes.array,
};

export default Choice;
