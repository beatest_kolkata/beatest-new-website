import React, {PropTypes, Component} from 'react';
import * as action from '../actions/action';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import UserPanel from './UserPanel';
import NavBarCollege from './NavBarCollege';
import UserMenu from './UserMenu';
import UserProfileMenu from './UserProfileMenu';
import CollegeRankPercentile from './CollegePercentile';
import NotFound from './NotFound';
import Footer from './Footer';
import {browserHistory} from "react-router";

import $ from 'jquery';
import Packages from "./Packages";

const initialState = {
    user: null,
    loginPanelVisibility: 'none',
    registrationPanelVisibility: 'none',
    userMenuVisibility: 'none',
    forgotPasswordPanelVisibility:'none',
}

class UserProfileContainer extends Component {
    constructor(props) {
        super(props);
        this.state = initialState;
        this.getUser = this.getUser.bind(this);
        this.getUser();
    }

    getUser() {
        $.get('/api/collegeuser', (data) => {
            if(typeof data === 'object') {
                this.props.actions.login(data.user);
            }
        }).then(() => {
            this.props.actions.loadingUserSuccess();
        });
    }

    render() {
        if (this.props.user !== null) {
            return (
                <div>
                    {(() => {
                        if(this.props.loginPanelVisibility === 'block' || this.props.registrationPanelVisibility === 'block') {
                            return <UserPanel loginPanelVisibility={this.props.loginPanelVisibility}
                                    toggleLoginPanelVisibility={this.props.actions.toggleLoginPanelVisibility}
                                    login={this.props.actions.login}
                                    registrationPanelVisibility={this.props.registrationPanelVisibility}
                                    forgotPasswordPanelVisibility={this.props.forgotPasswordPanelVisibility}
                                    toggleForgotPasswordPanelVisibility={this.props.actions.toggleForgotPasswordPanelVisibility}
                                    toggleRegistrationPanelVisibility={this.props.actions.toggleRegistrationPanelVisibility} />
                        }
                    })()}
                    <div>
                    <NavBarCollege toggleLoginPanelVisibility={this.props.actions.toggleLoginPanelVisibility}
                        toggleUserMenuVisibility={this.props.actions.toggleUserMenuVisibility}
                        onPage="user_profile" user={this.props.user} login={this.props.actions.login} />
                    <UserMenu userMenuVisibility={this.props.userMenuVisibility}
                        toggleUserMenuVisibility={this.props.actions.toggleUserMenuVisibility}
                        logout={this.props.actions.logout} />
                    </div>
                    <div className="container-fluid">
                    <div className="row">
                        <div className="col-lg-7">
                            <Packages/>
                            <UserProfileMenu user={this.props.user} />
                        </div>
                        <div className="col-lg-5">
                            <CollegeRankPercentile user={this.props.user} />
                        </div>
                    </div>
                    </div>
                    <Footer />
                </div>
            );    
        } else {
            return (
                <NotFound />
            );
        }
    }
}

UserProfileContainer.propTypes = {
    loginPanelVisibility: PropTypes.string,
    registrationPanelVisibility: PropTypes.string,
    userMenuVisibility: PropTypes.string,
    user: PropTypes.object,
    forgotPasswordPanelVisibility:PropTypes.string,
}

function mapStateToProps(state, props) {
    return {
        loginPanelVisibility: state.loginPanelVisibility,
        registrationPanelVisibility: state.registrationPanelVisibility,
        userMenuVisibility: state.userMenuVisibility,
        forgotPasswordPanelVisibility: state.forgotPasswordPanelVisibility,
        user: state.user
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(action, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserProfileContainer);
