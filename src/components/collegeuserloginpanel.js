/* eslint-disable */
import React, { Component } from 'react';
import $ from 'jquery';
import FacebookLogin from 'react-facebook-login';
import GoogleLogin from 'react-google-login';
import {browserHistory} from "react-router";

var mixpanel = require('mixpanel-browser');
mixpanel.init("86e70fa37444d5ab81c1ea058ae2fcd4");

class CollegeUserLogin extends Component {
    constructor(props) {
        super(props);
        this.state={data:''};
        this.login = this.login.bind(this);
        this.user_edit = this.user_edit.bind(this);
        this.responseFacebook = this.responseFacebook.bind(this);
        this.responseGoogle = this.responseGoogle.bind(this);
    }
        login(e) {
        e.preventDefault();
        var email = $('#email').val();
        mixpanel.identify(email);
        mixpanel.track("User Logged in");
        var password = $('#password').val();
        $('#login-form')[0].reset();
        $.ajax({
            url: '/api/user_login',
            dataType: 'json',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                email,
                password
            })
        }).then(function(data) {
          this.setState({"data":data})
            if(data.hasOwnProperty('invalid')) {
                if(data.invalid === 'Email') {
                    $('#user-panel-validator').html("Sorry, Beatest doesn't recognize that email.");
                } else if(data.invalid === 'Phone No and College') {
                    this.props.login(data.user);
                    location.reload();
                    browserHistory.push("/user_edit")
                }
                  else if (data.invalid === 'Password') {
                    $('#user-panel-validator').html("Wrong password. Try again.");
                } else if (data.invalid === 'User') {
                    $('#registration-panel-validator').html("Please add a password for your account.");
                    $('#new-full-name').val(data.name);
                    $('#new-email').val(data.email);
                }

            } else if (data.hasOwnProperty('user')) {
                this.props.login(data.user);
                location.reload();
                browserHistory.push("/user_profile");
            }
        }.bind(this));
    }

        user_edit(e) {
        e.preventDefault();
        var phone_no = $('#phone_no').val();
        var college_id = $('#college_id').val();
        $('#user-edit-form')[0].reset();
        $.ajax({
            url: '/api/user/edit',
            dataType: 'json',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                phone_no,
                college_id
            })
        }).then(function(data) {
            if(data.hasOwnProperty('invalid')) {
                if(data.invalid === 'Ph no and college Id') {
                    $('#user-panel-validator').html("Invalid Phone No. and College Id");
                } else if (data.invalid === 'User') {
                    this.props.toggleLoginPanelVisibility();
                    this.props.toggleRegistrationPanelVisibility();
                    $('#phone_no').val(data.phone_no);
                    $('#college_id').val(data.college_id);
                }
            } else if (data.hasOwnProperty('user')) {
                this.props.login(data.user);
                this.props.toggleLoginPanelVisibility();
                location.reload();
            }
        }.bind(this));
    }

    responseFacebook (response) {
        $.ajax({
            url: '/api/facebook_login',
            dataType: 'json',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                name: response.name,
                id: response.id,
                accessToken: response.accessToken,
                email: response.email,
                url: response.picture.data.url
            })
        }).then((data) => {
            this.props.login(data.user);
            this.props.toggleLoginPanelVisibility();
            browserHistory.push("/user_profile");

            // location.reload();
        });
    }

    responseGoogle = (response) => {
        $.ajax({
            url: '/api/google_login',
            dataType: 'json',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                accessToken: response.accessToken,
                googleId: response.googleId,
                tokenId: response.tokenId,
                email: response.profileObj.email,
                name: response.profileObj.name,
                imageUrl: response.profileObj.imageUrl
            })
        }).then((data) => {
            this.props.login(data.user);
            this.props.toggleLoginPanelVisibility();
            location.reload();
            browserHistory.push("/user_profile");

        });
    }

    render() {
         var userPanelClassName = 'user-panel ' + this.props.loginPanelVisibility;
         var userEditPanelClassName = 'user-panel-edit';
        return (
                   <div>
            <div className={userPanelClassName}>
                <div className="black-background" onClick={this.props.toggleLoginPanelVisibility}></div>
                <div className="panel">
                    <div className="close-panel" onClick={this.props.toggleLoginPanelVisibility}>X</div>
                    <form id="login-form" onSubmit={this.login}>
                        <input id="email" className="user-panel-input" type="email" placeholder="Email" required />
                        <input id="password" className="user-panel-input" type="password" placeholder="Password" required />
                        <div><label id="user-panel-validator"></label></div>
                        <button className="user-panel-button">Login</button>
                    </form>
                    <p className="login-panel-text">forgot password?
                        <a> click here</a>
                    </p>
                    <div className="social-media-login">
                        <FacebookLogin appId="1237812479598094" cssClass="facebook-login-button"
                            icon="fa-facebook" fields="name,email,picture" textButton="Facebook" callback={this.responseFacebook} />
                        <div className="space"></div>
                        <GoogleLogin clientId="709876791157-nh5o4s2rgn89b83hhpnrj56k9a9gupon.apps.googleusercontent.com"
                            icon="fa-google-plus" buttonText="Google" onSuccess={this.responseGoogle} onFailure={this.responseGoogle} className="google-login-button"/>
                    </div>
                </div>
            </div>
            </div>
        );
    }}

export default CollegeUserLogin
