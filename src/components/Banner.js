import React, { Component } from 'react';
import {browserHistory} from "react-router";
import IndexAchievement from './IndexAchievement';
var mixpanel = require('mixpanel-browser');
mixpanel.init("86e70fa37444d5ab81c1ea058ae2fcd4");

class Banner extends Component {
    constructor(props) {
          super(props);
        this.state = {};
        this.showIBPSTests = this.showIBPSTests.bind(this);
        this.showCATTests = this.showCATTests.bind(this);
	      this.showSBITests = this.showSBITests.bind(this);
	      this.showIBPSPOTests = this.showIBPSPOTests.bind(this);
	      this.showCATLink = this.showCATLink.bind(this);
    }

    showStudentSignupLink(){
        browserHistory.push("/colleges");
    }

    showCATLink(){
    	browserHistory.push("/tests/CAT");
    }

    showSBIPOLink(){
        browserHistory.push("/info");
    }

    showIBPSPOLink(){
        browserHistory.push("/infoIbpsPO");
    }

    showIBPSTestPage() {
        browserHistory.push("/tests/IBPS");
    }

    showIBPSTests() {
        this.props.showIBPSTests();
        window.location.href = "/tests/IBPS";
    }
	showSBITests() {
        this.props.showSBITests();
        window.location.href = "/tests/SBI";
    }

    showCATTests() {
	mixpanel.track("Viewed CAT Tests");
        this.props.showCATTests();
        window.location.href = "/tests/CAT";
    }
    showSBIPOTests() {
	mixpanel.track("Viewed SBI-PO Tests");
        window.location.href = "/info";
    }
    showIBPSPOTests() {
	mixpanel.track("Viewed IBPS-PO Tests");
        window.location.href = "/infoIbpsPO";
    }

    render() {
        return (
		<div className="banner-index">
			<div className="banner-embedded-index">
				<b>YOU CAN SCORE HIGHER!</b><br/>
				<div><p>Do you waste your time during a test looking for what to attempt?</p></div>
			</div>
			<div className="banner-button-div">
                    {/*<button className="banner-button-login" onClick={this.showStudentSignupLink}>College - Placements</button>*/}
					<button className="banner-button" id="banner-button-cat" onClick={this.showCATLink}><img src=""/>CAT</button>
					<button className="banner-button" id="banner-button-sbipo" onClick={this.showSBIPOLink}><img src=""/>SBI - PO</button>
                    <button className="banner-button" id="banner-button-ibpspo" onClick={this.showIBPSTestPage}><img src=""/>IBPS - RRB</button>
			</div>
		</div>
        );
    }
}



export default Banner;
