import React, { Component } from 'react';
import $ from 'jquery';
import 'jquery-ui';
import 'jquery-ui/ui/core';
import 'jquery-ui/ui/widgets/draggable';

class Calculator extends Component {
    constructor(props) {
        super(props);
        this.state = {
            memory: 0,
            currentValue: 0,
            equation: '',
            equationHasOperator: false
        };
    }

    componentDidMount() {
        $(".calculator").draggable({
            addClasses: false,
            containment: "window",
            cursor: "crosshair"
        });
    }

    getNumber(num) {
        var newCurrentValue;
        if(this.state.currentValue === 0) {
            newCurrentValue = num;
        }
        else {
            newCurrentValue = this.state.currentValue + num;
        }
        this.setState({
            currentValue: newCurrentValue
        });
    }

    getOperator(operator) {
        var newEquation;
        if(this.state.equation === '') {
            newEquation = this.state.currentValue + operator;
            this.setState({
                currentValue: 0,
                equation: newEquation,
                equationHasOperator: true
            });
        } else if(this.state.equationHasOperator) {
            newEquation = eval(this.state.equation + this.state.currentValue);
            newEquation += operator;
            this.setState({
                currentValue: 0,
                equation: newEquation
            });
        }
    }

    getPoint() {
        var newCurrentValue = this.state.currentValue + '.';
        this.setState({
            currentValue: newCurrentValue
        });
    }

    getResult() {
        var equation = this.state.equation;
        var newCurrentValue = eval(equation + this.state.currentValue);
        this.setState({
            currentValue: newCurrentValue,
            equation: '',
            equationHasOperator: false
        });
    }

    clear() {
        this.setState({
            currentValue: 0,
            equation: '',
            equationHasOperator: false
        });
    }

    memoryClear() {
        this.setState({
            memory: 0
        });
    }

    memoryRecall() {
        this.setState({
            currentValue: this.state.memory
        });
    }

    memoryStore() {
        this.setState({
            memory: this.state.currentValue
        });
    }

    memoryAdd() {
        var newMemory = this.state.memory + this.state.currentValue;
        this.setState({
            memory: newMemory
        });
    }

    memorySub() {
        var newMemory = this.state.memory - this.state.currentValue;
        this.setState({
            memory: newMemory
        });
    }

    backspace() {
        var currentValue = this.state.currentValue;
        if(currentValue > 9) {
            this.setState({
                currentValue: currentValue.toString().substr(0, currentValue.length - 1)
            });
        } else {
            this.setState({
                currentValue: 0
            });
        }
    }

    plusMinus() {
        this.setState({
            currentValue: -(this.state.currentValue)
        });
    }

    sqrt() {
        this.setState({
            currentValue: Math.sqrt(this.state.currentValue)
        });
    }

    getPercentage() {
        var newCurrentValue = eval(this.state.currentValue / 100);
        this.setState({
            currentValue: newCurrentValue
        });
    }

    getInverse() {
        this.setState({
            currentValue: eval(1 / this.state.currentValue)
        });
    }

    sqr() {
        this.setState({
            currentValue: eval(this.state.currentValue * this.state.currentValue)
        });
    }

    closeCalculator() {
        $('.calculator-div').removeClass('show').addClass('hide');
    }

    render() {
		return (
			<div className="calculator-div hide">
                <div className="calculator">
                    <div className="calculatorTitlebar">
                        <span>Calculator</span>
                        <div className="close fa fa-close" onClick={this.closeCalculator}></div>
                    </div>
                    <div className="calculatorContent">
                        <input className="display" type="text" value={this.state.equation} readOnly />
                        <input className="display" type="text" value={this.state.currentValue} readOnly />
                        <button className="button" onClick={this.memoryClear.bind(this)}>MC</button>
                        <button className="button" onClick={this.memoryRecall.bind(this)}>MR</button>
                        <button className="button" onClick={this.memoryStore.bind(this)}>MS</button>
                        <button className="button" onClick={this.memoryAdd.bind(this)}>M+</button>
                        <button className="button" onClick={this.memorySub.bind(this)}>M-</button>
                        <button className="button large red" onClick={this.backspace.bind(this)}>&larr;</button>
                        <button className="button red" onClick={this.clear.bind(this)}>C</button>
                        <button className="button red" onClick={this.plusMinus.bind(this)}>&plusmn;</button>
                        <button className="button" onClick={this.sqrt.bind(this)}>&radic;x</button>
                        <button className="button" onClick={this.getNumber.bind(this, "7")}>7</button>
                        <button className="button" onClick={this.getNumber.bind(this, "8")}>8</button>
                        <button className="button" onClick={this.getNumber.bind(this, "9")}>9</button>
                        <button className="button" onClick={this.getOperator.bind(this, "/")}>/</button>
                        <button className="button" onClick={this.getPercentage.bind(this)}>%</button>
                        <button className="button" onClick={this.getNumber.bind(this, "4")}>4</button>
                        <button className="button" onClick={this.getNumber.bind(this, "5")}>5</button>
                        <button className="button" onClick={this.getNumber.bind(this, "6")}>6</button>
                        <button className="button" onClick={this.getOperator.bind(this, "*")}>*</button>
                        <button className="button" onClick={this.getInverse.bind(this)}>1/x</button>
                        <button className="button" onClick={this.getNumber.bind(this, "1")}>1</button>
                        <button className="button" onClick={this.getNumber.bind(this, "2")}>2</button>
                        <button className="button" onClick={this.getNumber.bind(this, "3")}>3</button>
                        <button className="button" onClick={this.getOperator.bind(this, "-")}>-</button>
                        <button className="button" onClick={this.sqr.bind(this)}>x<sup>2</sup></button>
                        <button className="button" onClick={this.getNumber.bind(this, "0")}>0</button>
                        <button className="button" onClick={this.getPoint.bind(this)}>.</button>
                        <button className="button" onClick={this.getOperator.bind(this, "+")}>+</button>
                        <button className="button large equal" onClick={this.getResult.bind(this)}>=</button>
                    </div>
                </div>
            </div>
        );
    }
}

export default Calculator;
