import React, { Component } from 'react';
import classnames from 'classnames';
import PerformanceContainer from '../PerformanceContainer';
/*import './style.css';*/

class Performance extends Component {
    render() {
        const { className, ...props } = this.props;
        return (
            <PerformanceContainer className={classnames('Performance', className)} {...props} />
        );
    }
}

export default Performance;
