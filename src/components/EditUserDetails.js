import React, {PropTypes, Component } from 'react';
import $ from 'jquery';
import * as action from '../actions/action';
import {connect} from 'react-redux';
import {browserHistory} from "react-router";
import {bindActionCreators} from 'redux';
import Dropdown, { DropdownTrigger, DropdownContent } from 'react-simple-dropdown';

const initialState = {
    collegeList: [],
    tmp: 0
}

class EditUserDetails extends Component {
    constructor(props) {
        super(props);
        this.state = initialState;
        this.getColleges = this.getColleges.bind(this);
        this.getCollegeList = this.getCollegeList.bind(this);
        this.handleLinkClick = this.handleLinkClick.bind(this);
        this.submit = this.submit.bind(this);
        this.getColleges();
        this.tmp=0;
    }

    submit() {
        var y;
        var phone_no =$('#new-phone-no').val();

        y = document.getElementById("dropcon").innerHTML;
        if(y.length===0) {
        } else {
            var college_name = y;
                $.ajax({
                    url: '/api/user/edit',
                    dataType: 'json',
                    type: 'POST',
                    contentType: 'application/json',
                    data: JSON.stringify({
                        college_name,
                        phone_no
                    })
                }).then(function(data) {
                    browserHistory.push("/user_profile");
                }.bind(this));

        }
        
    }


    handleLinkClick = (e) => {
//        console.log(this.tmp);
        this.tmp = 0;
//        console.log(this.tmp);
        //document.getElementById("dropcon2x").innerHTML = "";
        //document.getElementById("dropcon2x").style.visibility = 'hidden';
        document.getElementById("dropcon").innerHTML = e;
        this.refs.user_edit_dropdown.hide();
    }

    getColleges() {
        $.get('/api/get_colleges', (data) => {
        this.props.actions.assignColleges(data);
        }).then(() => {
        });
    }

    getCollegeList() {
        return  this.props.collegeList;
    }

    render() {

        var lis=[];
        if(typeof this.props.collegeList === 'object'){
            {this.getCollegeList().map((college, index) => {
                 lis.push(  <li type="none" key={college.id} className="account-dropdown__link">
                              <a className="account-dropdown__link__anchor" onClick={() => this.handleLinkClick(college.college_name)}>
                                {college.college_name}
                              </a>
                            </li>
                 );
            })}
        }

        return (
        <div>
            <div className='user-edit-panel'>
                <form id="user-edit-form">
                    <input id="new-phone-no" type="text" placeholder="Phone No." required />
                </form>
                <Dropdown id="new-college-id" className="dropdown-user-college-edit" ref="user_edit_dropdown">
                    <DropdownTrigger>
                          <img className="account-dropdown__avatar" src="" /><span id="dropcon" className="account-dropdown__name"> Select College </span>
                    </DropdownTrigger>
                    <DropdownContent className="dropdown__content-old-user-college">
                          <ul className="account-dropdown__quick-links account-dropdown__segment">
                            {lis}
                          </ul>
                    </DropdownContent>
                </Dropdown>
                <br/>
                <div id="dropcon2" className="tt"> </div>
                <button className="user-edit-panel-button" onClick={this.submit}>Enter</button>
            </div>
            </div>
        );
    }
}

EditUserDetails.propTypes = {
    collegeList: PropTypes.array
}

function mapStateToProps(state, props) {
    return {
        collegeList: state.collegeList
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(action, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditUserDetails);