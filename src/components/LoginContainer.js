/* eslint-disable */
import React, {Component} from 'react';
import { Route, Redirect } from 'react-router'
import $ from 'jquery';
import 'isomorphic-fetch'

const initialState = {
    username: '',
    password: '',
    user: null
};

class LoginContainer extends Component {
    constructor(props) {
        super(props);
        this.state = initialState;
        this.getUser = this.getUser.bind(this);
        this.handleUsername = this.handleUsername.bind(this);
        this.handlePassword = this.handlePassword.bind(this);
        this.loginUser = this.loginUser.bind(this);
        this.getUser();
    }

    getUser() {
        $.get('/api/user', (data) => {
            if (typeof data === 'object') {
                window.location.href = '/profile';
            }
        });
    }

    handleUsername(event) {
        this.setState({username: event.target.value});
    }

    handlePassword(event) {
        this.setState({password: event.target.value});
    }

    loginUser(event) {
        event.preventDefault();
        let email = this.state.username;
        let password = this.state.password;

        $.ajax({
            url: '/api/login',
            dataType: 'json',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                email,
                password
            }),
            success: function(data) {
                if (data.hasOwnProperty('invalid')) {
                    if (data.invalid === 'Email') {
                        alert("Sorry, Invalid Username/Password");
                    } else if (data.invalid === 'Password') {
                        alert("Sorry, Invalid Username/Password");
                    }
                } else if (data.hasOwnProperty('user')) {
                    window.location.href = '/profile';
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {

            }
        });

        this.setState({
            username: '',
            password: ''
        });

        return false;
    }

    logoutUser(event) {
        event.preventDefault();
        $.ajax({
            url: '/api/logout',
            dataType: 'json',
            type: 'POST',
            contentType: 'application/json'
        }).then((data) => {
            window.location.href = "/index";
        });
    }

    render() {
        return (
            <div className="main-container">
                <section className="height-100 imagebg text-center" data-overlay={4}>
                    <div className="background-image-holder">
                        <img alt="background" src="/img/inner-2.jpg"/>
                    </div>
                    <div className="container pos-vertical-center">
                        <div className="row">
                            <div className="col-sm-7 col-md-5">
                                <h2>Login</h2>
                                <hr/>
                                <form>
                                    <div className="row">
                                        <div className="col-sm-12">
                                            <input type="text" placeholder="Username" value={this.state.username} onChange={this.handleUsername}/>
                                        </div>
                                        <div className="col-sm-12">
                                            <input type="password" placeholder="Password" value={this.state.password} onChange={this.handlePassword}/>
                                        </div>
                                        <div className="col-sm-12">
                                            <a className="btn btn--primary type--uppercase" onClick={this.loginUser}><span className="btn__text">Login</span></a>
                                        </div>
                                    </div>
                                </form>
                                <span className="type--fine-print block">Dont have an account yet?
                                  <a href="/signup"> Create account</a>
                                </span>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default LoginContainer;