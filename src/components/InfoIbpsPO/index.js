import React, { Component } from 'react';
//import logo from './logo.svg';
import classnames from 'classnames';
/*import './style.css';*/
import Ibps from '../Ibps';

class InfoIbpsPO extends Component {
    render() {
        const { className, ...props } = this.props;
        return (
            <Ibps className={classnames('InfoIbpsPO', className)} {...props}/>
        );
    }
}

export default InfoIbpsPO;
