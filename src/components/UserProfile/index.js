import React, { Component } from 'react';
import classnames from 'classnames';
/*import './style.css';*/
import UserProfileContainer from '../UserProfileContainer';

class UserProfile extends Component {
    render() {
        const { className, ...props } = this.props;
        return (
            <UserProfileContainer className={classnames('User', className)} {...props}/>
        );
    }
}

export default UserProfile;
