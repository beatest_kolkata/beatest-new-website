import React, { Component } from 'react';
//import logo from './logo.svg';
import classnames from 'classnames';
/*import './style.css';*/
import TestsContainer from '../TestsContainer';

class Tests extends Component {
    render() {
        const { className, ...props } = this.props;
        return (
            <TestsContainer className={classnames('Tests', className)} {...props}/>
        );
    }
}

export default Tests;
