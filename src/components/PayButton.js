import React, {Component} from 'react';
import $ from 'jquery';

const initialState = {};

let options = {
    "key": "rzp_live_IIFiNTCfcQrA0Z",
    amount: 0,
    wallet: 0,
    name: "Beatest",
    description: "Beatest Packages",
    image: "http://beatest.in/logo1.png",
    handler: function (response) {
        const user_id = options.notes.user_id;
        const payment_id = response.razorpay_payment_id;
        const payment_amount = options.amount;
        let payment_promo_code = options.notes.promo_code;
        const test_ids = options.testsID;

        let wallet_amount_used = options.wallet / 100;
        if (payment_promo_code === undefined)
            payment_promo_code = "";

        $.ajax({
            url: '/api/purchased_exams',
            dataType: 'json',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                test_ids,
                user_id,
                payment_id,
                payment_amount,
                payment_promo_code,
                wallet_amount_used
            })
        }).then(() => {
            alert('Payment Successful! Transaction ID: ' + payment_id);
            window.location.reload();
        })
    },
    prefill: {
        name: "",
        email: ""
    },
    testsID: [],
    notes: {
        test_id: "",
        user_id: "",
        promo_code: "",
    },
    theme: {
        color: "#D22733"
    }
};

class PayButton extends Component {

    constructor(props) {
        super(props);
        this.state = initialState;

        this.payAmount = this.payAmount.bind(this);
    }

    payAmount(e) {
        e.preventDefault();

        let txn_value = this.props.amount;
        if (this.props.promoCode !== '' && this.props.promoDiscount > 0) {
            options.notes.promo_code = this.props.promoCode;
            txn_value -= this.props.promoDiscount;
        }

        options.amount = txn_value * 100;
        options.notes.user_id = this.props.user.id;
        options.testsID = this.props.tests;

        this.props.resetPromoCode();

        window.Razorpay(options).open();
    }

    render() {
        return (
            <a className="btn btn--primary" onClick={this.payAmount} disabled={!this.props.userLogged}>
                <span className="btn__text">
                    {this.props.buttonText}
                </span>
            </a>
        )
    }
}

export default PayButton;