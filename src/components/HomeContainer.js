import React, { Component } from 'react';
import $ from 'jquery'
import { Banner } from "./Home/Banner";
import { Features } from "./Home/Features";
import { Courses } from "./Home/Courses";
import { CampusPartners } from "./Home/CampusPartners";
import { Testimonials } from "./Home/Testimonials";
import { QuickContact } from "./Home/QuickContact";
import Footer from './Common/InternalFooter';
import {staticData} from '../static-data';
import Header from './Common/InternalHeader';

class HomeContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: null,
            userLoading: true,
            features: [],
            courses: [],
            testimonials: [],
            campusPartners: []
        };
    }

    componentDidMount() {
        this.getUser();
        this.setState({
            features: staticData.features,
            courses: staticData.courses,
            testimonials: staticData.testimonials,
            campusPartners: staticData.campusPartners
        });
    }

    getUser() {
        $.get('/api/user', (data) => {
            if (typeof data === 'object') {
                this.setState({
                    user: data
                });
                this.setState({ userLoading: false });
            } else this.setState({ userLoading: false });
        });
    }

    render() {
        return (
            <div>
                {/*<NavBarHome user={this.props.user} />*/}
                <Header transparent={true} user={this.state.user} />
                <div className="main-container">
                    <Banner />
                    <Features data={this.state.features} />
                    <Courses data={this.state.courses} />
                    <CampusPartners data={this.state.campusPartners} />
                    <Testimonials data={this.state.testimonials} />
                    <section className="text-center space--xs">
                        <div className="container">
                            <div className="row" style={{marginTop: '20px', marginBottom: '20px'}}>
                                <div className="col-md-12 text-center space--xs space-top-0">
                                    <h2>Recognitions</h2>
                                </div>
                                <div className="col-md-3"/>
                                <div className="col-md-3">
                                    <a href="https://inc42.com/buzz/bigshift-kolkata-startups/?utm_source=facebook&utm_medium=social&utm_campaign=inc42-official-page&utm_content=features" target="_blank">
                                        <img alt="inc42" src="media/inc42.png" style={{paddingTop: '25px', height: '80px',width: '203px'}} />
                                    </a>
                                </div>
                                <div className="col-md-3">
                                    <a href="https://bangla.yourstory.com/read/b1bcc58ce4/beatest-makes-the-batt?utm_source=Facebook&utm_medium=Post" target="_blank">
                                        <img alt="YourStory" src="media/your_story.png" style={{height: '100px', width: '220px'}} />
                                    </a>
                                </div>
                                <div className="col-md-3"/>
                            </div>
                        </div>
                    </section>
                    <QuickContact />
                    <Footer />
                </div>
            </div>
        );
    }
}

export default HomeContainer;