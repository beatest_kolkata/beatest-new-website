import React, { Component } from 'react';
import classnames from 'classnames';
import './style.css'
import TestContainer from '../TestContainer'

class Test extends Component {
    render() {
        const { className, ...props } = this.props;
        return (
            <TestContainer className={classnames('Test', className)} {...props}/>
        );
    }
}

export default Test;
