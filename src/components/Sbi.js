import React, { Component } from 'react';

class Sbi extends Component {
    constructor(props) {
        super(props);
        this.state = {};
	 
    }
    show() {
        window.location.href = "/tests/SBI";	
    }

    showPricing() {
    	window.location.href = "/pricing";
    }

    render() {
        return (
            <div className="features">
		<div className="features-heading"><h1 id="detail">SBI PO 2017</h1></div>		
		<div className="tests-type-toggle">
                    <div className="type-toggle" onClick={this.show}>Go To Tests</div>
		    <div className="type-toggle" onClick={this.showPricing}>Pricing</div>
                </div>
		
		 <br /><br /><br /><br /><br />
                   <ul id="navbar">
			<div className="tests-type-toggle">
		            {(() => {return <div className="type-toggle"><a href="#detail"><li>ELIGIBILITY</li></a></div>})()}
			    {(() => {return <div className="type-toggle"><a href="#pattern"><li>PATTERN</li></a></div>})()}
			    {(() => {return <div className="type-toggle"><a href="#syllabus"><li>SYLLABUS</li></a></div>})()}
		    	    {(() => {return <div className="type-toggle"><a href="#date"><li>DATES</li></a></div>})()}
			    {(() => {return <div className="type-toggle"><a href="#procedure"><li>PROCEDURE</li></a></div>})()}
                   	</div>
                   </ul>
		<br />

<a href="#detail"><img src="https://cdn4.iconfinder.com/data/icons/ionicons/512/icon-arrow-up-b-128.png" id="fixedbutton" /></a>
<div className="sbi">

<h2>Eligibility Criteria</h2>
<p> <b>Education:</b><br />
Graduation degree from recognized university or institution is required. A final year student will not  be entertained. Students should have completed their graduation. There is no minimum percentage of marks.<br/>

<b>Nationality:</b><br />
Citizen of India, Nepal, Bhutan, Tibetan refugee and migrated Peoples from Pakistan, Burma, Sri Lanka, East African countries of Kenya, Uganda, the United Republic of Tanzania (formerly Tanganyika and Zanzibar), Zambia, Malawi, Zaire, Ethiopia and Vietnam with the intention of permanently settling in India are allowed to give exam. <br />

<b>Age Limit:</b><br />
Candidates applying must have their ages between 20 to 30 years. Candidates belonging to SC/ST/OBC and others covered under the Government Act, have the following age relaxation. <br/>

<table>
<tr><th>Sr. No.</th><th> Category </th> <th> Age Relaxation </th></tr>
<tr><td>1</td><td>SC/ST</td><td> 5 years</td></tr>
<tr><td>2</td><td>OBC (Non-creamy layer)</td><td> 3 years</td></tr>
<tr><td>3</td><td> Person with disabilities</td><td> 10 years</td></tr>
<tr><td>4</td><td>Persons ordinarily domiciled in the Kashmir Division of the State of Jammu &amp; Kashmir during the period 01.01.1980 to 31.12.1989</td><td> 5 years</td></tr>				
<tr><td>5</td><td>SPersons affected by 1984 riots</td><td> 5 years</td></tr>
</table>
<br/> </p>

<br/>			
<h2 id="pattern">Exam Pattern</h2>
				
<p>The SBI PO exam has two phase of written exam: Preliminary exam and main exam.<br />
		
1. Pre Exam Pattern<br/>

<table>
<tr><th> SL </th><th> Name of test</th><th> No of questions </th><th> Marks </th><th> Duration </th></tr>
<tr><td> 1. </td><td> English Language </td><td> 30 </td><td> 30 </td><td rowSpan="3"> 1 hour </td></tr>
<tr><td> 2. </td><td> Quantitative Aptitude </td><td> 35 </td><td> 35 </td></tr>
<tr><td> 3. </td><td> Reasoning Ability </td><td> 35 </td><td> 35 </td></tr>
<tr><td> </td><td> TOTAL </td><td> 100 </td><td> 100 </td><td> </td></tr>
</table>
<br />

For Preliminary Exam: Is consisting of three section reasoning, quant, and general awareness. The total marks are 100 and the total duration is one hour. In order to succeed in the prelims, you have to prepare well for all the three sections. <br /><br />

2. Mains Exam pattern :-<br />

SBI Po Mains exam conduct in two phase one is Objective and another one is descriptive. Here below we have updates the Objective exam pattern.< br />

<table>
<tr><th> SL </th><th> Name of test</th><th> No of questions </th><th> Marks </th><th> Duration </th></tr>
<tr><td> 1. </td><td> Reasoning and Computer Aptitude </td><td> 45 </td><td> 60 </td><td> 60 Minutes </td></tr>
<tr><td> 2. </td><td> Data Analysis and Interpretation </td><td> 35 </td><td> 60 </td><td> 45 Minutes </td></tr>
<tr><td> 3. </td><td> General/Economy/Banking Awareness </td><td> 40 </td><td> 40 </td><td> 35 Minutes </td></tr>
<tr><td> 4. </td><td> English Language </td><td> 35 </td><td> 40 </td><td> 40 Minutes </td></tr>
<tr><td> </td><td> TOTAL </td><td> 155 </td><td> 200 </td><td> 3 Hours </td></tr>
</table>
<br />

Now another descriptive test contains 30 minutes duration test with 50 marks of English language (Letter Writing &amp; Essay). All aspirants required to secure the passing marks in the descriptive test decided by the Bank. <br /></p>

<br />
<h2 id="syllabus">Syllabus:</h2>

<p> <br /><br />

<b>Reasoning</b><br /><br />

<table>
<tr><th> TOPIC </th><th> EXPECTED NUMBER OF QUESTIONS </th></tr>
<tr><td> Logical Reasoning </td><td> 5-10 </td></tr>
<tr><td> Syllogism </td><td> 5 </td></tr>
<tr><td> Blood Relations </td><td> 5 </td></tr>
<tr><td> Input Output </td><td> 0-5 </td></tr>
<tr><td> Coding Decoding </td><td> 5 </td></tr>
<tr><td> Alphanumeric Series </td><td> 0-5 </td></tr>
<tr><td> Ranking / Direction / Alphabet Test </td><td> 0-5 </td></tr>
<tr><td> Data Sufficiency </td><td> 0-5 </td></tr>
<tr><td> Coded Inequalities </td><td> 5 </td></tr>
<tr><td> Seating Arrangement </td><td> 10 </td></tr>
<tr><td> Puzzle </td><td> 5-7 </td></tr>
<tr><td> Tabulation </td><td> 5 </td></tr>
</table>
<br />

<b>Quantitative Aptitude</b><br /><br />

<table>
<tr><th> TOPIC </th><th> EXPECTED NUMBER OF QUESTIONS </th></tr>
<tr><td> Simplification </td><td> 5 </td></tr>
<tr><td> Ratio &amp; Proportion, Percentage </td><td> 3 </td></tr>
<tr><td> Number Systems </td><td> 3 </td></tr>
<tr><td> Profit &amp; Loss </td><td> 2 </td></tr>
<tr><td> Mixtures &amp; Alligations </td><td> 1-2 </td></tr>
<tr><td> Simple Interest &amp; Compound Interest &amp; Surds &amp; Indices </td><td> 1-2 </td></tr>
<tr><td> Work &amp; Time </td><td> 2 </td></tr>
<tr><td> Time &amp; Distance </td><td> 2 </td></tr>
<tr><td> Mensuration – Cylinder, Cone, Sphere </td><td> 1-2 </td></tr>
<tr><td> Sequence &amp; Series </td><td> 5 </td></tr>
<tr><td> Permutation, Combination &amp; Probability </td><td> 5 </td></tr>
<tr><td> Data Interpretation </td><td> 15 </td></tr>
</table>
<br />

<b>English Language</b><br /><br /><br />

<table>
<tr><th> TOPIC </th><th> EXPECTED NUMBER OF QUESTIONS </th></tr>
<tr><td> Reading Comprehension </td><td> 10 </td></tr>
<tr><td> Cloze Test </td><td> 10 </td></tr>
<tr><td> Fill in the blanks </td><td> 0-5 </td></tr>
<tr><td> Multiple Meaning/Error Spotting </td><td> 0-5 </td></tr>
<tr><td> Paragraph Complete/ Sentence Correction </td><td> 5 </td></tr>
<tr><td> Para jumbles </td><td> 5 </td></tr>
<tr><td> Miscellaneous </td><td> 5 </td></tr>
</table>
<br />

<b>Computer Knowledge</b><br /><br />

<table>
<tr><th> DETAILS </th></tr>
<tr><td> Hardware </td></tr>
<tr><td> Software </td></tr>
<tr><td> Database (introduction) </td></tr>
<tr><td> Communication (Basic Introduction) </td></tr>
<tr><td> Networking (LAN, WAN, MAN) </td></tr>
<tr><td> Internet (Concept, History, working environment, Application) </td></tr>
<tr><td> Security Tools, Virus, Hacker </td></tr>
<tr><td> MS Windows &amp; MS Office </td></tr>
<tr><td> Logic Gates </td></tr>
<tr><td> Number System </td></tr>
<tr><td> History of Computers </td></tr>

</table>
<br />
<b>General Awareness</b><br /><br />

<b>Current Affairs</b><br />
This primarily focuses on all news based items from various fields like sports, awards, summits, new

appointments, obituaries, Indian Defense, etc. Please be thorough with newspapers, reading blogs

and articles that you can get hands on.
<br /><br />
<b> Banking Awareness </b><br />
<ul>

<li><b> Indian Financial System – </b> Overview of Financial Markets (Money Market, Capital Market, Forex Market, Credit Market), Financial Intermediaries involved (like investment bankers, underwriters, stock exchanges, registrars, depositories, custodians, portfolio managers, mutual funds, primary dealers, Authorised dealers, self regulatory organizations, etc. Also read important committees and its recommendations. </li>

<li><b> History of Indian Banking Industry – </b> (Pre Independence, Post Independence, Nationalisation), Structure of Indian Banking, Types of Banks (Scheduled, Co-operatives), categorisation of Bank (Pvt, PSU, Foreign), NHB, SIDBI, EXIM, NABARD, IDBI its role etc and concept of NBFC. </li>

<li><b> Regulatory Bodies like – </b> RBI, (RBI History/ Functions/ Roles/) SEBI, IRDA, PFRDA, FSDC, FMC etc its role functions and fact based details. </li>

<li><b> Monetary &amp; Credit Policies </b> who decides, basis of change and its relation with Inflation and other related issues &amp; Inflation (Types) CRR/ SLR/ Repo/ Re-Repo/Bank Rate etc. </li>

<li><b> Budget Basics and Current Union Budget – </b> Discussion of Budget related terminologies like GDP, Fiscal Deficit, Various kinds of taxes, Government revenue model and spending etc. and discussion on Union Budget 2014. Also major pointers for Rail Budget and Economic Survey to be read. </li>

<li><b> International Organisations/ Financial Institutions – </b> IMF, World Bank, ADB, UN related agencies, recent development in them members heads etc functions, recent loans given by them to India or any country in the world where Crisis is going on. </li>

<li><b> Capital Market &amp; Money Market – </b> Concept, Types of Instruments they deal in like (1. Call/Notice Money 2. Treasury Bills 3. Term Money, 4. Certificate of Deposit 5. Commercial Papers or Capital Market Instruments like equity Shares, preference shares, or Debt segment like Debentures etc also talk about Stock Exchanges in India and world. </li>

<li><b> Government Schemes – </b> All important schemes for ex. Bharat Nirman, Swavlamban, Swabhiman , All schemes discussed in budget – the details like when was it introduced, important facts related to same, budgetary allocation etc. </li>

<li><b> Abbreviations and Economic terminologies – </b> Frequently discussed in newspaper. </li>

<li><b> Other important concepts like – </b> BASEL, Micro Finance, Base Rate, Negotiable Instruments, Credit Rating Agencies, Financial Inclusions, Teaser Rates, GAAR, Priority Sector Lending and all other important concepts. </li>

</ul>	</p>

<br />

<h2 id="date"> SBI PO 2017 Exam Dates : </h2>
<p>The following table has all the information about the SBI PO exam from the date of online application till the updating of the result.

<table>
<tr><td> Online registration including edit/modification </td><td> 07-02- 2017 to 06-03- 2017 </td></tr>
<tr><td> Payment of Application Fees/Intimation Charges </td><td> 07-02- 2017 to 06-03- 2017 </td></tr>
<tr><td> Download of call letters for Online Preliminary Examination </td><td> 15-04- 2017 onwards </td></tr>
<tr><td> Online Examination – Preliminary </td><td> 29, 30th April 2017, 6 &amp; 7 May 2017 </td></tr>
<tr><td> Result of Online Exam – Preliminary </td><td> 17th May 2017 onwards </td></tr>
<tr><td> Download of Call Letter for Online Main Exam </td><td> 22nd May 2017 </td></tr>
<tr><td> Conduct of Online Examination – Main </td><td> 4th June 2017 </td></tr>
<tr><td> Declaration of Result – Main </td><td> 19th June 2017 </td></tr>
<tr><td> Download Call Letter for Interview </td><td> 26th June 2017 </td></tr>
<tr><td> Conduct of Group Discussion &amp; Interview </td><td> 10th July 2017 </td></tr>
<tr><td> Declaration of Final Result </td><td> 5th Aug 2017 </td></tr>
</table> </p>

<br />

<h2 id="procedure">SBI PO Selection Procedure </h2>

<p>The SBI PO selection procedure 2017 has four stages: Prelims, Mains, Group discussion, and interview. <br />
<b> Preliminary Examination: </b> The candidate should pass in this prelim in order to get eligible for the main exam. There is minimum cutoff which is decided by the SBI PO authorities. <br />
<b> Main examination: </b> The cutoff is also set for the main exam. You have to get marks more than cut off in order to clear this exam. <br />
<b> Group Discussion: </b> The candidates who are selected in mains are eligible for appearing in Group discussion. Those who got selected in GD will go for the interview process. <br />
<b> Interview: </b> The interview will go to conduct by the sponsoring organization in each state across the country. <br />

The starting SBI PO pay scale is 27,620 (with four increments) and also there are annual incentives worth 7 -12 Lakhs. <br />
				</p>
				</div>
				
            </div>
        );
    }
}

export default Sbi;
