import React, { Component } from 'react';

class Ibps extends Component {
    constructor(props) {
        super(props);
        this.state = {};
	 
    }
	show() {
         
        window.location.href = "/tests/IBPS";
	
    }

    showPricing() {
    	window.location.href = "/pricing";
    }

    render() {
        return (
            <div className="features">
<div className="features-heading"><h1 id="detail">IBPS PO 2017</h1></div>
		
		<div className="tests-type-toggle">
                    <div className="type-toggle" onClick={this.show}>Go To Tests</div>
		    <div className="type-toggle" onClick={this.showPricing}>Pricing</div>
                </div>
		
		 <br /><br /><br />		 <br /><br />
                   <ul id="navbar">
			<div className="tests-type-toggle">
                    {(() => {return <div className="type-toggle"><a href="#detail"><li>ELIGIBILITY</li></a></div>})()}
		    {(() => {return <div className="type-toggle"><a href="#pattern"><li>PATTERN</li></a></div>})()}
		    {(() => {return <div className="type-toggle"><a href="#syllabus"><li>SYLLABUS</li></a></div>})()}
            	    {(() => {return <div className="type-toggle"><a href="#date"><li>DATES</li></a></div>})()}
		    {(() => {return <div className="type-toggle"><a href="#procedure"><li>PROCEDURE</li></a></div>})()}
                   </div>
               </ul>
			
			 
				<br />

<a href="#detail"><img src="https://cdn4.iconfinder.com/data/icons/ionicons/512/icon-arrow-up-b-128.png" id="fixedbutton" /></a>
<div className="ibps">

<h2>Eligibility Criteria</h2>
<p> <b>Education:</b><br />
Under IBPS PO Recruitment Eligibility Criteria 2016 Candidates must have completed their Graduation Degree from any recognized university or institution. Participants are also required to submit their mark sheet numbers while they submit IBPS PO online form 2016.<br/>

<b>Nationality:</b><br />
Citizen of India, Nepal, Bhutan, Tibetan refugee and migrated Peoples from Pakistan, Burma, Sri Lanka, East African countries of Kenya, Uganda, the United Republic of Tanzania (formerly Tanganyika and Zanzibar), Zambia, Malawi, Zaire, Ethiopia and Vietnam with the intention of permanently settling in India are allowed to give exam. <br />

<b>Age Limit:</b><br />
Candidates applying must have their ages between 20 to 30 years. Candidates belonging to SC/ST/OBC and others covered under the Government Act, have the following age relaxation. <br/>

<table>
<tr><th>Sr. No.</th><th> Category </th> <th> Age Relaxation </th></tr>
<tr><td>1</td><td>SC/ST</td><td> 5 years</td></tr>
<tr><td>2</td><td>OBC (Non-creamy layer)</td><td> 3 years</td></tr>
<tr><td>3</td><td> Person with disabilities</td><td> 10 years</td></tr>
<tr><td>4</td><td>Ex-Servicemen, Commissioned Officers including Emergency Commissioned Officers (ECOs)/Short Service Commissioned Officers (SSCOs) who have rendered at least 5 years military service and have been released on completion of assignment (including those whose assignment is due to be completed within the stipulated time) otherwise than by way of dismissal or discharge on account of misconduct or inefficiency or physical disability attributable to military service or invalidment</td><td> 5 years</td></tr>
<tr><td>5</td><td>Persons ordinarily domiciled in the Kashmir Division of the State of Jammu &amp; Kashmir during the period 01.01.1980 to 31.12.1989</td><td> 5 years</td></tr>				
<tr><td>6</td><td>SPersons affected by 1984 riots</td><td> 5 years</td></tr>
</table>
<br/> 


<b>For PWD Candidates</b><br />
The Person With Disability Act 1995, clearly mentions that every participant will take benefits of the person with the disability only if he/she suffer from more than 40% of the relevant disability. And should be certified by the Medical Board constituted by the Central /State Government. You will need to show your original certificates at the time of interview.

<h3>Visually Impaired (VI) = Blindness</h3><br />

1. The total absence of sight.<br/>
2. Visual acuity is not exceeding 6/60 or 20/200 (Snellen) in the better eye with correcting lenses.<br/>
3. Limitation of the field of vision subtending in angle of 20 degrees or worse. These are visual disability criteria.<br/>

<h3>Guidelines for Visually Impaired candidates</h3>

1. Visually Impaired candidates (who suffer from not less than 40% of disability) may opt to view the contents of the test in the magnified font, and all such candidates will be eligible for the compensatory time of 20 minutes for every hour Or otherwise advised of examination.<br />

2. The facility of viewing the substance of the test in the magnifying font will not be available to Visually Impaired candidates who use the services of a Scribe for the examination.<br />

<h3>Deaf and Hearing Impaired (HI)</h3>

A person with total loss of hearing in both the ears. A person cannot hear or understand sounds even after amplified speech. Hearing impairment means loss of sixty decibels or more in the better ear in the conversational range of frequencies.<br />

<h3>Orthopaedically Challenged (OC)</h3>

A person with the locomotor disability or cerebral palsy with locomotor impairment of minimum of 40% can apply.<br />

BL – Both legs affected but not arms,<br />
OA – One arm affected (Right or Left) with impaired reach or weakness of grip or ataxia,<br />
OL – One leg affected (Right or Left)<br />
MW – Muscular weakness and limited physical endurance.<br />

<h3>Guidelines for Persons With Disabilities using a Scribe</h3>

A candidate who uses writer gets extra 20 minutes for every hour of IBPS Exam.<br />
The candidate can use their scribe at his/her own cost during the online examination. The scribe can be from any academic stream.<br />
If a candidate is visually impaired and is not using the service of a scribe, then he/she may opt for the magnified font.<br />
For the post of Specialist Officer, the scribe should be from a different academic stream as that prescribed for the post.<br />
All participants must check carefully for their eligibility. And IBPS Department also gives ten years age relaxation in all post. IBPS Disability certificate format of PWD certificates to be submitted at the time of interview etc. can be downloaded from IBPS website www.ibps.in.<br />

</p>

<br />
				
<h2 id="pattern">Exam Pattern</h2>
				
<p>The IBPS PO exam has two phase of written exam: Preliminary exam and main exam.<br />
		
1. Pre Exam Pattern<br/>
For the prelims a total marks of 100 has to be answered comprising of 100 MCQ type questions. The time allotted is 1 hour (60 mins). The sectional bifurcation is as follows:

<table>
<tr><th> SL </th><th> Name of test</th><th> No of questions </th><th> Marks </th><th> Duration </th></tr>
<tr><td> 1. </td><td> English Language </td><td> 30 </td><td> 30 </td><td rowSpan="3"> 1 hour </td></tr>
<tr><td> 2. </td><td> Quantitative Aptitude </td><td> 35 </td><td> 35 </td><td> </td></tr>
<tr><td> 3. </td><td> Reasoning Ability </td><td> 35 </td><td> 35 </td><td> </td></tr>
<tr><td> </td><td> TOTAL </td><td> 100 </td><td> 100 </td><td> </td></tr>
</table>
<br />
Hovering over the sections is allowed. In order to practice mocks adhering to this structure login or register http://beatest.in/login.php
 <br /><br />

2. Mains Exam pattern :-<br />

The mains on the other hand are of quite a different structure. A total duration of 140 mins is allowed to answer 200 ques (200 marks) which is yet sectionally bifurcated, not giving you the option to hover over. The suggested pattern is as follows:< br />

<table>
<tr><th> SL </th><th> Name of test</th><th> No of questions </th><th> Marks </th><th> Duration </th></tr>
<tr><td> 1. </td><td> Reasoning Syllabus</td><td> 50 </td><td> 50 </td><td> 60 Minutes </td></tr>
<tr><td> 2. </td><td> General Awareness </td><td> 40 </td><td> 40 </td><td> 30 Minutes </td></tr>
<tr><td> 3. </td><td> Quantitative Aptitude </td><td> 50 </td><td> 50 </td><td> 40 Minutes </td></tr>
<tr><td> 4. </td><td> English Language </td><td> 40 </td><td> 40 </td><td> 20 Minutes </td></tr>
<tr><td> 4. </td><td> Computer Knowledge </td><td> 20 </td><td> 20 </td><td> 10 Minutes </td></tr>
<tr><td> </td><td> TOTAL </td><td> 200 </td><td> 200 </td><td> 140 Minutes </td></tr>
</table>
<br />

NOTE – Negative Marking of 1/4 marks.<br />

<b>Cutoff Score:</b> Each candidate will be required to obtain a minimum score in each test and also a minimum total score to be considered to be shortlisted for interview. Depending on the number of vacancies available, cutoffs will be decided and candidates will be shortlisted for interview. Prior to the completion of the interview process, scores obtained in the online examination will not be shared with the candidates shortlisted for interview.

</p>
<br />
<h2 id="syllabus">Syllabus:</h2>

<p> <br /><br />

<b>Reasoning</b><br /><br />

<table>
<tr><th> TOPIC </th><th> EXPECTED NUMBER OF QUESTIONS </th></tr>
<tr><td> Logical Reasoning </td><td> 5-10 </td></tr>
<tr><td> Syllogism </td><td> 5 </td></tr>
<tr><td> Blood Relations </td><td> 5 </td></tr>
<tr><td> Input Output </td><td> 0-5 </td></tr>
<tr><td> Coding Decoding </td><td> 5 </td></tr>
<tr><td> Alphanumeric Series </td><td> 0-5 </td></tr>
<tr><td> Ranking / Direction / Alphabet Test </td><td> 0-5 </td></tr>
<tr><td> Data Sufficiency </td><td> 0-5 </td></tr>
<tr><td> Coded Inequalities </td><td> 5 </td></tr>
<tr><td> Seating Arrangement </td><td> 10 </td></tr>
<tr><td> Puzzle </td><td> 5-7 </td></tr>
<tr><td> Tabulation </td><td> 5 </td></tr>
</table>
<br />

<b>Quantitative Aptitude</b><br /><br />

<table>
<tr><th> TOPIC </th><th> EXPECTED NUMBER OF QUESTIONS </th></tr>
<tr><td> Simplification </td><td> 5 </td></tr>
<tr><td> Ratio &amp; Proportion, Percentage </td><td> 3 </td></tr>
<tr><td> Number Systems </td><td> 3 </td></tr>
<tr><td> Profit &amp; Loss </td><td> 2 </td></tr>
<tr><td> Mixtures &amp; Alligations </td><td> 1-2 </td></tr>
<tr><td> Simple Interest &amp; Compound Interest &amp; Surds &amp; Indices </td><td> 1-2 </td></tr>
<tr><td> Work &amp; Time </td><td> 2 </td></tr>
<tr><td> Time &amp; Distance </td><td> 2 </td></tr>
<tr><td> Mensuration – Cylinder, Cone, Sphere </td><td> 1-2 </td></tr>
<tr><td> Sequence &amp; Series </td><td> 5 </td></tr>
<tr><td> Permutation, Combination &amp; Probability </td><td> 5 </td></tr>
<tr><td> Data Interpretation </td><td> 15 </td></tr>
</table>
<br />

<b>English Language</b><br /><br /><br />

<table>
<tr><th> TOPIC </th><th> EXPECTED NUMBER OF QUESTIONS </th></tr>
<tr><td> Reading Comprehension </td><td> 10 </td></tr>
<tr><td> Cloze Test </td><td> 10 </td></tr>
<tr><td> Fill in the blanks </td><td> 0-5 </td></tr>
<tr><td> Multiple Meaning/Error Spotting </td><td> 0-5 </td></tr>
<tr><td> Paragraph Complete/ Sentence Correction </td><td> 5 </td></tr>
<tr><td> Para jumbles </td><td> 5 </td></tr>
<tr><td> Miscellaneous </td><td> 5 </td></tr>
</table>
<br />

<b>Computer Knowledge</b><br /><br />

<table>
<tr><th> DETAILS </th></tr>
<tr><td> Hardware </td></tr>
<tr><td> Software </td></tr>
<tr><td> Database (introduction) </td></tr>
<tr><td> Communication (Basic Introduction) </td></tr>
<tr><td> Networking (LAN, WAN, MAN) </td></tr>
<tr><td> Internet (Concept, History, working environment, Application) </td></tr>
<tr><td> Security Tools, Virus, Hacker </td></tr>
<tr><td> MS Windows &amp; MS Office </td></tr>
<tr><td> Logic Gates </td></tr>
<tr><td> Number System </td></tr>
<tr><td> History of Computers </td></tr>
</table>
<br />

<b>General Awareness</b><br /><br />

<b>Current Affairs</b><br />
This primarily focuses on all news based items from various fields like sports, awards, summits, new appointments, obituaries, Indian Defense, etc. Please be thorough with newspapers, reading blogs and articles that you can get hands on.
<br /><br />
<b> Banking Awareness </b><br />
<ul>

<li><b> Indian Financial System – </b> Overview of Financial Markets (Money Market, Capital Market, Forex Market, Credit Market), Financial Intermediaries involved (like investment bankers, underwriters, stock exchanges, registrars, depositories, custodians, portfolio managers, mutual funds, primary dealers, Authorised dealers, self regulatory organizations, etc. Also read important committees and its recommendations. </li>

<li><b> History of Indian Banking Industry – </b> (Pre Independence, Post Independence, Nationalisation), Structure of Indian Banking, Types of Banks (Scheduled, Co-operatives), categorisation of Bank (Pvt, PSU, Foreign), NHB, SIDBI, EXIM, NABARD, IDBI its role etc and concept of NBFC. </li>

<li><b> Regulatory Bodies like – </b> RBI, (RBI History/ Functions/ Roles/) SEBI, IRDA, PFRDA, FSDC, FMC etc its role functions and fact based details. </li>

<li><b> Monetary &amp; Credit Policies </b> who decides, basis of change and its relation with Inflation and other related issues &amp; Inflation (Types) CRR/ SLR/ Repo/ Re-Repo/Bank Rate etc. </li>

<li><b> Budget Basics and Current Union Budget – </b> Discussion of Budget related terminologies like GDP, Fiscal Deficit, Various kinds of taxes, Government revenue model and spending etc. and discussion on Union Budget 2014. Also major pointers for Rail Budget and Economic Survey to be read. </li>

<li><b> International Organisations/ Financial Institutions – </b> IMF, World Bank, ADB, UN related agencies, recent development in them members heads etc functions, recent loans given by them to India or any country in the world where Crisis is going on. </li>

<li><b> Capital Market &amp; Money Market – </b> Concept, Types of Instruments they deal in like (1. Call/Notice Money 2. Treasury Bills 3. Term Money, 4. Certificate of Deposit 5. Commercial Papers or Capital Market Instruments like equity Shares, preference shares, or Debt segment like Debentures etc also talk about Stock Exchanges in India and world. </li>

<li><b> Government Schemes – </b> All important schemes for ex. Bharat Nirman, Swavlamban, Swabhiman , All schemes discussed in budget – the details like when was it introduced, important facts related to same, budgetary allocation etc. </li>

<li><b> Abbreviations and Economic terminologies – </b> Frequently discussed in newspaper. </li>

<li><b> Other important concepts like – </b> BASEL, Micro Finance, Base Rate, Negotiable Instruments, Credit Rating Agencies, Financial Inclusions, Teaser Rates, GAAR, Priority Sector Lending and all other important concepts. </li>

</ul>	</p>

<br />

<h2 id="salary"> Salary Structure </h2>
<p>Probationary Officer (PO) is a starting level post of any officer in a bank. So you will have kept under approx 1 to 2 years of probation period it also called a training period. In which you learn basic and essential duties of a PO in the bank.

<table>
<tr><th> Salary Details 2016 -2017 </th><th> Amount </th></tr>
<tr><td> Basic Pay </td><td> 23700.00 </td></tr>
<tr><td> Special Allowance </td><td> 1836.75 </td></tr>
<tr><td> DA </td><td> 10163.63 </td></tr>
<tr><td> CCA </td><td> 870 </td></tr>
<tr><td> Transport Allowance </td><td> - </td></tr>
<tr><td> Total (Without HRA) </td><td> 36570.38 </td></tr>
<tr><td> HRA </td><td> 2133.00 </td></tr>
<tr><td> Gross with HRA </td><td> 38703.38 </td></tr>
<tr><td> Medical Aid </td><td> 8000 </td></tr>
<tr><td> Entertainment </td><td> 500 </td></tr>
<tr><td> Newspaper </td><td> 300 </td></tr>
<tr><td> Petrol </td><td> 3000 </td></tr>
<tr><td> Telephone </td><td> 400 </td></tr>
<tr><td> Canteen Subsidy </td><td> 400 </td></tr>
<tr><td> Pension Contribution </td><td> 2500 </td></tr>
<tr><td> <b>Gross Annual CTC (with HRA but without leased accommodation)</b> </td><td> <b>5,57,640.52</b> </td></tr>
</table> 

<h3>PO Salary After 10th Bipartite Settlement –</h3>
After the wage revision, 10th Bipartite have settled for an increase of 15% in the IBPS PO salary. Expected hike in wages after permission of Indian Bank’s Association (IBA).

<table>
<tr><th> Increment (Percentage) % </th><th> Basic Pay </th><th> Gross Salary </th></tr>
<tr><td> 15 % </td><td> Rs.26,706/- </td><td> Rs.37,068/- </td></tr>
<tr><td> 20 % </td><td> Rs.27,866/- </td><td> Rs.38678/- </td></tr>
<tr><td> 25 % </td><td> Rs.29,026/- </td><td> Rs.40,288/- </td></tr>
<tr><td> 30 % </td><td> Rs.30,187/- </td><td> Rs.41,899/- </td></tr>
</table>

Also check out the effect of 7 th Pay Commission on salary. This depends upon the bank and may vary. Some banks do not follow pay commissions and the salary may get calculated accordingly to their structure.
</p>

				</div>

            </div>
        );
    }
}

export default Ibps;
