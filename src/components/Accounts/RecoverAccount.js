import React, { Component } from 'react';
import classnames from 'classnames';
import NavBar from '../NavBar'

class RecoverAccount extends Component {
    render() {
        const { className, ...props } = this.props;
        return (
            <div>
                <NavBar/>
                <div className="main-container">
                    <section className="height-100 text-center">
                        <div className="container pos-vertical-center">
                            <div className="row">
                                <div className="col-sm-7 col-md-5">
                                    <h2>Recover your account</h2>
                                    <p className="lead">
                                        Enter email address to send a recovery email.
                                    </p>
                                    <form>
                                        <input type="email" placeholder="Email Address" />
                                        <button className="btn btn--primary type--uppercase" type="submit">Recover Account</button>
                                    </form>
                                    <span className="type--fine-print block">Dont have an account yet?
                                      <a href="/create">Create account</a>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        );
    }
}

export default RecoverAccount;
