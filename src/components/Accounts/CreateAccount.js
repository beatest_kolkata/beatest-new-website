import React, { Component } from 'react';
import $ from 'jquery';

class CreateAccount extends Component {

    constructor(props) {
        super(props);

        this.state = {
            colleges: [],
            emailAddress: '',
            password: '',
            confirmPassword: '',
            fullName: '',
            phoneNumber: 0,
            college: ''
        };

        this.onEmailChange = this.onEmailChange.bind(this);
        this.onPasswordChange = this.onPasswordChange.bind(this);
        this.onConfirmPasswordChange = this.onConfirmPasswordChange.bind(this);
        this.onFullNameChange = this.onFullNameChange.bind(this);
        this.onPhoneNumberChange = this.onPhoneNumberChange.bind(this);
        this.onCollegeSelect = this.onCollegeSelect.bind(this);

        this.onSignUp = this.onSignUp.bind(this);
    }

    componentDidMount() {
        this.getUser();
        this.getColleges();
    }

    getColleges() {
        $.get('/api/get_colleges', (data) => {
            if (typeof data !== 'undefined') {
                this.setState({
                    colleges: data
                });
            }
        });
    }

    getUser() {
        $.get('/api/user', (data) => {
            if (typeof data === 'object') {
                window.location.href = '/profile';
            }
        });
    }

    onEmailChange(event) {
        this.setState({
            emailAddress: event.target.value
        });
    }

    onPasswordChange(event) {
        this.setState({
            password: event.target.value
        });
    }

    onConfirmPasswordChange(event) {
        this.setState({
            confirmPassword: event.target.value
        });
    }

    onFullNameChange(event) {
        this.setState({
            fullName: event.target.value
        });
    }

    onPhoneNumberChange(event) {
        this.setState({
            phoneNumber: event.target.value
        });
    }

    onCollegeSelect(event) {
        this.setState({
            college: event.target.value
        });
    }

    onSignUp(event) {
        event.preventDefault();

        if (this.state.emailAddress !== "" && this.state.password === this.state.confirmPassword && this.state.phoneNumber > 1000000000 && this.state.phoneNumber < 10000000000 && this.state.fullName !== "" && this.state.college !== "") {

            let full_name = this.state.fullName;
            let email = this.state.emailAddress;
            let password = this.state.password;
            let phone_no = this.state.phoneNumber;
            let college_id = parseInt(this.state.college, 10);

            $.ajax({
                url: '/api/signup/' + college_id,
                dataType: 'json',
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify({
                    full_name,
                    email,
                    phone_no,
                    password
                }),
                success: function(data) {
                    if(data.hasOwnProperty('user')) {
                        alert('Please visit your mail to activate your Beatest Account!');
                        window.location.href = "/login";
                    } else if (typeof data.invalid !== 'undefined') {
                        alert('Please correct: ' + data.invalid);
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {

                }
            });
        }
    }

    render() {
        return (
            <div>
                <div className="nav-container ">
                    <nav className="bar bar-4 bar--transparent bar--absolute" data-fixed-at={200}>
                        <div className="container">
                            <div className="row">
                                <div className="col-md-1 col-md-offset-0 col-sm-2 col-sm-offset-0 col-xs-4 col-xs-offset-4">
                                    <div className="bar__module">
                                        <a href="/">
                                            <img className="logo logo-dark" alt="logo" src="/img/logo-dark.png" />
                                            <img className="logo logo-light" alt="logo" src="/img/logo-light.png" />
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </nav>
                </div>
                <div className="main-container">
                    <section className="imageblock switchable feature-large height-100">
                        <div className="imageblock__content col-md-6 col-sm-4 pos-right">
                            <div className="background-image-holder">
                                <img alt="image" src="/img/inner-7.jpg" />
                            </div>
                        </div>
                        <div className="container pos-vertical-center">
                            <div className="row">
                                <div className="col-md-5 col-sm-7">
                                    <h2 className="text-center">Create a Beatest account</h2>
                                    <form>
                                        <div className="row">
                                            <div className="col-xs-12">
                                                <input type="text" name="Full Name" placeholder="Full Name" onChange={this.onFullNameChange} />
                                            </div>
                                            <div className="col-xs-12">
                                                <input type="email" name="Email Address" placeholder="Email Address" onChange={this.onEmailChange} />
                                            </div>
                                            <div className="col-xs-12">
                                                <input type="password" name="Password" placeholder="Password" onChange={this.onPasswordChange} />
                                            </div>
                                            <div className="col-xs-12">
                                                <input type="password" name="Confirm Password" placeholder="Confirm Password" onChange={this.onConfirmPasswordChange} />
                                            </div>
                                            <div className="col-xs-12">
                                                <input type="number" name="Phone Number" placeholder="Phone Number" onChange={this.onPhoneNumberChange} />
                                            </div>
                                            <div className="col-xs-12">
                                                <div className="input-select">
                                                    <select onChange={this.onCollegeSelect} value={this.state.college}>
                                                        <option value="">Select your College</option>
                                                        {
                                                            this.state.colleges.map((college) => {
                                                                return (
                                                                    <option key={college.id} value={college.id}>{college.college_name}</option>
                                                                );
                                                            })
                                                        }
                                                    </select>
                                                </div>
                                            </div>
                                            <div className="col-xs-12">
                                                <button type="submit" className="btn btn--primary type--uppercase" onClick={this.onSignUp}>Create Account</button>
                                            </div>
                                            <div className="col-xs-12 text-center">
                                              <span className="type--fine-print">By signing up, you agree to the &nbsp;<a href="/terms">Terms of Service</a></span>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        );
    }
}

export default CreateAccount;
