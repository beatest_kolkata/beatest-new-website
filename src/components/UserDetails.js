/* eslint-disable */
import React, { Component } from 'react';
import $ from 'jquery';
import Snackbar from './Snackbar';
import PriceTable from './PriceTable';
import Modal from 'react-modal';

const customStyles = {
  overlay : {
    position          : 'fixed',
    top               : 0,
    left              : 0,
    right             : 0,
    bottom            : 0,
    backgroundColor   : 'rgba(255, 255, 255, 0.90)'
  },
  content : {
    position                   : 'relative',
    top                        : '200px',
    left                       : '600px',
    width                      : '140px',
    border                     : '1px solid #ccc',
    background                 : '#fff',
    overflow                   : 'auto',
    WebkitOverflowScrolling    : 'touch',
    borderRadius               : '4px',
    outline                    : 'none',
    padding                    : '20px'
  }
};


class UserDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            tests: [],
            modalIsOpen: false,
            referral_used: false
        };

        this.changeProfilePic = this.changeProfilePic.bind(this);
        this.showChangePasswordBox = this.showChangePasswordBox.bind(this);
        this.hideChangePasswordBox = this.hideChangePasswordBox.bind(this);
        this.changePassword = this.changePassword.bind(this);
        this.startTest = this.startTest.bind(this);
        this.openModal = this.openModal.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    openModal() {
        if (this.props.user.referralCode) {
            this.setState({modalIsOpen: true});
        }
        else {
            alert ("You don't have a referral code.");
        }
    }

    afterOpenModal() {
        this.refs.subtitle.style.color = '#f00';
    }

    closeModal() {
        this.setState({modalIsOpen: false});
    }

    changeProfilePic() {
        var oFReader = new FileReader();
        oFReader.readAsDataURL($("#image-input").prop("files")[0]);
        var filetype = $('#image-input').prop('files')[0].type;
        oFReader.onload = function (oFREvent) {
            $("#profile-pic").attr("src", oFREvent.target.result);
            this.uploadFile(filetype, oFREvent.target.result);
        }.bind(this);
        this.showSnackbar();
    }

    showSnackbar() {
        $(".snackbar1").addClass("show");
        setTimeout(function(){ $(".snackbar1").removeClass("show"); }, 3000);
    }

    uploadFile(type, file) {
        $.ajax({
            url: '/api/uploads/profile_pic',
            dataType: 'json',
            type: 'POST',
            data: JSON.stringify({
                type,
                file
            }),
            contentType: 'application/json',
            success: function(data) {
                $('.add-image-title').addClass('hide');
            },
        }).then(() => {
            this.props.getUser();
        });
    }

    showChangePasswordBox() {
        $('.change-password-box').removeClass('hide');
    }

    hideChangePasswordBox() {
        $('.change-password-box').addClass('hide');
    }

    changePassword() {
        var currentPassword = $('#current-password').val();
        var newPassword = $('#new-password').val();
        var repeatNewPassword = $('#repeat-new-password').val();
        if(newPassword === repeatNewPassword) {
            $.ajax({
                url: '/api/change_password',
                dataType: 'json',
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify({
                    currentPassword,
                    newPassword,
                    repeatNewPassword
                })
            }).then((data) => {
                if(data.hasOwnProperty('success')) {
                    if(data.success) {
                        $(".snackbar2").addClass("show");
                        setTimeout(function(){ $(".snackbar2").removeClass("show"); }, 3000);
                    } else {
                        $(".snackbar3").addClass("show");
                        setTimeout(function(){ $(".snackbar3").removeClass("show"); }, 3000);
                    }
                }
            })
        }
        $('#current-password').val("");
        $('#new-password').val("");
        $('#repeat-new-password').val("");
    }

    startTest(test) {
        if(this.props.user !== null) {
            if(test.character === 'Mock') {
                var url = '/test_instructions/' + test.id;
            } else {
                url = '/test/' + test.id;
            }
            var height = screen.height - 150;
            var width = screen.width - 50;
            window.open(url,"Test","channelmode=1,height=" + height + ",width=" + width + ",fullscreen=1,resizable=0,location=0,menubar=0,toolbar=0,status=0,top=0,left=0");
            //window.location.href ='/index';
        } else {
            this.props.toggleLoginPanelVisibility();
        }
    }

    componentWillMount() {
        $.get('/api/user_profile', (profile) => {
            this.setState ({
                email: profile.email,
                tests: profile.tests,
                referral_used: profile.referral_used
            });
        });
    }

    /*render() {
        return (
            <div classNameNameNameName="user-details">
                <div classNameNameNameName="user-profile-pic">
                    <div classNameNameNameName="user-profile-pic-image">
                        {(() => {
                            if(this.props.user.profile_picture !== '' && this.props.user.profile_picture) {
                                $('.add-image-title').addClassNameName('hide');
                                $('.update-image-title').addClassNameName('show');
                                return <img id="profile-pic-img" src={this.props.user.profile_picture} height="120" alt="Profile"/>
                            } else {
                                $('.add-image-title').addClassNameName('show');
                                $('.update-image-title').addClassNameName('hide');
                                return <div id="profile-pic"></div>
                            }
                        })()}
                    </div>
                    <div>
                        <div classNameNameNameName="browse-wrap">
                            <div classNameNameNameName="add-image-title">Choose a file to upload</div>
                            <div classNameNameNameName="update-image-title ">Update Profile Picture</div>
                            <form id="upload-file-form" method="post" encType="multipart/form-data">
                                <input id="image-input" type="file" accept="image/x-png, image/jpe"
                                    name="upload-profile-pic" classNameNameNameName="upload" onChange={this.changeProfilePic}/>
                            </form>
                        </div>
                    </div>
                </div>
                <div classNameNameNameName="user-profile-details">
                    <span>Name : </span>{this.props.user.name}
                    <br />
                    <span classNameNameNameName="change-password" onClick={this.showChangePasswordBox}>Change Password</span>
                    <div classNameNameNameName="change-password-box hide">
                        <input id="current-password" classNameNameNameName="change-password" type="password" placeholder="current password" required />
                        <input id="new-password" classNameNameNameName="change-password" type="password" placeholder="new password" required />
                        <input id="repeat-new-password" classNameNameNameName="change-password" type="password" placeholder="repeat new password" required />
                        <button id="update-password" onClick={this.changePassword}>Update Password</button>
                    </div>
                    <Snackbar classNameNameNameName="snackbar2 " contentHtml="Password successfully changed" />
                    <Snackbar classNameNameNameName="snackbar3 " contentHtml="Please re-enter your current password" />
                </div>
                <Snack  bar classNameNameNameName="snackbar1" contentHtml="Uploading!" />
            </div>
        );
    }*/

    handleReferral(e) {
        this.setState({
            referral: e.target.value
        });
    }

    applyReferral() {
        var user_id = this.props.user.id;
        var ref_code = this.state.referral;
        $.ajax({
            url: '/api/use_referral_code',
            dataType: 'json',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                user_id,
                ref_code
            })
        }).then(() => {
            location.reload()
        });
    }

    render() {
        return (
            <div>
                <br/><br/><br/><br/>
                <h2 id="profile-header">Beatest Profile</h2>
                <Modal
                    isOpen={this.state.modalIsOpen}
                    onAfterOpen={this.afterOpenModal}
                    onRequestClose={this.closeModal}
                    style={customStyles}
                    contentLabel="Example Modal"
                >
                    <div>Share Referral Code <br/>  {this.props.user.referralCode}<br/> to avail bonus of &#x20b9;20!</div>
                </Modal>
                <div className="profile-card">
                    <img className="profile-pic" src={this.props.user.profile_picture} alt="Avatar" />
                    <div className="profile-container">
                        <h2 className="profile-title">{this.props.user.name}</h2>
                        <p className="profile-email">{this.state.email}</p> 
                        {(() => {
                            if(this.state.referral_used === false) {
                                return <div><input placeholder="Enter Referral Code!" onChange={this.handleReferral.bind(this)}/>
                                        <button className="user-panel-button" onClick={this.applyReferral.bind(this)}>
                                            Apply
                                        </button>
                                </div>
                            }
                        })()}
                        
                        <button id="profile-button1" onClick={this.openModal}>Share Referral</button>
                        <div>
                        <div className="browse-wrap">
                            <div className="update-image-title" id="profile-button1">Update Profile Picture</div>
                            <form id="upload-file-form" method="post" encType="multipart/form-data">
                                <input id="image-input" type="file" accept="image/x-png, image/jpe"
                                    name="upload-profile-pic" className="upload" onChange={this.changeProfilePic}/>
                            </form>
                        </div>
                    </div>
                    <div>
                    
                    <br />
                    <span className="change-password" id="profile-button1" onClick={this.showChangePasswordBox}>Change Password</span>
                    <div className="change-password-box hide">
                        <input id="current-password" className="change-password" type="password" placeholder="current password" required />
                        <input id="new-password" className="change-password" type="password" placeholder="new password" required />
                        <input id="repeat-new-password" className="change-password" type="password" placeholder="repeat new password" required />
                        <button id="update-password" onClick={this.changePassword}>Update Password</button>
                    
                    <Snackbar className="snackbar2 " contentHtml="Password successfully changed" />
                    <Snackbar className="snackbar3 " contentHtml="Please re-enter your current password" />
                </div>
                    </div>
                </div>
                </div>
                <Snackbar className="snackbar1" contentHtml="Uploading!" />
                <br/><br/>
                <div className="profile-test-card">
                    <h2>Your tests!</h2>
                    <table>
                        <tbody>
                        {this.state.tests.map (function (test) {
                            if (test.is_active)
                            return <tr key={test.id}>
                                <td className="profile-test">{test.name}</td>
                                <td className="profile-test">
                                    <button className="section-button2" onClick={this.startTest.bind(this, test)}>Go to Test!</button>
                                </td>
                            </tr>
                        
                        }.bind(this))}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

export default UserDetails;
