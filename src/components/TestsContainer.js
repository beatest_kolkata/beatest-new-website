/* eslint-disable */
import React, {PropTypes, Component} from 'react';
import $ from 'jquery';
import * as action from '../actions/action';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Navlink from './Navlink';
import UserPanel from './UserPanel';
import UserMenu from './UserMenu';
import TestsList from './TestsList';

const initialState = {
    currentView: 'TESTS_LIST',
    testsList: [],
    questionsList: [],
    user: null,
    sectionIndex: 0,
    timeleft: 0
}

class TestsContainer extends Component {
    constructor(props) {
        super(props);
        this.state = initialState;
        if(this.props.routeParams.type === 'CAT') {
            this.props.actions.showCATTests();
        } else if(this.props.routeParams.type === 'IBPS'){
            this.props.actions.showIBPSTests();
        } else if(this.props.routeParams.type === 'SBI'){
            this.props.actions.showSBITests();
        } else {
            window.location.href = '/tests';
        }

        this.props.actions.loadingTestsBegin();
        $.get('/api/tests', (tests) => {
            this.props.actions.loadingTestsSuccess(tests);
        });
    }

    render() {
        if (this.props.currentView === 'TESTS_LIST') {
            return (
                <div className="container">
                        <Navlink loadingTestsBegin={this.props.actions.loadingTestsBegin}
                            loadingTestsSuccess={this.props.actions.loadingTestsSuccess}
                            onPage="tests" user={this.props.user} login={this.props.actions.login}
                            toggleLoginPanelVisibility={this.props.actions.toggleLoginPanelVisibility}
                            toggleUserMenuVisibility={this.props.actions.toggleUserMenuVisibility} />
                        <UserMenu userMenuVisibility={this.props.userMenuVisibility}
                            toggleUserMenuVisibility={this.props.actions.toggleUserMenuVisibility}
                            logout={this.props.actions.logout} />
                        {(() => {
                            if(this.props.loginPanelVisibility === 'block' || this.props.registrationPanelVisibility === 'block') {
                                return <UserPanel loginPanelVisibility={this.props.loginPanelVisibility}
                                        toggleLoginPanelVisibility={this.props.actions.toggleLoginPanelVisibility}
                                        login={this.props.actions.login}
                                        registrationPanelVisibility={this.props.registrationPanelVisibility}
                                        toggleRegistrationPanelVisibility={this.props.actions.toggleRegistrationPanelVisibility} />
                            }
                        })()}
                        <TestsList user={this.props.user}
                            selectedTestType={this.props.selectedTestType}
                            testsList={this.props.testsList}
                            isTestsLoading={this.props.isTestsLoading}
                            sectionAttempts={this.props.sectionAttempts}
                            sectionIndex={this.props.sectionIndex}
                            toggleLoginPanelVisibility={this.props.actions.toggleLoginPanelVisibility}
                            createExamBegin={this.props.actions.createExamBegin}
                            createExamSuccess={this.props.actions.createExamSuccess}
                            loadingQuestionsBegin={this.props.actions.loadingQuestionsBegin}
                            loadingQuestionsSuccess={this.props.actions.loadingQuestionsSuccess}
                            currentTestType={this.props.currentTestType}
                            toggleTestType={this.props.actions.toggleTestType} />
                    </div>
                );
            }
    }
}

TestsContainer.propTypes = {
    currentView: PropTypes.string,
    testsList: PropTypes.array,
    questionsList: PropTypes.array,
    user: PropTypes.object,
    sectionIndex: PropTypes.number,
    isTestsLoading: PropTypes.bool,
    sectionAttempts: PropTypes.array,
    currentQuestionId: PropTypes.number,
    currentQuestionIndex: PropTypes.number,
    testAttempt: PropTypes.object,
    loginPanelVisibility: PropTypes.string,
    registrationPanelVisibility: PropTypes.string,
    userMenuVisibility: PropTypes.string,
    testName: PropTypes.string,
    timeleft: PropTypes.number,
    selectedTestType: PropTypes.string,
    completionStatus: PropTypes.string,
    currentTestType: PropTypes.string
}

function mapStateToProps(state, props) {
    return {
        currentView: state.currentView,
        testsList: state.testsList,
        questionsList: state.questionsList,
        user: state.user,
        sectionIndex: state.sectionIndex,
        isTestsLoading: state.isTestsLoading,
        sectionAttempts: state.sectionAttempts,
        currentQuestionId: state.currentQuestionId,
        currentQuestionIndex: state.currentQuestionIndex,
        testAttempt: state.testAttempt,
        loginPanelVisibility: state.loginPanelVisibility,
        registrationPanelVisibility: state.registrationPanelVisibility,
        userMenuVisibility: state.userMenuVisibility,
        testName: state.testName,
        timeleft: state.timeleft,
        selectedTestType: state.selectedTestType,
        completionStatus: state.completionStatus,
        currentTestType: state.currentTestType
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(action, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TestsContainer);
