/* eslint-disable */
import React, {PropTypes, Component} from 'react';
import $ from 'jquery';


var mixpanel = require('mixpanel-browser');
mixpanel.init("7d625ca8668abbe55ee264f1496ab610");

class CollegeTestOption extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.startTest = this.startTest.bind(this);
        this.showTestDetails = this.showTestDetails.bind(this);
        this.showTestScores = this.showTestScores.bind(this);
    }

    startTest() {
        mixpanel.track(this.props.name);
        if(this.props.user !== null) {
            if(this.props.test.character === 'Mock') {
                var url = '/test_instructions/' + this.props.test.id;
            } else {
                url = '/test/' + this.props.test.id;
            }
            var height = screen.height - 150;
            var width = screen.width - 50;
            window.open(url,"Test","channelmode=1,height=" + height + ",width=" + width + ",fullscreen=1,resizable=0,location=0,menubar=0,toolbar=0,status=0,top=0,left=0");
            window.location.href ='/user_profile';
            location.reload();
        } else {
            this.props.toggleLoginPanelVisibility();
        }
    }

    showTestDetails() {
        window.location.href = '/performance/' + this.props.id;
    }

    showTestScores() {
        window.location.href = '/scores/' + this.props.id;
    }

    render() {
        if((this.props.user === null || (this.props.user !== null && this.props.test.status === 'not attempted'))) {
            return (
                <div className='college-test-details-option'>
                    <button className="section-button" onClick={this.startTest}>Go to Test!</button>
                </div>
            );
        } else if(this.props.test.status === 'submitted') {
            return (
                <div className='college-test-details-option'>
                    <button className="section-button" onClick={this.showTestScores}>View Scores</button>
                </div>
            );
        } else {
            return (
                <div className='college-test-details-option' onClick={this.startTest}>
                    <button className="section-button" onClick={this.startTest}>Go to Test!</button>
                </div>
            )
        }
    }
}



export default CollegeTestOption;