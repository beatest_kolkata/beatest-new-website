import React, { Component } from 'react';
import classnames from 'classnames';
import ProfileContainer from '../ProfileNewContainer';
/*import './style.css';*/

class Profile extends Component {
    render() {
        const { className, ...props } = this.props;
        return (
            <ProfileContainer className={classnames('Profile', className)} {...props}/>
        );
    }
}

export default Profile;