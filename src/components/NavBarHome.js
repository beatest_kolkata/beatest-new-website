import React, { Component } from 'react';
import Menu from './Common/Menu';
import LoginModalContainer from './Common/LoginModalContainer';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import {getData} from '../actions/UserAction';

class NavBarHome extends Component {
    constructor(props) {
        super(props);

        props.getData();
    }

    render() {
        return (
            <div className="nav-container">
                <div className="bar bar--sm visible-xs">
                    <div className="container">
                        <div className="row">
                            <div className="col-xs-3 col-sm-2">
                                <a href="/">
                                    <img className="logo logo-dark" alt="logo" src="/img/logo-dark.png" />
                                    <img className="logo logo-light" alt="logo" src="/img/logo-dark.png" />
                                </a>
                            </div>
                            <div className="col-xs-9 col-sm-10 text-right">
                                <a href="#" className="hamburger-toggle" data-toggle-class="#menu2; hidden-xs hidden-sm">
                                    <i className="icon icon--sm stack-interface stack-menu"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <nav id="menu2" className="bar bar-2 hidden-xs bar--transparent bar--absolute" data-scroll-class='90vh:pos-fixed'>
                    <div className="container">
                        <div className="row">
                            <div className="col-md-2 text-center text-left-sm hidden-xs col-md-push-5">
                                <div className="bar__module">
                                    <a href="/">
                                        <img className="logo logo-dark" alt="logo" src="/img/logo-dark.png" />
                                        <img className="logo logo-light" alt="logo" src="/img/logo-light.png" />
                                    </a>
                                </div>
                            </div>
                            <div className="col-md-5 col-md-pull-2">
                                <div className="bar__module">
                                    <Menu />
                                </div>
                            </div>
                            <div className="col-md-5 text-right text-left-xs text-left-sm">
                                <div className="bar__module">
                                    {(() => {
                                        if(this.props.user) {
                                            return(
                                                <ul className="menu-horizontal text-left">
                                                    <li className="dropdown">
                                                        <span className="dropdown__trigger"><b>{this.props.user.name}</b></span>
                                                        <div className="dropdown__container">
                                                            <div className="container">
                                                                <div className="row">
                                                                    <div className="dropdown__content col-md-3 col-sm-6">
                                                                        <ul className="menu-vertical">
                                                                            <li>
                                                                                <Link to="/userprofile">Profile</Link>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#">Logout</a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            )
                                        } else {
                                            return(
                                                <ul className="menu-horizontal text-left">
                                                    <li>
                                                        <LoginModalContainer />
                                                    </li>
                                                    <li>
                                                        <div className="modal-instance">
                                                            <a className="btn btn--sm btn--primary type--uppercase modal-trigger" href="#">
                                                                <span className="btn__text">
                                                                    SIGNUP
                                                                    </span>
                                                            </a>
                                                            <div className="modal-container">
                                                                <div className="modal-content">
                                                                    <section className="imageblock feature-large bg--white border--round ">
                                                                        <div className="imageblock__content col-md-5 col-sm-3 pos-left">
                                                                            <div className="background-image-holder">
                                                                                <img alt="image" src="/img/dropdown-1.jpg" />
                                                                            </div>
                                                                        </div>
                                                                        <div className="container">
                                                                            <div className="row">
                                                                                <div className="col-md-5 col-md-push-6 col-sm-7 col-sm-push-4">
                                                                                    <h2>Create an account</h2>
                                                                                    <p className="lead">Get started with a free account &mdash; purchase tests &amp; courses to improve your skills</p>
                                                                                    {/*<a className="btn block btn--icon bg--facebook type--uppercase" href="#">
                                                                                        <span className="btn__text">
                                                                                            <i className="socicon-facebook"></i>
                                                                                            Sign up with Facebook
                                                                                            </span>
                                                                                    </a>
                                                                                    <a className="btn block btn--icon bg--googleplus type--uppercase" href="#">
                                                                                        <span className="btn__text">
                                                                                            <i className="socicon-googleplus"></i>
                                                                                            Sign up with Google
                                                                                            </span>
                                                                                    </a>*/}
                                                                                    <hr data-title="OR" />
                                                                                    <form>
                                                                                        <div className="row">
                                                                                            <div className="col-xs-12">
                                                                                                <input type="email" name="Email Address" placeholder="Email Address" />
                                                                                            </div>
                                                                                            <div className="col-xs-12">
                                                                                                <input type="password" name="Password" placeholder="Password" />
                                                                                            </div>
                                                                                            <div className="col-xs-12">
                                                                                                <button type="submit" className="btn btn--primary type--uppercase">Create Account</button>
                                                                                            </div>
                                                                                            <div className="col-xs-12">
                                                                                                <span className="type--fine-print">By signing up, you agree to the
                                                                                                        <a href="#">Terms of Service</a>
                                                                                                </span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </form>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </section>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            )
                                        }
                                    })()}
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        user: state.user
    };
}

const matchDispatchToProps = dispatch => {
    return bindActionCreators({getData: getData},dispatch);
}

export default connect(mapStateToProps,matchDispatchToProps)(NavBarHome);
