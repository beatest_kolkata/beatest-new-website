import React, {Component} from 'react';
import $ from 'jquery';

var options = {
    "key": "rzp_live_IIFiNTCfcQrA0Z",
    amount: 100,
    wallet: 0,
    name: "Beatest",
    description: "Purchase Test",
    image: "http://beatest.in/logo1.png",
    handler: function (response) {
        var user_id = options.notes.user_id;
        var payment_id = response.razorpay_payment_id;
        var payment_amount = options.amount;
        var payment_promo_code = options.notes.promo_code;
        var test_ids = options.testsID;
        let wallet_amount_used = options.wallet / 100;
        if (payment_promo_code === undefined)
            payment_promo_code = "";
        $.ajax({
            url: '/api/purchased_exams',
            dataType: 'json',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                test_ids,
                user_id,
                payment_id,
                payment_amount,
                payment_promo_code,
                wallet_amount_used
            })
        }).then(() => {
            alert('Payment Successful! Transaction ID: ' + payment_id);
            window.location.reload();
        })
    },
    prefill: {
        name: "",
        email: ""
    },
    testsID: [],
    notes: {
        test_id: "",
        user_id: "",
        promo_code: "",
    },
    theme: {
        color: "#D22733"
    }
};

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)'
    },
};

export default class PriceTable extends Component {
    constructor(props) {
        super(props);
        this.state = {
            testsList: [],
            mocktestsIDCAT: [],
            mocktestsIDIBPS: [],
            mocktestsIDSBI: [],
            priceMockIBPS: 0,
            priceMockSBI: 0,
            priceMockCAT: 0,
            discountMockCATBundle1: 0,
            discountMockIBPSBundle1: 0,
            discountMockSBIBundle1: 0,
            discountMockSBIBundle2: 0,
            discountCollegeAptitudeMocks: 0,
            /*txnMockCAT: '',
            txnMockIBPS: '',
            txnMockSBI: '',
            txnMockCATBundle1: '',
            txnMockCATBundle2: '',*/
            promoMockCAT: '',
            promoMockIBPS: '',
            promoMockSBI: '',
            promoMockCATBundle1: '',
            promoMockCATBundle2: '',
            promoMockIBPSBundle1: '',
            promoMockSBIBundle1: '',
            promoCollegeMockBundle: '',
            walletInUse: false
        };

        this.payMockCAT = this.payMockCAT.bind(this);
        this.payMockIBPS = this.payMockIBPS.bind(this);
        this.payMockSBI = this.payMockSBI.bind(this);
        this.payMockCATBundle1 = this.payMockCATBundle1.bind(this);
        this.payMockCATBundle2 = this.payMockCATBundle2.bind(this);
        this.payMockIBPSBundle1 = this.payMockIBPSBundle1.bind(this);
        this.payMockSBIBundle1 = this.payMockSBIBundle1.bind(this);
        this.payMockSBIBundle2 = this.payMockSBIBundle2.bind(this);
        this.payCollegeOfflineProgram = this.payCollegeOfflineProgram.bind(this);
        this.payCollegeAptitudeMocks = this.payCollegeAptitudeMocks.bind(this);
        this.toggleWalletUse = this.toggleWalletUse.bind(this);

        $.get('/api/tests', (test) => {
            this.setState({
                testsList: test
            });
        });
    }

    toggleWalletUse() {
        this.setState({
            walletInUse: !(this.state.walletInUse)
        });
    }

    componentDidMount() {
        if (this.props.user !== null) {
            $.get('/api/user', (data) => {
                if (typeof data === 'object') {
                    if (data.user.wallet !== undefined) {
                        this.setState({
                            wallet: data.user.wallet
                        });
                    }
                }
            });
        }
    }

    payMockCATBundle1(val, e) {
        let i = 0;
        let j = 0;
        let size = this.state.testsList.length;
        let test_ids = [];
        while (i < size) {
            if (this.state.testsList[i].character === "Mock"
                && this.state.testsList[i].type === "CAT"
                && this.state.testsList[i].id !== 1 && this.state.testsList[i].paymentStatus === false) {
                test_ids.push(this.state.testsList[i].id);
                j += 1;
            }
            if (j === 10) {
                break;
            }
            i += 1;
        }
        if (test_ids.length < 10) {
            alert("Not enough tests");
            return;
        }
        var txn_id = this.state.txnMockCATBundle1;
        var txn_value = 249 - this.state.discountMockCATBundle1;
        var user_id = this.props.user.id;
        var promo_code = this.state.promoMockCATBundle1;
        if (promo_code === undefined)
            promo_code = "";
        txn_value = Math.max(1, txn_value);

        options.amount = txn_value * 100;
        options.testsID = test_ids;
        options.notes.user_id = user_id;
        options.notes.promo_code = promo_code;
        window.Razorpay(options).open();
        e.preventDefault();
        /*$.ajax ({
          url: '/api/purchase_paytm_test',
                dataType: 'json',
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify({
                    txn_id,
                    txn_value,
                    test_ids,
                    user_id,
                    promo_code
                })
        }).then((data) => {
                if (data.success) {
                  alert (data.message);
                  window.location.reload();
                }
            })*/
    }

    payMockCATBundle2(val, e) {
        let i = 0;
        let j = 0;
        let size = this.state.testsList.length;
        let test_ids = [];
        while (i < size) {
            if (this.state.testsList[i].character === "Mock"
                && this.state.testsList[i].type === "CAT"
                && this.state.testsList[i].id !== 1 && this.state.testsList[i].paymentStatus === false) {
                test_ids.push(this.state.testsList[i].id);
                j += 1;
            }
            if (j === 20) {
                break;
            }
            i += 1;
        }
        if (test_ids.length < 20) {
            alert("Not enough tests");
            return;
        }
        var txn_id = this.state.txnMockCATBundle1;
        var txn_value = 599 - this.state.discountMockCATBundle2;
        var user_id = this.props.user.id;
        var promo_code = this.state.promoMockCATBundle2;
        if (promo_code === undefined)
            promo_code = "";
        txn_value = Math.max(1, txn_value);

        options.amount = txn_value * 100;
        options.testsID = test_ids;
        options.notes.user_id = user_id;
        options.notes.promo_code = promo_code;
        window.Razorpay(options).open();
        e.preventDefault();

        /*$.ajax ({
          url: '/api/purchase_paytm_test',
                dataType: 'json',
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify({
                    txn_id,
                    txn_value,
                    test_ids,
                    user_id,
                    promo_code
                })
        }).then((data) => {
                if (data.success) {
                  alert (data.message);
                  window.location.reload();
                }
            })*/
    }

    payMockIBPSBundle1(val, e) {
        let i = 0;
        let j = 0;
        let size = this.state.testsList.length;
        let test_ids = [];

        if (test_ids.length < 19) {
            alert("We keep uploading an additional test everyday to provide you a reason to practice everyday! :)");
            test_ids = [113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131];
        } else {
            while (i < size) {
                if (this.state.testsList[i].character === "Mock"
                    && this.state.testsList[i].type === "IBPS"
                    && this.state.testsList[i].price !== 0 && this.state.testsList[i].paymentStatus === false) {
                    test_ids.push(this.state.testsList[i].id);
                    j += 1;
                }
                if (j === 19) {
                    break;
                }
                i += 1;
            }
        }

        var txn_value = 249 - this.state.discountMockIBPSBundle1;
        var user_id = this.props.user.id;
        var promo_code = this.state.promoMockIBPSBundle1;
        if (promo_code === undefined)
            promo_code = "";
        txn_value = Math.max(1, txn_value);
        let sub = 0;
        if (this.state.walletInUse) {
            options.wallet = Math.min(this.props.user.wallet * 100, txn_value * 100);
            alert(this.props.user.wallet)
            if (options.wallet === txn_value * 100) {
                let wallet_amount_used = txn_value;
                $.ajax({
                    url: '/api/enable_tests',
                    dataType: 'json',
                    type: 'POST',
                    contentType: 'application/json',
                    data: JSON.stringify({
                        test_ids,
                        user_id,
                        wallet_amount_used
                    })
                }).then(() => {
                    alert('Payment Successful!');
                    window.location.reload();
                });
                return;
            }
            sub = this.props.user.wallet;
        }
        options.amount = (txn_value - sub) * 100;
        options.testsID = test_ids;
        options.notes.user_id = this.props.user.id;
        options.notes.promo_code = this.state.promoMockIBPSBundle1;
        window.Razorpay(options).open();
        e.preventDefault();
    }

    payMockSBIBundle1(val, e) {
        let i = 0;
        let j = 0;
        let size = this.state.testsList.length;
        let test_ids = [];
        while (i < size) {
            if (this.state.testsList[i].character === "Mock"
                && this.state.testsList[i].type === "SBI"
                && this.state.testsList[i].price !== 0 && this.state.testsList[i].paymentStatus === false) {
                test_ids.push(this.state.testsList[i].id);
                j += 1;
            }
            if (j === 19) {
                break;
            }
            i += 1;
        }
        if (test_ids.length < 19) {
            alert("Seems like there are less than 20 tests. Please drop us a mail to hello@beatest.in to claim refund!");
        }

        var txn_value = 249 - this.state.discountMockSBIBundle1;
        var user_id = this.props.user.id;
        var promo_code = this.state.promoMockSBIBundle1;
        if (promo_code === undefined)
            promo_code = "";
        txn_value = Math.max(1, txn_value);
        let sub = 0;
        if (this.state.walletInUse) {
            options.wallet = Math.min(this.props.user.wallet * 100, txn_value * 100);
            alert(this.props.user.wallet)
            if (options.wallet === txn_value * 100) {
                let wallet_amount_used = txn_value;
                $.ajax({
                    url: '/api/enable_tests',
                    dataType: 'json',
                    type: 'POST',
                    contentType: 'application/json',
                    data: JSON.stringify({
                        test_ids,
                        user_id,
                        wallet_amount_used
                    })
                }).then(() => {
                    alert('Payment Successful!');
                    window.location.reload();
                });
                return;
            }
            sub = this.props.user.wallet;
        }
        options.amount = (txn_value - sub) * 100;
        options.testsID = test_ids;
        options.notes.user_id = this.props.user.id;
        options.notes.promo_code = this.state.promoMockSBIBundle1;
        window.Razorpay(options).open();
        e.preventDefault();
        /*
        $.ajax ({
          url: '/api/purchase_paytm_test',
                dataType: 'json',
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify({
                    txn_id,
                    txn_value,
                    test_ids,
                    user_id,
                    promo_code
                })
        }).then((data) => {
                if (data.success) {
                  alert (data.message);
                  window.location.reload();
                }
            })*/
    }

    payMockSBIBundle2(val, e) {
        let i = 0;
        let j = 0;
        let size = this.state.testsList.length;
        let test_ids = [];
        while (i < size) {
            if (this.state.testsList[i].character === "Mock"
                && this.state.testsList[i].type === "SBI"
                && this.state.testsList[i].id !== 59 && this.state.testsList[i].paymentStatus === false) {
                test_ids.push(this.state.testsList[i].id);
                j += 1;
            }
            if (j === 19) {
                break;
            }
            i += 1;
        }
        if (test_ids.length < 19) {
            alert("Not enough tests");
            return;
        }

        var txn_value = 349 - this.state.discountMockSBIBundle2;
        var user_id = this.props.user.id;
        var promo_code = this.state.promoMockSBIBundle2;
        if (promo_code === undefined)
            promo_code = "";
        txn_value = Math.max(1, txn_value);
        let sub = 0;
        if (this.state.walletInUse) {
            options.wallet = Math.min(this.props.user.wallet * 100, txn_value * 100);
            alert(this.props.user.wallet)
            if (options.wallet === txn_value * 100) {
                let wallet_amount_used = txn_value;
                $.ajax({
                    url: '/api/enable_tests',
                    dataType: 'json',
                    type: 'POST',
                    contentType: 'application/json',
                    data: JSON.stringify({
                        test_ids,
                        user_id,
                        wallet_amount_used
                    })
                }).then(() => {
                    alert('Payment Successful!');
                    window.location.reload();
                });
                return;
            }
            sub = this.props.user.wallet;
        }
        options.amount = (txn_value - sub) * 100;
        options.testsID = test_ids;
        options.notes.user_id = user_id;
        options.notes.promo_code = promo_code;
        window.Razorpay(options).open();
        e.preventDefault();

        /*$.ajax ({
          url: '/api/purchase_paytm_test',
                dataType: 'json',
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify({
                    txn_id,
                    txn_value,
                    test_ids,
                    user_id,
                    promo_code
                })
        }).then((data) => {
                if (data.success) {
                  alert (data.message);
                  window.location.reload();
                }
            }) */
    }

    addPriceMockCAT(val, id, e) {
        if (e.target.checked === true) {
            this.setState({
                priceMockCAT: this.state.priceMockCAT + val,
                mocktestsIDCAT: this.state.mocktestsIDCAT.concat([id])
            });
        }
        else {
            var mocktestsID = this.state.mocktestsIDCAT;
            mocktestsID = mocktestsID.splice(mocktestsID.indexOf(id), 1);
            this.setState({
                priceMockCAT: this.state.priceMockCAT - val,
                mocktestsIDCAT: mocktestsID
            });
        }
    }

    addPriceMockIBPS(val, id, e) {
        if (e.target.checked === true) {
            this.setState({
                priceMockIBPS: this.state.priceMockIBPS + val,
                mocktestsIDIBPS: this.state.mocktestsIDIBPS.concat([id])
            });
        }
        else {
            var mocktestsID = this.state.mocktestsIDIBPS;
            mocktestsID = mocktestsID.splice(mocktestsID.indexOf(id), 1);
            this.setState({
                priceMockIBPS: this.state.priceMockIBPS - val,
                mocktestsIDIBPS: mocktestsID
            });
        }
    }

    addPriceMockSBI(val, id, e) {
        if (e.target.checked === true) {
            this.setState({
                priceMockSBI: this.state.priceMockSBI + val,
                mocktestsIDSBI: this.state.mocktestsIDSBI.concat([id])
            });
        }
        else {
            var mocktestsID = this.state.mocktestsIDIBPS;
            mocktestsID = mocktestsID.splice(mocktestsID.indexOf(id), 1);
            this.setState({
                priceMockSBI: this.state.priceMockSBI - val,
                mocktestsIDSBI: mocktestsID
            });
        }
    }

    handlePromoMockCAT(e) {
        $.get('/api/validate_code/' + this.state.promoMockCAT + '/' + this.props.user.id, (promo) => {
            if (promo.valid) {
                this.setState({
                    priceMockCAT: Math.max(1, this.state.priceMockCAT - promo.value)
                });
                alert('Promo Code Applied!\nPay Total Amount - ' + promo.value);
                $('.promo-section').hide();
            }

            else {
                alert('Promo Code Invalid');
                this.setState({
                    promoMockCAT: ''
                });
            }
        });
        e.preventDefault();
    }

    handlePromoMockIBPS(e) {
        $.get('/api/validate_code/' + this.state.promoMockIBPS + '/' + this.props.user.id, (promo) => {
            if (promo.valid) {
                this.setState({
                    priceMockIBPS: Math.max(1, this.state.priceMockIBPS - promo.value)
                });
                alert('Promo Code Applied!\nPay Total Amount - ' + promo.value);
                $('.promo-section').hide();
            }

            else {
                alert('Promo Code Invalid');
                this.setState({
                    promoMockIBPS: ''
                });
            }
        });
        e.preventDefault();
    }

    handlePromoMockSBI() {
        $.get('/api/validate_code/' + this.state.promoMockSBI + '/' + this.props.user.id, (promo) => {
            if (promo.valid) {
                this.setState({
                    priceMockSBI: Math.max(1, this.state.priceMockSBI - promo.value)
                });
                alert('Promo Code Applied!\nPay Total Amount - ' + promo.value);
                $('.promo-section').hide();
            }

            else {
                alert('Promo Code Invalid');
                this.setState({
                    promoMockSBI: ''
                });
            }
        });
    }

    handlePromoMockIBPSBundle() {
        $.get('/api/validate_code/' + this.state.promoMockIBPSBundle + '/' + this.props.user.id, (promo) => {
            if (promo.valid) {
                this.setState({
                    priceMockIBPS: this.state.priceMockIBPSBundle - 10
                });
                alert('Promo Code Applied!\nPay Total Amount - ' + promo.value);
                $('.promo-section').hide();
            }

            else {
                alert('Promo Code Invalid');
                this.setState({
                    promoMockIBPS: ''
                });
            }
        });
    }

    handlePromoMockCATBundle1(e) {
        $.get('/api/validate_code/' + this.state.promoMockCATBundle1 + '/' + this.props.user.id, (promo) => {
            if (promo.valid) {
                this.setState({
                    discountMockCATBundle1: promo.value
                });
                alert('Promo Code Applied!\nPay Total Amount - ' + promo.value);
                $('.promo-section').hide();
            }

            else {
                alert('Promo Code Invalid');
                this.setState({
                    promoMockCATBundle1: ''
                });
            }
        });
        e.preventDefault();
    }

    handlePromoMockCATBundle2(e) {
        $.get('/api/validate_code/' + this.state.promoMockCATBundle2 + '/' + this.props.user.id, (promo) => {
            if (promo.valid) {
                this.setState({
                    discountMockCATBundle2: promo.value
                });
                alert('Promo Code Applied!\nPay Total Amount - ' + promo.value);
                $('.promo-section').hide();
            }

            else {
                alert('Promo Code Invalid');
                this.setState({
                    promoMockCATBundle2: ''
                });
            }
        });
        e.preventDefault();
    }

    handlePromoMockSBIBundle1(e) {
        $.get('/api/validate_code/' + this.state.promoMockSBIBundle1 + '/' + this.props.user.id, (promo) => {
            if (promo.valid) {
                this.setState({
                    discountMockSBIBundle1: promo.value
                });
                let price = 249 - promo.value;
                alert('Promo Code Applied!\nPay INR ' + price);
                $('.promo-section').hide();
                $('.price-table-checkbox').hide();
            }

            else {
                alert('Promo Code Invalid');
                this.setState({
                    promoMockSBIBundle1: ''
                });
            }
        });
        e.preventDefault();
    }

    handlePromoMockIBPSBundle1(e) {
        $.get('/api/validate_code/' + this.state.promoMockIBPSBundle1 + '/' + this.props.user.id, (promo) => {
            if (promo.valid) {
                this.setState({
                    discountMockIBPSBundle1: promo.value
                });
                let price = 249 - promo.value;
                alert('Promo Code Applied!\nPay INR ' + price);
                $('.promo-section').hide();
                $('.price-table-checkbox').hide();
            }

            else {
                alert('Promo Code Invalid');
                this.setState({
                    promoMockIBPSBundle1: ''
                });
            }
        });
        e.preventDefault();
    }

    handlePromoMockSBIBundle2(e) {
        $.get('/api/validate_code/' + this.state.promoMockSBIBundle2 + '/' + this.props.user.id, (promo) => {
            if (promo.valid) {
                this.setState({
                    discountMockSBIBundle2: promo.value
                });
                let price = 349 - promo.value;
                alert('Promo Code Applied!\nPay INR ' + price);
                $('.promo-section').hide();
            }

            else {
                alert('Promo Code Invalid');
                this.setState({
                    promoMockSBIBundle2: ''
                });
            }
        });
        e.preventDefault();
    }

    handlePromoCollegeMockBundle(e) {
        $.get('/api/validate_code/' + this.state.promoCollegeMockBundle + '/' + this.props.user.id, (promo) => {
            if (promo.valid) {
                this.setState({
                    discountCollegeAptitudeMocks: promo.value
                });
                alert('Promo Code Applied!\nDiscount Amount - ' + promo.value);
                $('.promo-section').hide();
            }

            else {
                alert('Promo Code Invalid');
                this.setState({
                    promoCollegeMockBundle: ''
                });
            }
        });
        e.preventDefault();
    }

    /*handleTxnInputMockCAT(e) {
      this.setState({
        txnMockCAT: e.target.value
      });
    }

    handleTxnInputMockIBPS(e) {
      this.setState({
        txnMockIBPS: e.target.value
      });
    }

    handleTxnInputMockSBI(e) {
      this.setState({
        txnMockSBI: e.target.value
      });
    }

    handleTxnInputMockCATBundle1(e) {
      this.setState({
        txnMockCATBundle1: e.target.value
      });
    }

    handleTxnInputMockCATBundle2(e) {
      this.setState({
        txnMockCATBundle2: e.target.value
      });
    }

    handleTxnInputMockSBIBundle1(e) {
      this.setState({
        txnMockSBIBundle1: e.target.value
      });
    }

    handleTxnInputMockSBIBundle2(e) {
      this.setState({
        txnMockSBIBundle2: e.target.value
      });
    }*/

    handlePromoInputMockCAT(e) {
        this.setState({
            promoMockCAT: e.target.value
        });
    }

    handlePromoInputMockIBPS(e) {
        this.setState({
            promoMockIBPS: e.target.value
        });
    }

    handlePromoInputMockSBI(e) {
        this.setState({
            promoMockSBI: e.target.value
        });
    }

    handlePromoInputMockCATBundle1(e) {
        this.setState({
            promoMockCATBundle1: e.target.value
        });
    }

    handlePromoInputMockCATBundle2(e) {
        this.setState({
            promoMockCATBundle2: e.target.value
        });
    }

    handlePromoInputMockSBIBundle1(e) {
        this.setState({
            promoMockSBIBundle1: e.target.value
        });
    }

    handlePromoInputMockIBPSBundle1(e) {
        this.setState({
            promoMockIBPSBundle1: e.target.value
        });
    }

    handlePromoInputMockSBIBundle2(e) {
        this.setState({
            promoMockSBIBundle2: e.target.value
        });
    }

    handlePromoInputCollegeMockBundle(e) {
        this.setState({
            promoCollegeMockBundle: e.target.value
        });
    }

    payMockCAT(e) {
        var txn_value = this.state.priceMockCAT;
        var test_ids = this.state.mocktestsIDCAT;
        var user_id = this.props.user.id;
        var promo_code = this.state.promoMockCAT;
        if (promo_code === undefined)
            promo_code = "";
        txn_value = Math.max(1, txn_value);
        let sub = 0;
        if (this.state.walletInUse) {
            options.wallet = Math.min(this.props.user.wallet * 100, txn_value * 100);
            alert(this.props.user.wallet)
            if (options.wallet === txn_value * 100) {
                let wallet_amount_used = txn_value;
                $.ajax({
                    url: '/api/enable_tests',
                    dataType: 'json',
                    type: 'POST',
                    contentType: 'application/json',
                    data: JSON.stringify({
                        test_ids,
                        user_id,
                        wallet_amount_used
                    })
                }).then(() => {
                    alert('Payment Successful!');
                    window.location.reload();
                });
                return;
            }
            sub = this.props.user.wallet;
        }
        options.amount = (txn_value - sub) * 100;
        options.testsID = test_ids;
        options.notes.user_id = user_id;
        options.notes.promo_code = promo_code;
        window.Razorpay(options).open();
        e.preventDefault();

        /*$.ajax ({
          url: '/api/purchase_paytm_test',
                dataType: 'json',
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify({
                    txn_id,
                    txn_value,
                    test_ids,
                    user_id,
                    promo_code
                })
        }).then((data) => {
                if (data.success) {
                  alert (data.message);
                  window.location.reload();
                }
            })*/
    }

    payMockIBPS(e) {
        var txn_value = this.state.priceMockIBPS;
        var test_ids = this.state.mocktestsIDIBPS;
        var user_id = this.props.user.id;
        var promo_code = this.state.promoMockIBPS;
        if (promo_code === undefined)
            promo_code = "";
        txn_value = Math.max(1, txn_value);
        let sub = 0;
        if (this.state.walletInUse) {
            options.wallet = Math.min(this.props.user.wallet * 100, txn_value * 100);
            alert(this.props.user.wallet)
            if (options.wallet === txn_value * 100) {
                let wallet_amount_used = txn_value;
                $.ajax({
                    url: '/api/enable_tests',
                    dataType: 'json',
                    type: 'POST',
                    contentType: 'application/json',
                    data: JSON.stringify({
                        test_ids,
                        user_id,
                        wallet_amount_used
                    })
                }).then(() => {
                    alert('Payment Successful!');
                    window.location.reload();
                });
                return;
            }
            sub = this.props.user.wallet;
        }
        options.amount = (txn_value - sub) * 100;
        options.testsID = test_ids;
        options.notes.user_id = user_id;
        options.notes.promo_code = promo_code;
        window.Razorpay(options).open();
        e.preventDefault();

        /*$.ajax ({
          url: '/api/purchase_paytm_test',
                dataType: 'json',
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify({
                    txn_id,
                    txn_value,
                    test_ids,
                    user_id,
                    promo_code
                })
        }).then((data) => {
            if (data.success) {
                alert (data.message);
                window.location.reload();
            }
        })*/
    }

    payMockSBI(e) {
        var txn_id = this.state.txnMockSBI;
        var txn_value = this.state.priceMockSBI;
        var test_ids = this.state.mocktestsIDSBI;
        var user_id = this.props.user.id;
        var promo_code = this.state.promoMockSBI;
        if (promo_code === undefined)
            promo_code = "";
        txn_value = Math.max(1, txn_value);
        let sub = 0;
        if (this.state.walletInUse) {
            options.wallet = Math.min(this.props.user.wallet * 100, txn_value * 100);
            alert(this.props.user.wallet)
            if (options.wallet === txn_value * 100) {
                let wallet_amount_used = txn_value;
                $.ajax({
                    url: '/api/enable_tests',
                    dataType: 'json',
                    type: 'POST',
                    contentType: 'application/json',
                    data: JSON.stringify({
                        test_ids,
                        user_id,
                        wallet_amount_used
                    })
                }).then(() => {
                    alert('Payment Successful!');
                    window.location.reload();
                });
                return;
            }
            sub = this.props.user.wallet;
        }
        options.amount = (txn_value - sub) * 100;
        options.testsID = test_ids;
        options.notes.user_id = user_id;
        options.notes.promo_code = promo_code;
        window.Razorpay(options).open();
        e.preventDefault();

        /*$.ajax ({
          url: '/api/purchase_paytm_test',
                dataType: 'json',
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify({
                    txn_id,
                    txn_value,
                    test_ids,
                    user_id,
                    promo_code
                })
        }).then((data) => {
            if (data.success) {
              alert (data.message);
              window.location.reload();
            }
        })*/
    }

    payCollegeOfflineProgram(e) {
        var txn_value = 349;
        var test_ids = [];
        var user_id = this.props.user.id;
        txn_value = Math.max(1, txn_value);
        let sub = 0;
        options.amount = (txn_value - sub) * 100;
        options.paymentType = "College Program";
        options.notes.user_id = user_id;
        window.Razorpay(options).open();
        e.preventDefault();
    }

    payCollegeAptitudeMocks(e) {
        let promo_code = this.state.promoCollegeMockBundle;
        let promo_value = this.state.discountCollegeAptitudeMocks;
        if (promo_code === undefined)
            promo_code = "";

        let txn_value = 249;

        let test_ids = [105,132,133,134,135,136,137,138,139,140];
        let user_id = this.props.user.id;
        txn_value = Math.max(1, txn_value);

        options.amount = (txn_value - promo_value) * 100;
        options.notes.user_id = user_id;
        options.notes.promo_code = promo_code;
        options.notes.payment_type = "College Mocks";
        options.testsID = test_ids;
        window.Razorpay(options).open();
        e.preventDefault();
    }

    render() {
        var pricemocktestsCAT = 0, pricemocktestsIBPS = 0, pricemocktestsSBI = 0;
        return (
            <div>
                <div className="wallet-check-card">
                    <ul className="price">
                        <li>
                            <input className="wallet" type="checkbox" onClick={this.toggleWalletUse}/>
                            Use Wallet
                        </li>
                    </ul>
                </div>
                <br/><br/><br/><br/><br/><br/>

                <h1 className="course-price-name" id="CAT">CAT</h1>
                <div className="columns">
                    <ul className="price">
                        <li className="header">Prelims MOCK TESTS</li>
                        <div>
                            {this.state.testsList.map(function (test) {
                                if (test.type === "CAT" && test.character === "Mock") {
                                    if (test.paymentStatus === false && test.price !== 0) {
                                        pricemocktestsCAT += 39;
                                    }
                                    if (test.paymentStatus === true || test.price === 0)
                                        return <li className="paidTest">&#x2714;{test.name + ": "}
                                            Purchased &#x270C;</li>
                                    else return <div>
                                        <li>
                                            <input
                                                className="price-table-checkbox"
                                                type="checkbox"
                                                onClick={this.addPriceMockCAT.bind(this, test.price, test.id)}/>
                                            {test.name + ":  "} &#x20b9;{test.price}
                                        </li>
                                    </div>
                                }
                            }.bind(this))}
                        </div>
                        <li></li>
                        <li className="price-table-totalprice">Total Price: <p>&#x20b9;{this.state.priceMockCAT}</p>
                        </li>
                        <li></li>
                        <li></li>

                        <li>
                            <button className="section-button" onClick={this.payMockCAT}>
                                Buy
                            </button>
                        </li>
                    </ul>
                </div>

                <div className="columns">
                    <ul className="price">
                        <li className="header">PACKAGES</li>
                        <li>
                            10 Mocks
                            <form>
                                <div className="promo-section">
                                    <input placeholder="Promo Code"
                                           type="text" id="promo-input"
                                           onChange={this.handlePromoInputMockCATBundle1.bind(this)}/>
                                    <br/>
                                    <button className="promo-code-button"
                                            onClick={this.handlePromoMockCATBundle1.bind(this)}>
                                        Apply Promo
                                    </button>
                                </div>
                            </form>

                        </li>
                        <li> 20 Mocks
                            <form>
                                <div className="promo-section">
                                    <input placeholder="Promo Code"
                                           type="text" id="promo-input"
                                           onChange={this.handlePromoInputMockCATBundle2.bind(this)}/>
                                    <br/>
                                    <button className="promo-code-button"
                                            onClick={this.handlePromoMockCATBundle2.bind(this)}>
                                        Apply Promo
                                    </button>
                                </div>
                            </form>
                        </li>
                    </ul>
                </div>

                <div>
                    <h1 className="course-price-name" id="COLLEGE">Beatest College Packages</h1>
                    <div className="columns">
                        <ul className="price">
                            <li className="header">Offline Aptitude Program</li>
                            <button className="section-button" onClick={this.payCollegeOfflineProgram}>
                                Buy College Offline Program
                            </button>
                        </ul>
                    </div>

                    <div className="columns">
                        <ul className="price">
                            <li className="header">College Aptitude Mock Tests</li>
                            <li className="promo-section">
                                10 Mocks
                                <form>
                                    <div>
                                        <input placeholder="Promo Code"
                                               type="text" id="promo-input"
                                               onChange={this.handlePromoInputCollegeMockBundle.bind(this)}/>
                                        <br/>
                                        <button className="promo-code-button"
                                                onClick={this.handlePromoCollegeMockBundle.bind(this)}>
                                            Apply Promo
                                        </button>
                                    </div>
                                </form>
                            </li>
                            <li>
                                <button className="section-button" onClick={this.payCollegeAptitudeMocks}>
                                    Buy College Aptitude Mocks
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>

                <h1 className="course-price-name" id="IBPS">IBPS RRB PO</h1>
                <div className="columns">
                    <ul className="price">
                        <li className="header">Prelims MOCK TESTS</li>
                        <div>
                            {this.state.testsList.map(function (test) {
                                if (test.type === "IBPS" && test.character === "Mock") {
                                    if (test.paymentStatus === false && test.price !== 0) {
                                        pricemocktestsIBPS += 15;
                                    }
                                    if (test.paymentStatus === true || test.price === 0)
                                        return <li className="paidTest">&#x2714;{test.name + ": "}
                                            Purchased &#x270C;</li>
                                    else return <div>
                                        <li>
                                            <input
                                                className="price-table-checkbox"
                                                type="checkbox"
                                                onClick={this.addPriceMockIBPS.bind(this, test.price, test.id)}/>
                                            {test.name + ":  "} &#x20b9;{test.price}
                                        </li>
                                    </div>
                                }
                            }.bind(this))}
                        </div>
                        <li></li>
                        <li className="price-table-totalprice">Total Price:
                            <li>&#x20b9;{this.state.priceMockIBPS}</li>
                        </li>
                        <li></li>
                        <li></li>

                        <li>
                            <button className="section-button" onClick={this.payMockIBPS}>
                                Buy
                            </button>
                        </li>
                    </ul>
                </div>

                <div className="columns">
                    <ul className="price">
                        <li className="header">PACKAGES</li>
                        <li>
                            20 Mocks &#64; &#x20b9;249
                            <form>
                                <div className="promo-section">
                                    <input placeholder="Promo Code"
                                           type="text" id="promo-input"
                                           onChange={this.handlePromoInputMockIBPSBundle1.bind(this)}/>
                                    <br/>
                                    <button className="promo-code-button"
                                            onClick={this.handlePromoMockIBPSBundle1.bind(this)}>
                                        Apply Promo
                                    </button>
                                </div>
                            </form>
                            <li>
                                <button className="section-button" onClick={this.payMockIBPSBundle1}>
                                    Buy
                                </button>
                            </li>
                        </li>
                    </ul>
                </div>


                <h1 className="course-price-name" id="SBIPO">SBIPO</h1>
                <div className="columns">
                    <ul className="price">
                        <li className="header">Prelims MOCK TESTS</li>
                        <div>
                            {this.state.testsList.map(function (test) {
                                if (test.type === "SBI" && test.character === "Mock") {
                                    if (test.paymentStatus === false && test.price !== 0) {
                                        pricemocktestsSBI += 15;
                                    }
                                    if (test.paymentStatus === true || test.price === 0)
                                        return <li className="paidTest">&#x2714;{test.name + ": "}
                                            Purchased &#x270C;</li>
                                    else return <div>
                                        <li>
                                            <input
                                                className="price-table-checkbox"
                                                type="checkbox"
                                                onClick={this.addPriceMockSBI.bind(this, test.price, test.id)}/>
                                            {test.name + ":  "} &#x20b9;{test.price}
                                        </li>
                                    </div>
                                }
                            }.bind(this))}
                        </div>
                        <li className="price-table-totalprice">Total Price:
                            <li>&#x20b9;{this.state.priceMockSBI}</li>
                        </li>
                        <li>
                            <button className="section-button" onClick={this.payMockSBI}>
                                Buy
                            </button>
                        </li>
                    </ul>
                </div>

                <div className="columns">
                    <ul className="price">
                        <li className="header">PACKAGES</li>
                        <li>
                            20 Mocks &#64; &#x20b9;249
                            <form>
                                <div className="promo-section">
                                    <input placeholder="Promo Code"
                                           type="text" id="promo-input"
                                           onChange={this.handlePromoInputMockSBIBundle1.bind(this)}/>
                                    <br/>
                                    <button className="promo-code-button"
                                            onClick={this.handlePromoMockSBIBundle1.bind(this)}>
                                        Apply Promo
                                    </button>
                                </div>
                            </form>
                            <li>
                                <button className="section-button" onClick={this.payMockSBIBundle1}>
                                    Buy
                                </button>
                            </li>
                        </li>
                    </ul>
                </div>
            </div>
        );
    }
}