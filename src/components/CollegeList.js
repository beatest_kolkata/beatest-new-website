import React, {PropTypes, Component } from 'react';
import $ from 'jquery';
import * as action from '../actions/action';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Navlink from './Navlink';
import UserPanel from './UserPanel';
import UserMenu from './UserMenu';
import NotFound from './NotFound';
import Banner from './CollegesBackground';

const initialState = {
    loginPanelVisibility: 'none',
    currentView: 'PERFORMANCE',
    user: null,
    isUserLoading: true,
    collegeList: null
}

class CollegeList extends Component {
    constructor(props) {
        super(props);
        this.state = initialState;
        this.getColleges = this.getColleges.bind(this);
        this.getColleges();
//        this.getUser = this.getUser.bind(this);
//        this.getUser();
    }

    getColleges() {
        $.get('/get_colleges', (data) => {
//        console.log(typeof data);
//        console.log(data);
//        this.collegeList = data;
        }).then(() => {
//            this.props.actions.loadingUserSuccess();
        });
    }
//    getUser() {
//        $.get('/api/user', (data) => {
//            if(typeof data === 'object') {
//                this.props.actions.login(data.user);
//            }
//        }).then(() => {
//            this.props.actions.loadingUserSuccess();
//        });
//    }

    render() {
        return (
            <div>
            <Banner />
            </div>
        );
    }
}

CollegeList.propTypes = {
    currentView: PropTypes.string,
    user: PropTypes.object,
    loginPanelVisibility: PropTypes.string,
    registrationPanelVisibility: PropTypes.string,
    userMenuVisibility: PropTypes.string,
    isUserLoading: PropTypes.bool
}

function mapStateToProps(state, props) {
    return {
        currentView: state.currentView,
        user: state.user,
        loginPanelVisibility: state.loginPanelVisibility,
        registrationPanelVisibility: state.registrationPanelVisibility,
        userMenuVisibility: state.userMenuVisibility,
        isUserLoading: state.isUserLoading
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(action, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CollegeList);
