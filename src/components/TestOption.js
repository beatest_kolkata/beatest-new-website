/* eslint-disable */
import React, {PropTypes, Component} from 'react';
import $ from 'jquery';


var mixpanel = require('mixpanel-browser');
mixpanel.init("7d625ca8668abbe55ee264f1496ab610");

class TestOption extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.startTest = this.startTest.bind(this);
        this.showTestDetails = this.showTestDetails.bind(this);
        this.showTestScores = this.showTestScores.bind(this);
        this.showPerfAnalysis = this.showPerfAnalysis.bind(this);
    }

    startTest() {
	    mixpanel.track(this.props.name);
        if (this.props.test.character === 'Mock' && this.props.test.paymentStatus === false && this.props.test.price != 0) {
            window.location.href = '/pricing';
            return;
        }
        if(this.props.user !== null) {
            if(this.props.test.character === 'Mock') {
                var url = '/test_instructions/' + this.props.test.id;
            } else {
                url = '/test/' + this.props.test.id;
            }
            var height = screen.height - 150;
            var width = screen.width - 50;
            window.open(url,"Test","channelmode=1,height=" + height + ",width=" + width + ",fullscreen=1,resizable=0,location=0,menubar=0,toolbar=0,status=0,top=0,left=0");
            window.location.href ='/index';
        } else {
            this.props.toggleLoginPanelVisibility();
        }
    }

    showTestDetails() {
        window.location.href = '/performance/' + this.props.id;
    }

    showTestScores() {
        window.location.href = '/scores/' + this.props.id;
    }

    showPerfAnalysis() {
        window.location.href = '/perfAnalysis/' + this.props.id;
    }

    render() {
        if((this.props.user === null || (this.props.user !== null && this.props.test.status === 'unattempted')) && this.props.test.isa === 1) {
            return (
                    <div className='test-option'>
                        <p><br /> {this.props.name}</p>
                        <button className="section-button" onClick={this.startTest}>Go to Test!</button>
                    </div>
            );
        } else if(this.props.test.isa === 0) {
	    return (
                    <div className='test-option'>
                        <p><br /> {this.props.name}</p>
                        This test will be uploaded soon!
                    </div>
            );
	} else if(this.props.test.status === 'submitted' || this.props.test.status === 'completed') {
            return (
                <div className='test-option'>
                    <p><br /> {this.props.name}</p>
                    <button className="section-button" onClick={this.showTestScores}>View Scores</button>
                </div>
            );
        } else if(this.props.test.status === 'attempted') {
            return (
                <div className='test-option' onClick={this.startTest}>
                    <p>Continue <br /> {this.props.name}</p>
                    <button className="section-button" onClick={this.startTest}>Go to Test!</button>
                </div>
            )
        } else {
            return (
                <div className='test-option' onClick={this.startTest}>
                    <p><br /> {this.props.name}</p>
                    <button className="section-button" onClick={this.startTest}>Go to Test!</button>
                </div>
            )
        }
    }
}

TestOption.propTypes = {
    sectionIndex: PropTypes.number,
    sectionAttempts: PropTypes.array,
    createExamBegin: PropTypes.func,
    createExamSuccess: PropTypes.func,
    loadingQuestionsBegin: PropTypes.func,
    loadingQuestionsSuccess: PropTypes.func
};

export default TestOption;
