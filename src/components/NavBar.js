import React, {PropTypes, Component} from 'react';
import $ from 'jquery';

const initialState = {
    user: null
};

class NavBar extends Component {
    constructor(props) {
        super(props);
        this.state = initialState;
    }

    render() {
        return(
            <div className="nav-container ">
                <div className="bar bar--sm visible-xs">
                    <div className="container">
                        <div className="row">
                            <div className="col-xs-3 col-sm-2">
                                <a href="/">
                                    <img className="logo logo-dark" alt="logo" src="/img/logo-dark.png" />
                                    <img className="logo logo-light" alt="logo" src="/img/logo-light.png" />
                                </a>
                            </div>
                            <div className="col-xs-9 col-sm-10 text-right">
                                <a href="#" className="hamburger-toggle" data-toggle-class="#menu1;hidden-xs">
                                    <i className="icon icon--sm stack-interface stack-menu" />
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <nav id="menu1" className="bar bar--sm bar-1 hidden-xs ">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-1 col-sm-2 hidden-xs">
                                <div className="bar__module">
                                    <a href="/">
                                        <img className="logo logo-dark" alt="logo" src="/img/logo-dark.png" />
                                        <img className="logo logo-light" alt="logo" src="/img/logo-light.png" />
                                    </a>
                                </div>
                            </div>
                            <div className="col-md-11 col-sm-12 text-right text-left-xs text-left-sm">
                                <div className="bar__module">
                                    <ul className="menu-horizontal text-left">
                                        <li className="dropdown">
                                            <span className="dropdown__trigger">Courses</span>
                                            <div className="dropdown__container">
                                                <div className="container">
                                                    <div className="row">
                                                        <div className="dropdown__content col-md-3 col-sm-6">
                                                            <ul className="menu-vertical">
                                                                <li className="dropdown">
                                                                    <span className="dropdown__trigger">MBA</span>
                                                                    <div className="dropdown__container">
                                                                        <div className="container">
                                                                            <div className="row">
                                                                                <div className="dropdown__content col-md-2 col-sm-4">
                                                                                    <ul className="menu-vertical">
                                                                                        <li>
                                                                                            <a href="portfolio-one-1col.html">
                                                                                                CAT
                                                                                            </a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a href="portfolio-one-2col.html">
                                                                                                Exam 1
                                                                                            </a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a href="portfolio-one-3col.html">
                                                                                                Exam 2
                                                                                            </a>
                                                                                        </li>
                                                                                    </ul>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li className="dropdown">
                                                                    <span className="dropdown__trigger">Banking</span>
                                                                    <div className="dropdown__container">
                                                                        <div className="container">
                                                                            <div className="row">
                                                                                <div className="dropdown__content col-md-2 col-sm-4">
                                                                                    <ul className="menu-vertical">
                                                                                        <li>
                                                                                            <a href="#">
                                                                                                SBI PO - Prelims
                                                                                            </a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a href="#">
                                                                                                SBI PO - Mains
                                                                                            </a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a href="#">
                                                                                                IBPS PO - Prelims
                                                                                            </a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a href="#">
                                                                                                IBPS PO - Mains
                                                                                            </a>
                                                                                        </li>
                                                                                    </ul>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li className="dropdown separate">
                                                                    <span className="dropdown__trigger">College Placements</span>
                                                                    <div className="dropdown__container">
                                                                        <div className="container">
                                                                            <div className="row">
                                                                                <div className="dropdown__content col-md-2 col-sm-4">
                                                                                    <ul className="menu-vertical">
                                                                                        <li>
                                                                                            <a href="#">
                                                                                                Online Aptitude Tests
                                                                                            </a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a href="#">
                                                                                                Aptitude Training
                                                                                            </a>
                                                                                        </li>
                                                                                    </ul>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li className="dropdown">
                                            <a href="/aboutus">Team</a>
                                        </li>
                                        <li className="dropdown">
                                            <a href="/pricing">Pricing</a>
                                        </li>
                                        {(() => {
                                            if(this.props.user !== null) {
                                                return (
                                                    <li>
                                                        <div className="bar__module">
                                                            <a className="btn btn--sm btn--primary type--uppercase" href="/profile">
                                                                  <span className="btn__text">
                                                                    Profile
                                                                  </span>
                                                            </a>
                                                        </div>
                                                    </li>
                                                );
                                            }

                                            if (this.props.user === null) {
                                                return (
                                                    <li>
                                                        <div className="bar__module">
                                                            <a className="btn btn--sm btn--primary type--uppercase" href="/login">
                                                                  <span className="btn__text">
                                                                    Login
                                                                  </span>
                                                            </a>
                                                        </div>
                                                    </li>
                                                );
                                            }
                                        })()}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        );
    }
}

NavBar.propTypes = {
    user: PropTypes.object
};

export default NavBar;