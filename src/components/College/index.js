import React, { Component } from 'react';
//import logo from './logo.svg';
import classnames from 'classnames';
/*import './style.css';*/
import CollegeList from '../CollegeList';

class College extends Component {
    render() {
        const { className, ...props } = this.props;
        return (
            <CollegeList className={classnames('College', className)} {...props}/>
        );
    }
}

export default College;