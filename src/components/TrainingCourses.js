import React, { Component } from 'react';
import $ from 'jquery';
import Header from './Common/InternalHeader';
import Footer from './Common/InternalFooter';
import Faq from './Common/Faq';

export default class TrainingCourses extends Component {
	constructor(props) {
		super(props);
		this.state = {
			user: null
		};
	}

	componentDidMount() {
		this.getUser();
	}

    getUser() {
        $.get('/api/user', (data) => {
            if (typeof data === 'object') {
                this.setState({user: data.user});
            }
        });
    }

	render() {
		return (
			<div>
				<a id="start"></a>
				<Header transparent={false} user={this.state.user} />
				<div className="main-container">

					<section className="text-center height-50 bg--secondary">
						<div className="container pos-vertical-center">
							<div className="row" style={{marginTop: '80px'}}>
								<div className="col-sm-8">
									<h1>Courses &amp; Trainings</h1>
									<p className="lead">Whether you’re preparing for a competitive exam or for your placements, we have your prep needs covered!</p>
								</div>
							</div>
						</div>
					</section>

					<section className="text-center">
						<div className="container">
							<div className="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
								<h2>Our Courses</h2>
							</div>
						</div>
						<div className="container" style={{marginTop: '50px'}}>
							<div className="row">
								<div className="col-sm-4">
									<div className="feature feature-3 boxed boxed--lg boxed--border">
										<i className="icon icon--lg icon-Column-3"></i>
										<h4>Campus Recruitment Training</h4>
										<p>
											Training provides complete crash course for Students looking to crack the aptitude exam for College Placements.
										</p>
										<span className="label">Aptitude</span>
									</div>
								</div>
								<div className="col-sm-4">
									<div className="feature feature-3 boxed boxed--lg boxed--border">
										<i className="icon icon--lg icon-File-Excel"></i>
										<h4>Data Exploration with Excel</h4>
										<p>
											Basics of Excel operations and Advanced Functions to help do preliminary Data Exploration and Cleaning.
										</p>
										<span className="label">Data Science</span>
									</div>
								</div>
								<div className="col-sm-4">
									<div className="feature feature-3 boxed boxed--lg boxed--border">
										<i className="icon icon--lg icon-Data-Key"></i>
										<h4>Big Data with Hadoop &amp; Elastic</h4>
										<p>
											Combine blocks from a range of categories to build pages that are rich in visual style and interactivity
										</p>
										<span className="label">Big Data</span>
									</div>
								</div>
							</div>

							<div className="row" style={{marginTop: '20px'}}>
								<div className="col-sm-4">
									<div className="feature feature-3 boxed boxed--lg boxed--border">
										<i className="icon icon--lg icon-Dashboard"></i>
										<h4>NodeJS, ExpressJS with MongoDB</h4>
										<p>
											One of the latest Web Development stack. Ideal for bootstrapping and scaling up websites.
										</p>
										<span className="label">Web Dev</span>
									</div>
								</div>
								<div className="col-sm-4">
									<div className="feature feature-3 boxed boxed--lg boxed--border">
										<i className="icon icon--lg icon-Security-Check"></i>
										<h4>Single Page Applications with AngularJS</h4>
										<p>
											One of the most popular Frontend Framework by Google. Allows you to develop &amp; deploy UI quickly.
										</p>
										<span className="label">Web Dev</span>
									</div>
								</div>
								<div className="col-sm-4">
									<div className="feature feature-3 boxed boxed--lg boxed--border">
										<i className="icon icon--lg icon-Security-Settings"></i>
										<h4>Introduction to Web</h4>
										<p>
											Basics of developing web applications. Building scalable structure and deploying them.
										</p>
										<span className="label">Web Dev</span>
									</div>
								</div>
							</div>

							<div className="row" style={{marginTop: '20px'}}>
								<div className="col-sm-4">
									<div className="feature feature-3 boxed boxed--lg boxed--border">
										<i className="icon icon--lg icon-Scale"></i>
										<h4>Web Development with Python</h4>
										<p>
											Developing scalable web application with Python using Flask micro-framework. Scaling up with Amazon Web Services.
										</p>
										<span className="label">Web Dev</span>
									</div>
								</div>
								<div className="col-sm-4">
									<div className="feature feature-3 boxed boxed--lg boxed--border">
										<i className="icon icon--lg icon-Management"></i>
										<h4>Crash Course for CAT</h4>
										<p>
											Mock Tests &amp; Topic Tests that will help you to prepare better for CAT. Questions prepared by IIM-ites.
										</p>
										<span className="label">MBA</span>
									</div>
								</div>
								<div className="col-sm-4">
									<div className="feature feature-3 boxed boxed--lg boxed--border">
										<i className="icon icon--lg icon-Bank"></i>
										<h4>Crash Course for Banking</h4>
										<p>
											Mock Tests &amp; Topic Tests that will help you to prepare better for SBI-PO and IBPS-PO. Questions prepared by Experts.
										</p>
										<span className="label">Banking</span>
									</div>
								</div>
							</div>
						</div>
					</section>

					<section className="text-center bg--secondary">
						<div className="container">
							<div className="row">
								<div className="col-sm-10 col-sm-offset-2 col-md-8 col-md-offset-3">
									<div className="cta">
										<h2>Our Courses are always updated!</h2>
										<p className="lead">
											Do you wish to learn? Do you wish to upgrade your skills?<br/>Our Trainers &amp; Mentors will help you learn and complete Projects.
                                		</p>
										<a className="btn btn--primary type--uppercase" href="/pricing">
											<span className="btn__text">
												See Pricing
                                    		</span>
										</a>
									</div>
								</div>
							</div>

						</div>
					</section>

					<Faq/>

				</div>
				<Footer />
				<a className="back-to-top inner-link" href="#start" data-scroll-className="100vh:active">
					<i className="stack-interface stack-up-open-big"></i>
				</a>
			</div>

		);
	}
}
