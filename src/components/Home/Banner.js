


import React, {Component} from 'react';


export class Banner extends Component{
    render(){
        return(
            <section className="cover height-100 imagebg text-center" data-overlay="7" id="home">
                <div className="background-image-holder">
                    <img alt="background" src="/img/education-6.jpg" />
                </div>
                <div className="container pos-vertical-center">
                    <div className="row">
                        <div className="col-sm-8">
                            <div className="typed-headline" id="home-header">
                                <h1 className="typed-text-cust typed-text--cursor">YOU CAN SCORE HIGHER!</h1>
                                <span className="h4 block">Are you confused during a test looking for what to attempt?</span>
                            </div>
                            <a className="btn btn--primary type--uppercase" href="/pricing">
                                <span className="btn__text"> Get Started - Easy & Cheap! </span>
                            </a>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}