import React, { Component } from 'react';
export class Courses extends Component {
    renderCourseExams(exams) {
        return exams.map((exam) => {
            return (
                <li>
                    <a className="btn btn--sm type--uppercase flip-bg-white" href="/trainings">
                        <span className="btn__text">
                            {exam}
                        </span>
                    </a>
                </li>
            );
        });
    }
    renderCourses() {
        return this.props.data.map((courses) => {
            return (
                <li className="col-sm-4 col-xs-12 flip-container">
                    <div className="flipper">
                        <div className="front feature feature-3 boxed boxed--lg boxed--border text-center">
                            <i className={`color--primary icon icon--lg ${courses.icon_class}`}></i>
                            <h4>{courses.heading}</h4>
                            <p>{courses.text}</p>
                        </div>
                        <div className="back flip-back-bg feature feature-3 boxed boxed--lg boxed--border text-center">
                            <ul className="text-center flip-list">
                                {this.renderCourseExams(courses.exams)}
                            </ul>
                        </div>
                    </div>

                </li>
            );
        });
    }
    render() {
        return (
            <section className="bg--secondary space--xs">
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12 text-center space--xs space-top-0">
                            <h2>Our Courses</h2>
                        </div>
                    </div>

                    <div className="row">
                        <ul className="slides">
                            {this.renderCourses()}
                            {/* <li className="col-sm-4 col-xs-12 flip-container">
                                <div className="flipper">
                                    <div className="front feature feature-3 boxed boxed--lg boxed--border text-center">
                                        <i className="color--primary icon icon--lg icon-Student-Male"></i>
                                        <h4>MBA</h4>
                                        <p>
                                            Aiming for to the 99th percentile and more? Start your journey to your dream B-school with Beatest
                                    </p>
                                    </div>
                                    <div className="back flip-back-bg feature feature-3 boxed boxed--lg boxed--border text-center">
                                        <ul className="text-center flip-list">
                                            <li>
                                                <a className="btn btn--sm type--uppercase flip-bg-white" href="tests.html">
                                                    <span className="btn__text">
                                                        CAT
                                                    </span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                            </li>
                            <li className="col-sm-4 col-xs-12 flip-container">
                                <div className="flipper">
                                    <div className="front feature feature-3 boxed boxed--lg boxed--border text-center">
                                        <i className="color--primary icon icon--lg icon-Bank"></i>
                                        <h4>Banking</h4>
                                        <p>
                                            Gear up for your banking exams with carefully designed mock tests and topic tests in the latest pattern
                                    </p>
                                    </div>

                                    <div className="back flip-back-bg feature feature-3 boxed boxed--lg boxed--border text-center">
                                        <ul className="text-center flip-list">
                                            <li>
                                                <a className="btn btn--sm type--uppercase flip-bg-white" href="#" data-modal-index="0">
                                                    <span className="btn__text">SBI PO</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a className="btn btn--sm type--uppercase flip-bg-white" href="#" data-modal-index="0">
                                                    <span className="btn__text">IBPS RRB</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            <li className="col-sm-4 col-xs-12 flip-container">
                                <div className="flipper">
                                    <div className="front feature feature-3 boxed boxed--lg boxed--border text-center">
                                        <i className="color--primary icon icon--lg icon-University"></i>
                                        <h4>Campus Placements</h4>
                                        <p>
                                            Prepare for your upcoming placement season with the aptitude exams in line with the various companies
                                    </p>
                                    </div>

                                    <div className="back flip-back-bg feature feature-3 boxed boxed--lg boxed--border text-center">
                                        <ul className="text-center flip-list">
                                            <li>
                                                <a className="btn btn--sm type--uppercase flip-bg-white" href="#" data-modal-index="0">
                                                    <span className="btn__text">Aptitude Exams</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a className="btn btn--sm type--uppercase flip-bg-white" href="#" data-modal-index="0">
                                                    <span className="btn__text">Aptitude Workshop</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </li> */}
                        </ul>
                    </div>

                </div>

            </section>
        );
    }
}

