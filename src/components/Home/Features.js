import React, { Component } from 'react';

export class Features extends Component{
    renderFeatures(){
        return this.props.data.map((feature) => {
            return (
                <div className="col-sm-3">
                    <div className="feature feature-3 boxed boxed--lg boxed--border">
                        <i className={`icon ${feature.icon_class} icon--lg color--primary`}></i>
                        <h5>{feature.heading}</h5>
                    </div>
                </div>
            );
        });
    }
    render(){
        return(
            <section id="features" className="text-center space--xs">
                <div className="container">
                    <div className="row">
                        { this.renderFeatures()}
                    </div>
                </div>
            </section>
        );
    }
}

