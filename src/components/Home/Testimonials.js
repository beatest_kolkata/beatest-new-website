import React, { Component } from 'react'

export class Testimonials extends Component {
    renderTestimonials(){
        return this.props.data.map((testimonials) =>{
        return(
            <li>
                <div className="row">
                    <div className="testimonial">
                        <div className="col-md-2 col-md-offset-1 col-sm-4 col-xs-6 text-center">
                            <img className="testimonial__image" alt="Image" src={`/testimonials/${testimonials.profile_img}`} />
                        </div>
                        <div className="col-md-7 col-md-offset-1 col-sm-8 col-xs-12">
                            <span className="h3"> {testimonials.quote} </span>
                            <h5>{testimonials.client_name}</h5>
                            <span>{testimonials.designation}</span>
                        </div>
                    </div>
                </div>

            </li>
        );
    });
    }
    render() {
        return (
            <section className="bg--dark space--xs" id="testimonials">
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="slider slider--inline-arrows" data-arrows="true">
                                <ul className="slides">                                    
                                    {this.renderTestimonials()}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}
