import React, { Component } from 'react'

const partnerStyle = {
    height: '120px',
    width: '120px'
};

export class CampusPartners extends Component {
    renderCampusPlacement(){
        return this.props.data.map((campusplacement) => {
            return(
                <li className="col-sm-3">
                    <img alt="Image" src={`colleges/${campusplacement}`} style={partnerStyle} />
                </li>
            );
        });
    }
    render() {
        return (
            <section className="text-center space--xs">
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12 text-center space--xs space-top-0">
                            <h2>Student Partners</h2>
                        </div>
                        <div className="col-sm-12">
                            <div className="slider" data-arrows="true" data-paging="true">
                                <ul className="slides">
                                    {this.renderCampusPlacement()}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }

}