import React, { Component } from 'react'
export class QuickContact extends Component {
    render() {
        var captchContainer = {
            width: 304,
            height: 78
        }; 
        var captchateatarea ={
            width: 250,
            height: 40,
            border: '1 solid #c1c1c1',
            margin: '10 25',
            padding: 0,
            resize: 'none',
            display: 'none'
        }
        return (
            <section className="switchable space--xs" id="contact-us">
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12 text-center space--xs space-top-0">
                            <h2>Contact Us</h2>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-5">
                            <p className="lead">
                                Email:
                                    <a href="#">hello@beatest.in</a>
                                <br /> Phone: <span> +91-7980681451</span>
                            </p>
                            <p className="lead">
                                #7th Floor, Monibhandar Building Webel Bhavan Premises, Sector 5, Ring Road, Bidhannagar, West Bengal-700091, India
                                </p>
                            <address>
                                Give us a call or drop by anytime, we endeavour to answer all enquiries within 24 hours on business days.-
                                    </address>
                        </div>
                        <div className="col-sm-6 col-xs-12">
                            <div className="row">
                                <form className="form-email" data-success="Thanks for your enquiry, we'll be in touch shortly." data-error="Please fill in all fields correctly." data-recaptcha-sitekey="6LewhCIUAAAAACSwFvBDhgtTbw6EnW6e9dip8o2u" data-recaptcha-theme="light" noValidate="true">
                                    <div className="col-sm-6 col-xs-12">
                                        <label>Your Name:</label>
                                        <input type="text" name="Name" className="validate-required" />
                                    </div>
                                    <div className="col-sm-6 col-xs-12">
                                        <label>Email Address:</label>
                                        <input type="email" name="email" className="validate-required validate-email" />
                                    </div>
                                    <div className="col-sm-12 col-xs-12">
                                        <label>Message:</label>
                                        <textarea rows="4" name="Message" className="validate-required"></textarea>
                                    </div>
                                    <div className="col-xs-12">
                                        <div className="recaptcha">
                                            <div style={captchContainer}>
                                                <div>
                                                    <iframe src="https://www.google.com/recaptcha/api2/anchor?k=6LewhCIUAAAAACSwFvBDhgtTbw6EnW6e9dip8o2u&amp;co=aHR0cDovL3RyeXN0YWNrLm1lZGl1bXJhLnJlOjgw&amp;hl=en&amp;v=r20171011122914&amp;theme=light&amp;size=normal&amp;cb=hrxedle6eeoh" title="recaptcha widget" width="304" height="78" frameBorder="0" scrolling="no" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-top-navigation allow-modals allow-popups-to-escape-sandbox"></iframe>
                                                </div>
                                                <textarea id="g-recaptcha-response-4" name="g-recaptcha-response" className="g-recaptcha-response" style={captchateatarea}></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-5 col-md-4 col-xs-6">
                                        <button type="submit" className="btn btn--primary type--uppercase">Send Enquiry</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>

                </div>
            </section>
        )
    }
}
