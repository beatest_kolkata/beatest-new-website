import React, { Component } from 'react';
import classnames from 'classnames';
import HomeContainer from "../HomeContainer";

class Home extends Component {
    render() {
        const { className, ...props } = this.props;
        return (
            <HomeContainer className={classnames('Index', className)} {...props}/>
        );
    }
}

export default Home;
