import React, { Component } from 'react';

class Features extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div className="features">
				<div className="features-heading"><h1>What we offer</h1></div>
				<div className="feature">
					<h3>CONTENT</h3>
					<p>Carefully designed and unique, our CAT and IBPS mock tests and free practice
					 	questions with solution sets help you sharpen your skills to achieve your target.</p>
				</div>
				<div className="feature">
					<h3>PREP-PLANS</h3>
					<p>Let us help you plan your journey towards accomplishing your goal. Our prudently
						planned test preparation scheme that can be tailor-made to suit your strengths and
						weaknesses will help you to strategize your time and improve your results.</p>
				</div>
				<div className="feature">
					<h3>ANALYSIS</h3>
					<p>Use our platorm to discover your strengths and weaknesses. Compare yourself with
						fellow competitors to see where you stand and track your progress in real time. Boost
						your proficiency at every stage to emerge victorious on the final day!</p>
				</div>
				<div className="feature">
					<h3>PRO-EXP</h3>
					<p>We bring to you blogs from the finest in the field- their journey, their struggles and their
						success stories. This further aids in shaping your study plan. After all, a wise man learns
						from the experience of others.</p>
				</div>
            </div>
        );
    }
}

export default Features;
