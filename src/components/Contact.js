import React, { Component } from 'react';
import $ from 'jquery';

class Contact extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.submitContactForm = this.submitContactForm.bind(this);
    }

    submitContactForm(e) {
        e.preventDefault();
        var formName = $('#name-input').val();
        var formEmail = $('#email-input').val();
        var formMessage = $('#comment-textarea').val();
        var formPhone = $('#phno-input').val();
        $('#contact')[0].reset();
        if(formName !== '') {
            if(formEmail !== '') {
                if (formPhone !== '') {
                    if(formMessage !== '') {
                        $.ajax({
                            url: '/api/contact_us',
                            dataType: 'json',
                            type: 'POST',
                            contentType: 'application/json',
                            data: JSON.stringify({
                                formName,
                                formEmail,
                                formMessage,
                                formPhone
                            })
                        }).then(() => {
                            alert('Your Enquiry has been submitted. We will get back to you soon.');
                            $('#contact')[0].reset();
                        });
                    } else {
                        alert('Please enter the message.');
                    }
                } else {
                    alert('Please enter your phone number.');
                }
            } else {
                alert('Please enter your email.');
            }
        } else {
            alert('Please enter your name.');
        }
    }

    render() {
        return (
			<div className="contact">

                <div className="contact-overlay"><div className="contact_spot spot spot_1"></div></div>
                <div className="contact-content">
                    <div className="contact-heading">Contact Us</div>
                    <div className="contact-form">
                        <form id="contact" onSubmit={this.submitContactForm}>
                            <div>
                                <div className="form-group">
                                    <input type="text" className="form-control" id="name-input" placeholder="Name" />
                                </div>
                                <div className="form-group">
                                    <input type="email" className="form-control" id="email-input" placeholder="Email" />
                                </div>
                                <div className="form-group">
                                    <input type="phno" className="form-control" id="phno-input" placeholder="Phone No." />
                                </div>
                                <div className="form-group">
                                    <input type="text" className="form-control" rows="1" id="comment-textarea" placeholder="Your Message" />
                                </div>
                            
                            <button className="contact-form-button">
                                <i className="fa fa-arrow-right"></i>
                                Submit
                            </button>
                            </div>
                        </form>
                    </div>
                    <div className="social-media">
                        #7th Floor, Monibhandar Building Webel Bhavan Premises, Sector 5, Ring Road, Bidhannagar, West Bengal-700091, India
                    </div>
                    <br/>
                    <div className="social-media">
                        <a href="https://www.facebook.com/beatest.in/" target="_blank"><i className="fa fa-facebook"></i></a>
                        <a href="https://plus.google.com/112722904416257444024" target="_blank"><i className="fa fa-google-plus"></i></a>
                        <a href="https://twitter.com/beatest_in" target="_blank"><i className="fa fa-twitter"></i></a>
                        
                    </div>
                </div>
            </div>
        );
    }
}

export default Contact;