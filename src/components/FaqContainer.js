import React, { Component } from 'react';
import Header from './Common/InternalHeader';
import Metatags from 'react-meta-tags';

class FaqContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div>
                <meta property="og:title" content="Beatest | Free Test Prep Solution | CAT, Banking Exams" />
                <meta name="keywords" content="MBA, CAT, IIM, 2016, 2017, Free exams, Free Tests, Free mock tests, Daily Tests, CAT Mock Tests, IBPS PO Mock Tests, Test Strategy, Exam Strategy, Toppers, IBPS, Bank PO, SBI PO, SBI Clerk, SBI PO Mock Test, SBI PO Mains, SBI PO Mains Mock Test, SBI Clerk Mock test, <SBI PO 2017, SBI Clerk 2017, IBPS PO 2017, IBPS Clerk 2017, IBPS Clerk Mock Test " />
                <meta name="description" content="A FREE one-stop solution to ace your MBA and Banking exams test preparations online with an excellent team of CAT and Banking exam toppers" />
                <meta property="og:url" content="https://beatest.in/" />
                <meta name="author" content="beatest" />
                <Header transparent={false} />
                <div className="main-container">
                    <section>
                        <div className="container">
                            <div className="row">
                                <div className="col-sm-12">
                                    <h1>Trainings &amp; Courses FAQs</h1>
                                    <hr />
                                </div>
                            </div>
                        </div>
                        <div className="container">
                            <div className="row">
                                <div className="col-md-4">
                                    <div className="text-block">
                                        <h4>Frequently Asked Questions</h4>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="text-block">
                                        <h5>What kind of background knowledge is necessary for taking up the courses?</h5>
                                        <p>
                                            The courses usually have no pre-requisites. A basic idea abour the technology
                                            /subject would be really helpful but we try to keep it extremely basic in
                                            the first phase for you to have an idea about it.
                                        </p>
                                    </div>
                                    <div className="text-block">
                                        <h5>How are you different from other course offerings?</h5>
                                        <p>
                                            We believe that a new learning process requires a mentor. Hence, we have
                                            introduced a <b>human-in-the-loop</b> model where you can learn live from
                                            the best mentors across the globe. Also, we feel that theoretical knowledge
                                            really doesn't help you a lot in the real world. Hence, our courses are
                                            designed in a manner for you to solve our innovative <b>capstone projects</b>
                                            which could add more value to your learning
                                        </p>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="text-block">
                                        <h5>What is a capstone project?</h5>
                                        <p>
                                            A capstone project is an innovative hands-on problem/application that you
                                            would need to solve/build using the learnings of the course. We make sure
                                            to create some of the most innovative projects that would boost up your
                                            resume along with the certification.
                                        </p>
                                    </div>
                                    <div className="text-block">
                                        <h5>Are the courses certified? What is the validity?</h5>
                                        <p>
                                            Yes. On completion of these courses, you would get a digital certificate of
                                            completion which you can share on LinkedIn directly. <br/>
                                            The certificates are valid for a lifetime.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div className="row" style={{marginTop: '20px'}}>
                                <div className="col-md-4"/>
                                <div className="col-md-4">
                                    <div className="text-block">
                                        <h5>What is the length of a course?</h5>
                                        <p>
                                            The learning segment of a course ranges between 12-16 hours. On completion
                                            of this learning section, you will typically be given a month's time to
                                            solve the capstone project.
                                        </p>
                                    </div>
                                    <div className="text-block">
                                        <h5>What should I expect to learn from the course?</h5>
                                        <p>
                                            The course structure along with the capstone project and the profile of the
                                            mentors can be viewed on the brochure of the course. To obtain the brochure,
                                            click on the <b>Download Brochure</b> link beside every course on
                                            the pricing page
                                        </p>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="text-block">
                                        <h5>How frequently will we have the sessions?</h5>
                                        <p>
                                            There will be two sessions held each week during your learning segment.
                                            Each sessions will be for 2 hours. On completion fo the learning section
                                            of the course, there will be additional mentoring sessions for the capstone
                                            project solving and the doubt clearing sessions
                                        </p>
                                    </div>
                                    <div className="text-block">
                                        <h5>Will I be certified if I do not complete the capstone project?</h5>
                                        <p>
                                            No. We make it compulsory for a student to solve the capstone project in
                                            order to be certified
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div className="row" style={{marginTop: '20px'}}>
                                <div className="col-md-4"/>
                                <div className="col-md-4">
                                    <div className="text-block">
                                        <h5>Is there a passing grade for the capstone project?</h5>
                                        <p>
                                            Yes. You are expected to acheive a 40% passing grade for the capstone
                                            project. In case you do not obtain the passing grade, you would be asked
                                            to complete the capstone project again and re-submit it within 6 months
                                            from the starting date of your course
                                        </p>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="text-block">
                                        <h5>What if I cannot complete my capstone project within a month?</h5>
                                        <p>
                                            You are allowed to take an extension on solving the capstone project if
                                            you have a valid reason for not being able to complete it. You can
                                            reach out to us at <a href="mailto:hello@beatest.in">hello@beatest.in </a>
                                            if such a scenario arises.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        );}
}

export default FaqContainer;