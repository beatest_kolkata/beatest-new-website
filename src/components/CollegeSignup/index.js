import React, { Component } from 'react';
import classnames from 'classnames';
/*import './style.css';*/
import CollegeSignupContainer from '../CollegeSignupContainer';

class CollegeSignup extends Component {
    render() {
        const { className, ...props } = this.props;
        return (
            <CollegeSignupContainer className={classnames('College', className)} {...props}/>
        );
    }
}

export default CollegeSignup;
