import React from 'react';
import { browserHistory } from 'react-router';
import { api } from '../settings';

export const loadMockTests = () => {
    return {
        type: "TESTS_MOCK_TEST"
    }
};

export const loadTopicTests = () => {
    return {
        type: "TESTS_TOPIC_TEST"
    }
};

export const setUserData = data => {
    return {
        type: 'SET_USER_DATA',
        data
    };
};

export const getTests = callback => {
    return dispatch => {
        api.get('/api/tests').then(function (response) {
            let data = response.data;
            if(callback) callback(data);
        })
    }
};