export const loadingTestsBegin = () => {
    return {
        type: 'LOADING_TESTS_BEGIN'
    };
}

export const assignColleges = (collegeList) => {

    return {
        type: 'ASSIGN_COLLEGES',
        collegeList
    };
}

export const loadingTestsSuccess = (tests) => {
    return {
        type: 'LOADING_TESTS_SUCCESS',
        tests
    };
}

export const createExamBegin = () => {
    return {
        type: 'CREATE_EXAM_BEGIN'
    };
}

export const createExamSuccess = (testAttempt, sectionAttempts, testName) => {
    return {
        type: 'CREATE_EXAM_SUCCESS',
        testAttempt,
        sectionAttempts,
        testName
    };
}

export const loadingQuestionsBegin = () => {
    return {
        type: 'LOADING_QUESTIONS_BEGIN'
    };
}

export const loadingQuestionsSuccess = (questionsList) => {
    return {
        type: 'LOADING_QUESTIONS_SUCCESS',
        questionsList
    };
}

export const showSelectedQuestion = (selectedQuestionId, selectedQuestionIndex) => {
    return {
        type: 'SHOW_SELECTED_QUESTION',
        selectedQuestionId,
        selectedQuestionIndex
    };
}

export const markAnswer = () => {
    return {
        type: 'MARK_ANSWER'
    };
}

export const markTitaAnswer = (tita_choice) => {
    return {
        type: 'MARK_TITA_ANSWER',
        tita_choice
    };
}

export const submitAnswer = () => {
    return {
        type: 'SUBMIT_ANSWER'
    };
}

export const submitTitaAnswer = (titaChoice) => {
    return {
        type: 'SUBMIT_TITA_ANSWER',
        titaChoice
    };
}

export const showNextQuestion = () => {
    return {
        type: 'SHOW_NEXT_QUESTION'
    };
}

export const showPrevQuestion = () => {
    return {
        type: 'SHOW_PREV_QUESTION'
    };
}

export const deleteAnswer = () => {
    return {
        type: 'DELETE_ANSWER'
    };
}

export const deleteTitaAnswer = () => {
    return {
        type: 'DELETE_TITA_ANSWER'
    };
}

export const gotoNextSection = () => {
    return {
        type: 'GOTO_NEXT_SECTION'
    };
}

export const gotoSelectedSection = (selectedSectionIndex) => {
    return {
        type: 'GOTO_SELECTED_SECTION',
        selectedSectionIndex
    };
}

export const choiceSelected = (choiceId, bool) => {
    return {
        type: 'CHOICE_SELECTED',
        choiceId,
        bool
    };
}

export const toggleLoginPanelVisibility = () => {
    return {
        type: 'TOGGLE_LOGIN_PANEL_VISIBILITY'
    };
}

export const toggleRegistrationPanelVisibility = () => {
    return {
        type: 'TOGGLE_REGISTRATION_PANEL_VISIBILITY'
    };
}

export const login = (user) => {
    return {
        type: 'LOGIN',
        user
    };
}

export const logout = () => {
    return {
        type: 'LOGOUT'
    };
}

export const toggleUserMenuVisibility = () => {
    return {
        type: 'TOGGLE_USER_MENU_VISIBILITY'
    };
}

export const changeTimeleft = (newTimeleft) => {
    return {
        type: 'CHANGE_TIME_LEFT',
        newTimeleft
    };
}

export const showCATTests = () => {
    return {
        type: 'SHOW_CAT_TESTS'
    };
}

export const showIBPSTests = () => {
    return {
        type: 'SHOW_IBPS_TESTS'
    };
}
export const showSBITests = () => {
    return {
        type: 'SHOW_SBI_TESTS'
    };
}

export const showIBPSPOTests = () => {
    return {
        type: 'SHOW_IBPSPO_TESTS'
    };
}

export const changeCompletionStatus = (status) => {
    return {
        type: 'CHANGE_COMPLETION_STATUS',
        status
    };
}

export const setTestDetails = (test) => {
    return {
        type: 'SET_TEST_DETAILS',
        test
    };
}

export const setSolutionData = (data) => {
    return {
        type: 'SET_SOLUTION_DATA',
        data
    };
}


export const gotoNextQuestion = () => {
    return {
        type: 'GOTO_NEXT_QUESTION'
    };
}


export const gotoPrevQuestion = () => {
    return {
        type: 'GOTO_PREV_QUESTION'
    };
}

export const gotoPrevSection = () => {
    return {
        type: 'GOTO_PREV_SECTION'
    };
}

export const loadingUserSuccess = () => {
    return {
        type: 'LOADING_USER_SUCCESS'
    };
}

export const setTestScore = (scoreData) => {
    return {
        type: 'SET_TEST_SCORE',
        scoreData
    };
}

export const toggleTestType = (testType) => {
    return {
        type: 'TOGGLE_TEST_TYPE',
        testType
    };
}
