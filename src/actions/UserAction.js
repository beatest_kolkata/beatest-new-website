import React from 'react';
import { browserHistory } from 'react-router';
import { api } from '../settings';

const createCookie = require('../components/cookie.js').createCookie;
const eraseCookie = require('../components/cookie.js').eraseCookie;

export const setUserData = data => {
    return {
        type: 'SET_USER_DATA',
        data
    };
};

export const setProfileData = data => {
    return {
        type: 'SET_PROFILE_DATA',
        data
    };
};

export const getData = callback => {
    return dispatch => {
        api.get('/api/user').then(function (response) {
            let data = response.data;
            if(data !== null) {
                dispatch(setUserData(data.user));
                createCookie('userData', JSON.stringify(data.user), 7);
                if(callback) callback(data.user);
            } else {
                if(callback) callback(null);
            }
        }).catch(function (error) {
            console.log(error);
        });
    }
};

export const getProfileData = callback => {
    return dispatch => {
        api.get('/api/user_profile').then(function (response) {
            let data = response.data;
            dispatch(setProfileData(data));
            createCookie('profileData', JSON.stringify(data), 7);
            if(callback) callback(data);
        }).catch(function (error) {
            console.log(error);
        });
    }
};

export const logout = callback => {
    return dispatch => {
        api.post('/api/logout')
        .then((data) => {
            dispatch(setUserData(null));
            eraseCookie('userData');
            if(data.data.success === true) window.location.pathname = '/';
        }).catch((err)=>{
            console.log(err);
        });
    };
};