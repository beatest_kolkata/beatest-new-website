import React from 'react';
import { browserHistory } from 'react-router';
import { api } from '../settings';

export const loadExams = () => {
    return {
        type: "PRICING_LOAD_EXAMS"
    }
};

export const loadToppers = () => {
    return {
        type: "PRICING_OUR_TOPPERS"
    }
};

export const getUserTests = callback => {
    return dispatch => {
        api.get('/api/user_profile').then(function (response) {
            let data = response.data;
            if(callback) callback(data.tests);
        })
    }
};