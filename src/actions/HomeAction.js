import $ from 'jquery';
import React from 'react';
import { browserHistory } from 'react-router';
import { api } from '../settings';

const createCookie = require('../components/cookie.js').createCookie;
// const readCookie = require('../components/cookie.js').readCookie;

export const loadFeatures = () => {
    return {
        type: "HOME_LOAD_FEATURES"
    }
};
export const loadCourses = () => {
    return {
        type: "HOME_LOAD_COURSES"
    }
};
export const loadCampusPartners = () => {
    return {
        type: "HOME_LOAD_CAMPUS_PARTNERS"
    }
};
export const loadTestimonials = () => {
    return {
        type: "HOME_LOAD_TESTIMONIALS"
    }
};

export const login = (postData) => {
    return dispatch => {
        api.post("/api/login", postData).then(function (response) {
            let data = response.data;
            if (data.hasOwnProperty('invalid')) {
                if (data.invalid === 'Email') {
                    dispatch(loginError("Sorry, Beatest doesn't recognize that email."));
                //     $('#user-panel-validator').html("Sorry, Beatest doesn't recognize that email.");
                } else if (data.invalid === 'Password') {
                    dispatch(loginError("Wrong password. Try again."));
                //     $('#user-panel-validator').html("Wrong password. Try again.");
                // } else if (data.invalid === 'User') {
                //     this.props.toggleLoginPanelVisibility();
                //     this.props.toggleRegistrationPanelVisibility();
                //     $('#registration-panel-validator').html("Please add a password for your account.");
                //     $('#new-full-name').val(data.name);
                //     $('#new-email').val(data.email);
                }
            } else if (data.hasOwnProperty('user')) {
                dispatch(loginAction(data.user));
                createCookie('userData', JSON.stringify(data.user), 7);
                window.location.pathname = '/userprofile';
            }
        }).catch(function (error) {
            console.log(error);
        });
    }
};

export const loginAction = data => {
    return {
        type: "LOGIN",
        data
    }
};

export const loginError = data => {
    return {
        type: "LOGIN_ERROR",
        data
    }
}